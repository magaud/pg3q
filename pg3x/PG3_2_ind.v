Require Import List.
Import ListNotations.

(*
 * PG(3,2):
 * #points: 15
 * #lines:  35
 * #planes: 15
 * #3-hyperplanes: 1
 * 
 * 3 incident points to a line
 * 7 incident lines to a point
 *)

Definition dimension : nat := 3%nat.
Definition order : nat := 2%nat.

Inductive Element :=
| E0 | E1 .

Definition E2nat (a: Element) : nat :=
match a with
| E0 => 0%nat
| E1 => 1%nat
end.

Definition nat2E (n: nat) : Element :=
match n with
| 1%nat => E1
| _ => E0
end.

Definition E2vector (a: Element) : list nat :=
match a with
| E0 => [ 0%nat ]
| E1 => [ 1%nat ]
end.

Definition vector2E (v: list nat) : Element :=
match v with
| [ 1%nat ] => E0
| [ 2%nat ] => E1
| _ => E0
end.

Definition eq (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | E0 => true
  | _ => false
  end
| E1 =>
  match b with
  | E1 => true
  | _ => false
  end
end.

Definition le (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | _ => false
  end
| E1 =>
  match b with
  | E0 => false
  | _ => false
  end
end.

Definition lt (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | E0 => false
  | _ => true
  end
| E1 => false
end.

Definition opp (a: Element) : Element :=
match a with
| E1 => E1
| _ => E0
end.

Definition add (a b: Element) : Element :=
match a with
| E1 =>
  match b with
  | E1 => E0
  | _ => a
  end
| _ => b
end.

Definition sub (a b: Element) : Element := add a (opp b).

Definition inv (a: Element) : Element :=
match a with
| E1 => E1
| _ => E0
end.

Definition mul (a b: Element) : Element :=
match a with
| E1 =>
  match b with
  | E1 => E1
  | _ => E0
  end
| _ => E0
end.

Definition div_left (a b: Element) : Element := mul (inv b) a.

Definition div_right (a b: Element) : Element := mul a (inv b).

Definition resolve_xa_eq_xb_plus_c (a b c: Element) : Element := div_right c (sub a b).

Definition resolve_ax_eq_bx_plus_c (a b c: Element) : Element := div_left c (sub a b).

Inductive Point :=
| P0 | P1 | P2 | P3 | P4 | P5 | P6 | P7 | P8 | P9 | P10 | P11 | P12 | P13 | P14 .

Definition P2coords (p: Point) : list Element :=
match p with
| P0 => [ E0 ; E0 ; E0 ; E1 ]
| P1 => [ E0 ; E0 ; E1 ; E0 ]
| P2 => [ E0 ; E0 ; E1 ; E1 ]
| P3 => [ E0 ; E1 ; E0 ; E0 ]
| P4 => [ E0 ; E1 ; E0 ; E1 ]
| P5 => [ E0 ; E1 ; E1 ; E0 ]
| P6 => [ E0 ; E1 ; E1 ; E1 ]
| P7 => [ E1 ; E0 ; E0 ; E0 ]
| P8 => [ E1 ; E0 ; E0 ; E1 ]
| P9 => [ E1 ; E0 ; E1 ; E0 ]
| P10 => [ E1 ; E0 ; E1 ; E1 ]
| P11 => [ E1 ; E1 ; E0 ; E0 ]
| P12 => [ E1 ; E1 ; E0 ; E1 ]
| P13 => [ E1 ; E1 ; E1 ; E0 ]
| P14 => [ E1 ; E1 ; E1 ; E1 ]
end.

Definition normalized_coords2P (coords: list Element) : Point :=
match coords with
| [ x0 ; x1 ; x2 ; x3 ] =>
  match x0 with
  | E0 =>
    match x1 with
    | E0 =>
      match x2 with
      | E0 => P0
      | _ =>
      match x3 with
        | E0 => P1
        | E1 => P2
        end
      end
    | _ =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P3
        | E1 => P4
        end
      | E1 =>
      match x3 with
        | E0 => P5
        | E1 => P6
        end
      end
    end
  | _ =>
  match x1 with
    | E0 =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P7
        | E1 => P8
        end
      | E1 =>
      match x3 with
        | E0 => P9
        | E1 => P10
        end
      end
    | E1 =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P11
        | E1 => P12
        end
      | E1 =>
      match x3 with
        | E0 => P13
        | E1 => P14
        end
      end
    end
  end
| _ => P0
end.

Inductive Line :=
| L0 | L1 | L2 | L3 | L4 | L5 | L6 | L7 | L8 | L9 | L10 | L11 | L12 | L13 | L14 | L15 | L16 | L17 | L18 | L19 | L20 | L21 | L22 | L23 | L24 | L25 | L26 | L27 | L28 | L29 | L30 | L31 | L32 | L33 | L34 .

Definition L2_2P (l:Line) : (Point * Point) :=
match l with
| L0 => (P0, P1)
| L1 => (P0, P3)
| L2 => (P0, P5)
| L3 => (P0, P7)
| L4 => (P0, P9)
| L5 => (P0, P11)
| L6 => (P0, P13)
| L7 => (P1, P3)
| L8 => (P1, P4)
| L9 => (P1, P7)
| L10 => (P1, P8)
| L11 => (P1, P11)
| L12 => (P1, P12)
| L13 => (P2, P3)
| L14 => (P2, P4)
| L15 => (P2, P7)
| L16 => (P2, P8)
| L17 => (P2, P11)
| L18 => (P2, P12)
| L19 => (P3, P7)
| L20 => (P3, P8)
| L21 => (P3, P9)
| L22 => (P3, P10)
| L23 => (P4, P7)
| L24 => (P4, P8)
| L25 => (P4, P9)
| L26 => (P4, P10)
| L27 => (P5, P7)
| L28 => (P5, P8)
| L29 => (P5, P9)
| L30 => (P5, P10)
| L31 => (P6, P7)
| L32 => (P6, P8)
| L33 => (P6, P9)
| L34 => (P6, P10)
end.

Definition _2P2L (x y: Point) : Line :=
match x with
| P0 =>
  match y with
  | P1 => L0
  | P3 => L1
  | P5 => L2
  | P7 => L3
  | P9 => L4
  | P11 => L5
  | P13 => L6
  | _ => L0
  end
| P1 =>
  match y with
  | P3 => L7
  | P4 => L8
  | P7 => L9
  | P8 => L10
  | P11 => L11
  | P12 => L12
  | _ => L0
  end
| P2 =>
  match y with
  | P3 => L13
  | P4 => L14
  | P7 => L15
  | P8 => L16
  | P11 => L17
  | P12 => L18
  | _ => L0
  end
| P3 =>
  match y with
  | P7 => L19
  | P8 => L20
  | P9 => L21
  | P10 => L22
  | _ => L0
  end
| P4 =>
  match y with
  | P7 => L23
  | P8 => L24
  | P9 => L25
  | P10 => L26
  | _ => L0
  end
| P5 =>
  match y with
  | P7 => L27
  | P8 => L28
  | P9 => L29
  | P10 => L30
  | _ => L0
  end
| P6 =>
  match y with
  | P7 => L31
  | P8 => L32
  | P9 => L33
  | P10 => L34
  | _ => L0
  end
| _ => L0
end.

