Require Import PG3X.PG3_3_ind.
Require Import PG3X.PG3_3_rel.
Require Import ssrbool.
Require Import List.
Import ListNotations.

(* normalize_coords *)
Lemma lemma_normalize_coords :
forall (a: Element) (p: Point),
let coords := P2coords p in
(a <> E0) -> (normalize_coords (mul_left_list a coords) = coords).
Proof.
intros a p coords H; destruct a, p;
try [> destruct H; reflexivity] ||
    [> reflexivity].
Qed.

Check lemma_normalize_coords.

(* coords2P *)
Lemma lemma_coords2P :
forall (a: Element) (p: Point),
(a <> E0) -> (coords2P (mul_left_list a (P2coords p)) = p).
Proof.
intros a p H; destruct a, p;
try [> destruct H; reflexivity] ||
    [> reflexivity].
Qed.

Check lemma_coords2P.

(* normalize_line *)
Lemma lemma_normalize_line :
forall (a b: Element) (l: Line),
let (x,y) := L2_2P l in
let p1 := coords2P (add_list (P2coords y) (mul_left_list a (P2coords x))) in
let p2 := coords2P (add_list (P2coords y) (mul_left_list b (P2coords x))) in
normalize_line x p1 = (x,y) /\
normalize_line p1 x = (x,y) /\
((a <> b) -> ((normalize_line p1 p2 = (x,y)) /\ normalize_line p2 p1 = (x,y))).
Proof.
intros; destruct a, b, l;
simpl; split; reflexivity || [> split; reflexivity || [> intro H;
try [> destruct H; reflexivity] ||
    [> split; reflexivity]
]].
Qed.

Check lemma_normalize_line.

(* line_from_points *)
Lemma lemma_line_from_points :
forall (a b: Element) (l: Line),
let (x,y) := L2_2P l in
let p1 := coords2P (add_list (P2coords y) (mul_left_list a (P2coords x))) in
let p2 := coords2P (add_list (P2coords y) (mul_left_list b (P2coords x))) in
line_from_points x p1 = l /\
line_from_points p1 x = l /\
((a <> b) -> ((line_from_points p1 p2 = l) /\ line_from_points p2 p1 = l)).
Proof.
intros; destruct a, b, l;
simpl; split; reflexivity || [> split; reflexivity || [> intro H;
try [> destruct H; reflexivity] ||
    [> split; reflexivity]
]].
Qed.

Check lemma_line_from_points.

Definition line_from_points_eq (l: Line) (x y: Point) : bool := (eqP x y) || (eqL l (line_from_points x y)).

Definition Onl_forall (l: Line) (x: list Point) (y: Point) : bool :=
forallb (line_from_points_eq l y) x.

Definition map_square {X Y: Type} (f: X -> X -> Y) (l: list X) : list (list Y) :=
let fix aux (f: X -> X -> Y) (l t: list X) : list (list Y) :=
match t with
| h::t2 => (map (f h) l)::(aux f l t2)
| [] => []
end in
aux f l l.

Lemma incid_points_from_line :
forall (p: Point) (l: Line),
incid_lp p l = true <-> (In p (points_from_line l)).
Proof.
intros; destruct p;
destruct l;
split;
  [>
    try [> unfold incid_lp; simpl; unfold eqP; simpl; unfold eq_list; simpl; discriminate] ||
        [> simpl; repeat try [> left; reflexivity] || right]
  ] ||
  simpl; intros H; repeat [> destruct H; try [> discriminate H] || reflexivity].
Qed.

Check incid_points_from_line.

Lemma line_from_points_from_line :
forall (l: Line),
forallb (Onl_forall l (points_from_line l)) (points_from_line l) = true.
Proof.
intros; destruct l;
unfold points_from_line; simpl; reflexivity.
Qed.

Check line_from_points_from_line.

Lemma incid_line_from_points :
forall (x y: Point),
let l := line_from_points x y in
(incid_lp x l = true) /\ (incid_lp y l = true).
Proof.
intros; destruct x; destruct y; simpl; split; reflexivity.
Qed.

Check incid_line_from_points.

Definition intersection (l1 l2 : list Point) : list Point :=
  filter (fun n => existsb (eqP n) l2) l1.

Lemma coplanarity_inter_lines :
forall (l m: Line),
intersection (points_from_line l) (points_from_line m) <> [] <-> coplanarity l m = true.
Proof.
intros; destruct l; destruct m;
split; simpl;
  [> try [> reflexivity] || intro H; destruct H; reflexivity] ||
  [> try [> discriminate]].
Qed.

Check coplanarity_inter_lines.

Lemma coplanarity2D :
(dimension = 2%nat) -> (forall (l m: Line), coplanarity l m = true).
Proof.
intro H;
try [> inversion H] ||
    [> intros; destruct l, m; reflexivity].
Qed.

Check coplanarity2D.

Lemma incid_intersection :
forall (l m: Line),
coplanarity l m = true ->
(
  let p := f_a2 l m in
  incid_lp p l = true /\ incid_lp p m = true
).
Proof.
intros l m H p; destruct l;
destruct m;
try [> discriminate H] ||
unfold p; split; reflexivity.
Qed.

Check incid_intersection.
