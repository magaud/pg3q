Require Import PG3X.PG3_3_ind.
Require Import ssrbool.
Require Import List.
Import ListNotations.


(* ---------------------------- LIST OPERATIONS ----------------------------- *)
Fixpoint map2 {X Y Z: Type} (f: X -> Y -> Z) (l1: list X) (l2: list Y) : list Z :=
match l1 with
| h1::t1 =>
  match l2 with
  | h2::t2 => (f h1 h2)::(map2 f t1 t2)
  | []     => []
  end
| []     => []
end.

Fixpoint forallb2 {X Y: Type} (f: X -> Y -> bool) (l1: list X) (l2: list Y) : bool :=
match l1 with
| h1::t1 =>
  match l2 with
  | h2::t2 => (f h1 h2) && (forallb2 f t1 t2)
  | []     => true
  end
| []     => true
end.

Definition add_list (l1 l2: list Element) : list Element := map2 add l1 l2.

Definition sub_list (l1 l2: list Element) : list Element := map2 sub l1 l2.

Definition mul_left_list (a: Element) (l: list Element) : list Element := map (mul a) l.

Definition mul_right_list (a: Element) (l: list Element) : list Element :=
let mul_right (a b: Element) : Element := mul b a in
map (mul_right a) l.

Definition div_left_list (a: Element) (l: list Element) : list Element :=
let div_left2 (a b: Element) : Element := div_left b a in
map (div_left2 a) l.

Definition div_right_list (a: Element) (l: list Element) : list Element :=
let div_right2 (a b: Element) : Element := div_right b a in
map (div_right2 a) l.

Definition eq_list (l1 l2: list Element) : bool := forallb2 eq l1 l2.

Fixpoint le_list (l1 l2: list Element) : bool :=
match l1 with
| h1::t1 =>
  match l2 with
  | h2::t2 =>
    (lt h1 h2) || (eq h1 h2 && le_list t1 t2)
  | [] => true
  end
| [] => true
end.

Fixpoint lt_list (l1 l2: list Element) : bool :=
match l1 with
| h1::t1 =>
  match l2 with
  | h2::t2 =>
    (lt h1 h2) || (eq h1 h2 && lt_list t1 t2)
  | [] => false
  end
| [] => false
end.


(* ------------------------------ COMPARISONS ------------------------------- *)

(* Points *)
Definition eqP (x y: Point) : bool := eq_list (P2coords x) (P2coords y).

Definition leP (x y: Point) : bool := le_list (P2coords x) (P2coords y).

Definition ltP (x y: Point) : bool := lt_list (P2coords x) (P2coords y).

(* Lines *)
Definition eqL (l m: Line) : bool :=
let (a,b) := L2_2P l in
let (c,d) := L2_2P m in
eqP a c && eqP b d.

Definition leL (l m: Line) : bool :=
let (a,b) := L2_2P l in
let (c,d) := L2_2P m in
ltP a c || (eqP a c && leP b d).

Definition ltL (l m: Line) : bool :=
let (a,b) := L2_2P l in
let (c,d) := L2_2P m in
ltP a c || (eqP a c && ltP b d).


(* ----------------------------- NORMALIZATION ------------------------------ *)

(* Points *)
Fixpoint normalize_coords (coords: list Element) : list Element :=
match coords with
| E0::t => E0::(normalize_coords t)
| h::t  => E1::(div_left_list h t)
| []    => []
end.

Definition coords2P (coords: list Element) : Point := normalized_coords2P (normalize_coords coords).

(* Lines *)
Definition normalize_line (a b: Point) : (Point * Point) :=

(*
 * Renvoie, à partir des coordonnées u et v, avec (u₀ = 1) et (v₀ = 0), le
 * facteur a tel que (u - a × v) correspond aux coordonnées du point ayant
 * le deuxième plus petit indice de la droite.
 *)
let fix aux10 (u v: list Element) : Element :=
match u with
| uh::ut =>
  match v with
  | E0::vt => aux10 ut vt
  | vh::vt => div_left uh vh
  | []     => E0
  end
| []     => E0
end in

(*
 * Renvoie, à partir des coordonnées u et v, avec (u₀ = 1) et (v₀ = 1), les
 * facteurs (a,b) tels que (a × u - b × v) correspond aux coordonnées du point
 * ayant le deuxième plus petit indice de la droite.
 *)
let fix aux11 (u v: list Element) : list Element :=
match u with
| uh::ut =>
  match v with
  | vh::vt => (* if (eq uh vh) then (aux11 ut vt) else (vh,uh) *)
              if (eq uh vh)
                then E0::(aux11 ut vt)
                else E1::(map (resolve_ax_eq_bx_plus_c uh vh) (sub_list ut vt))
  | []     => []
  end
| []     => [] (* a = b *)
end in

(*
 * Renvoie, à partir des coordonnées u et v, les coordonnées des points de plus
 * petit indices appartenant à la droite des points de coordonnées u et v.
 *)
let fix aux (u v: list Element) : ((list Element) * (list Element)) :=
match u with
| E0::ut =>
  match v with
  (* (uᵢ = 0) et (vᵢ = 0) => On passe aux composantes uᵢ₊₁ et vᵢ₊₁ *)
  | E0::vt => let (c1,c2) := aux ut vt in (E0::c1, E0::c2)
  (*
   * (uᵢ = 0) et (vᵢ = 1) => u correspond aux coordonnées du point ayant le
   * plus petit indice passant par la droite. Les coordonnées du point ayant le
   * deuxième plus petit indice sont égales v - u × (aux10 v u).
   *)
  | E1::vt => (u, sub_list v (mul_left_list (aux10 v u) u))
  | _     => ([],[])
  end
| E1::ut =>
  match v with
  (*
   * (uᵢ = 1) et (vᵢ = 0) => v correspond aux coordonnées du point ayant le
   * plus petit indice passant par la droite. Les coordonnées du point ayant le
   * deuxième plus petit indice sont égales u - v × (aux10 u v).
   *)
  | E0::vt => (v, sub_list u (mul_left_list (aux10 u v) v))
  (*
   * (uᵢ = 1) et (vᵢ = 1) => (u-v) correspond aux coordonnées du point ayant le
   * plus petit indice passant par la droite. Les coordonnées du point ayant le
   * deuxième plus petit indice sont égales à (a × u + b × v), avec
   * (a,b) = (aux11 u v).
   *)
  | E1::vt => (*
              let (a,b) := aux11 u v in
              (sub_list u v, sub_list (div_list a u) (div_list b v))
              *)
              let c1 := E0::(aux11 vt ut) in (* normalize_coords (sub_list v u) in *)
              (c1, sub_list u (mul_left_list (aux10 u c1) c1))
  | _      => ([],[])
  end
| _      => ([],[])
end in

let (c1,c2) :=
if (eqP a b)
then aux (P2coords a) (P2coords P0)
else aux (P2coords a) (P2coords b) in
(coords2P c1, coords2P c2).

Definition line_from_points (x: Point) (y: Point) : Line :=
let (x2,y2) := normalize_line x y in _2P2L x2 y2.


(* ------------------- RELATIONS BETWEEN POINTS AND LINES ------------------- *)

(* Incidence *)
Definition incid_lp (p: Point) (l: Line) : bool :=
let (a,b) := L2_2P l in

let fix incid_aux (u v w: list Element) : bool :=
match v with
| vh::vt =>
  match w with
  | wh::wt =>
    match u with
    | E0::ut => (eq vh wh) && (incid_aux ut vt wt)
    | E1::ut => eq_list wt (add_list vt (mul_left_list wh ut))
    | _      => false
    end
  | _      => false
  end
| _      => false
end in

let fix incid (u v w: list Element) : bool :=
match u with
| _::ut =>
  match v with
  | E0::vt =>
    match w with
    | E0::wt => incid ut vt wt
    | _      => false
    end
  | E1::vt =>
    match w with
    | E0::wt => eq_list ut wt
    | E1::wt => incid_aux ut vt wt
    | _      => false
    end
  | _      => false
  end
| _     => false
end in

incid (P2coords a) (P2coords b) (P2coords p).


(* Incidents point to a line *)
Definition points_from_line (l: Line) : (list Point) :=
let (a,b) := L2_2P l in
let u := P2coords a in
let v := P2coords b in

let fix aux (u v: list Element) (i: nat) : list Point :=
match i with
| 0%nat => []
| S j   => (aux u v j) ++ [ normalized_coords2P (add_list v (mul_left_list (nat2E j) u)) ]
end in

a::(aux u v order).


(* Incidents lines to a point *)
Definition incident_lines (p: Point) : list Line :=

let max_coord : nat := order - 1 in

let fix aux1_all (u: list Element) (coords: list Element) : nat -> list Line :=
fix aux1_all_fix (i: nat) : list Line :=
match u with
| _::ut =>
  match i with
  | 0%nat => aux1_all ut (coords ++ [E0]) max_coord
  | S j   => (aux1_all_fix j) ++ (aux1_all ut (coords ++ [nat2E i]) max_coord)
  end
| _     => [line_from_points p (coords2P coords)]
end in

let fix aux0_all (u: list Element) (coords: list Element) : list Line :=
match u with
| _::ut => (aux0_all ut (coords ++ [E0])) ++ (aux1_all ut (coords ++ [E1]) max_coord)
| _     => []
end in

let fix aux1 (u: list Element) (coords: list Element) : nat -> list Line :=
fix aux1_fix (i: nat) : list Line :=
match u with
| E0::ut =>
  match i with
  | 0%nat => aux1 ut (coords ++ [E0]) max_coord
  | S j   => (aux1_fix j) ++ (aux1 ut (coords ++ [nat2E i]) max_coord)
  end
| E1::ut => aux1_all ut (coords ++ [E0]) max_coord
| _      => [line_from_points p (coords2P coords)]
end in

let fix aux0 (u: list Element) (coords: list Element) : list Line :=
match u with
| E0::ut => (aux0 ut (coords ++ [E0])) ++ (aux1 ut (coords ++ [E1]) max_coord)
| E1::ut => aux0_all ut (coords ++ [E0])
| _      => []
end in

aux0 (P2coords p) [].


(* Coplanarity of two lines *)
Definition coplanarity (l: Line) (m: Line) : bool :=
let (A,B) := L2_2P l in
let (C,D) := L2_2P m in

let U := P2coords A in
let V := P2coords B in
let W := P2coords C in
let X := P2coords D in

let fix incid_aux (u v w: list Element) : bool :=
match v with
| vh::vt =>
  match w with
  | wh::wt =>
    match u with
    | E0::ut => (eq vh wh) && (incid_aux ut vt wt)
    | E1::ut => eq_list wt (add_list vt (mul_left_list wh ut))
    | _      => false
    end
  | _      => false
  end
| _      => false
end in

let fix incid (u v w: list Element) : bool :=
match u with
| _::ut =>
  match v with
  | E0::vt =>
    match w with
    | E0::wt => incid ut vt wt
    | _      => false
    end
  | E1::vt =>
    match w with
    | E0::wt => eq_list ut wt
    | E1::wt => incid_aux ut vt wt
    | _      => false
    end
  | _      => false
  end
| _     => false
end in

let fix aux1010 (u v w x: list Element) : bool :=
match u with
| uh::ut =>
  match v with
  | vh::vt =>
    match w with
    | wh::wt =>
      match x with
      | xh::xt =>
        if (eq uh wh)
        then
          if (eq vh xh)
          then aux1010 ut vt wt xt
          else eq_list ut wt (* A = C *)
        else
          if (eq vh xh)
          then eq_list vt xt (* B = D *)
          else
            let factor := (resolve_xa_eq_xb_plus_c uh wh (sub xh vh)) in
            eq_list (add_list vt (mul_left_list factor ut)) (add_list xt (mul_left_list factor wt))
            (* B + f × A = D + f × C *)
      | _      => false (* impossible case *)
      end
    | _      => false (* impossible case *)
    end
  | _      => false (* impossible case *)
  end
| _      => true (* l = m *)
end in

let fix aux0101 (u v w x: list Element) : bool :=
match v with
| vh::vt =>
  match x with
  | xh::xt =>
    match u with
    | E0::ut =>
      match w with
      | E0::wt =>
        if (eq vh xh)
        then aux0101 ut vt wt xt
        else eq_list ut wt (* A = C *)
      | E1::wt => incid_aux ut vt (add_list xt (mul_left_list vh wt))
      | _      => false (* impossible case *)
      end
    | E1::ut =>
      match w with
      | E0::wt => incid_aux wt xt (add_list vt (mul_left_list xh ut))
      | E1::wt => aux1010 ut vt wt xt
      | _      => false (* impossible case *)
      end
    | _      => false (* impossible case *)
    end
  | _     => false (* impossible case *)
  end
| _     => false (* impossible case *)
end in

let fix aux (u v w x: list Element) : bool :=
match u with
| E0::ut =>
  match w with
  | E0::wt =>
    match v with
    | E0::vt =>
      match x with
      | E0::xt => aux ut vt wt xt
      | _      => incid ut vt wt (* C on (AB) *)
      end
    | E1::vt   =>
      match x with
      | E0::xt => incid wt xt ut (* A on (CD) *)
      | E1::xt => aux0101 ut vt wt xt
      | _      => false (* impossible case *)
      end
    | _      => false (* impossible case *)
    end
  | _      => false (* impossible case *)
  end
| _      => false (* impossible case *)
end in

aux U V W X.

(* Intersection of two lines *)
Definition f_a2 (l: Line) (m: Line) : Point :=

let (A,B) := L2_2P l in
let (C,D) := L2_2P m in

let U := P2coords A in
let V := P2coords B in
let W := P2coords C in
let X := P2coords D in

let fix aux1010 (u v w x: list Element) : Point :=
match u with
| uh::ut =>
  match v with
  | vh::vt =>
    match w with
    | wh::wt =>
      match x with
      | xh::xt =>
        if (eq uh wh)
        then
          if (eq vh xh)
          then aux1010 ut vt wt xt
          else A (* A = C *)
        else
          if (eq vh xh)
          then B (* B = D *)
          else
            let factor := (resolve_xa_eq_xb_plus_c uh wh (sub xh vh)) in
            coords2P (add_list V (mul_left_list factor U)) (* B + f × A = D + f × C *)
      | _      => P0 (* impossible case *)
      end
    | _      => P0 (* impossible case *)
    end
  | _      => P0 (* impossible case *)
  end
| _      => A (* l = m *)
end in


let fix aux0101 (u v w x: list Element) : Point :=
match v with
| vh::vt =>
  match x with
  | xh::xt =>
    match u with
    | E0::ut =>
      match w with
      | E0::wt => aux0101 ut vt wt xt
      | E1::wt => coords2P (add_list X (mul_left_list vh W))
      | _      => P0 (* impossible case *)
      end
    | E1::ut =>
      match w with
      | E0::wt => coords2P (add_list V (mul_left_list xh U))
      | E1::wt => aux1010 ut vt wt xt
      | _      => P0 (* impossible case *)
      end
    | _      => P0 (* impossible case *)
    end
  | _     => P0 (* impossible case *)
  end
| _     => P0 (* impossible case *)
end in

let fix aux (u v w x: list Element) : Point :=
match u with
| E0::ut =>
  match w with
  | E0::wt =>
    match v with
    | E0::vt =>
      match x with
      | E0::xt => aux ut vt wt xt
      | _      => C (* C on (AB) *)
      end
    | E1::vt   =>
      match x with
      | E0::xt => A (* A on (CD) *)
      | E1::xt => aux0101 ut vt wt xt
      | _      => P0 (* impossible case *)
      end
    | _      => P0 (* impossible case *)
    end
  | _      => P0 (* impossible case *)
  end
| _      => P0 (* impossible case *)
end in

aux U V W X.
