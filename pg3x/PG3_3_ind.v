Require Import List.
Import ListNotations.

(*
 * PG(3,3):
 * #points: 40
 * #lines:  130
 * #planes: 40
 * #3-hyperplanes: 1
 * 
 * 4 incident points to a line
 * 13 incident lines to a point
 *)

Definition dimension : nat := 3%nat.
Definition order : nat := 3%nat.

Inductive Element :=
| E0 | E1 | E2 .

Definition E2nat (a: Element) : nat :=
match a with
| E0 => 0%nat
| E1 => 1%nat
| E2 => 2%nat
end.

Definition nat2E (n: nat) : Element :=
match n with
| 1%nat => E1
| 2%nat => E2
| _ => E0
end.

Definition E2vector (a: Element) : list nat :=
match a with
| E0 => [ 0%nat ]
| E1 => [ 1%nat ]
| E2 => [ 2%nat ]
end.

Definition vector2E (v: list nat) : Element :=
match v with
| [ 1%nat ] => E0
| [ 2%nat ] => E1
| [ 3%nat ] => E2
| _ => E0
end.

Definition eq (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | E0 => true
  | _ => false
  end
| E1 =>
  match b with
  | E1 => true
  | _ => false
  end
| E2 =>
  match b with
  | E2 => true
  | _ => false
  end
end.

Definition le (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | _ => false
  end
| E1 =>
  match b with
  | E0 => false
  | _ => false
  end
| E2 =>
  match b with
  | E0 => false
  | E1 => false
  | _ => false
  end
end.

Definition lt (a b: Element) : bool :=
match a with
| E0 =>
  match b with
  | E0 => false
  | _ => true
  end
| E1 =>
  match b with
  | E0 => false
  | E1 => false
  | _ => true
  end
| E2 => false
end.

Definition opp (a: Element) : Element :=
match a with
| E1 => E2
| E2 => E1
| _ => E0
end.

Definition add (a b: Element) : Element :=
match a with
| E1 =>
  match b with
  | E1 => E2
  | E2 => E0
  | _ => a
  end
| E2 =>
  match b with
  | E1 => E0
  | E2 => E1
  | _ => a
  end
| _ => b
end.

Definition sub (a b: Element) : Element := add a (opp b).

Definition inv (a: Element) : Element :=
match a with
| E1 => E1
| E2 => E2
| _ => E0
end.

Definition mul (a b: Element) : Element :=
match a with
| E1 =>
  match b with
  | E1 => E1
  | E2 => E2
  | _ => E0
  end
| E2 =>
  match b with
  | E1 => E2
  | E2 => E1
  | _ => E0
  end
| _ => E0
end.

Definition div_left (a b: Element) : Element := mul (inv b) a.

Definition div_right (a b: Element) : Element := mul a (inv b).

Definition resolve_xa_eq_xb_plus_c (a b c: Element) : Element := div_right c (sub a b).

Definition resolve_ax_eq_bx_plus_c (a b c: Element) : Element := div_left c (sub a b).

Inductive Point :=
| P0 | P1 | P2 | P3 | P4 | P5 | P6 | P7 | P8 | P9 | P10 | P11 | P12 | P13 | P14 | P15 | P16 | P17 | P18 | P19 | P20 | P21 | P22 | P23 | P24 | P25 | P26 | P27 | P28 | P29 | P30 | P31 | P32 | P33 | P34 | P35 | P36 | P37 | P38 | P39 .

Definition P2coords (p: Point) : list Element :=
match p with
| P0 => [ E0 ; E0 ; E0 ; E1 ]
| P1 => [ E0 ; E0 ; E1 ; E0 ]
| P2 => [ E0 ; E0 ; E1 ; E1 ]
| P3 => [ E0 ; E0 ; E1 ; E2 ]
| P4 => [ E0 ; E1 ; E0 ; E0 ]
| P5 => [ E0 ; E1 ; E0 ; E1 ]
| P6 => [ E0 ; E1 ; E0 ; E2 ]
| P7 => [ E0 ; E1 ; E1 ; E0 ]
| P8 => [ E0 ; E1 ; E1 ; E1 ]
| P9 => [ E0 ; E1 ; E1 ; E2 ]
| P10 => [ E0 ; E1 ; E2 ; E0 ]
| P11 => [ E0 ; E1 ; E2 ; E1 ]
| P12 => [ E0 ; E1 ; E2 ; E2 ]
| P13 => [ E1 ; E0 ; E0 ; E0 ]
| P14 => [ E1 ; E0 ; E0 ; E1 ]
| P15 => [ E1 ; E0 ; E0 ; E2 ]
| P16 => [ E1 ; E0 ; E1 ; E0 ]
| P17 => [ E1 ; E0 ; E1 ; E1 ]
| P18 => [ E1 ; E0 ; E1 ; E2 ]
| P19 => [ E1 ; E0 ; E2 ; E0 ]
| P20 => [ E1 ; E0 ; E2 ; E1 ]
| P21 => [ E1 ; E0 ; E2 ; E2 ]
| P22 => [ E1 ; E1 ; E0 ; E0 ]
| P23 => [ E1 ; E1 ; E0 ; E1 ]
| P24 => [ E1 ; E1 ; E0 ; E2 ]
| P25 => [ E1 ; E1 ; E1 ; E0 ]
| P26 => [ E1 ; E1 ; E1 ; E1 ]
| P27 => [ E1 ; E1 ; E1 ; E2 ]
| P28 => [ E1 ; E1 ; E2 ; E0 ]
| P29 => [ E1 ; E1 ; E2 ; E1 ]
| P30 => [ E1 ; E1 ; E2 ; E2 ]
| P31 => [ E1 ; E2 ; E0 ; E0 ]
| P32 => [ E1 ; E2 ; E0 ; E1 ]
| P33 => [ E1 ; E2 ; E0 ; E2 ]
| P34 => [ E1 ; E2 ; E1 ; E0 ]
| P35 => [ E1 ; E2 ; E1 ; E1 ]
| P36 => [ E1 ; E2 ; E1 ; E2 ]
| P37 => [ E1 ; E2 ; E2 ; E0 ]
| P38 => [ E1 ; E2 ; E2 ; E1 ]
| P39 => [ E1 ; E2 ; E2 ; E2 ]
end.

Definition normalized_coords2P (coords: list Element) : Point :=
match coords with
| [ x0 ; x1 ; x2 ; x3 ] =>
  match x0 with
  | E0 =>
    match x1 with
    | E0 =>
      match x2 with
      | E0 => P0
      | _ =>
      match x3 with
        | E0 => P1
        | E1 => P2
        | E2 => P3
        end
      end
    | _ =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P4
        | E1 => P5
        | E2 => P6
        end
      | E1 =>
      match x3 with
        | E0 => P7
        | E1 => P8
        | E2 => P9
        end
      | E2 =>
      match x3 with
        | E0 => P10
        | E1 => P11
        | E2 => P12
        end
      end
    end
  | _ =>
  match x1 with
    | E0 =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P13
        | E1 => P14
        | E2 => P15
        end
      | E1 =>
      match x3 with
        | E0 => P16
        | E1 => P17
        | E2 => P18
        end
      | E2 =>
      match x3 with
        | E0 => P19
        | E1 => P20
        | E2 => P21
        end
      end
    | E1 =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P22
        | E1 => P23
        | E2 => P24
        end
      | E1 =>
      match x3 with
        | E0 => P25
        | E1 => P26
        | E2 => P27
        end
      | E2 =>
      match x3 with
        | E0 => P28
        | E1 => P29
        | E2 => P30
        end
      end
    | E2 =>
    match x2 with
      | E0 =>
      match x3 with
        | E0 => P31
        | E1 => P32
        | E2 => P33
        end
      | E1 =>
      match x3 with
        | E0 => P34
        | E1 => P35
        | E2 => P36
        end
      | E2 =>
      match x3 with
        | E0 => P37
        | E1 => P38
        | E2 => P39
        end
      end
    end
  end
| _ => P0
end.

Inductive Line :=
| L0 | L1 | L2 | L3 | L4 | L5 | L6 | L7 | L8 | L9 | L10 | L11 | L12 | L13 | L14 | L15 | L16 | L17 | L18 | L19 | L20 | L21 | L22 | L23 | L24 | L25 | L26 | L27 | L28 | L29 | L30 | L31 | L32 | L33 | L34 | L35 | L36 | L37 | L38 | L39 | L40 | L41 | L42 | L43 | L44 | L45 | L46 | L47 | L48 | L49 | L50 | L51 | L52 | L53 | L54 | L55 | L56 | L57 | L58 | L59 | L60 | L61 | L62 | L63 | L64 | L65 | L66 | L67 | L68 | L69 | L70 | L71 | L72 | L73 | L74 | L75 | L76 | L77 | L78 | L79 | L80 | L81 | L82 | L83 | L84 | L85 | L86 | L87 | L88 | L89 | L90 | L91 | L92 | L93 | L94 | L95 | L96 | L97 | L98 | L99 | L100 | L101 | L102 | L103 | L104 | L105 | L106 | L107 | L108 | L109 | L110 | L111 | L112 | L113 | L114 | L115 | L116 | L117 | L118 | L119 | L120 | L121 | L122 | L123 | L124 | L125 | L126 | L127 | L128 | L129 .

Definition L2_2P (l:Line) : (Point * Point) :=
match l with
| L0 => (P0, P1)
| L1 => (P0, P4)
| L2 => (P0, P7)
| L3 => (P0, P10)
| L4 => (P0, P13)
| L5 => (P0, P16)
| L6 => (P0, P19)
| L7 => (P0, P22)
| L8 => (P0, P25)
| L9 => (P0, P28)
| L10 => (P0, P31)
| L11 => (P0, P34)
| L12 => (P0, P37)
| L13 => (P1, P4)
| L14 => (P1, P5)
| L15 => (P1, P6)
| L16 => (P1, P13)
| L17 => (P1, P14)
| L18 => (P1, P15)
| L19 => (P1, P22)
| L20 => (P1, P23)
| L21 => (P1, P24)
| L22 => (P1, P31)
| L23 => (P1, P32)
| L24 => (P1, P33)
| L25 => (P2, P4)
| L26 => (P2, P5)
| L27 => (P2, P6)
| L28 => (P2, P13)
| L29 => (P2, P14)
| L30 => (P2, P15)
| L31 => (P2, P22)
| L32 => (P2, P23)
| L33 => (P2, P24)
| L34 => (P2, P31)
| L35 => (P2, P32)
| L36 => (P2, P33)
| L37 => (P3, P4)
| L38 => (P3, P5)
| L39 => (P3, P6)
| L40 => (P3, P13)
| L41 => (P3, P14)
| L42 => (P3, P15)
| L43 => (P3, P22)
| L44 => (P3, P23)
| L45 => (P3, P24)
| L46 => (P3, P31)
| L47 => (P3, P32)
| L48 => (P3, P33)
| L49 => (P4, P13)
| L50 => (P4, P14)
| L51 => (P4, P15)
| L52 => (P4, P16)
| L53 => (P4, P17)
| L54 => (P4, P18)
| L55 => (P4, P19)
| L56 => (P4, P20)
| L57 => (P4, P21)
| L58 => (P5, P13)
| L59 => (P5, P14)
| L60 => (P5, P15)
| L61 => (P5, P16)
| L62 => (P5, P17)
| L63 => (P5, P18)
| L64 => (P5, P19)
| L65 => (P5, P20)
| L66 => (P5, P21)
| L67 => (P6, P13)
| L68 => (P6, P14)
| L69 => (P6, P15)
| L70 => (P6, P16)
| L71 => (P6, P17)
| L72 => (P6, P18)
| L73 => (P6, P19)
| L74 => (P6, P20)
| L75 => (P6, P21)
| L76 => (P7, P13)
| L77 => (P7, P14)
| L78 => (P7, P15)
| L79 => (P7, P16)
| L80 => (P7, P17)
| L81 => (P7, P18)
| L82 => (P7, P19)
| L83 => (P7, P20)
| L84 => (P7, P21)
| L85 => (P8, P13)
| L86 => (P8, P14)
| L87 => (P8, P15)
| L88 => (P8, P16)
| L89 => (P8, P17)
| L90 => (P8, P18)
| L91 => (P8, P19)
| L92 => (P8, P20)
| L93 => (P8, P21)
| L94 => (P9, P13)
| L95 => (P9, P14)
| L96 => (P9, P15)
| L97 => (P9, P16)
| L98 => (P9, P17)
| L99 => (P9, P18)
| L100 => (P9, P19)
| L101 => (P9, P20)
| L102 => (P9, P21)
| L103 => (P10, P13)
| L104 => (P10, P14)
| L105 => (P10, P15)
| L106 => (P10, P16)
| L107 => (P10, P17)
| L108 => (P10, P18)
| L109 => (P10, P19)
| L110 => (P10, P20)
| L111 => (P10, P21)
| L112 => (P11, P13)
| L113 => (P11, P14)
| L114 => (P11, P15)
| L115 => (P11, P16)
| L116 => (P11, P17)
| L117 => (P11, P18)
| L118 => (P11, P19)
| L119 => (P11, P20)
| L120 => (P11, P21)
| L121 => (P12, P13)
| L122 => (P12, P14)
| L123 => (P12, P15)
| L124 => (P12, P16)
| L125 => (P12, P17)
| L126 => (P12, P18)
| L127 => (P12, P19)
| L128 => (P12, P20)
| L129 => (P12, P21)
end.

Definition _2P2L (x y: Point) : Line :=
match x with
| P0 =>
  match y with
  | P1 => L0
  | P4 => L1
  | P7 => L2
  | P10 => L3
  | P13 => L4
  | P16 => L5
  | P19 => L6
  | P22 => L7
  | P25 => L8
  | P28 => L9
  | P31 => L10
  | P34 => L11
  | P37 => L12
  | _ => L0
  end
| P1 =>
  match y with
  | P4 => L13
  | P5 => L14
  | P6 => L15
  | P13 => L16
  | P14 => L17
  | P15 => L18
  | P22 => L19
  | P23 => L20
  | P24 => L21
  | P31 => L22
  | P32 => L23
  | P33 => L24
  | _ => L0
  end
| P2 =>
  match y with
  | P4 => L25
  | P5 => L26
  | P6 => L27
  | P13 => L28
  | P14 => L29
  | P15 => L30
  | P22 => L31
  | P23 => L32
  | P24 => L33
  | P31 => L34
  | P32 => L35
  | P33 => L36
  | _ => L0
  end
| P3 =>
  match y with
  | P4 => L37
  | P5 => L38
  | P6 => L39
  | P13 => L40
  | P14 => L41
  | P15 => L42
  | P22 => L43
  | P23 => L44
  | P24 => L45
  | P31 => L46
  | P32 => L47
  | P33 => L48
  | _ => L0
  end
| P4 =>
  match y with
  | P13 => L49
  | P14 => L50
  | P15 => L51
  | P16 => L52
  | P17 => L53
  | P18 => L54
  | P19 => L55
  | P20 => L56
  | P21 => L57
  | _ => L0
  end
| P5 =>
  match y with
  | P13 => L58
  | P14 => L59
  | P15 => L60
  | P16 => L61
  | P17 => L62
  | P18 => L63
  | P19 => L64
  | P20 => L65
  | P21 => L66
  | _ => L0
  end
| P6 =>
  match y with
  | P13 => L67
  | P14 => L68
  | P15 => L69
  | P16 => L70
  | P17 => L71
  | P18 => L72
  | P19 => L73
  | P20 => L74
  | P21 => L75
  | _ => L0
  end
| P7 =>
  match y with
  | P13 => L76
  | P14 => L77
  | P15 => L78
  | P16 => L79
  | P17 => L80
  | P18 => L81
  | P19 => L82
  | P20 => L83
  | P21 => L84
  | _ => L0
  end
| P8 =>
  match y with
  | P13 => L85
  | P14 => L86
  | P15 => L87
  | P16 => L88
  | P17 => L89
  | P18 => L90
  | P19 => L91
  | P20 => L92
  | P21 => L93
  | _ => L0
  end
| P9 =>
  match y with
  | P13 => L94
  | P14 => L95
  | P15 => L96
  | P16 => L97
  | P17 => L98
  | P18 => L99
  | P19 => L100
  | P20 => L101
  | P21 => L102
  | _ => L0
  end
| P10 =>
  match y with
  | P13 => L103
  | P14 => L104
  | P15 => L105
  | P16 => L106
  | P17 => L107
  | P18 => L108
  | P19 => L109
  | P20 => L110
  | P21 => L111
  | _ => L0
  end
| P11 =>
  match y with
  | P13 => L112
  | P14 => L113
  | P15 => L114
  | P16 => L115
  | P17 => L116
  | P18 => L117
  | P19 => L118
  | P20 => L119
  | P21 => L120
  | _ => L0
  end
| P12 =>
  match y with
  | P13 => L121
  | P14 => L122
  | P15 => L123
  | P16 => L124
  | P17 => L125
  | P18 => L126
  | P19 => L127
  | P20 => L128
  | P21 => L129
  | _ => L0
  end
| _ => L0
end.

