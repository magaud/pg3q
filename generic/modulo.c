#include<stdio.h>
#include<stdlib.h>
#include<assert.h>


int main(int argc, char *argv[])
{
  int i,j;
  if (argc!=3) {printf("usage: %s output nb\n", argv[0]); exit(1);}

  int n = atoi(argv[2]);
  
  printf("n=%d\n",n);

  FILE *g=fopen(argv[1],"w");
  fprintf(g, "Require Import Arith.\n");
  fprintf(g, "Require Import Lia.\n\n");

  fprintf(g, "Lemma modulo_S%d : forall n:nat,\n", n);
  fprintf(g, "(Nat.modulo n %d = %d /\\ (Nat.modulo (S n) %d = 0)) \\/ (Nat.modulo (S n) %d = S (Nat.modulo n %d)).\n", n,n-1,n,n,n);
  fprintf(g, "Proof.\n");
  fprintf(g,"intros n.\n");
  fprintf(g,"pattern n.\n");
  fprintf(g, "pose (P:=(fun n0 : nat => n0 mod %d = %d /\\ S n0 mod %d = 0 \\/ S n0 mod %d = S (n0 mod %d))).\n", n,n-1,n,n,n);
  fprintf(g, "fold P.\n");
  fprintf(g, "assert (");
  for(i=0;i<n;i++)
    {
      fprintf(g,"P ");
      fprintf(g,"(%d+n)",i);
      /* for(j=0;j<i;j++)
	{
	  fprintf(g, "(S ");
	}
      fprintf(g,"n");
      for(j=0;j<i;j++)
	{
	fprintf(g, ") ");
	}*/
      if (i!=n-1) fprintf(g," /\\");
    }
  fprintf(g,").\n");
  fprintf(g,"unfold P.\n");
  fprintf(g,"induction n.\n");
  fprintf(g,"repeat split; try solve [right; simpl; trivial | left; split; reflexivity].\n");
  fprintf(g, "decompose [and] IHn.\n"); /*destruct IHn as [H1 [H2 H3]].*/
  fprintf(g,"repeat split; try assumption.\n");
  /*replace (S (S (S (S n)))) with ((S n)+(1*3)) by lia.*/
  fprintf(g, "replace (S");
  for(j=0;j<n;j++)
	{
	  fprintf(g, "(S ");
	}
      fprintf(g,"n");
      for(j=0;j<n;j++)
	{
	  fprintf(g, ") ");
	}
  fprintf(g, ") with ");
  fprintf(g, "((S n)+(1*%d)) ",n);
  fprintf(g, "by lia.\n");
  /*replace (S (S (S n))) with (n+(1*3)) by lia.*/
  fprintf(g, "replace ");
  for(j=0;j<n;j++)
	{
	  fprintf(g, "(S ");
	}
      fprintf(g,"n");
      for(j=0;j<n;j++)
	{
	  fprintf(g, ") ");
	}
  fprintf(g, "with ");
  fprintf(g, "(n+(1*%d)) ",n);
  fprintf(g, "by lia.\n");
  fprintf(g,"rewrite PeanoNat.Nat.mod_add.\n");
  fprintf(g,"rewrite PeanoNat.Nat.mod_add.\n");
  fprintf(g, "assumption.\n");
  fprintf(g,"lia.\n");
  fprintf(g,"lia.\n");
  fprintf(g,"intuition.\n");

  fprintf(g, "Qed.\n\n");
  
  fclose(g);
  
  printf("end !\n");

  
  return 0;
}
