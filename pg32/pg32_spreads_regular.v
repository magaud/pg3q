Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas Generic.wlog.

Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads_packings PG32.pg32_spreads.
Require Import List.

Import ListNotations.
(* One of the spreads of PG(3,2) *)
Print S0.
(* [L0; L19; L24; L28; L33] *)

Check is_spread5.
(* In three-dimensional space, a regulus R is a set of skew lines, 
every point of which is on a transversal which intersects an element of R only once, 
and such that every point on a transversal lies on a line of R *)

(* in PG(3,2), a regulus is set R of q+1 (=3) lines*)

Definition all_points_of_line (l:  Line) := match (points_from_line l) with (A,B,C) => [A;B;C] end.

Definition exact_inter (l1 l2 : Line) : list Point :=
  List.filter (fun n => List.existsb (eqP n) (all_points_of_line l2)) (all_points_of_line l1).

Fixpoint and_list (l:list Prop) (v:Prop) : Prop :=
  match l with
  | [] => v
  | x::xs => and_list xs (x/\v)
  end.

Fixpoint skew_aux (l:list Line) (v:Prop) : Prop :=
  match l with
  | [] => v
  | x::xs => skew_aux xs (and_list (map (fun (t:Line) => (exact_inter x t=[])) xs) v)
  end.

Definition skew_set (l:list Line) : Prop := skew_aux l True.

Eval compute in skew_set [L0;L19;L24]. (* yes *)
Eval compute in skew_set [L24;L28;L33]. (* yes *)
Eval compute in skew_set [L24;L28;L28]. (* no *)
Eval compute in skew_set [L0;L19;L1]. (* no *)

Fixpoint intersect_all_once (x:Line) (l:list Line) : Prop :=
  match l with
    [] => True
   | b::bs => (length (exact_inter x b) = 1) /\ intersect_all_once x bs
  end.

Eval compute in  all_points_of_line L0.
Eval compute in intersect_all_once L0 [].
Eval compute in intersect_all_once L0 [L0].

Fixpoint is_transversal x l :=
  match l with
    [] => True
  | b::bs => ~exact_inter x b = [] /\ is_transversal x bs
    end.

Fixpoint incid_lp_list p l :=
  match l with
    [] => False
  | x::xs => incid_lp p x \/ incid_lp_list p xs
  end.
(* search for a Ltac trick to build the list of all constructors of Point and/or Line *)
Definition all_lines := [ L0 ; L1 ; L2 ; L3 ; L4 ; L5 ; L6 ; L7 ; L8 ; L9 ; L10 ; L11 ; L12 ; L13 ; L14 ; L15 ; L16 ; L17 ; L18 ; L19 ; L20 ; L21 ; L22 ; L23 ; L24 ; L25 ; L26 ; L27 ; L28 ; L29 ; L30 ; L31 ; L32 ; L33 ; L34 ].
Check filter.

Definition regulus (l1 l2 l3:Line)(Hl1l2l3: skew_set [l1;l2;l3]) : list Line :=
filter 
  (fun l:Line => is_transversal l [l1;l2;l3] -> ) all_lines.



Definition is_regulus r :=
  length r = 3 /\ skew_set r /\
  forall l:Line, (is_transversal l r -> intersect_all_once l r /\ forall p:Point, incid_lp p l -> incid_lp_list p r).

Lemma not_regulus : ~is_regulus [L2;L5;L18].
Proof.
intro.
unfold is_regulus, skew_set in *; simpl in *; unfold exact_inter in *; simpl in *.
solve [intuition;discriminate].
Qed.

(* are there sets of 3 skew lines which do not form a regulus ? *)

Lemma r_L0_L19_L24 :is_regulus [L0;L19;L24].
Proof.
  unfold is_regulus; repeat split.
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
 destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  intros p Hp.
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  try solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  par:destruct p;
    try solve [left; reflexivity | right; left; reflexivity | right; right; left; reflexivity | apply False_ind; discriminate].
Qed.
  
Lemma r_L19_L33_L24 :is_regulus [L0;L19;L28].
Proof.
  unfold is_regulus; repeat split.
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
 destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  intros p Hp.
  destruct l; unfold is_transversal, exact_inter,all_points_of_line in H; simpl in H; destruct H as [H1 [H2 [H3 H4]]]; simpl;
  try solve [reflexivity | apply False_ind; solve [apply H1; reflexivity | apply H2; reflexivity | apply H3; reflexivity]].
  par:destruct p;
    try solve [left; reflexivity | right; left; reflexivity | right; right; left; reflexivity | apply False_ind; discriminate].
Qed.
(*Definition regulus (l:list Line) := {m:Line | }.*)
(* belongs_to_regulus *)
(* there exists a unique regulus *)
Check incl.
Parameter regulus : (list Line) -> (list Line).
Check is_spread5.

Definition belongs_to_regulus l r := (is_transversal l r -> intersect_all_once l r /\ forall p:Point, incid_lp p l -> incid_lp_list p r).

Lemma belongs_to_regulus_1 : belongs_to_regulus L24 [L0;L19;L28].
Proof.
  unfold belongs_to_regulus.
  intros H; unfold is_transversal in H; destruct H as [H1 H2]; apply False_ind; apply H1; reflexivity.
Qed.

Lemma belongs_to_regulus_2 : belongs_to_regulus L33 [L0;L19;L28].
Proof.
 unfold belongs_to_regulus.
 intros H; unfold is_transversal in H; destruct H as [H1 H2]; apply False_ind; apply H1; reflexivity.
Qed.

(*
Lemma does_not_belong_to_regulus_1 : ~belongs_to_regulus L23 [L0;L19;L28].
Proof.
  unfold belongs_to_regulus; simpl; unfold exact_inter; simpl.
  intro.

  assert ([] <> [] /\ [P14] <> [] /\ [] <> [] /\ True)
*)  

Definition is_spread (l:list Line) :=
  match l with
    [a;b;c;d;e] => is_spread5 a b c d e
  | _ => false
  end.

Definition regular_spread (l:list Line) :=
  is_spread l /\
  forall s:list Line, incl s l -> length s = 3 -> (forall m:Line,In m (regulus s) -> In m s).

(* in PG(3,2), all spreads are regular *)
(* TODO: Lemma all_regular_spreads : forall l:list Line, In l spreads -> regular_spread l. *)

(* Desargues ? *)

Lemma points_line : forall T Z:Point, forall x:Line,
        incid_lp T x -> incid_lp Z x -> (T<>Z) -> x = (l_from_points T Z).
  Proof.
    idtac "-> proving points_line".
    time (intros T Z x;
          case x; case T; intros HTx;
          first [ discriminate | 
                 case Z; intros HZx HTZ;
                 solve  [ discriminate |  exact (@erefl Line _) | apply False_rect; auto ] ]).
    Time Qed.

  Check points_line.
  
  Ltac handle x :=
    match goal with Ht  : is_true (incid_lp ?T x),
                    Hz  : is_true (incid_lp ?Z x),
                    Htz : (not (@eq Point ?T ?Z))  |- _ =>
                    let HP := fresh in pose proof (@points_line T Z x Ht Hz Htz) as HP;
                                       clear Ht Hz; subst  end.

Definition on_line := fun A B C l => incid_lp A l /\ incid_lp B l /\ incid_lp C l.
Definition collinear A B C :=  exists l, incid_lp A l /\ incid_lp B l /\ incid_lp C l.
(*
Theorem Desargues : 
 forall O P Q R P' Q' R' alpha beta gamma lP lQ lR lPQ lPR lQR lP'Q' lP'R' lQ'R',
((on_line P Q gamma lPQ) /\ (on_line P' Q' gamma lP'Q')) /\
((on_line P R beta lPR) /\ (on_line P' R' beta lP'R')) /\
((on_line Q R alpha lQR) /\ (on_line Q' R' alpha lQ'R')) /\
((on_line O P P' lP) /\ (on_line O Q Q' lQ) /\(on_line O R R' lR)) /\ 
~collinear O P Q /\  ~collinear O P R /\ ~collinear O Q R /\ 
~collinear P Q R /\ ~collinear P' Q' R' /\ ((~P=P')\/(~Q=Q')\/(~R=R')) ->
collinear alpha beta gamma.
Proof.
  unfold on_line, collinear.
  intros O P Q R P' Q' R' alpha beta gamma.
  intros lP lQ lR lPQ lPR lQR lP'Q' lP'R' lQ'R'.
  intros H; intuition.
  case_eq (Point_dec P Q).
 *)
(* DG ? *)
