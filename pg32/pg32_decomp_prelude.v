Require Import ssreflect ssrfun ssrbool.

Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads.
Require Import PG32.pg32_spreads_collineations.
Require Import PG32.pg32_spreads_packings.
Require Import PG32.pg32_packings_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_is_col_fp.

Require Import List.
Import ListNotations.

(* Lemma l_can_be_built_from_two_points : forall l, exists p1 p2, p1<>p2 /\ l = l_from_points p1 p2.
Proof.
intros l; destruct l.
exists P0; exists P1; split; [discriminate | apply erefl].
exists P0; exists P3; split; [discriminate | apply erefl].
exists P0; exists P6; split; [discriminate | apply erefl].
exists P0; exists P7; split; [discriminate | apply erefl].
exists P0; exists P10; split; [discriminate | apply erefl].
exists P0; exists P11; split; [discriminate | apply erefl].
exists P0; exists P13; split; [discriminate | apply erefl].
exists P1; exists P4; split; [discriminate | apply erefl].
exists P1; exists P8; split; [discriminate | apply erefl].
exists P1; exists P12; split; [discriminate | apply erefl].
exists P1; exists P7; split; [discriminate | apply erefl].
exists P1; exists P13; split; [discriminate | apply erefl].
exists P1; exists P3; split; [discriminate | apply erefl].
exists P2; exists P7; split; [discriminate | apply erefl].
exists P2; exists P11; split; [discriminate | apply erefl].
exists P2; exists P3; split; [discriminate | apply erefl].
exists P2; exists P12; split; [discriminate | apply erefl].
exists P2; exists P4; split; [discriminate | apply erefl].
exists P2; exists P8; split; [discriminate | apply erefl].
exists P3; exists P10; split; [discriminate | apply erefl].
exists P3; exists P8; split; [discriminate | apply erefl].
exists P3; exists P9; split; [discriminate | apply erefl].
exists P3; exists P7; split; [discriminate | apply erefl].
exists P4; exists P9; split; [discriminate | apply erefl].
exists P4; exists P8; split; [discriminate | apply erefl].
exists P4; exists P10; split; [discriminate | apply erefl].
exists P4; exists P7; split; [discriminate | apply erefl].
exists P5; exists P8; split; [discriminate | apply erefl].
exists P5; exists P7; split; [discriminate | apply erefl].
exists P5; exists P9; split; [discriminate | apply erefl].
exists P5; exists P10; split; [discriminate | apply erefl].
exists P6; exists P7; split; [discriminate | apply erefl].
exists P6; exists P8; split; [discriminate | apply erefl].
exists P6; exists P9; split; [discriminate | apply erefl].
exists P6; exists P10; split; [discriminate | apply erefl].
Qed.
*)
Lemma fp_ext :
  forall (fp:Point->Point) (fp':Point->Point), (forall (p:Point), fp p = fp' p) -> fp = fp'.
Admitted.

(*Lemma fl_ext :
  forall (fl:Line->Line) (fl':Line->Line), (forall (l:Line), fl l = fl' l) -> fl = fl'.
Admitted.
 *)

Lemma is_c_diffP : forall fp, is_collineation2 fp -> forall x y z, x<>y -> fp x = z -> fp y = z -> False.
Proof.
intros fp Hcol x y z  Hyz fp_x fp_z.
destruct Hcol as [[Hinjfp Hsurjfp] Hincid].
rewrite <- fp_z in fp_x.
apply Hyz; apply Hinjfp; assumption.
Qed.
(*
Lemma is_c_diffL : forall fp fl, is_collineation fp fl -> forall  x y z, x<>y -> fl x = z -> fl y = z -> False.
Proof.
intros fp fl Hcol x y z Hyz fp_x fp_z.
destruct Hcol as [HbijP [[Hinjl Hsurjl] Hincid]].
rewrite <- fp_z in fp_x.
apply Hyz; apply Hinjl; assumption.
Qed.
*)
Ltac not_bijP :=
  match goal with H:?fp ?P = ?Q, H':?fp ?R = ?Q |- _ =>
                  eapply (is_c_diffP fp _ _ P R Q); solve [assumption | discriminate] end.
(*
Ltac not_bijL :=
  match goal with H:?fl ?P = ?Q, H':?fl ?R = ?Q |- _ =>
                  eapply (is_c_diffL _ fl _ P R Q); solve [assumption | discriminate] end.
*)

Lemma simple_rewrite : forall fp, is_collineation2 fp -> forall (T U Z:Point), fp T=Z -> fp U=Z -> T=U.
Proof.
  intros fp Hfpfl T U Z HTZ HUZ.
  assert (fp T=fp U).
  transitivity Z; [assumption | symmetry; assumption].
  destruct Hfpfl as [[HinjP _]]; apply HinjP in H; assumption.
Qed.

Ltac elim_degen_cases fp Hfpfl :=
  match goal with HX : fp ?X = _, HY : fp ?Y = _ |- _ =>
                  let Hnew := fresh in
                  assert (Hnew:(fp (third X Y)) = third (fp X) (fp Y))
                    by (apply (fp_lemma2 fp Hfpfl); discriminate);  rewrite HX in Hnew; rewrite HY in Hnew; cbn in Hnew  end;
  match goal with HT : fp ?T = ?Z, HU : fp ?U = ?Z |- _ => 
                  let Hnew2 := fresh in
                  assert (Hnew2:T =U) by (apply (simple_rewrite fp Hfpfl T U Z HT HU)); discriminate end.

Ltac build_hyps fp Hfpfl HP0 HP1 :=
  let HP0P1 := fresh "HP0P1" in
  assert (HP0P1:(P0<>P1)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P0 P1 HP0P1); clear HP0P1; rewrite HP0; rewrite HP1; intros HP2; cbn in HP2; 
  case_eq (fp P3); intros HP3; try solve [not_bijP | elim_degen_cases fp Hfpfl];
  let HP0P3 := fresh "HP0P3" in
  assert (HP0P3:(P0<>P3)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P0 P3 HP0P3); clear HP0P3; rewrite HP0; rewrite HP3; intros HP4; cbn in HP4;
  let HP1P3 := fresh "HP1P3" in
  assert (HP1P3:(P1<>P3)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P1 P3 HP1P3); clear HP1P3; rewrite HP1; rewrite HP3; intros HP5; cbn in HP5;
  let HP0P5 := fresh "HP0P5" in
  assert (HP0P5:(P0<>P5)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P0 P5 HP0P5); clear HP0P5; rewrite HP0; rewrite HP5; intros HP6; cbn in HP6;
  case_eq (fp P7); intros HP7; try solve [not_bijP | elim_degen_cases fp Hfpfl];
  let HP0P1 := fresh "HP0P1" in
  assert (HP0P7:(P0<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P0 P7 HP0P7); clear HP0P7; rewrite HP0; rewrite HP7; intros HP8; cbn in HP8;
  let HP1P7 := fresh "HP1P7" in
  assert (HP1P7:(P1<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P1 P7 HP1P7); clear HP1P7; rewrite HP1; rewrite HP7; intros HP9; cbn in HP9;
  let HP2P7 := fresh "HP2P7" in
  assert (HP2P7:(P2<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P2 P7 HP2P7); clear HP2P7; rewrite HP2; rewrite HP7; intros HP10; cbn in HP10;
  let HP3P7 := fresh "HP3P7" in
  assert (HP3P7:(P3<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P3 P7 HP3P7); clear HP3P7; rewrite HP3; rewrite HP7; intros HP11; cbn in HP11;
  let HP4P7 := fresh "HP4P7" in
  assert (HP4P7:(P4<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P4 P7 HP4P7); clear HP4P7; rewrite HP4; rewrite HP7; intros HP12; cbn in HP12;
  let HP5P7 := fresh "HP5P7" in
  assert (HP5P7:(P5<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P5 P7 HP5P7); clear HP5P7; rewrite HP5; rewrite HP7; intros HP13; cbn in HP13;
  let HP6P7 := fresh "HP6P7" in
  assert (HP6P7:(P6<>P7)) by discriminate;
  generalize (fp_lemma2 fp Hfpfl P6 P7 HP6P7); clear HP6P7; rewrite HP6; rewrite HP7; intros HP14; cbn in HP14; 
  match goal with |- In ?fp ?all_c0 => unfold all_c0 end.

Ltac in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14 := (repeat (match goal with |- In ?fp (?fp_i::_) =>
                let HeqP := fresh in assert (HeqP:fp=fp_i) by (apply fp_ext; intros p; destruct p;
                                       solve [rewrite HP0; apply erefl |
                                              rewrite HP1; apply erefl |
                                              rewrite HP2; apply erefl |
                                              rewrite HP3; apply erefl |
                                              rewrite HP4; apply erefl |
                                              rewrite HP5; apply erefl |
                                              rewrite HP6; apply erefl |
                                              rewrite HP7; apply erefl |
                                              rewrite HP8; apply erefl |
                                              rewrite HP9; apply erefl |
                                              rewrite HP10; apply erefl |
                                              rewrite HP11; apply erefl |
                                              rewrite HP12; apply erefl |
                                              rewrite HP13; apply erefl |
                                              rewrite HP14; apply erefl ]);
                rewrite HeqP; apply in_eq
end || apply in_cons)).
