Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.

Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads.
Require Import PG32.pg32_spreads_collineations. (* for bij *)
(*Require Import PG32.pg32_spreads_packings.
Require Import PG32.pg32_packings_collineations.
Require Import PG32.pg32_automorphisms.*)

Require Import List.
Import ListNotations.

(* library lemmas to be moved somewhere else e.g. pg32_specific_lemmas.v *)

Lemma no_more_than_3_points_per_line : forall l:Line, forall x y z t:Point, x<>y -> x <>z -> x<>t -> y<>z -> y<>t -> z<>t -> incid_lp x l -> incid_lp  y l -> incid_lp z l -> ~incid_lp t l.
Proof.
  intros l x y z t; destruct l; 
    intros Hxy Hxz Hxt Hyz Hyt Hzt Hi1 Hi2 Hi3; destruct x; destruct y;
      try solve [apply False_ind; apply Hxy; apply erefl].
  par:time (destruct z;
            try solve [apply False_ind; apply Hxz; apply erefl |
                       apply False_ind; apply Hyz; apply erefl |
                       apply (degen_bool _ Hi1) |
                       apply (degen_bool _ Hi2) |
                       apply (degen_bool _ Hi3)] ;
            destruct t; solve [apply False_ind; apply Hxt; apply erefl |
                               apply False_ind; apply Hyt; apply erefl |
                               apply False_ind; apply Hzt; apply erefl |
                               apply erefl |
                               intro; apply (degen_bool _ H)]).
Qed.

Lemma eqP_eq : forall a b, ~~ eqP a b <-> a <> b.
Proof.
  intros a b; split.
  destruct a; destruct b; intros; intro; solve [discriminate | apply (degen_bool _ H)].
  destruct a; destruct b; intros; try solve [apply erefl | apply False_ind; apply H; apply erefl].
Qed.  

Definition p2l (fp:Point->Point) (l:Line) : Line :=
  let (xy,z) := PG32.pg32_inductive.points_from_line l in
  let (x,y) := xy in 
  l_from_points (fp x) (fp y).

Definition is_collineation2 (fp:Point-> Point) :=
  bij fp /\ (forall (x : Point) (l : Line), incid_lp x l -> incid_lp (fp x) (p2l fp l)).

Lemma is_collineation_l_from_points_p2l :
  forall fp, is_collineation2 fp ->
                forall Pa Pb, Pa<>Pb -> p2l fp (l_from_points Pa Pb) = l_from_points (fp Pa) (fp Pb).
Proof.
intros fp [HbijP Hincid].
intros Pa Pb HPaPb.
assert (HPa : (incid_lp Pa (l_from_points Pa Pb))) by apply incid_lp_l_from_point1.
assert (HPb : (incid_lp Pb (l_from_points Pa Pb))) by apply incid_lp_l_from_point2.
generalize (Hincid Pa (l_from_points Pa Pb) HPa); clear HPa; intros HPa.
generalize (Hincid Pb (l_from_points Pa Pb) HPb); clear HPb; intros Hpb.
assert (HPa' : incid_lp (fp Pa) (l_from_points (fp Pa) (fp Pb))) by apply incid_lp_l_from_point1.
assert (HPb' : incid_lp (fp Pb) (l_from_points (fp Pa) (fp Pb))) by apply incid_lp_l_from_point2.
eapply (a1_unique (fp Pa)  (fp Pb)); try assumption.
rewrite <- Bool.negb_true_iff; apply eqP_eq.
intro; apply HPaPb; destruct HbijP as [HinjP HsurjP]; apply HinjP; assumption.
Qed.

Definition points_from_line (l:Line) := 
match l with 
| L0  =>  [P0;P1;P2] 
| L1  =>  [P0;P3;P4] 
| L2  =>  [P0;P5;P6] 
| L3  =>  [P0;P7;P8] 
| L4  =>  [P0;P10;P9] 
| L5  =>  [P0;P11;P12] 
| L6  =>  [P0;P13;P14] 
| L7  =>  [P1;P4;P6] 
| L8  =>  [P1;P8;P10] 
| L9  =>  [P1;P12;P14] 
| L10  =>  [P1;P7;P9] 
| L11  =>  [P1;P13;P11] 
| L12  =>  [P1;P3;P5] 
| L13  =>  [P2;P7;P10] 
| L14  =>  [P2;P11;P14] 
| L15  =>  [P2;P3;P6] 
| L16  =>  [P2;P12;P13] 
| L17  =>  [P2;P4;P5] 
| L18  =>  [P2;P8;P9] 
| L19  =>  [P3;P10;P14] 
| L20  =>  [P3;P8;P12] 
| L21  =>  [P3;P9;P13] 
| L22  =>  [P3;P7;P11] 
| L23  =>  [P4;P9;P14] 
| L24  =>  [P4;P8;P11] 
| L25  =>  [P4;P10;P13] 
| L26  =>  [P4;P7;P12] 
| L27  =>  [P5;P8;P14] 
| L28  =>  [P5;P7;P13] 
| L29  =>  [P5;P9;P11] 
| L30  =>  [P5;P10;P12] 
| L31  =>  [P6;P7;P14] 
| L32  =>  [P6;P8;P13] 
| L33  =>  [P6;P9;P12] 
| L34  =>  [P6;P10;P11] 
end.

Definition third (A B:Point) :=
  match (points_from_line (l_from_points A B)) with
    [x1;x2;x3] =>
    if (Point_dec x1 A)
    then
      if (Point_dec x2 B)
      then x3
      else x2
    else
      if (Point_dec x1 B) then
        if (Point_dec x2 A)
        then x3
        else x2
      else x1 | _ => P0 end.                        

Lemma incid_lp_l_from_points : forall A B, A<>B -> forall x, incid_lp x (l_from_points A B) -> x=A \/ x=B \/x=third A B.
Proof.  
intros A B HAB.
destruct A; destruct B; try (apply False_ind; apply HAB; apply erefl);
intros x Hx; destruct x; try solve [left; apply erefl | right; left; apply erefl | right; right; apply erefl | discriminate Hx].
Qed.

Lemma fp_lemma2 : forall fp, is_collineation2 fp -> forall X Y, X<>Y -> fp (third X Y) = third (fp X) (fp Y).
Proof.
intros fp Hfp X Y HXY.
pose (l:=l_from_points X Y).
assert (Hi: (incid_lp (third X Y) l)) by (destruct X; destruct Y; unfold l; try solve [ apply erefl | (apply False_ind; apply HXY; apply erefl)]).
assert (Hi':(incid_lp (fp (third X Y)) (p2l fp l)))  by (destruct Hfp as [HbijP Hincid]; apply Hincid; assumption).
assert (HFXFY:fp X <> fp Y) by (intro Heq; destruct Hfp as [[HinjP _] Hincid]; apply HXY;  apply (HinjP _ _ Heq)).
rewrite (is_collineation_l_from_points_p2l fp Hfp  _ _ HXY) in Hi'.
destruct (incid_lp_l_from_points (fp X) (fp Y) HFXFY (fp (third X Y)) Hi') as [Heq | [Heq | Heq]].
revert Heq; destruct X; destruct Y;
  try solve [apply False_ind; apply HXY; apply erefl | 
             intros Heq; destruct Hfp as [[HinjP _] Hincid]; apply HinjP in Heq; discriminate Heq].
revert Heq; destruct X; destruct Y;
  try solve [apply False_ind; apply HXY; apply erefl | 
             intros Heq; destruct Hfp as [[HinjP _] Hincid]; apply HinjP in Heq; discriminate Heq].
assumption.
Qed.

Lemma l_from_points_third_1 : forall x y, x<>y -> l_from_points x y = l_from_points x (third x y).
Proof.
  intros x y; destruct x; destruct y; intros; simpl;
    solve [apply False_ind; apply H; apply erefl | apply erefl].
Qed.

Lemma l_from_points_third_2 : forall x y, x<>y -> l_from_points x y = l_from_points (third x y) y.
Proof.
  intros x y; destruct x; destruct y; intros; simpl;
    solve [apply False_ind; apply H; apply erefl | apply erefl].
Qed.

Ltac dist HbijP :=  match goal with  |- (?fp ?Q <> ?fp ?R) =>
  let Hnew3 := fresh "Hnew3" in
  assert (Hnew3:Q<>R) by discriminate;intro; apply Hnew3; destruct HbijP as [Hinj Hsurj]; apply Hinj; assumption end.

Ltac both_incid fp HbijP Hincid H x0 x1 x2 x3 :=
 let Hnew := fresh "Hnew" in
  assert (Hnew:incid_lp (fp x3) (l_from_points (fp x0) (fp x1))) by (rewrite H; apply incid_lp_l_from_point2);
  let Hnew2:= fresh "Hnew2" in
  assert (Hnew2:~incid_lp (fp x3) (l_from_points (fp x0) (fp x1)));
  [apply (no_more_than_3_points_per_line (l_from_points (fp x0) (fp x1)) (fp x0) (fp x1) (third (fp x0) (fp x1)) (fp x3))| idtac];
  try solve [ apply incid_lp_l_from_point1 | apply incid_lp_l_from_point2 | dist HbijP | rewrite <- (fp_lemma2 fp (conj HbijP Hincid)); solve [dist HbijP | discriminate]
              |
              rewrite <- (fp_lemma2 fp (conj HbijP Hincid)); solve [dist HbijP | discriminate | rewrite <-  (is_collineation_l_from_points_p2l fp (conj HbijP Hincid)); solve [apply Hincid; apply erefl | discriminate]
            ]]; elim (Hnew2 Hnew).


Lemma bij_p2l_fp : forall fp, bij fp -> (forall (x : Point) (l : Line), incid_lp x l -> incid_lp (fp x) (p2l fp l)) -> bij (p2l fp).
Proof.
  intros fp HbijP Hincid.  
  split.
  + unfold inj, p2l; intros; destruct x; destruct y; simpl in H; try solve [apply erefl].
  par:time (match goal with H : l_from_points (?fp ?x0) (?fp ?x1) = l_from_points (?fp ?x2) (?fp ?x3) |- _ =>
                      both_incid fp HbijP Hincid H x0 x1 x2 x3
                 | H : l_from_points (?fp ?x0) (?fp ?x1) = l_from_points (?fp ?x2) (?fp ?x3) |- _ =>
                      rewrite (l_from_points_sym  (fp x2) (fp x3)) in H; both_incid fp HbijP Hincid H x0 x1 x3 x2

      end).
  + unfold surj.
    unfold surj; intros.
    destruct (a3_1 y) as [a [b [c Habc]]].
    destruct HbijP as [Hfp_inj Hfp_surj].
    destruct (Hfp_surj a) as [a' Ha'].
    destruct (Hfp_surj b) as [b' Hb'].
    destruct (Hfp_surj c) as [c' Hc'].
    assert (Ha'b':a'<>b').
    intro;
    rewrite H in Ha';
    rewrite <- Ha' in Hb';
    revert Habc; repeat rewrite and_bool;intros [[[Habc _] _] _];
    rewrite Hb' in Habc;
    destruct a; solve [auto].

    exists (l_from_points a' b');
      unfold p2l; destruct a'; destruct b'; simpl;

      try solve [(* degenerated cases *)
            apply False_ind; apply  Ha'b'; apply erefl
                                             
          |
          
          (* basic cases *)
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition] ]
          end

          |
          (* basic symmetric cases *)
          match goal with H:?P<>_ |- _= l_from_points (fp _) (fp ?P) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end
          
          |
          (* third is 2 *)
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?R) =>
                           let Hlp := fresh "Hlp" in
                           assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp P) (fp R));
                           [ rewrite (l_from_points_third_1 (fp P) (fp Q)); [intro; apply Ha'b';apply Hfp_inj; assumption | idtac];
                             rewrite <- (fp_lemma2 fp); [apply erefl |  split; [split; assumption |  assumption] | discriminate]
                           | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                           rewrite <- Ha'; rewrite <- Hb';
                           apply points_line; [ revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption | 
                                                revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption |
                                                rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end     


          |
          match goal with H:?P<>_ |- _= l_from_points (fp _) (fp ?P) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?R) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp P) (fp R));
                          [ rewrite (l_from_points_third_1 (fp P) (fp Q)); [intro; apply Ha'b';apply Hfp_inj; assumption | idtac];
                            rewrite <- (fp_lemma2 fp); [apply erefl |  split; [split; assumption | assumption] | discriminate]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end    
          
          |

          (* third is 3 *)
          match goal with H:_<>?Q |- _= l_from_points (fp ?Q) (fp _) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?R) (fp ?Q) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp R) (fp Q));
                          [ rewrite (l_from_points_third_2 (fp P) (fp Q));
                            [intro; apply Ha'b'; apply Hfp_inj; assumption |
                             rewrite <- (fp_lemma2 fp); [apply erefl |  split; [split; assumption | assumption] | discriminate]]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end

          |

          match goal with H:?P<>?Q |- _= l_from_points (fp ?R) (fp ?Q) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp R) (fp Q));
                          [ rewrite (l_from_points_third_2 (fp P) (fp Q));
                            [intro; apply Ha'b'; apply Hfp_inj; assumption |
                             rewrite <- (fp_lemma2 fp); [apply erefl |  split; [split; assumption | assumption] | discriminate]]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end
          ].
Qed.

Lemma is_collineation_l_from_points :
  forall fp fl, is_collineation fp fl ->
                forall Pa Pb, Pa<>Pb -> fl (l_from_points Pa Pb) = l_from_points (fp Pa) (fp Pb).
Proof.
intros fp fl [HbijP [HbijL Hincid]].
intros Pa Pb HPaPb.
assert (HPa : (incid_lp Pa (l_from_points Pa Pb))) by apply incid_lp_l_from_point1.
assert (HPb : (incid_lp Pb (l_from_points Pa Pb))) by apply incid_lp_l_from_point2.
generalize (Hincid Pa (l_from_points Pa Pb) HPa); clear HPa; intros HPa.
generalize (Hincid Pb (l_from_points Pa Pb) HPb); clear HPb; intros Hpb.
assert (HPa' : incid_lp (fp Pa) (l_from_points (fp Pa) (fp Pb))) by apply incid_lp_l_from_point1.
assert (HPb' : incid_lp (fp Pb) (l_from_points (fp Pa) (fp Pb))) by apply incid_lp_l_from_point2.
eapply (a1_unique (fp Pa)  (fp Pb)); try assumption.
rewrite <- Bool.negb_true_iff; apply eqP_eq.
intro; apply HPaPb; destruct HbijP as [HinjP HsurjP]; apply HinjP; assumption.
Qed.

Lemma fp_lemma : forall fp fl, is_collineation fp fl -> forall X Y, X<>Y -> fp (third X Y) = third (fp X) (fp Y).
Proof.
intros fp fl Hfpfl X Y HXY.
pose (l:=l_from_points X Y).
assert (Hi: (incid_lp (third X Y) l)) by (destruct X; destruct Y; unfold l; try solve [ apply erefl | (apply False_ind; apply HXY; apply erefl)]).
assert (Hi':(incid_lp (fp (third X Y)) (fl l))) by (destruct Hfpfl as [HbijP [HbijL Hincid]]; apply Hincid; assumption).
assert (HFXFY:fp X <> fp Y) by (intro Heq; destruct Hfpfl as [[HinjP _] [HbijL Hincid]]; apply HXY;  apply (HinjP _ _ Heq)).
rewrite (is_collineation_l_from_points fp fl Hfpfl  _ _ HXY) in Hi'.
destruct (incid_lp_l_from_points (fp X) (fp Y) HFXFY (fp (third X Y)) Hi') as [Heq | [Heq | Heq]].
revert Heq; destruct X; destruct Y;
  try solve [apply False_ind; apply HXY; apply erefl | 
             intros Heq; destruct Hfpfl as [[HinjP _] [HbijL Hincid]]; apply HinjP in Heq; discriminate Heq].
revert Heq; destruct X; destruct Y;
  try solve [apply False_ind; apply HXY; apply erefl | 
             intros Heq; destruct Hfpfl as [[HinjP _] [HbijL Hincid]]; apply HinjP in Heq; discriminate Heq].
assumption.
Qed.

Lemma is_col_p2l : forall fp fl, is_collineation fp fl -> is_collineation fp (p2l fp).
Proof.
  intros fp fl [Hfp [Hfl Hincid]].
  split.
  assumption.
  split.
  split.
  + unfold inj, p2l; intros; destruct x; destruct y; simpl in H; try solve [apply erefl].
  par:rewrite <- (is_collineation_l_from_points fp fl) in H; [simpl in H | split; [assumption | split; assumption] | discriminate];
  rewrite <- (is_collineation_l_from_points fp fl) in H; [simpl in H | split; [assumption | split; assumption] | discriminate]; destruct Hfl as [Hfl_inj Hfl_surj]; apply Hfl_inj; assumption.
  + unfold surj; intros.
    destruct (a3_1 y) as [a [b [c Habc]]].
    destruct Hfp as [Hfp_inj Hfp_surj].
    destruct (Hfp_surj a) as [a' Ha'].
    destruct (Hfp_surj b) as [b' Hb'].
    destruct (Hfp_surj c) as [c' Hc'].
    assert (Ha'b':a'<>b').
    intro;
    rewrite H in Ha';
    rewrite <- Ha' in Hb';
    revert Habc; repeat rewrite and_bool;intros [[[Habc _] _] _];
    rewrite Hb' in Habc;
    destruct a; solve [auto].
    
    exists (l_from_points a' b');
      unfold p2l; destruct a'; destruct b'; simpl; 

      try solve [(* degenerated cases *)
            apply False_ind; apply  Ha'b'; apply erefl
                                             
          |
          
          (* basic cases *)
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end

          |
          (* basic symmetric cases *)
          match goal with H:?P<>_ |- _= l_from_points (fp _) (fp ?P) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end
          
         |

         match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?R) => 
                         let Hlp := fresh "Hlp" in
                         assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp P) (fp R));
         [ rewrite (l_from_points_third_1 (fp P) (fp Q)); [intro; apply Ha'b';apply Hfp_inj; assumption | idtac];
           rewrite <- (fp_lemma fp fl); [apply erefl |  split; [split; assumption | split; assumption] | discriminate]
         | rewrite <- Hlp]
            end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                           rewrite <- Ha'; rewrite <- Hb';
                           apply points_line; [ revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption | 
                                                revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption |
                                                rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end     



    |
          (* third is 2 *)
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?R) =>
                           let Hlp := fresh "Hlp" in
                           assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp P) (fp R));
                           [ rewrite (l_from_points_third_1 (fp P) (fp Q)); [intro; apply Ha'b';apply Hfp_inj; assumption | idtac];
                             rewrite <- (fp_lemma fp fl); [apply erefl |  split; [split; assumption | split; assumption] | discriminate]
                           | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                           rewrite <- Ha'; rewrite <- Hb';
                           apply points_line; [ revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption | 
                                                revert Habc; repeat rewrite and_bool;intros [ _ [[Ha Hb] _]];assumption |
                                                rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end     


          |
          match goal with H:?P<>_ |- _= l_from_points (fp _) (fp ?P) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?R) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp P) (fp R));
                          [ rewrite (l_from_points_third_1 (fp P) (fp Q)); [intro; apply Ha'b';apply Hfp_inj; assumption | idtac];
                            rewrite <- (fp_lemma fp fl); [apply erefl |  split; [split; assumption | split; assumption] | discriminate]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end    
          
          |

          (* third is 3 *)
          match goal with H:_<>?Q |- _= l_from_points (fp ?Q) (fp _) => rewrite l_from_points_sym
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?R) (fp ?Q) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp R) (fp Q));
                          [ rewrite (l_from_points_third_2 (fp P) (fp Q));
                            [intro; apply Ha'b'; apply Hfp_inj; assumption |
                             rewrite <- (fp_lemma fp fl); [apply erefl |  split; [split; assumption | split; assumption] | discriminate]]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end

          |

          match goal with H:?P<>?Q |- _= l_from_points (fp ?R) (fp ?Q) =>
                          let Hlp := fresh "Hlp" in
                          assert (Hlp: l_from_points (fp P) (fp Q) = l_from_points (fp R) (fp Q));
                          [ rewrite (l_from_points_third_2 (fp P) (fp Q));
                            [intro; apply Ha'b'; apply Hfp_inj; assumption |
                             rewrite <- (fp_lemma fp fl); [apply erefl |  split; [split; assumption | split; assumption] | discriminate]]
                          | rewrite <- Hlp]
          end;
          match goal with H:?P<>?Q |- _= l_from_points (fp ?P) (fp ?Q) => 
                          rewrite <- Ha'; rewrite <- Hb';
                          apply points_line; [ revert Habc; repeat rewrite and_bool;solve [intuition] | 
                                               revert Habc; repeat rewrite and_bool;solve [intuition] |
                                               rewrite <- Bool.negb_true_iff; apply eqP_eq; solve [subst;intuition]]
          end
          ].
  + intros x l Hi; generalize (Hincid x l Hi); intros Hincid'; unfold p2l; destruct l.
  par:simpl; rewrite <- (is_collineation_l_from_points fp fl);
  [simpl; assumption | split; [assumption | split; assumption] | discriminate].
Qed.

Lemma is_col2_is_col : forall (fp : Point -> Point), is_collineation2 fp -> is_collineation fp (p2l fp).
Proof.
  intros fp [Hfp Hincid].
  split.
  assumption.
  split.
  apply bij_p2l_fp; assumption.
  assumption.
Qed.   

Lemma is_collineation_equiv : forall fp,
    is_collineation2 fp <-> is_collineation fp (p2l fp).
Proof.  
  intros.
  split.
  intros.
  apply is_col2_is_col; assumption.
  intros.
  destruct H as [HbijP [HbijL Hincid]].
  split; assumption.
Qed.

Lemma is_col_equiv : forall fp, is_collineation2 fp <-> exists fl:Line->Line, is_collineation fp fl.
Proof.
  intros fp; split.
  intros.
  exists (p2l fp).
  apply is_collineation_equiv; assumption.
  intros.
  destruct H as [fl Hfl]. 
  destruct (is_col_p2l fp fl Hfl) as [HbijP [HbijL Hincid]].
  split; assumption.
Qed.


