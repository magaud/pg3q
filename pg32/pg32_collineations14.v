Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG32.pg32_inductive PG32.pg32_spreads_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_automorphisms_inv.
Require Import PG32.pg32_is_col_fp.

Require Import Lists.List.
Import ListNotations.
Require Import Arith.

Lemma collineation_18816 : is_collineation2 fp_18816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18816.

Lemma collineation_18817 : is_collineation2 fp_18817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18817.

Lemma collineation_18818 : is_collineation2 fp_18818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18818.

Lemma collineation_18819 : is_collineation2 fp_18819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18819.

Lemma collineation_18820 : is_collineation2 fp_18820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18820.

Lemma collineation_18821 : is_collineation2 fp_18821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18821.

Lemma collineation_18822 : is_collineation2 fp_18822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18822.

Lemma collineation_18823 : is_collineation2 fp_18823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18823.

Lemma collineation_18824 : is_collineation2 fp_18824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18824.

Lemma collineation_18825 : is_collineation2 fp_18825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18825.

Lemma collineation_18826 : is_collineation2 fp_18826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18826.

Lemma collineation_18827 : is_collineation2 fp_18827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18827.

Lemma collineation_18828 : is_collineation2 fp_18828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18828.

Lemma collineation_18829 : is_collineation2 fp_18829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18829.

Lemma collineation_18830 : is_collineation2 fp_18830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18830.

Lemma collineation_18831 : is_collineation2 fp_18831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18831.

Lemma collineation_18832 : is_collineation2 fp_18832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18832.

Lemma collineation_18833 : is_collineation2 fp_18833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18833.

Lemma collineation_18834 : is_collineation2 fp_18834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18834.

Lemma collineation_18835 : is_collineation2 fp_18835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18835.

Lemma collineation_18836 : is_collineation2 fp_18836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18836.

Lemma collineation_18837 : is_collineation2 fp_18837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18837.

Lemma collineation_18838 : is_collineation2 fp_18838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18838.

Lemma collineation_18839 : is_collineation2 fp_18839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18839.

Lemma collineation_18840 : is_collineation2 fp_18840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18840.

Lemma collineation_18841 : is_collineation2 fp_18841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18841.

Lemma collineation_18842 : is_collineation2 fp_18842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18842.

Lemma collineation_18843 : is_collineation2 fp_18843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18843.

Lemma collineation_18844 : is_collineation2 fp_18844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18844.

Lemma collineation_18845 : is_collineation2 fp_18845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18845.

Lemma collineation_18846 : is_collineation2 fp_18846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18846.

Lemma collineation_18847 : is_collineation2 fp_18847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18847.

Lemma collineation_18848 : is_collineation2 fp_18848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18848.

Lemma collineation_18849 : is_collineation2 fp_18849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18849.

Lemma collineation_18850 : is_collineation2 fp_18850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18850.

Lemma collineation_18851 : is_collineation2 fp_18851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18851.

Lemma collineation_18852 : is_collineation2 fp_18852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18852.

Lemma collineation_18853 : is_collineation2 fp_18853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18853.

Lemma collineation_18854 : is_collineation2 fp_18854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18854.

Lemma collineation_18855 : is_collineation2 fp_18855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18855.

Lemma collineation_18856 : is_collineation2 fp_18856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18856.

Lemma collineation_18857 : is_collineation2 fp_18857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18857.

Lemma collineation_18858 : is_collineation2 fp_18858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18858.

Lemma collineation_18859 : is_collineation2 fp_18859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18859.

Lemma collineation_18860 : is_collineation2 fp_18860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18860.

Lemma collineation_18861 : is_collineation2 fp_18861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18861.

Lemma collineation_18862 : is_collineation2 fp_18862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18862.

Lemma collineation_18863 : is_collineation2 fp_18863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18863.

Lemma collineation_18864 : is_collineation2 fp_18864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18864.

Lemma collineation_18865 : is_collineation2 fp_18865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18865.

Lemma collineation_18866 : is_collineation2 fp_18866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18866.

Lemma collineation_18867 : is_collineation2 fp_18867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18867.

Lemma collineation_18868 : is_collineation2 fp_18868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18868.

Lemma collineation_18869 : is_collineation2 fp_18869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18869.

Lemma collineation_18870 : is_collineation2 fp_18870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18870.

Lemma collineation_18871 : is_collineation2 fp_18871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18871.

Lemma collineation_18872 : is_collineation2 fp_18872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18872.

Lemma collineation_18873 : is_collineation2 fp_18873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18873.

Lemma collineation_18874 : is_collineation2 fp_18874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18874.

Lemma collineation_18875 : is_collineation2 fp_18875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18875.

Lemma collineation_18876 : is_collineation2 fp_18876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18876.

Lemma collineation_18877 : is_collineation2 fp_18877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18877.

Lemma collineation_18878 : is_collineation2 fp_18878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18878.

Lemma collineation_18879 : is_collineation2 fp_18879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18879.

Lemma collineation_18880 : is_collineation2 fp_18880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18880.

Lemma collineation_18881 : is_collineation2 fp_18881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18881.

Lemma collineation_18882 : is_collineation2 fp_18882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18882.

Lemma collineation_18883 : is_collineation2 fp_18883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18883.

Lemma collineation_18884 : is_collineation2 fp_18884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18884.

Lemma collineation_18885 : is_collineation2 fp_18885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18885.

Lemma collineation_18886 : is_collineation2 fp_18886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18886.

Lemma collineation_18887 : is_collineation2 fp_18887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18887.

Lemma collineation_18888 : is_collineation2 fp_18888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18888.

Lemma collineation_18889 : is_collineation2 fp_18889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18889.

Lemma collineation_18890 : is_collineation2 fp_18890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18890.

Lemma collineation_18891 : is_collineation2 fp_18891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18891.

Lemma collineation_18892 : is_collineation2 fp_18892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18892.

Lemma collineation_18893 : is_collineation2 fp_18893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18893.

Lemma collineation_18894 : is_collineation2 fp_18894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18894.

Lemma collineation_18895 : is_collineation2 fp_18895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18895.

Lemma collineation_18896 : is_collineation2 fp_18896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18896.

Lemma collineation_18897 : is_collineation2 fp_18897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18897.

Lemma collineation_18898 : is_collineation2 fp_18898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18898.

Lemma collineation_18899 : is_collineation2 fp_18899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18899.

Lemma collineation_18900 : is_collineation2 fp_18900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18900.

Lemma collineation_18901 : is_collineation2 fp_18901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18901.

Lemma collineation_18902 : is_collineation2 fp_18902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18902.

Lemma collineation_18903 : is_collineation2 fp_18903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18903.

Lemma collineation_18904 : is_collineation2 fp_18904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18904.

Lemma collineation_18905 : is_collineation2 fp_18905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18905.

Lemma collineation_18906 : is_collineation2 fp_18906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18906.

Lemma collineation_18907 : is_collineation2 fp_18907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18907.

Lemma collineation_18908 : is_collineation2 fp_18908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18908.

Lemma collineation_18909 : is_collineation2 fp_18909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18909.

Lemma collineation_18910 : is_collineation2 fp_18910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18910.

Lemma collineation_18911 : is_collineation2 fp_18911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18911.

Lemma collineation_18912 : is_collineation2 fp_18912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18912.

Lemma collineation_18913 : is_collineation2 fp_18913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18913.

Lemma collineation_18914 : is_collineation2 fp_18914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18914.

Lemma collineation_18915 : is_collineation2 fp_18915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18915.

Lemma collineation_18916 : is_collineation2 fp_18916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18916.

Lemma collineation_18917 : is_collineation2 fp_18917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18917.

Lemma collineation_18918 : is_collineation2 fp_18918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18918.

Lemma collineation_18919 : is_collineation2 fp_18919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18919.

Lemma collineation_18920 : is_collineation2 fp_18920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18920.

Lemma collineation_18921 : is_collineation2 fp_18921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18921.

Lemma collineation_18922 : is_collineation2 fp_18922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18922.

Lemma collineation_18923 : is_collineation2 fp_18923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18923.

Lemma collineation_18924 : is_collineation2 fp_18924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18924.

Lemma collineation_18925 : is_collineation2 fp_18925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18925.

Lemma collineation_18926 : is_collineation2 fp_18926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18926.

Lemma collineation_18927 : is_collineation2 fp_18927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18927.

Lemma collineation_18928 : is_collineation2 fp_18928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18928.

Lemma collineation_18929 : is_collineation2 fp_18929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18929.

Lemma collineation_18930 : is_collineation2 fp_18930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18930.

Lemma collineation_18931 : is_collineation2 fp_18931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18931.

Lemma collineation_18932 : is_collineation2 fp_18932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18932.

Lemma collineation_18933 : is_collineation2 fp_18933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18933.

Lemma collineation_18934 : is_collineation2 fp_18934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18934.

Lemma collineation_18935 : is_collineation2 fp_18935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18935.

Lemma collineation_18936 : is_collineation2 fp_18936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18936.

Lemma collineation_18937 : is_collineation2 fp_18937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18937.

Lemma collineation_18938 : is_collineation2 fp_18938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18938.

Lemma collineation_18939 : is_collineation2 fp_18939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18939.

Lemma collineation_18940 : is_collineation2 fp_18940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18940.

Lemma collineation_18941 : is_collineation2 fp_18941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18941.

Lemma collineation_18942 : is_collineation2 fp_18942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18942.

Lemma collineation_18943 : is_collineation2 fp_18943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18943.

Lemma collineation_18944 : is_collineation2 fp_18944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18944.

Lemma collineation_18945 : is_collineation2 fp_18945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18945.

Lemma collineation_18946 : is_collineation2 fp_18946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18946.

Lemma collineation_18947 : is_collineation2 fp_18947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18947.

Lemma collineation_18948 : is_collineation2 fp_18948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18948.

Lemma collineation_18949 : is_collineation2 fp_18949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18949.

Lemma collineation_18950 : is_collineation2 fp_18950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18950.

Lemma collineation_18951 : is_collineation2 fp_18951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18951.

Lemma collineation_18952 : is_collineation2 fp_18952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18952.

Lemma collineation_18953 : is_collineation2 fp_18953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18953.

Lemma collineation_18954 : is_collineation2 fp_18954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18954.

Lemma collineation_18955 : is_collineation2 fp_18955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18955.

Lemma collineation_18956 : is_collineation2 fp_18956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18956.

Lemma collineation_18957 : is_collineation2 fp_18957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18957.

Lemma collineation_18958 : is_collineation2 fp_18958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18958.

Lemma collineation_18959 : is_collineation2 fp_18959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18959.

Lemma collineation_18960 : is_collineation2 fp_18960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18960.

Lemma collineation_18961 : is_collineation2 fp_18961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18961.

Lemma collineation_18962 : is_collineation2 fp_18962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18962.

Lemma collineation_18963 : is_collineation2 fp_18963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18963.

Lemma collineation_18964 : is_collineation2 fp_18964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18964.

Lemma collineation_18965 : is_collineation2 fp_18965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18965.

Lemma collineation_18966 : is_collineation2 fp_18966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18966.

Lemma collineation_18967 : is_collineation2 fp_18967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18967.

Lemma collineation_18968 : is_collineation2 fp_18968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18968.

Lemma collineation_18969 : is_collineation2 fp_18969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18969.

Lemma collineation_18970 : is_collineation2 fp_18970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18970.

Lemma collineation_18971 : is_collineation2 fp_18971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18971.

Lemma collineation_18972 : is_collineation2 fp_18972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18972.

Lemma collineation_18973 : is_collineation2 fp_18973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18973.

Lemma collineation_18974 : is_collineation2 fp_18974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18974.

Lemma collineation_18975 : is_collineation2 fp_18975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18975.

Lemma collineation_18976 : is_collineation2 fp_18976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18976.

Lemma collineation_18977 : is_collineation2 fp_18977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18977.

Lemma collineation_18978 : is_collineation2 fp_18978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18978.

Lemma collineation_18979 : is_collineation2 fp_18979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18979.

Lemma collineation_18980 : is_collineation2 fp_18980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18980.

Lemma collineation_18981 : is_collineation2 fp_18981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18981.

Lemma collineation_18982 : is_collineation2 fp_18982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18982.

Lemma collineation_18983 : is_collineation2 fp_18983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18983.

Lemma collineation_18984 : is_collineation2 fp_18984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18984.

Lemma collineation_18985 : is_collineation2 fp_18985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18985.

Lemma collineation_18986 : is_collineation2 fp_18986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18986.

Lemma collineation_18987 : is_collineation2 fp_18987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18987.

Lemma collineation_18988 : is_collineation2 fp_18988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18988.

Lemma collineation_18989 : is_collineation2 fp_18989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18989.

Lemma collineation_18990 : is_collineation2 fp_18990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18990.

Lemma collineation_18991 : is_collineation2 fp_18991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18991.

Lemma collineation_18992 : is_collineation2 fp_18992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18992.

Lemma collineation_18993 : is_collineation2 fp_18993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18993.

Lemma collineation_18994 : is_collineation2 fp_18994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18994.

Lemma collineation_18995 : is_collineation2 fp_18995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18995.

Lemma collineation_18996 : is_collineation2 fp_18996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18996.

Lemma collineation_18997 : is_collineation2 fp_18997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18997.

Lemma collineation_18998 : is_collineation2 fp_18998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18998.

Lemma collineation_18999 : is_collineation2 fp_18999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18999.

Lemma collineation_19000 : is_collineation2 fp_19000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19000.

Lemma collineation_19001 : is_collineation2 fp_19001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19001.

Lemma collineation_19002 : is_collineation2 fp_19002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19002.

Lemma collineation_19003 : is_collineation2 fp_19003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19003.

Lemma collineation_19004 : is_collineation2 fp_19004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19004.

Lemma collineation_19005 : is_collineation2 fp_19005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19005.

Lemma collineation_19006 : is_collineation2 fp_19006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19006.

Lemma collineation_19007 : is_collineation2 fp_19007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19007.

Lemma collineation_19008 : is_collineation2 fp_19008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19008.

Lemma collineation_19009 : is_collineation2 fp_19009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19009.

Lemma collineation_19010 : is_collineation2 fp_19010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19010.

Lemma collineation_19011 : is_collineation2 fp_19011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19011.

Lemma collineation_19012 : is_collineation2 fp_19012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19012.

Lemma collineation_19013 : is_collineation2 fp_19013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19013.

Lemma collineation_19014 : is_collineation2 fp_19014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19014.

Lemma collineation_19015 : is_collineation2 fp_19015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19015.

Lemma collineation_19016 : is_collineation2 fp_19016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19016.

Lemma collineation_19017 : is_collineation2 fp_19017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19017.

Lemma collineation_19018 : is_collineation2 fp_19018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19018.

Lemma collineation_19019 : is_collineation2 fp_19019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19019.

Lemma collineation_19020 : is_collineation2 fp_19020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19020.

Lemma collineation_19021 : is_collineation2 fp_19021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19021.

Lemma collineation_19022 : is_collineation2 fp_19022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19022.

Lemma collineation_19023 : is_collineation2 fp_19023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19023.

Lemma collineation_19024 : is_collineation2 fp_19024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19024.

Lemma collineation_19025 : is_collineation2 fp_19025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19025.

Lemma collineation_19026 : is_collineation2 fp_19026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19026.

Lemma collineation_19027 : is_collineation2 fp_19027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19027.

Lemma collineation_19028 : is_collineation2 fp_19028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19028.

Lemma collineation_19029 : is_collineation2 fp_19029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19029.

Lemma collineation_19030 : is_collineation2 fp_19030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19030.

Lemma collineation_19031 : is_collineation2 fp_19031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19031.

Lemma collineation_19032 : is_collineation2 fp_19032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19032.

Lemma collineation_19033 : is_collineation2 fp_19033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19033.

Lemma collineation_19034 : is_collineation2 fp_19034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19034.

Lemma collineation_19035 : is_collineation2 fp_19035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19035.

Lemma collineation_19036 : is_collineation2 fp_19036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19036.

Lemma collineation_19037 : is_collineation2 fp_19037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19037.

Lemma collineation_19038 : is_collineation2 fp_19038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19038.

Lemma collineation_19039 : is_collineation2 fp_19039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19039.

Lemma collineation_19040 : is_collineation2 fp_19040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19040.

Lemma collineation_19041 : is_collineation2 fp_19041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19041.

Lemma collineation_19042 : is_collineation2 fp_19042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19042.

Lemma collineation_19043 : is_collineation2 fp_19043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19043.

Lemma collineation_19044 : is_collineation2 fp_19044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19044.

Lemma collineation_19045 : is_collineation2 fp_19045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19045.

Lemma collineation_19046 : is_collineation2 fp_19046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19046.

Lemma collineation_19047 : is_collineation2 fp_19047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19047.

Lemma collineation_19048 : is_collineation2 fp_19048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19048.

Lemma collineation_19049 : is_collineation2 fp_19049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19049.

Lemma collineation_19050 : is_collineation2 fp_19050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19050.

Lemma collineation_19051 : is_collineation2 fp_19051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19051.

Lemma collineation_19052 : is_collineation2 fp_19052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19052.

Lemma collineation_19053 : is_collineation2 fp_19053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19053.

Lemma collineation_19054 : is_collineation2 fp_19054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19054.

Lemma collineation_19055 : is_collineation2 fp_19055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19055.

Lemma collineation_19056 : is_collineation2 fp_19056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19056.

Lemma collineation_19057 : is_collineation2 fp_19057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19057.

Lemma collineation_19058 : is_collineation2 fp_19058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19058.

Lemma collineation_19059 : is_collineation2 fp_19059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19059.

Lemma collineation_19060 : is_collineation2 fp_19060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19060.

Lemma collineation_19061 : is_collineation2 fp_19061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19061.

Lemma collineation_19062 : is_collineation2 fp_19062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19062.

Lemma collineation_19063 : is_collineation2 fp_19063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19063.

Lemma collineation_19064 : is_collineation2 fp_19064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19064.

Lemma collineation_19065 : is_collineation2 fp_19065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19065.

Lemma collineation_19066 : is_collineation2 fp_19066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19066.

Lemma collineation_19067 : is_collineation2 fp_19067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19067.

Lemma collineation_19068 : is_collineation2 fp_19068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19068.

Lemma collineation_19069 : is_collineation2 fp_19069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19069.

Lemma collineation_19070 : is_collineation2 fp_19070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19070.

Lemma collineation_19071 : is_collineation2 fp_19071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19071.

Lemma collineation_19072 : is_collineation2 fp_19072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19072.

Lemma collineation_19073 : is_collineation2 fp_19073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19073.

Lemma collineation_19074 : is_collineation2 fp_19074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19074.

Lemma collineation_19075 : is_collineation2 fp_19075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19075.

Lemma collineation_19076 : is_collineation2 fp_19076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19076.

Lemma collineation_19077 : is_collineation2 fp_19077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19077.

Lemma collineation_19078 : is_collineation2 fp_19078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19078.

Lemma collineation_19079 : is_collineation2 fp_19079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19079.

Lemma collineation_19080 : is_collineation2 fp_19080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19080.

Lemma collineation_19081 : is_collineation2 fp_19081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19081.

Lemma collineation_19082 : is_collineation2 fp_19082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19082.

Lemma collineation_19083 : is_collineation2 fp_19083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19083.

Lemma collineation_19084 : is_collineation2 fp_19084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19084.

Lemma collineation_19085 : is_collineation2 fp_19085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19085.

Lemma collineation_19086 : is_collineation2 fp_19086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19086.

Lemma collineation_19087 : is_collineation2 fp_19087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19087.

Lemma collineation_19088 : is_collineation2 fp_19088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19088.

Lemma collineation_19089 : is_collineation2 fp_19089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19089.

Lemma collineation_19090 : is_collineation2 fp_19090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19090.

Lemma collineation_19091 : is_collineation2 fp_19091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19091.

Lemma collineation_19092 : is_collineation2 fp_19092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19092.

Lemma collineation_19093 : is_collineation2 fp_19093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19093.

Lemma collineation_19094 : is_collineation2 fp_19094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19094.

Lemma collineation_19095 : is_collineation2 fp_19095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19095.

Lemma collineation_19096 : is_collineation2 fp_19096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19096.

Lemma collineation_19097 : is_collineation2 fp_19097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19097.

Lemma collineation_19098 : is_collineation2 fp_19098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19098.

Lemma collineation_19099 : is_collineation2 fp_19099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19099.

Lemma collineation_19100 : is_collineation2 fp_19100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19100.

Lemma collineation_19101 : is_collineation2 fp_19101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19101.

Lemma collineation_19102 : is_collineation2 fp_19102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19102.

Lemma collineation_19103 : is_collineation2 fp_19103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19103.

Lemma collineation_19104 : is_collineation2 fp_19104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19104.

Lemma collineation_19105 : is_collineation2 fp_19105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19105.

Lemma collineation_19106 : is_collineation2 fp_19106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19106.

Lemma collineation_19107 : is_collineation2 fp_19107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19107.

Lemma collineation_19108 : is_collineation2 fp_19108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19108.

Lemma collineation_19109 : is_collineation2 fp_19109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19109.

Lemma collineation_19110 : is_collineation2 fp_19110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19110.

Lemma collineation_19111 : is_collineation2 fp_19111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19111.

Lemma collineation_19112 : is_collineation2 fp_19112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19112.

Lemma collineation_19113 : is_collineation2 fp_19113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19113.

Lemma collineation_19114 : is_collineation2 fp_19114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19114.

Lemma collineation_19115 : is_collineation2 fp_19115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19115.

Lemma collineation_19116 : is_collineation2 fp_19116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19116.

Lemma collineation_19117 : is_collineation2 fp_19117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19117.

Lemma collineation_19118 : is_collineation2 fp_19118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19118.

Lemma collineation_19119 : is_collineation2 fp_19119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19119.

Lemma collineation_19120 : is_collineation2 fp_19120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19120.

Lemma collineation_19121 : is_collineation2 fp_19121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19121.

Lemma collineation_19122 : is_collineation2 fp_19122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19122.

Lemma collineation_19123 : is_collineation2 fp_19123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19123.

Lemma collineation_19124 : is_collineation2 fp_19124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19124.

Lemma collineation_19125 : is_collineation2 fp_19125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19125.

Lemma collineation_19126 : is_collineation2 fp_19126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19126.

Lemma collineation_19127 : is_collineation2 fp_19127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19127.

Lemma collineation_19128 : is_collineation2 fp_19128.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19128 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19128.

Lemma collineation_19129 : is_collineation2 fp_19129.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19129 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19129.

Lemma collineation_19130 : is_collineation2 fp_19130.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19130 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19130.

Lemma collineation_19131 : is_collineation2 fp_19131.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19131 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19131.

Lemma collineation_19132 : is_collineation2 fp_19132.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19132 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19132.

Lemma collineation_19133 : is_collineation2 fp_19133.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19133 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19133.

Lemma collineation_19134 : is_collineation2 fp_19134.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19134 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19134.

Lemma collineation_19135 : is_collineation2 fp_19135.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19135 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19135.

Lemma collineation_19136 : is_collineation2 fp_19136.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19136 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19136.

Lemma collineation_19137 : is_collineation2 fp_19137.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19137 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19137.

Lemma collineation_19138 : is_collineation2 fp_19138.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19138 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19138.

Lemma collineation_19139 : is_collineation2 fp_19139.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19139 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19139.

Lemma collineation_19140 : is_collineation2 fp_19140.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19140 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19140.

Lemma collineation_19141 : is_collineation2 fp_19141.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19141 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19141.

Lemma collineation_19142 : is_collineation2 fp_19142.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19142 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19142.

Lemma collineation_19143 : is_collineation2 fp_19143.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19143 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19143.

Lemma collineation_19144 : is_collineation2 fp_19144.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19144 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19144.

Lemma collineation_19145 : is_collineation2 fp_19145.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19145 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19145.

Lemma collineation_19146 : is_collineation2 fp_19146.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19146 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19146.

Lemma collineation_19147 : is_collineation2 fp_19147.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19147 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19147.

Lemma collineation_19148 : is_collineation2 fp_19148.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19148 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19148.

Lemma collineation_19149 : is_collineation2 fp_19149.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19149 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19149.

Lemma collineation_19150 : is_collineation2 fp_19150.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19150 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19150.

Lemma collineation_19151 : is_collineation2 fp_19151.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19151 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19151.

Lemma collineation_19152 : is_collineation2 fp_19152.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19152 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19152.

Lemma collineation_19153 : is_collineation2 fp_19153.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19153 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19153.

Lemma collineation_19154 : is_collineation2 fp_19154.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19154 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19154.

Lemma collineation_19155 : is_collineation2 fp_19155.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19155 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19155.

Lemma collineation_19156 : is_collineation2 fp_19156.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19156 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19156.

Lemma collineation_19157 : is_collineation2 fp_19157.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19157 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19157.

Lemma collineation_19158 : is_collineation2 fp_19158.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19158 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19158.

Lemma collineation_19159 : is_collineation2 fp_19159.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19159 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19159.

Lemma collineation_19160 : is_collineation2 fp_19160.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19160 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19160.

Lemma collineation_19161 : is_collineation2 fp_19161.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19161 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19161.

Lemma collineation_19162 : is_collineation2 fp_19162.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19162 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19162.

Lemma collineation_19163 : is_collineation2 fp_19163.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19163 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19163.

Lemma collineation_19164 : is_collineation2 fp_19164.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19164 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19164.

Lemma collineation_19165 : is_collineation2 fp_19165.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19165 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19165.

Lemma collineation_19166 : is_collineation2 fp_19166.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19166 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19166.

Lemma collineation_19167 : is_collineation2 fp_19167.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19167 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19167.

Lemma collineation_19168 : is_collineation2 fp_19168.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19168 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19168.

Lemma collineation_19169 : is_collineation2 fp_19169.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19169 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19169.

Lemma collineation_19170 : is_collineation2 fp_19170.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19170 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19170.

Lemma collineation_19171 : is_collineation2 fp_19171.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19171 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19171.

Lemma collineation_19172 : is_collineation2 fp_19172.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19172 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19172.

Lemma collineation_19173 : is_collineation2 fp_19173.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19173 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19173.

Lemma collineation_19174 : is_collineation2 fp_19174.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19174 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19174.

Lemma collineation_19175 : is_collineation2 fp_19175.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19175 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19175.

Lemma collineation_19176 : is_collineation2 fp_19176.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19176 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19176.

Lemma collineation_19177 : is_collineation2 fp_19177.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19177 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19177.

Lemma collineation_19178 : is_collineation2 fp_19178.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19178 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19178.

Lemma collineation_19179 : is_collineation2 fp_19179.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19179 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19179.

Lemma collineation_19180 : is_collineation2 fp_19180.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19180 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19180.

Lemma collineation_19181 : is_collineation2 fp_19181.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19181 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19181.

Lemma collineation_19182 : is_collineation2 fp_19182.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19182 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19182.

Lemma collineation_19183 : is_collineation2 fp_19183.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19183 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19183.

Lemma collineation_19184 : is_collineation2 fp_19184.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19184 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19184.

Lemma collineation_19185 : is_collineation2 fp_19185.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19185 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19185.

Lemma collineation_19186 : is_collineation2 fp_19186.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19186 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19186.

Lemma collineation_19187 : is_collineation2 fp_19187.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19187 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19187.

Lemma collineation_19188 : is_collineation2 fp_19188.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19188 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19188.

Lemma collineation_19189 : is_collineation2 fp_19189.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19189 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19189.

Lemma collineation_19190 : is_collineation2 fp_19190.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19190 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19190.

Lemma collineation_19191 : is_collineation2 fp_19191.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19191 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19191.

Lemma collineation_19192 : is_collineation2 fp_19192.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19192 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19192.

Lemma collineation_19193 : is_collineation2 fp_19193.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19193 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19193.

Lemma collineation_19194 : is_collineation2 fp_19194.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19194 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19194.

Lemma collineation_19195 : is_collineation2 fp_19195.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19195 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19195.

Lemma collineation_19196 : is_collineation2 fp_19196.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19196 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19196.

Lemma collineation_19197 : is_collineation2 fp_19197.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19197 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19197.

Lemma collineation_19198 : is_collineation2 fp_19198.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19198 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19198.

Lemma collineation_19199 : is_collineation2 fp_19199.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19199 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19199.

Lemma collineation_19200 : is_collineation2 fp_19200.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19200 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19200.

Lemma collineation_19201 : is_collineation2 fp_19201.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19201 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19201.

Lemma collineation_19202 : is_collineation2 fp_19202.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19202 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19202.

Lemma collineation_19203 : is_collineation2 fp_19203.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19203 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19203.

Lemma collineation_19204 : is_collineation2 fp_19204.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19204 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19204.

Lemma collineation_19205 : is_collineation2 fp_19205.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19205 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19205.

Lemma collineation_19206 : is_collineation2 fp_19206.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19206 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19206.

Lemma collineation_19207 : is_collineation2 fp_19207.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19207 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19207.

Lemma collineation_19208 : is_collineation2 fp_19208.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19208 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19208.

Lemma collineation_19209 : is_collineation2 fp_19209.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19209 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19209.

Lemma collineation_19210 : is_collineation2 fp_19210.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19210 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19210.

Lemma collineation_19211 : is_collineation2 fp_19211.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19211 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19211.

Lemma collineation_19212 : is_collineation2 fp_19212.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19212 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19212.

Lemma collineation_19213 : is_collineation2 fp_19213.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19213 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19213.

Lemma collineation_19214 : is_collineation2 fp_19214.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19214 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19214.

Lemma collineation_19215 : is_collineation2 fp_19215.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19215 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19215.

Lemma collineation_19216 : is_collineation2 fp_19216.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19216 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19216.

Lemma collineation_19217 : is_collineation2 fp_19217.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19217 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19217.

Lemma collineation_19218 : is_collineation2 fp_19218.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19218 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19218.

Lemma collineation_19219 : is_collineation2 fp_19219.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19219 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19219.

Lemma collineation_19220 : is_collineation2 fp_19220.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19220 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19220.

Lemma collineation_19221 : is_collineation2 fp_19221.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19221 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19221.

Lemma collineation_19222 : is_collineation2 fp_19222.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19222 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19222.

Lemma collineation_19223 : is_collineation2 fp_19223.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19223 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19223.

Lemma collineation_19224 : is_collineation2 fp_19224.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19224 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19224.

Lemma collineation_19225 : is_collineation2 fp_19225.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19225 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19225.

Lemma collineation_19226 : is_collineation2 fp_19226.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19226 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19226.

Lemma collineation_19227 : is_collineation2 fp_19227.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19227 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19227.

Lemma collineation_19228 : is_collineation2 fp_19228.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19228 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19228.

Lemma collineation_19229 : is_collineation2 fp_19229.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19229 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19229.

Lemma collineation_19230 : is_collineation2 fp_19230.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19230 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19230.

Lemma collineation_19231 : is_collineation2 fp_19231.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19231 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19231.

Lemma collineation_19232 : is_collineation2 fp_19232.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19232 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19232.

Lemma collineation_19233 : is_collineation2 fp_19233.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19233 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19233.

Lemma collineation_19234 : is_collineation2 fp_19234.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19234 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19234.

Lemma collineation_19235 : is_collineation2 fp_19235.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19235 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19235.

Lemma collineation_19236 : is_collineation2 fp_19236.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19236 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19236.

Lemma collineation_19237 : is_collineation2 fp_19237.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19237 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19237.

Lemma collineation_19238 : is_collineation2 fp_19238.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19238 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19238.

Lemma collineation_19239 : is_collineation2 fp_19239.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19239 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19239.

Lemma collineation_19240 : is_collineation2 fp_19240.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19240 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19240.

Lemma collineation_19241 : is_collineation2 fp_19241.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19241 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19241.

Lemma collineation_19242 : is_collineation2 fp_19242.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19242 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19242.

Lemma collineation_19243 : is_collineation2 fp_19243.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19243 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19243.

Lemma collineation_19244 : is_collineation2 fp_19244.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19244 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19244.

Lemma collineation_19245 : is_collineation2 fp_19245.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19245 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19245.

Lemma collineation_19246 : is_collineation2 fp_19246.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19246 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19246.

Lemma collineation_19247 : is_collineation2 fp_19247.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19247 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19247.

Lemma collineation_19248 : is_collineation2 fp_19248.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19248 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19248.

Lemma collineation_19249 : is_collineation2 fp_19249.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19249 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19249.

Lemma collineation_19250 : is_collineation2 fp_19250.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19250 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19250.

Lemma collineation_19251 : is_collineation2 fp_19251.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19251 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19251.

Lemma collineation_19252 : is_collineation2 fp_19252.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19252 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19252.

Lemma collineation_19253 : is_collineation2 fp_19253.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19253 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19253.

Lemma collineation_19254 : is_collineation2 fp_19254.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19254 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19254.

Lemma collineation_19255 : is_collineation2 fp_19255.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19255 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19255.

Lemma collineation_19256 : is_collineation2 fp_19256.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19256 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19256.

Lemma collineation_19257 : is_collineation2 fp_19257.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19257 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19257.

Lemma collineation_19258 : is_collineation2 fp_19258.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19258 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19258.

Lemma collineation_19259 : is_collineation2 fp_19259.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19259 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19259.

Lemma collineation_19260 : is_collineation2 fp_19260.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19260 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19260.

Lemma collineation_19261 : is_collineation2 fp_19261.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19261 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19261.

Lemma collineation_19262 : is_collineation2 fp_19262.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19262 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19262.

Lemma collineation_19263 : is_collineation2 fp_19263.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19263 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19263.

Lemma collineation_19264 : is_collineation2 fp_19264.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19264 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19264.

Lemma collineation_19265 : is_collineation2 fp_19265.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19265 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19265.

Lemma collineation_19266 : is_collineation2 fp_19266.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19266 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19266.

Lemma collineation_19267 : is_collineation2 fp_19267.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19267 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19267.

Lemma collineation_19268 : is_collineation2 fp_19268.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19268 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19268.

Lemma collineation_19269 : is_collineation2 fp_19269.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19269 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19269.

Lemma collineation_19270 : is_collineation2 fp_19270.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19270 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19270.

Lemma collineation_19271 : is_collineation2 fp_19271.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19271 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19271.

Lemma collineation_19272 : is_collineation2 fp_19272.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19272 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19272.

Lemma collineation_19273 : is_collineation2 fp_19273.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19273 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19273.

Lemma collineation_19274 : is_collineation2 fp_19274.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19274 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19274.

Lemma collineation_19275 : is_collineation2 fp_19275.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19275 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19275.

Lemma collineation_19276 : is_collineation2 fp_19276.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19276 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19276.

Lemma collineation_19277 : is_collineation2 fp_19277.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19277 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19277.

Lemma collineation_19278 : is_collineation2 fp_19278.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19278 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19278.

Lemma collineation_19279 : is_collineation2 fp_19279.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19279 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19279.

Lemma collineation_19280 : is_collineation2 fp_19280.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19280 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19280.

Lemma collineation_19281 : is_collineation2 fp_19281.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19281 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19281.

Lemma collineation_19282 : is_collineation2 fp_19282.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19282 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19282.

Lemma collineation_19283 : is_collineation2 fp_19283.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19283 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19283.

Lemma collineation_19284 : is_collineation2 fp_19284.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19284 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19284.

Lemma collineation_19285 : is_collineation2 fp_19285.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19285 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19285.

Lemma collineation_19286 : is_collineation2 fp_19286.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19286 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19286.

Lemma collineation_19287 : is_collineation2 fp_19287.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19287 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19287.

Lemma collineation_19288 : is_collineation2 fp_19288.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19288 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19288.

Lemma collineation_19289 : is_collineation2 fp_19289.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19289 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19289.

Lemma collineation_19290 : is_collineation2 fp_19290.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19290 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19290.

Lemma collineation_19291 : is_collineation2 fp_19291.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19291 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19291.

Lemma collineation_19292 : is_collineation2 fp_19292.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19292 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19292.

Lemma collineation_19293 : is_collineation2 fp_19293.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19293 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19293.

Lemma collineation_19294 : is_collineation2 fp_19294.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19294 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19294.

Lemma collineation_19295 : is_collineation2 fp_19295.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19295 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19295.

Lemma collineation_19296 : is_collineation2 fp_19296.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19296 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19296.

Lemma collineation_19297 : is_collineation2 fp_19297.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19297 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19297.

Lemma collineation_19298 : is_collineation2 fp_19298.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19298 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19298.

Lemma collineation_19299 : is_collineation2 fp_19299.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19299 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19299.

Lemma collineation_19300 : is_collineation2 fp_19300.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19300 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19300.

Lemma collineation_19301 : is_collineation2 fp_19301.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19301 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19301.

Lemma collineation_19302 : is_collineation2 fp_19302.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19302 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19302.

Lemma collineation_19303 : is_collineation2 fp_19303.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19303 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19303.

Lemma collineation_19304 : is_collineation2 fp_19304.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19304 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19304.

Lemma collineation_19305 : is_collineation2 fp_19305.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19305 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19305.

Lemma collineation_19306 : is_collineation2 fp_19306.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19306 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19306.

Lemma collineation_19307 : is_collineation2 fp_19307.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19307 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19307.

Lemma collineation_19308 : is_collineation2 fp_19308.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19308 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19308.

Lemma collineation_19309 : is_collineation2 fp_19309.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19309 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19309.

Lemma collineation_19310 : is_collineation2 fp_19310.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19310 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19310.

Lemma collineation_19311 : is_collineation2 fp_19311.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19311 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19311.

Lemma collineation_19312 : is_collineation2 fp_19312.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19312 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19312.

Lemma collineation_19313 : is_collineation2 fp_19313.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19313 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19313.

Lemma collineation_19314 : is_collineation2 fp_19314.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19314 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19314.

Lemma collineation_19315 : is_collineation2 fp_19315.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19315 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19315.

Lemma collineation_19316 : is_collineation2 fp_19316.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19316 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19316.

Lemma collineation_19317 : is_collineation2 fp_19317.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19317 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19317.

Lemma collineation_19318 : is_collineation2 fp_19318.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19318 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19318.

Lemma collineation_19319 : is_collineation2 fp_19319.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19319 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19319.

Lemma collineation_19320 : is_collineation2 fp_19320.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19320 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19320.

Lemma collineation_19321 : is_collineation2 fp_19321.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19321 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19321.

Lemma collineation_19322 : is_collineation2 fp_19322.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19322 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19322.

Lemma collineation_19323 : is_collineation2 fp_19323.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19323 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19323.

Lemma collineation_19324 : is_collineation2 fp_19324.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19324 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19324.

Lemma collineation_19325 : is_collineation2 fp_19325.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19325 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19325.

Lemma collineation_19326 : is_collineation2 fp_19326.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19326 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19326.

Lemma collineation_19327 : is_collineation2 fp_19327.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19327 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19327.

Lemma collineation_19328 : is_collineation2 fp_19328.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19328 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19328.

Lemma collineation_19329 : is_collineation2 fp_19329.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19329 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19329.

Lemma collineation_19330 : is_collineation2 fp_19330.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19330 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19330.

Lemma collineation_19331 : is_collineation2 fp_19331.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19331 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19331.

Lemma collineation_19332 : is_collineation2 fp_19332.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19332 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19332.

Lemma collineation_19333 : is_collineation2 fp_19333.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19333 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19333.

Lemma collineation_19334 : is_collineation2 fp_19334.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19334 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19334.

Lemma collineation_19335 : is_collineation2 fp_19335.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19335 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19335.

Lemma collineation_19336 : is_collineation2 fp_19336.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19336 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19336.

Lemma collineation_19337 : is_collineation2 fp_19337.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19337 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19337.

Lemma collineation_19338 : is_collineation2 fp_19338.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19338 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19338.

Lemma collineation_19339 : is_collineation2 fp_19339.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19339 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19339.

Lemma collineation_19340 : is_collineation2 fp_19340.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19340 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19340.

Lemma collineation_19341 : is_collineation2 fp_19341.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19341 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19341.

Lemma collineation_19342 : is_collineation2 fp_19342.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19342 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19342.

Lemma collineation_19343 : is_collineation2 fp_19343.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19343 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19343.

Lemma collineation_19344 : is_collineation2 fp_19344.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19344 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19344.

Lemma collineation_19345 : is_collineation2 fp_19345.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19345 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19345.

Lemma collineation_19346 : is_collineation2 fp_19346.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19346 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19346.

Lemma collineation_19347 : is_collineation2 fp_19347.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19347 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19347.

Lemma collineation_19348 : is_collineation2 fp_19348.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19348 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19348.

Lemma collineation_19349 : is_collineation2 fp_19349.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19349 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19349.

Lemma collineation_19350 : is_collineation2 fp_19350.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19350 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19350.

Lemma collineation_19351 : is_collineation2 fp_19351.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19351 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19351.

Lemma collineation_19352 : is_collineation2 fp_19352.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19352 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19352.

Lemma collineation_19353 : is_collineation2 fp_19353.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19353 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19353.

Lemma collineation_19354 : is_collineation2 fp_19354.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19354 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19354.

Lemma collineation_19355 : is_collineation2 fp_19355.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19355 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19355.

Lemma collineation_19356 : is_collineation2 fp_19356.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19356 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19356.

Lemma collineation_19357 : is_collineation2 fp_19357.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19357 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19357.

Lemma collineation_19358 : is_collineation2 fp_19358.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19358 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19358.

Lemma collineation_19359 : is_collineation2 fp_19359.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19359 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19359.

Lemma collineation_19360 : is_collineation2 fp_19360.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19360 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19360.

Lemma collineation_19361 : is_collineation2 fp_19361.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19361 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19361.

Lemma collineation_19362 : is_collineation2 fp_19362.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19362 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19362.

Lemma collineation_19363 : is_collineation2 fp_19363.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19363 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19363.

Lemma collineation_19364 : is_collineation2 fp_19364.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19364 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19364.

Lemma collineation_19365 : is_collineation2 fp_19365.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19365 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19365.

Lemma collineation_19366 : is_collineation2 fp_19366.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19366 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19366.

Lemma collineation_19367 : is_collineation2 fp_19367.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19367 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19367.

Lemma collineation_19368 : is_collineation2 fp_19368.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19368 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19368.

Lemma collineation_19369 : is_collineation2 fp_19369.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19369 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19369.

Lemma collineation_19370 : is_collineation2 fp_19370.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19370 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19370.

Lemma collineation_19371 : is_collineation2 fp_19371.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19371 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19371.

Lemma collineation_19372 : is_collineation2 fp_19372.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19372 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19372.

Lemma collineation_19373 : is_collineation2 fp_19373.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19373 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19373.

Lemma collineation_19374 : is_collineation2 fp_19374.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19374 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19374.

Lemma collineation_19375 : is_collineation2 fp_19375.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19375 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19375.

Lemma collineation_19376 : is_collineation2 fp_19376.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19376 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19376.

Lemma collineation_19377 : is_collineation2 fp_19377.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19377 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19377.

Lemma collineation_19378 : is_collineation2 fp_19378.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19378 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19378.

Lemma collineation_19379 : is_collineation2 fp_19379.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19379 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19379.

Lemma collineation_19380 : is_collineation2 fp_19380.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19380 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19380.

Lemma collineation_19381 : is_collineation2 fp_19381.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19381 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19381.

Lemma collineation_19382 : is_collineation2 fp_19382.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19382 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19382.

Lemma collineation_19383 : is_collineation2 fp_19383.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19383 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19383.

Lemma collineation_19384 : is_collineation2 fp_19384.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19384 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19384.

Lemma collineation_19385 : is_collineation2 fp_19385.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19385 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19385.

Lemma collineation_19386 : is_collineation2 fp_19386.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19386 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19386.

Lemma collineation_19387 : is_collineation2 fp_19387.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19387 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19387.

Lemma collineation_19388 : is_collineation2 fp_19388.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19388 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19388.

Lemma collineation_19389 : is_collineation2 fp_19389.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19389 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19389.

Lemma collineation_19390 : is_collineation2 fp_19390.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19390 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19390.

Lemma collineation_19391 : is_collineation2 fp_19391.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19391 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19391.

Lemma collineation_19392 : is_collineation2 fp_19392.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19392 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19392.

Lemma collineation_19393 : is_collineation2 fp_19393.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19393 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19393.

Lemma collineation_19394 : is_collineation2 fp_19394.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19394 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19394.

Lemma collineation_19395 : is_collineation2 fp_19395.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19395 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19395.

Lemma collineation_19396 : is_collineation2 fp_19396.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19396 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19396.

Lemma collineation_19397 : is_collineation2 fp_19397.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19397 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19397.

Lemma collineation_19398 : is_collineation2 fp_19398.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19398 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19398.

Lemma collineation_19399 : is_collineation2 fp_19399.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19399 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19399.

Lemma collineation_19400 : is_collineation2 fp_19400.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19400 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19400.

Lemma collineation_19401 : is_collineation2 fp_19401.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19401 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19401.

Lemma collineation_19402 : is_collineation2 fp_19402.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19402 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19402.

Lemma collineation_19403 : is_collineation2 fp_19403.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19403 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19403.

Lemma collineation_19404 : is_collineation2 fp_19404.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19404 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19404.

Lemma collineation_19405 : is_collineation2 fp_19405.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19405 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19405.

Lemma collineation_19406 : is_collineation2 fp_19406.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19406 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19406.

Lemma collineation_19407 : is_collineation2 fp_19407.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19407 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19407.

Lemma collineation_19408 : is_collineation2 fp_19408.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19408 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19408.

Lemma collineation_19409 : is_collineation2 fp_19409.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19409 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19409.

Lemma collineation_19410 : is_collineation2 fp_19410.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19410 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19410.

Lemma collineation_19411 : is_collineation2 fp_19411.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19411 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19411.

Lemma collineation_19412 : is_collineation2 fp_19412.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19412 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19412.

Lemma collineation_19413 : is_collineation2 fp_19413.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19413 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19413.

Lemma collineation_19414 : is_collineation2 fp_19414.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19414 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19414.

Lemma collineation_19415 : is_collineation2 fp_19415.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19415 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19415.

Lemma collineation_19416 : is_collineation2 fp_19416.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19416 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19416.

Lemma collineation_19417 : is_collineation2 fp_19417.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19417 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19417.

Lemma collineation_19418 : is_collineation2 fp_19418.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19418 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19418.

Lemma collineation_19419 : is_collineation2 fp_19419.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19419 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19419.

Lemma collineation_19420 : is_collineation2 fp_19420.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19420 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19420.

Lemma collineation_19421 : is_collineation2 fp_19421.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19421 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19421.

Lemma collineation_19422 : is_collineation2 fp_19422.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19422 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19422.

Lemma collineation_19423 : is_collineation2 fp_19423.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19423 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19423.

Lemma collineation_19424 : is_collineation2 fp_19424.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19424 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19424.

Lemma collineation_19425 : is_collineation2 fp_19425.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19425 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19425.

Lemma collineation_19426 : is_collineation2 fp_19426.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19426 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19426.

Lemma collineation_19427 : is_collineation2 fp_19427.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19427 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19427.

Lemma collineation_19428 : is_collineation2 fp_19428.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19428 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19428.

Lemma collineation_19429 : is_collineation2 fp_19429.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19429 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19429.

Lemma collineation_19430 : is_collineation2 fp_19430.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19430 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19430.

Lemma collineation_19431 : is_collineation2 fp_19431.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19431 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19431.

Lemma collineation_19432 : is_collineation2 fp_19432.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19432 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19432.

Lemma collineation_19433 : is_collineation2 fp_19433.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19433 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19433.

Lemma collineation_19434 : is_collineation2 fp_19434.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19434 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19434.

Lemma collineation_19435 : is_collineation2 fp_19435.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19435 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19435.

Lemma collineation_19436 : is_collineation2 fp_19436.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19436 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19436.

Lemma collineation_19437 : is_collineation2 fp_19437.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19437 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19437.

Lemma collineation_19438 : is_collineation2 fp_19438.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19438 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19438.

Lemma collineation_19439 : is_collineation2 fp_19439.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19439 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19439.

Lemma collineation_19440 : is_collineation2 fp_19440.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19440 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19440.

Lemma collineation_19441 : is_collineation2 fp_19441.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19441 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19441.

Lemma collineation_19442 : is_collineation2 fp_19442.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19442 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19442.

Lemma collineation_19443 : is_collineation2 fp_19443.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19443 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19443.

Lemma collineation_19444 : is_collineation2 fp_19444.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19444 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19444.

Lemma collineation_19445 : is_collineation2 fp_19445.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19445 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19445.

Lemma collineation_19446 : is_collineation2 fp_19446.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19446 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19446.

Lemma collineation_19447 : is_collineation2 fp_19447.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19447 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19447.

Lemma collineation_19448 : is_collineation2 fp_19448.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19448 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19448.

Lemma collineation_19449 : is_collineation2 fp_19449.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19449 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19449.

Lemma collineation_19450 : is_collineation2 fp_19450.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19450 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19450.

Lemma collineation_19451 : is_collineation2 fp_19451.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19451 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19451.

Lemma collineation_19452 : is_collineation2 fp_19452.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19452 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19452.

Lemma collineation_19453 : is_collineation2 fp_19453.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19453 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19453.

Lemma collineation_19454 : is_collineation2 fp_19454.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19454 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19454.

Lemma collineation_19455 : is_collineation2 fp_19455.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19455 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19455.

Lemma collineation_19456 : is_collineation2 fp_19456.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19456 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19456.

Lemma collineation_19457 : is_collineation2 fp_19457.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19457 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19457.

Lemma collineation_19458 : is_collineation2 fp_19458.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19458 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19458.

Lemma collineation_19459 : is_collineation2 fp_19459.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19459 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19459.

Lemma collineation_19460 : is_collineation2 fp_19460.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19460 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19460.

Lemma collineation_19461 : is_collineation2 fp_19461.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19461 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19461.

Lemma collineation_19462 : is_collineation2 fp_19462.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19462 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19462.

Lemma collineation_19463 : is_collineation2 fp_19463.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19463 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19463.

Lemma collineation_19464 : is_collineation2 fp_19464.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19464 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19464.

Lemma collineation_19465 : is_collineation2 fp_19465.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19465 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19465.

Lemma collineation_19466 : is_collineation2 fp_19466.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19466 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19466.

Lemma collineation_19467 : is_collineation2 fp_19467.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19467 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19467.

Lemma collineation_19468 : is_collineation2 fp_19468.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19468 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19468.

Lemma collineation_19469 : is_collineation2 fp_19469.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19469 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19469.

Lemma collineation_19470 : is_collineation2 fp_19470.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19470 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19470.

Lemma collineation_19471 : is_collineation2 fp_19471.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19471 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19471.

Lemma collineation_19472 : is_collineation2 fp_19472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19472.

Lemma collineation_19473 : is_collineation2 fp_19473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19473.

Lemma collineation_19474 : is_collineation2 fp_19474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19474.

Lemma collineation_19475 : is_collineation2 fp_19475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19475.

Lemma collineation_19476 : is_collineation2 fp_19476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19476.

Lemma collineation_19477 : is_collineation2 fp_19477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19477.

Lemma collineation_19478 : is_collineation2 fp_19478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19478.

Lemma collineation_19479 : is_collineation2 fp_19479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19479.

Lemma collineation_19480 : is_collineation2 fp_19480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19480.

Lemma collineation_19481 : is_collineation2 fp_19481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19481.

Lemma collineation_19482 : is_collineation2 fp_19482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19482.

Lemma collineation_19483 : is_collineation2 fp_19483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19483.

Lemma collineation_19484 : is_collineation2 fp_19484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19484.

Lemma collineation_19485 : is_collineation2 fp_19485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19485.

Lemma collineation_19486 : is_collineation2 fp_19486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19486.

Lemma collineation_19487 : is_collineation2 fp_19487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19487.

Lemma collineation_19488 : is_collineation2 fp_19488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19488.

Lemma collineation_19489 : is_collineation2 fp_19489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19489.

Lemma collineation_19490 : is_collineation2 fp_19490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19490.

Lemma collineation_19491 : is_collineation2 fp_19491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19491.

Lemma collineation_19492 : is_collineation2 fp_19492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19492.

Lemma collineation_19493 : is_collineation2 fp_19493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19493.

Lemma collineation_19494 : is_collineation2 fp_19494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19494.

Lemma collineation_19495 : is_collineation2 fp_19495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19495.

Lemma collineation_19496 : is_collineation2 fp_19496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19496.

Lemma collineation_19497 : is_collineation2 fp_19497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19497.

Lemma collineation_19498 : is_collineation2 fp_19498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19498.

Lemma collineation_19499 : is_collineation2 fp_19499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19499.

Lemma collineation_19500 : is_collineation2 fp_19500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19500.

Lemma collineation_19501 : is_collineation2 fp_19501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19501.

Lemma collineation_19502 : is_collineation2 fp_19502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19502.

Lemma collineation_19503 : is_collineation2 fp_19503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19503.

Lemma collineation_19504 : is_collineation2 fp_19504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19504.

Lemma collineation_19505 : is_collineation2 fp_19505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19505.

Lemma collineation_19506 : is_collineation2 fp_19506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19506.

Lemma collineation_19507 : is_collineation2 fp_19507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19507.

Lemma collineation_19508 : is_collineation2 fp_19508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19508.

Lemma collineation_19509 : is_collineation2 fp_19509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19509.

Lemma collineation_19510 : is_collineation2 fp_19510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19510.

Lemma collineation_19511 : is_collineation2 fp_19511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19511.

Lemma collineation_19512 : is_collineation2 fp_19512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19512.

Lemma collineation_19513 : is_collineation2 fp_19513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19513.

Lemma collineation_19514 : is_collineation2 fp_19514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19514.

Lemma collineation_19515 : is_collineation2 fp_19515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19515.

Lemma collineation_19516 : is_collineation2 fp_19516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19516.

Lemma collineation_19517 : is_collineation2 fp_19517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19517.

Lemma collineation_19518 : is_collineation2 fp_19518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19518.

Lemma collineation_19519 : is_collineation2 fp_19519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19519.

Lemma collineation_19520 : is_collineation2 fp_19520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19520.

Lemma collineation_19521 : is_collineation2 fp_19521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19521.

Lemma collineation_19522 : is_collineation2 fp_19522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19522.

Lemma collineation_19523 : is_collineation2 fp_19523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19523.

Lemma collineation_19524 : is_collineation2 fp_19524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19524.

Lemma collineation_19525 : is_collineation2 fp_19525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19525.

Lemma collineation_19526 : is_collineation2 fp_19526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19526.

Lemma collineation_19527 : is_collineation2 fp_19527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19527.

Lemma collineation_19528 : is_collineation2 fp_19528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19528.

Lemma collineation_19529 : is_collineation2 fp_19529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19529.

Lemma collineation_19530 : is_collineation2 fp_19530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19530.

Lemma collineation_19531 : is_collineation2 fp_19531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19531.

Lemma collineation_19532 : is_collineation2 fp_19532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19532.

Lemma collineation_19533 : is_collineation2 fp_19533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19533.

Lemma collineation_19534 : is_collineation2 fp_19534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19534.

Lemma collineation_19535 : is_collineation2 fp_19535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19535.

Lemma collineation_19536 : is_collineation2 fp_19536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19536.

Lemma collineation_19537 : is_collineation2 fp_19537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19537.

Lemma collineation_19538 : is_collineation2 fp_19538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19538.

Lemma collineation_19539 : is_collineation2 fp_19539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19539.

Lemma collineation_19540 : is_collineation2 fp_19540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19540.

Lemma collineation_19541 : is_collineation2 fp_19541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19541.

Lemma collineation_19542 : is_collineation2 fp_19542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19542.

Lemma collineation_19543 : is_collineation2 fp_19543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19543.

Lemma collineation_19544 : is_collineation2 fp_19544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19544.

Lemma collineation_19545 : is_collineation2 fp_19545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19545.

Lemma collineation_19546 : is_collineation2 fp_19546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19546.

Lemma collineation_19547 : is_collineation2 fp_19547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19547.

Lemma collineation_19548 : is_collineation2 fp_19548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19548.

Lemma collineation_19549 : is_collineation2 fp_19549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19549.

Lemma collineation_19550 : is_collineation2 fp_19550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19550.

Lemma collineation_19551 : is_collineation2 fp_19551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19551.

Lemma collineation_19552 : is_collineation2 fp_19552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19552.

Lemma collineation_19553 : is_collineation2 fp_19553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19553.

Lemma collineation_19554 : is_collineation2 fp_19554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19554.

Lemma collineation_19555 : is_collineation2 fp_19555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19555.

Lemma collineation_19556 : is_collineation2 fp_19556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19556.

Lemma collineation_19557 : is_collineation2 fp_19557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19557.

Lemma collineation_19558 : is_collineation2 fp_19558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19558.

Lemma collineation_19559 : is_collineation2 fp_19559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19559.

Lemma collineation_19560 : is_collineation2 fp_19560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19560.

Lemma collineation_19561 : is_collineation2 fp_19561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19561.

Lemma collineation_19562 : is_collineation2 fp_19562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19562.

Lemma collineation_19563 : is_collineation2 fp_19563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19563.

Lemma collineation_19564 : is_collineation2 fp_19564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19564.

Lemma collineation_19565 : is_collineation2 fp_19565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19565.

Lemma collineation_19566 : is_collineation2 fp_19566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19566.

Lemma collineation_19567 : is_collineation2 fp_19567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19567.

Lemma collineation_19568 : is_collineation2 fp_19568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19568.

Lemma collineation_19569 : is_collineation2 fp_19569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19569.

Lemma collineation_19570 : is_collineation2 fp_19570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19570.

Lemma collineation_19571 : is_collineation2 fp_19571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19571.

Lemma collineation_19572 : is_collineation2 fp_19572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19572.

Lemma collineation_19573 : is_collineation2 fp_19573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19573.

Lemma collineation_19574 : is_collineation2 fp_19574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19574.

Lemma collineation_19575 : is_collineation2 fp_19575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19575.

Lemma collineation_19576 : is_collineation2 fp_19576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19576.

Lemma collineation_19577 : is_collineation2 fp_19577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19577.

Lemma collineation_19578 : is_collineation2 fp_19578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19578.

Lemma collineation_19579 : is_collineation2 fp_19579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19579.

Lemma collineation_19580 : is_collineation2 fp_19580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19580.

Lemma collineation_19581 : is_collineation2 fp_19581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19581.

Lemma collineation_19582 : is_collineation2 fp_19582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19582.

Lemma collineation_19583 : is_collineation2 fp_19583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19583.

Lemma collineation_19584 : is_collineation2 fp_19584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19584.

Lemma collineation_19585 : is_collineation2 fp_19585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19585.

Lemma collineation_19586 : is_collineation2 fp_19586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19586.

Lemma collineation_19587 : is_collineation2 fp_19587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19587.

Lemma collineation_19588 : is_collineation2 fp_19588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19588.

Lemma collineation_19589 : is_collineation2 fp_19589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19589.

Lemma collineation_19590 : is_collineation2 fp_19590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19590.

Lemma collineation_19591 : is_collineation2 fp_19591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19591.

Lemma collineation_19592 : is_collineation2 fp_19592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19592.

Lemma collineation_19593 : is_collineation2 fp_19593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19593.

Lemma collineation_19594 : is_collineation2 fp_19594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19594.

Lemma collineation_19595 : is_collineation2 fp_19595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19595.

Lemma collineation_19596 : is_collineation2 fp_19596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19596.

Lemma collineation_19597 : is_collineation2 fp_19597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19597.

Lemma collineation_19598 : is_collineation2 fp_19598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19598.

Lemma collineation_19599 : is_collineation2 fp_19599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19599.

Lemma collineation_19600 : is_collineation2 fp_19600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19600.

Lemma collineation_19601 : is_collineation2 fp_19601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19601.

Lemma collineation_19602 : is_collineation2 fp_19602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19602.

Lemma collineation_19603 : is_collineation2 fp_19603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19603.

Lemma collineation_19604 : is_collineation2 fp_19604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19604.

Lemma collineation_19605 : is_collineation2 fp_19605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19605.

Lemma collineation_19606 : is_collineation2 fp_19606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19606.

Lemma collineation_19607 : is_collineation2 fp_19607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19607.

Lemma collineation_19608 : is_collineation2 fp_19608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19608.

Lemma collineation_19609 : is_collineation2 fp_19609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19609.

Lemma collineation_19610 : is_collineation2 fp_19610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19610.

Lemma collineation_19611 : is_collineation2 fp_19611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19611.

Lemma collineation_19612 : is_collineation2 fp_19612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19612.

Lemma collineation_19613 : is_collineation2 fp_19613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19613.

Lemma collineation_19614 : is_collineation2 fp_19614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19614.

Lemma collineation_19615 : is_collineation2 fp_19615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19615.

Lemma collineation_19616 : is_collineation2 fp_19616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19616.

Lemma collineation_19617 : is_collineation2 fp_19617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19617.

Lemma collineation_19618 : is_collineation2 fp_19618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19618.

Lemma collineation_19619 : is_collineation2 fp_19619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19619.

Lemma collineation_19620 : is_collineation2 fp_19620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19620.

Lemma collineation_19621 : is_collineation2 fp_19621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19621.

Lemma collineation_19622 : is_collineation2 fp_19622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19622.

Lemma collineation_19623 : is_collineation2 fp_19623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19623.

Lemma collineation_19624 : is_collineation2 fp_19624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19624.

Lemma collineation_19625 : is_collineation2 fp_19625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19625.

Lemma collineation_19626 : is_collineation2 fp_19626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19626.

Lemma collineation_19627 : is_collineation2 fp_19627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19627.

Lemma collineation_19628 : is_collineation2 fp_19628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19628.

Lemma collineation_19629 : is_collineation2 fp_19629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19629.

Lemma collineation_19630 : is_collineation2 fp_19630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19630.

Lemma collineation_19631 : is_collineation2 fp_19631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19631.

Lemma collineation_19632 : is_collineation2 fp_19632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19632.

Lemma collineation_19633 : is_collineation2 fp_19633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19633.

Lemma collineation_19634 : is_collineation2 fp_19634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19634.

Lemma collineation_19635 : is_collineation2 fp_19635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19635.

Lemma collineation_19636 : is_collineation2 fp_19636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19636.

Lemma collineation_19637 : is_collineation2 fp_19637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19637.

Lemma collineation_19638 : is_collineation2 fp_19638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19638.

Lemma collineation_19639 : is_collineation2 fp_19639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19639.

Lemma collineation_19640 : is_collineation2 fp_19640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19640.

Lemma collineation_19641 : is_collineation2 fp_19641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19641.

Lemma collineation_19642 : is_collineation2 fp_19642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19642.

Lemma collineation_19643 : is_collineation2 fp_19643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19643.

Lemma collineation_19644 : is_collineation2 fp_19644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19644.

Lemma collineation_19645 : is_collineation2 fp_19645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19645.

Lemma collineation_19646 : is_collineation2 fp_19646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19646.

Lemma collineation_19647 : is_collineation2 fp_19647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19647.

Lemma collineation_19648 : is_collineation2 fp_19648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19648.

Lemma collineation_19649 : is_collineation2 fp_19649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19649.

Lemma collineation_19650 : is_collineation2 fp_19650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19650.

Lemma collineation_19651 : is_collineation2 fp_19651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19651.

Lemma collineation_19652 : is_collineation2 fp_19652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19652.

Lemma collineation_19653 : is_collineation2 fp_19653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19653.

Lemma collineation_19654 : is_collineation2 fp_19654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19654.

Lemma collineation_19655 : is_collineation2 fp_19655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19655.

Lemma collineation_19656 : is_collineation2 fp_19656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19656.

Lemma collineation_19657 : is_collineation2 fp_19657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19657.

Lemma collineation_19658 : is_collineation2 fp_19658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19658.

Lemma collineation_19659 : is_collineation2 fp_19659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19659.

Lemma collineation_19660 : is_collineation2 fp_19660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19660.

Lemma collineation_19661 : is_collineation2 fp_19661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19661.

Lemma collineation_19662 : is_collineation2 fp_19662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19662.

Lemma collineation_19663 : is_collineation2 fp_19663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19663.

Lemma collineation_19664 : is_collineation2 fp_19664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19664.

Lemma collineation_19665 : is_collineation2 fp_19665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19665.

Lemma collineation_19666 : is_collineation2 fp_19666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19666.

Lemma collineation_19667 : is_collineation2 fp_19667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19667.

Lemma collineation_19668 : is_collineation2 fp_19668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19668.

Lemma collineation_19669 : is_collineation2 fp_19669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19669.

Lemma collineation_19670 : is_collineation2 fp_19670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19670.

Lemma collineation_19671 : is_collineation2 fp_19671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19671.

Lemma collineation_19672 : is_collineation2 fp_19672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19672.

Lemma collineation_19673 : is_collineation2 fp_19673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19673.

Lemma collineation_19674 : is_collineation2 fp_19674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19674.

Lemma collineation_19675 : is_collineation2 fp_19675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19675.

Lemma collineation_19676 : is_collineation2 fp_19676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19676.

Lemma collineation_19677 : is_collineation2 fp_19677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19677.

Lemma collineation_19678 : is_collineation2 fp_19678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19678.

Lemma collineation_19679 : is_collineation2 fp_19679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19679.

Lemma collineation_19680 : is_collineation2 fp_19680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19680.

Lemma collineation_19681 : is_collineation2 fp_19681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19681.

Lemma collineation_19682 : is_collineation2 fp_19682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19682.

Lemma collineation_19683 : is_collineation2 fp_19683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19683.

Lemma collineation_19684 : is_collineation2 fp_19684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19684.

Lemma collineation_19685 : is_collineation2 fp_19685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19685.

Lemma collineation_19686 : is_collineation2 fp_19686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19686.

Lemma collineation_19687 : is_collineation2 fp_19687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19687.

Lemma collineation_19688 : is_collineation2 fp_19688.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19688 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19688.

Lemma collineation_19689 : is_collineation2 fp_19689.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19689 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19689.

Lemma collineation_19690 : is_collineation2 fp_19690.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19690 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19690.

Lemma collineation_19691 : is_collineation2 fp_19691.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19691 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19691.

Lemma collineation_19692 : is_collineation2 fp_19692.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19692 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19692.

Lemma collineation_19693 : is_collineation2 fp_19693.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19693 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19693.

Lemma collineation_19694 : is_collineation2 fp_19694.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19694 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19694.

Lemma collineation_19695 : is_collineation2 fp_19695.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19695 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19695.

Lemma collineation_19696 : is_collineation2 fp_19696.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19696 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19696.

Lemma collineation_19697 : is_collineation2 fp_19697.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19697 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19697.

Lemma collineation_19698 : is_collineation2 fp_19698.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19698 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19698.

Lemma collineation_19699 : is_collineation2 fp_19699.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19699 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19699.

Lemma collineation_19700 : is_collineation2 fp_19700.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19700 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19700.

Lemma collineation_19701 : is_collineation2 fp_19701.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19701 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19701.

Lemma collineation_19702 : is_collineation2 fp_19702.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19702 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19702.

Lemma collineation_19703 : is_collineation2 fp_19703.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19703 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19703.

Lemma collineation_19704 : is_collineation2 fp_19704.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19704 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19704.

Lemma collineation_19705 : is_collineation2 fp_19705.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19705 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19705.

Lemma collineation_19706 : is_collineation2 fp_19706.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19706 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19706.

Lemma collineation_19707 : is_collineation2 fp_19707.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19707 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19707.

Lemma collineation_19708 : is_collineation2 fp_19708.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19708 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19708.

Lemma collineation_19709 : is_collineation2 fp_19709.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19709 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19709.

Lemma collineation_19710 : is_collineation2 fp_19710.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19710 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19710.

Lemma collineation_19711 : is_collineation2 fp_19711.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19711 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19711.

Lemma collineation_19712 : is_collineation2 fp_19712.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19712 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19712.

Lemma collineation_19713 : is_collineation2 fp_19713.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19713 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19713.

Lemma collineation_19714 : is_collineation2 fp_19714.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19714 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19714.

Lemma collineation_19715 : is_collineation2 fp_19715.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19715 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19715.

Lemma collineation_19716 : is_collineation2 fp_19716.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19716 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19716.

Lemma collineation_19717 : is_collineation2 fp_19717.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19717 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19717.

Lemma collineation_19718 : is_collineation2 fp_19718.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19718 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19718.

Lemma collineation_19719 : is_collineation2 fp_19719.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19719 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19719.

Lemma collineation_19720 : is_collineation2 fp_19720.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19720 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19720.

Lemma collineation_19721 : is_collineation2 fp_19721.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19721 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19721.

Lemma collineation_19722 : is_collineation2 fp_19722.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19722 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19722.

Lemma collineation_19723 : is_collineation2 fp_19723.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19723 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19723.

Lemma collineation_19724 : is_collineation2 fp_19724.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19724 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19724.

Lemma collineation_19725 : is_collineation2 fp_19725.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19725 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19725.

Lemma collineation_19726 : is_collineation2 fp_19726.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19726 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19726.

Lemma collineation_19727 : is_collineation2 fp_19727.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19727 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19727.

Lemma collineation_19728 : is_collineation2 fp_19728.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19728 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19728.

Lemma collineation_19729 : is_collineation2 fp_19729.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19729 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19729.

Lemma collineation_19730 : is_collineation2 fp_19730.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19730 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19730.

Lemma collineation_19731 : is_collineation2 fp_19731.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19731 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19731.

Lemma collineation_19732 : is_collineation2 fp_19732.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19732 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19732.

Lemma collineation_19733 : is_collineation2 fp_19733.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19733 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19733.

Lemma collineation_19734 : is_collineation2 fp_19734.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19734 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19734.

Lemma collineation_19735 : is_collineation2 fp_19735.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19735 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19735.

Lemma collineation_19736 : is_collineation2 fp_19736.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19736 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19736.

Lemma collineation_19737 : is_collineation2 fp_19737.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19737 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19737.

Lemma collineation_19738 : is_collineation2 fp_19738.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19738 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19738.

Lemma collineation_19739 : is_collineation2 fp_19739.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19739 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19739.

Lemma collineation_19740 : is_collineation2 fp_19740.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19740 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19740.

Lemma collineation_19741 : is_collineation2 fp_19741.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19741 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19741.

Lemma collineation_19742 : is_collineation2 fp_19742.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19742 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19742.

Lemma collineation_19743 : is_collineation2 fp_19743.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19743 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19743.

Lemma collineation_19744 : is_collineation2 fp_19744.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19744 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19744.

Lemma collineation_19745 : is_collineation2 fp_19745.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19745 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19745.

Lemma collineation_19746 : is_collineation2 fp_19746.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19746 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19746.

Lemma collineation_19747 : is_collineation2 fp_19747.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19747 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19747.

Lemma collineation_19748 : is_collineation2 fp_19748.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19748 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19748.

Lemma collineation_19749 : is_collineation2 fp_19749.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19749 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19749.

Lemma collineation_19750 : is_collineation2 fp_19750.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19750 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19750.

Lemma collineation_19751 : is_collineation2 fp_19751.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19751 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19751.

Lemma collineation_19752 : is_collineation2 fp_19752.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19752 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19752.

Lemma collineation_19753 : is_collineation2 fp_19753.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19753 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19753.

Lemma collineation_19754 : is_collineation2 fp_19754.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19754 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19754.

Lemma collineation_19755 : is_collineation2 fp_19755.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19755 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19755.

Lemma collineation_19756 : is_collineation2 fp_19756.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19756 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19756.

Lemma collineation_19757 : is_collineation2 fp_19757.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19757 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19757.

Lemma collineation_19758 : is_collineation2 fp_19758.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19758 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19758.

Lemma collineation_19759 : is_collineation2 fp_19759.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19759 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19759.

Lemma collineation_19760 : is_collineation2 fp_19760.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19760 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19760.

Lemma collineation_19761 : is_collineation2 fp_19761.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19761 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19761.

Lemma collineation_19762 : is_collineation2 fp_19762.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19762 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19762.

Lemma collineation_19763 : is_collineation2 fp_19763.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19763 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19763.

Lemma collineation_19764 : is_collineation2 fp_19764.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19764 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19764.

Lemma collineation_19765 : is_collineation2 fp_19765.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19765 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19765.

Lemma collineation_19766 : is_collineation2 fp_19766.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19766 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19766.

Lemma collineation_19767 : is_collineation2 fp_19767.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19767 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19767.

Lemma collineation_19768 : is_collineation2 fp_19768.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19768 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19768.

Lemma collineation_19769 : is_collineation2 fp_19769.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19769 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19769.

Lemma collineation_19770 : is_collineation2 fp_19770.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19770 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19770.

Lemma collineation_19771 : is_collineation2 fp_19771.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19771 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19771.

Lemma collineation_19772 : is_collineation2 fp_19772.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19772 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19772.

Lemma collineation_19773 : is_collineation2 fp_19773.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19773 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19773.

Lemma collineation_19774 : is_collineation2 fp_19774.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19774 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19774.

Lemma collineation_19775 : is_collineation2 fp_19775.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19775 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19775.

Lemma collineation_19776 : is_collineation2 fp_19776.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19776 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19776.

Lemma collineation_19777 : is_collineation2 fp_19777.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19777 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19777.

Lemma collineation_19778 : is_collineation2 fp_19778.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19778 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19778.

Lemma collineation_19779 : is_collineation2 fp_19779.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19779 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19779.

Lemma collineation_19780 : is_collineation2 fp_19780.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19780 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19780.

Lemma collineation_19781 : is_collineation2 fp_19781.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19781 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19781.

Lemma collineation_19782 : is_collineation2 fp_19782.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19782 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19782.

Lemma collineation_19783 : is_collineation2 fp_19783.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19783 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19783.

Lemma collineation_19784 : is_collineation2 fp_19784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19784.

Lemma collineation_19785 : is_collineation2 fp_19785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19785.

Lemma collineation_19786 : is_collineation2 fp_19786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19786.

Lemma collineation_19787 : is_collineation2 fp_19787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19787.

Lemma collineation_19788 : is_collineation2 fp_19788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19788.

Lemma collineation_19789 : is_collineation2 fp_19789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19789.

Lemma collineation_19790 : is_collineation2 fp_19790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19790.

Lemma collineation_19791 : is_collineation2 fp_19791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19791.

Lemma collineation_19792 : is_collineation2 fp_19792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19792.

Lemma collineation_19793 : is_collineation2 fp_19793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19793.

Lemma collineation_19794 : is_collineation2 fp_19794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19794.

Lemma collineation_19795 : is_collineation2 fp_19795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19795.

Lemma collineation_19796 : is_collineation2 fp_19796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19796.

Lemma collineation_19797 : is_collineation2 fp_19797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19797.

Lemma collineation_19798 : is_collineation2 fp_19798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19798.

Lemma collineation_19799 : is_collineation2 fp_19799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19799.

Lemma collineation_19800 : is_collineation2 fp_19800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19800.

Lemma collineation_19801 : is_collineation2 fp_19801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19801.

Lemma collineation_19802 : is_collineation2 fp_19802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19802.

Lemma collineation_19803 : is_collineation2 fp_19803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19803.

Lemma collineation_19804 : is_collineation2 fp_19804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19804.

Lemma collineation_19805 : is_collineation2 fp_19805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19805.

Lemma collineation_19806 : is_collineation2 fp_19806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19806.

Lemma collineation_19807 : is_collineation2 fp_19807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19807.

Lemma collineation_19808 : is_collineation2 fp_19808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19808.

Lemma collineation_19809 : is_collineation2 fp_19809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19809.

Lemma collineation_19810 : is_collineation2 fp_19810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19810.

Lemma collineation_19811 : is_collineation2 fp_19811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19811.

Lemma collineation_19812 : is_collineation2 fp_19812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19812.

Lemma collineation_19813 : is_collineation2 fp_19813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19813.

Lemma collineation_19814 : is_collineation2 fp_19814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19814.

Lemma collineation_19815 : is_collineation2 fp_19815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19815.

Lemma collineation_19816 : is_collineation2 fp_19816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19816.

Lemma collineation_19817 : is_collineation2 fp_19817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19817.

Lemma collineation_19818 : is_collineation2 fp_19818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19818.

Lemma collineation_19819 : is_collineation2 fp_19819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19819.

Lemma collineation_19820 : is_collineation2 fp_19820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19820.

Lemma collineation_19821 : is_collineation2 fp_19821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19821.

Lemma collineation_19822 : is_collineation2 fp_19822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19822.

Lemma collineation_19823 : is_collineation2 fp_19823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19823.

Lemma collineation_19824 : is_collineation2 fp_19824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19824.

Lemma collineation_19825 : is_collineation2 fp_19825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19825.

Lemma collineation_19826 : is_collineation2 fp_19826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19826.

Lemma collineation_19827 : is_collineation2 fp_19827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19827.

Lemma collineation_19828 : is_collineation2 fp_19828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19828.

Lemma collineation_19829 : is_collineation2 fp_19829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19829.

Lemma collineation_19830 : is_collineation2 fp_19830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19830.

Lemma collineation_19831 : is_collineation2 fp_19831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19831.

Lemma collineation_19832 : is_collineation2 fp_19832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19832.

Lemma collineation_19833 : is_collineation2 fp_19833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19833.

Lemma collineation_19834 : is_collineation2 fp_19834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19834.

Lemma collineation_19835 : is_collineation2 fp_19835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19835.

Lemma collineation_19836 : is_collineation2 fp_19836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19836.

Lemma collineation_19837 : is_collineation2 fp_19837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19837.

Lemma collineation_19838 : is_collineation2 fp_19838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19838.

Lemma collineation_19839 : is_collineation2 fp_19839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19839.

Lemma collineation_19840 : is_collineation2 fp_19840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19840.

Lemma collineation_19841 : is_collineation2 fp_19841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19841.

Lemma collineation_19842 : is_collineation2 fp_19842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19842.

Lemma collineation_19843 : is_collineation2 fp_19843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19843.

Lemma collineation_19844 : is_collineation2 fp_19844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19844.

Lemma collineation_19845 : is_collineation2 fp_19845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19845.

Lemma collineation_19846 : is_collineation2 fp_19846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19846.

Lemma collineation_19847 : is_collineation2 fp_19847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19847.

Lemma collineation_19848 : is_collineation2 fp_19848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19848.

Lemma collineation_19849 : is_collineation2 fp_19849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19849.

Lemma collineation_19850 : is_collineation2 fp_19850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19850.

Lemma collineation_19851 : is_collineation2 fp_19851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19851.

Lemma collineation_19852 : is_collineation2 fp_19852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19852.

Lemma collineation_19853 : is_collineation2 fp_19853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19853.

Lemma collineation_19854 : is_collineation2 fp_19854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19854.

Lemma collineation_19855 : is_collineation2 fp_19855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19855.

Lemma collineation_19856 : is_collineation2 fp_19856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19856.

Lemma collineation_19857 : is_collineation2 fp_19857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19857.

Lemma collineation_19858 : is_collineation2 fp_19858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19858.

Lemma collineation_19859 : is_collineation2 fp_19859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19859.

Lemma collineation_19860 : is_collineation2 fp_19860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19860.

Lemma collineation_19861 : is_collineation2 fp_19861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19861.

Lemma collineation_19862 : is_collineation2 fp_19862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19862.

Lemma collineation_19863 : is_collineation2 fp_19863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19863.

Lemma collineation_19864 : is_collineation2 fp_19864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19864.

Lemma collineation_19865 : is_collineation2 fp_19865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19865.

Lemma collineation_19866 : is_collineation2 fp_19866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19866.

Lemma collineation_19867 : is_collineation2 fp_19867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19867.

Lemma collineation_19868 : is_collineation2 fp_19868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19868.

Lemma collineation_19869 : is_collineation2 fp_19869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19869.

Lemma collineation_19870 : is_collineation2 fp_19870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19870.

Lemma collineation_19871 : is_collineation2 fp_19871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19871.

Lemma collineation_19872 : is_collineation2 fp_19872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19872.

Lemma collineation_19873 : is_collineation2 fp_19873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19873.

Lemma collineation_19874 : is_collineation2 fp_19874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19874.

Lemma collineation_19875 : is_collineation2 fp_19875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19875.

Lemma collineation_19876 : is_collineation2 fp_19876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19876.

Lemma collineation_19877 : is_collineation2 fp_19877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19877.

Lemma collineation_19878 : is_collineation2 fp_19878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19878.

Lemma collineation_19879 : is_collineation2 fp_19879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19879.

Lemma collineation_19880 : is_collineation2 fp_19880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19880.

Lemma collineation_19881 : is_collineation2 fp_19881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19881.

Lemma collineation_19882 : is_collineation2 fp_19882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19882.

Lemma collineation_19883 : is_collineation2 fp_19883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19883.

Lemma collineation_19884 : is_collineation2 fp_19884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19884.

Lemma collineation_19885 : is_collineation2 fp_19885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19885.

Lemma collineation_19886 : is_collineation2 fp_19886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19886.

Lemma collineation_19887 : is_collineation2 fp_19887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19887.

Lemma collineation_19888 : is_collineation2 fp_19888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19888.

Lemma collineation_19889 : is_collineation2 fp_19889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19889.

Lemma collineation_19890 : is_collineation2 fp_19890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19890.

Lemma collineation_19891 : is_collineation2 fp_19891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19891.

Lemma collineation_19892 : is_collineation2 fp_19892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19892.

Lemma collineation_19893 : is_collineation2 fp_19893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19893.

Lemma collineation_19894 : is_collineation2 fp_19894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19894.

Lemma collineation_19895 : is_collineation2 fp_19895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19895.

Lemma collineation_19896 : is_collineation2 fp_19896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19896.

Lemma collineation_19897 : is_collineation2 fp_19897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19897.

Lemma collineation_19898 : is_collineation2 fp_19898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19898.

Lemma collineation_19899 : is_collineation2 fp_19899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19899.

Lemma collineation_19900 : is_collineation2 fp_19900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19900.

Lemma collineation_19901 : is_collineation2 fp_19901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19901.

Lemma collineation_19902 : is_collineation2 fp_19902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19902.

Lemma collineation_19903 : is_collineation2 fp_19903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19903.

Lemma collineation_19904 : is_collineation2 fp_19904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19904.

Lemma collineation_19905 : is_collineation2 fp_19905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19905.

Lemma collineation_19906 : is_collineation2 fp_19906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19906.

Lemma collineation_19907 : is_collineation2 fp_19907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19907.

Lemma collineation_19908 : is_collineation2 fp_19908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19908.

Lemma collineation_19909 : is_collineation2 fp_19909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19909.

Lemma collineation_19910 : is_collineation2 fp_19910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19910.

Lemma collineation_19911 : is_collineation2 fp_19911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19911.

Lemma collineation_19912 : is_collineation2 fp_19912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19912.

Lemma collineation_19913 : is_collineation2 fp_19913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19913.

Lemma collineation_19914 : is_collineation2 fp_19914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19914.

Lemma collineation_19915 : is_collineation2 fp_19915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19915.

Lemma collineation_19916 : is_collineation2 fp_19916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19916.

Lemma collineation_19917 : is_collineation2 fp_19917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19917.

Lemma collineation_19918 : is_collineation2 fp_19918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19918.

Lemma collineation_19919 : is_collineation2 fp_19919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19919.

Lemma collineation_19920 : is_collineation2 fp_19920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19920.

Lemma collineation_19921 : is_collineation2 fp_19921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19921.

Lemma collineation_19922 : is_collineation2 fp_19922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19922.

Lemma collineation_19923 : is_collineation2 fp_19923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19923.

Lemma collineation_19924 : is_collineation2 fp_19924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19924.

Lemma collineation_19925 : is_collineation2 fp_19925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19925.

Lemma collineation_19926 : is_collineation2 fp_19926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19926.

Lemma collineation_19927 : is_collineation2 fp_19927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19927.

Lemma collineation_19928 : is_collineation2 fp_19928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19928.

Lemma collineation_19929 : is_collineation2 fp_19929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19929.

Lemma collineation_19930 : is_collineation2 fp_19930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19930.

Lemma collineation_19931 : is_collineation2 fp_19931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19931.

Lemma collineation_19932 : is_collineation2 fp_19932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19932.

Lemma collineation_19933 : is_collineation2 fp_19933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19933.

Lemma collineation_19934 : is_collineation2 fp_19934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19934.

Lemma collineation_19935 : is_collineation2 fp_19935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19935.

Lemma collineation_19936 : is_collineation2 fp_19936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19936.

Lemma collineation_19937 : is_collineation2 fp_19937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19937.

Lemma collineation_19938 : is_collineation2 fp_19938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19938.

Lemma collineation_19939 : is_collineation2 fp_19939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19939.

Lemma collineation_19940 : is_collineation2 fp_19940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19940.

Lemma collineation_19941 : is_collineation2 fp_19941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19941.

Lemma collineation_19942 : is_collineation2 fp_19942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19942.

Lemma collineation_19943 : is_collineation2 fp_19943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19943.

Lemma collineation_19944 : is_collineation2 fp_19944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19944.

Lemma collineation_19945 : is_collineation2 fp_19945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19945.

Lemma collineation_19946 : is_collineation2 fp_19946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19946.

Lemma collineation_19947 : is_collineation2 fp_19947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19947.

Lemma collineation_19948 : is_collineation2 fp_19948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19948.

Lemma collineation_19949 : is_collineation2 fp_19949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19949.

Lemma collineation_19950 : is_collineation2 fp_19950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19950.

Lemma collineation_19951 : is_collineation2 fp_19951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19951.

Lemma collineation_19952 : is_collineation2 fp_19952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19952.

Lemma collineation_19953 : is_collineation2 fp_19953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19953.

Lemma collineation_19954 : is_collineation2 fp_19954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19954.

Lemma collineation_19955 : is_collineation2 fp_19955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19955.

Lemma collineation_19956 : is_collineation2 fp_19956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19956.

Lemma collineation_19957 : is_collineation2 fp_19957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19957.

Lemma collineation_19958 : is_collineation2 fp_19958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19958.

Lemma collineation_19959 : is_collineation2 fp_19959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19959.

Lemma collineation_19960 : is_collineation2 fp_19960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19960.

Lemma collineation_19961 : is_collineation2 fp_19961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19961.

Lemma collineation_19962 : is_collineation2 fp_19962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19962.

Lemma collineation_19963 : is_collineation2 fp_19963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19963.

Lemma collineation_19964 : is_collineation2 fp_19964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19964.

Lemma collineation_19965 : is_collineation2 fp_19965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19965.

Lemma collineation_19966 : is_collineation2 fp_19966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19966.

Lemma collineation_19967 : is_collineation2 fp_19967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19967.

Lemma collineation_19968 : is_collineation2 fp_19968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19968.

Lemma collineation_19969 : is_collineation2 fp_19969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19969.

Lemma collineation_19970 : is_collineation2 fp_19970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19970.

Lemma collineation_19971 : is_collineation2 fp_19971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19971.

Lemma collineation_19972 : is_collineation2 fp_19972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19972.

Lemma collineation_19973 : is_collineation2 fp_19973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19973.

Lemma collineation_19974 : is_collineation2 fp_19974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19974.

Lemma collineation_19975 : is_collineation2 fp_19975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19975.

Lemma collineation_19976 : is_collineation2 fp_19976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19976.

Lemma collineation_19977 : is_collineation2 fp_19977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19977.

Lemma collineation_19978 : is_collineation2 fp_19978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19978.

Lemma collineation_19979 : is_collineation2 fp_19979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19979.

Lemma collineation_19980 : is_collineation2 fp_19980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19980.

Lemma collineation_19981 : is_collineation2 fp_19981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19981.

Lemma collineation_19982 : is_collineation2 fp_19982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19982.

Lemma collineation_19983 : is_collineation2 fp_19983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19983.

Lemma collineation_19984 : is_collineation2 fp_19984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19984.

Lemma collineation_19985 : is_collineation2 fp_19985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19985.

Lemma collineation_19986 : is_collineation2 fp_19986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19986.

Lemma collineation_19987 : is_collineation2 fp_19987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19987.

Lemma collineation_19988 : is_collineation2 fp_19988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19988.

Lemma collineation_19989 : is_collineation2 fp_19989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19989.

Lemma collineation_19990 : is_collineation2 fp_19990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19990.

Lemma collineation_19991 : is_collineation2 fp_19991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19991.

Lemma collineation_19992 : is_collineation2 fp_19992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19992.

Lemma collineation_19993 : is_collineation2 fp_19993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19993.

Lemma collineation_19994 : is_collineation2 fp_19994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19994.

Lemma collineation_19995 : is_collineation2 fp_19995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19995.

Lemma collineation_19996 : is_collineation2 fp_19996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19996.

Lemma collineation_19997 : is_collineation2 fp_19997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19997.

Lemma collineation_19998 : is_collineation2 fp_19998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19998.

Lemma collineation_19999 : is_collineation2 fp_19999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_19999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_19999.

Lemma collineation_20000 : is_collineation2 fp_20000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20000.

Lemma collineation_20001 : is_collineation2 fp_20001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20001.

Lemma collineation_20002 : is_collineation2 fp_20002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20002.

Lemma collineation_20003 : is_collineation2 fp_20003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20003.

Lemma collineation_20004 : is_collineation2 fp_20004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20004.

Lemma collineation_20005 : is_collineation2 fp_20005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20005.

Lemma collineation_20006 : is_collineation2 fp_20006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20006.

Lemma collineation_20007 : is_collineation2 fp_20007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20007.

Lemma collineation_20008 : is_collineation2 fp_20008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20008.

Lemma collineation_20009 : is_collineation2 fp_20009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20009.

Lemma collineation_20010 : is_collineation2 fp_20010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20010.

Lemma collineation_20011 : is_collineation2 fp_20011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20011.

Lemma collineation_20012 : is_collineation2 fp_20012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20012.

Lemma collineation_20013 : is_collineation2 fp_20013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20013.

Lemma collineation_20014 : is_collineation2 fp_20014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20014.

Lemma collineation_20015 : is_collineation2 fp_20015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20015.

Lemma collineation_20016 : is_collineation2 fp_20016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20016.

Lemma collineation_20017 : is_collineation2 fp_20017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20017.

Lemma collineation_20018 : is_collineation2 fp_20018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20018.

Lemma collineation_20019 : is_collineation2 fp_20019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20019.

Lemma collineation_20020 : is_collineation2 fp_20020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20020.

Lemma collineation_20021 : is_collineation2 fp_20021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20021.

Lemma collineation_20022 : is_collineation2 fp_20022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20022.

Lemma collineation_20023 : is_collineation2 fp_20023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20023.

Lemma collineation_20024 : is_collineation2 fp_20024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20024.

Lemma collineation_20025 : is_collineation2 fp_20025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20025.

Lemma collineation_20026 : is_collineation2 fp_20026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20026.

Lemma collineation_20027 : is_collineation2 fp_20027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20027.

Lemma collineation_20028 : is_collineation2 fp_20028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20028.

Lemma collineation_20029 : is_collineation2 fp_20029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20029.

Lemma collineation_20030 : is_collineation2 fp_20030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20030.

Lemma collineation_20031 : is_collineation2 fp_20031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20031.

Lemma collineation_20032 : is_collineation2 fp_20032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20032.

Lemma collineation_20033 : is_collineation2 fp_20033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20033.

Lemma collineation_20034 : is_collineation2 fp_20034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20034.

Lemma collineation_20035 : is_collineation2 fp_20035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20035.

Lemma collineation_20036 : is_collineation2 fp_20036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20036.

Lemma collineation_20037 : is_collineation2 fp_20037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20037.

Lemma collineation_20038 : is_collineation2 fp_20038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20038.

Lemma collineation_20039 : is_collineation2 fp_20039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20039.

Lemma collineation_20040 : is_collineation2 fp_20040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20040.

Lemma collineation_20041 : is_collineation2 fp_20041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20041.

Lemma collineation_20042 : is_collineation2 fp_20042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20042.

Lemma collineation_20043 : is_collineation2 fp_20043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20043.

Lemma collineation_20044 : is_collineation2 fp_20044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20044.

Lemma collineation_20045 : is_collineation2 fp_20045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20045.

Lemma collineation_20046 : is_collineation2 fp_20046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20046.

Lemma collineation_20047 : is_collineation2 fp_20047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20047.

Lemma collineation_20048 : is_collineation2 fp_20048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20048.

Lemma collineation_20049 : is_collineation2 fp_20049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20049.

Lemma collineation_20050 : is_collineation2 fp_20050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20050.

Lemma collineation_20051 : is_collineation2 fp_20051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20051.

Lemma collineation_20052 : is_collineation2 fp_20052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20052.

Lemma collineation_20053 : is_collineation2 fp_20053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20053.

Lemma collineation_20054 : is_collineation2 fp_20054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20054.

Lemma collineation_20055 : is_collineation2 fp_20055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20055.

Lemma collineation_20056 : is_collineation2 fp_20056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20056.

Lemma collineation_20057 : is_collineation2 fp_20057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20057.

Lemma collineation_20058 : is_collineation2 fp_20058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20058.

Lemma collineation_20059 : is_collineation2 fp_20059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20059.

Lemma collineation_20060 : is_collineation2 fp_20060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20060.

Lemma collineation_20061 : is_collineation2 fp_20061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20061.

Lemma collineation_20062 : is_collineation2 fp_20062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20062.

Lemma collineation_20063 : is_collineation2 fp_20063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20063.

Lemma collineation_20064 : is_collineation2 fp_20064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20064.

Lemma collineation_20065 : is_collineation2 fp_20065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20065.

Lemma collineation_20066 : is_collineation2 fp_20066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20066.

Lemma collineation_20067 : is_collineation2 fp_20067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20067.

Lemma collineation_20068 : is_collineation2 fp_20068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20068.

Lemma collineation_20069 : is_collineation2 fp_20069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20069.

Lemma collineation_20070 : is_collineation2 fp_20070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20070.

Lemma collineation_20071 : is_collineation2 fp_20071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20071.

Lemma collineation_20072 : is_collineation2 fp_20072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20072.

Lemma collineation_20073 : is_collineation2 fp_20073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20073.

Lemma collineation_20074 : is_collineation2 fp_20074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20074.

Lemma collineation_20075 : is_collineation2 fp_20075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20075.

Lemma collineation_20076 : is_collineation2 fp_20076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20076.

Lemma collineation_20077 : is_collineation2 fp_20077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20077.

Lemma collineation_20078 : is_collineation2 fp_20078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20078.

Lemma collineation_20079 : is_collineation2 fp_20079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20079.

Lemma collineation_20080 : is_collineation2 fp_20080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20080.

Lemma collineation_20081 : is_collineation2 fp_20081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20081.

Lemma collineation_20082 : is_collineation2 fp_20082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20082.

Lemma collineation_20083 : is_collineation2 fp_20083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20083.

Lemma collineation_20084 : is_collineation2 fp_20084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20084.

Lemma collineation_20085 : is_collineation2 fp_20085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20085.

Lemma collineation_20086 : is_collineation2 fp_20086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20086.

Lemma collineation_20087 : is_collineation2 fp_20087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20087.

Lemma collineation_20088 : is_collineation2 fp_20088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20088.

Lemma collineation_20089 : is_collineation2 fp_20089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20089.

Lemma collineation_20090 : is_collineation2 fp_20090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20090.

Lemma collineation_20091 : is_collineation2 fp_20091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20091.

Lemma collineation_20092 : is_collineation2 fp_20092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20092.

Lemma collineation_20093 : is_collineation2 fp_20093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20093.

Lemma collineation_20094 : is_collineation2 fp_20094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20094.

Lemma collineation_20095 : is_collineation2 fp_20095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20095.

Lemma collineation_20096 : is_collineation2 fp_20096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20096.

Lemma collineation_20097 : is_collineation2 fp_20097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20097.

Lemma collineation_20098 : is_collineation2 fp_20098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20098.

Lemma collineation_20099 : is_collineation2 fp_20099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20099.

Lemma collineation_20100 : is_collineation2 fp_20100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20100.

Lemma collineation_20101 : is_collineation2 fp_20101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20101.

Lemma collineation_20102 : is_collineation2 fp_20102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20102.

Lemma collineation_20103 : is_collineation2 fp_20103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20103.

Lemma collineation_20104 : is_collineation2 fp_20104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20104.

Lemma collineation_20105 : is_collineation2 fp_20105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20105.

Lemma collineation_20106 : is_collineation2 fp_20106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20106.

Lemma collineation_20107 : is_collineation2 fp_20107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20107.

Lemma collineation_20108 : is_collineation2 fp_20108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20108.

Lemma collineation_20109 : is_collineation2 fp_20109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20109.

Lemma collineation_20110 : is_collineation2 fp_20110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20110.

Lemma collineation_20111 : is_collineation2 fp_20111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20111.

Lemma collineation_20112 : is_collineation2 fp_20112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20112.

Lemma collineation_20113 : is_collineation2 fp_20113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20113.

Lemma collineation_20114 : is_collineation2 fp_20114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20114.

Lemma collineation_20115 : is_collineation2 fp_20115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20115.

Lemma collineation_20116 : is_collineation2 fp_20116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20116.

Lemma collineation_20117 : is_collineation2 fp_20117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20117.

Lemma collineation_20118 : is_collineation2 fp_20118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20118.

Lemma collineation_20119 : is_collineation2 fp_20119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20119.

Lemma collineation_20120 : is_collineation2 fp_20120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20120.

Lemma collineation_20121 : is_collineation2 fp_20121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20121.

Lemma collineation_20122 : is_collineation2 fp_20122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20122.

Lemma collineation_20123 : is_collineation2 fp_20123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20123.

Lemma collineation_20124 : is_collineation2 fp_20124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20124.

Lemma collineation_20125 : is_collineation2 fp_20125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20125.

Lemma collineation_20126 : is_collineation2 fp_20126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20126.

Lemma collineation_20127 : is_collineation2 fp_20127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20127.

Lemma collineation_20128 : is_collineation2 fp_20128.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20128 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20128.

Lemma collineation_20129 : is_collineation2 fp_20129.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20129 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20129.

Lemma collineation_20130 : is_collineation2 fp_20130.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20130 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20130.

Lemma collineation_20131 : is_collineation2 fp_20131.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20131 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20131.

Lemma collineation_20132 : is_collineation2 fp_20132.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20132 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20132.

Lemma collineation_20133 : is_collineation2 fp_20133.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20133 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20133.

Lemma collineation_20134 : is_collineation2 fp_20134.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20134 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20134.

Lemma collineation_20135 : is_collineation2 fp_20135.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20135 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20135.

Lemma collineation_20136 : is_collineation2 fp_20136.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20136 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20136.

Lemma collineation_20137 : is_collineation2 fp_20137.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20137 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20137.

Lemma collineation_20138 : is_collineation2 fp_20138.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20138 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20138.

Lemma collineation_20139 : is_collineation2 fp_20139.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20139 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20139.

Lemma collineation_20140 : is_collineation2 fp_20140.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20140 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20140.

Lemma collineation_20141 : is_collineation2 fp_20141.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20141 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20141.

Lemma collineation_20142 : is_collineation2 fp_20142.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20142 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20142.

Lemma collineation_20143 : is_collineation2 fp_20143.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20143 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20143.

Lemma collineation_20144 : is_collineation2 fp_20144.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20144 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20144.

Lemma collineation_20145 : is_collineation2 fp_20145.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20145 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20145.

Lemma collineation_20146 : is_collineation2 fp_20146.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20146 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20146.

Lemma collineation_20147 : is_collineation2 fp_20147.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20147 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20147.

Lemma collineation_20148 : is_collineation2 fp_20148.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20148 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20148.

Lemma collineation_20149 : is_collineation2 fp_20149.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20149 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20149.

Lemma collineation_20150 : is_collineation2 fp_20150.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20150 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20150.

Lemma collineation_20151 : is_collineation2 fp_20151.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20151 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20151.

Lemma collineation_20152 : is_collineation2 fp_20152.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20152 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20152.

Lemma collineation_20153 : is_collineation2 fp_20153.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20153 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20153.

Lemma collineation_20154 : is_collineation2 fp_20154.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20154 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20154.

Lemma collineation_20155 : is_collineation2 fp_20155.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20155 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20155.

Lemma collineation_20156 : is_collineation2 fp_20156.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20156 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20156.

Lemma collineation_20157 : is_collineation2 fp_20157.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20157 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20157.

Lemma collineation_20158 : is_collineation2 fp_20158.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20158 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20158.

Lemma collineation_20159 : is_collineation2 fp_20159.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_20159 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_20159.

Lemma is_col_all_c196 : forall fp, In fp (all_c196++all_c197++all_c198++all_c199++all_c200++all_c201++all_c202++all_c203++all_c204++all_c205++all_c206++all_c207++all_c208++all_c209) -> is_collineation2 fp.
Proof.
 intros fp HIn_S.
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19127 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19128 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19129 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19130 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19131 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19132 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19133 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19134 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19135 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19136 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19137 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19138 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19139 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19140 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19141 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19142 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19143 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19144 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19145 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19146 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19147 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19148 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19149 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19150 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19151 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19152 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19153 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19154 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19155 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19156 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19157 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19158 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19159 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19160 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19161 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19162 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19163 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19164 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19165 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19166 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19167 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19168 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19169 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19170 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19171 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19172 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19173 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19174 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19175 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19176 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19177 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19178 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19179 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19180 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19181 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19182 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19183 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19184 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19185 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19186 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19187 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19188 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19189 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19190 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19191 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19192 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19193 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19194 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19195 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19196 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19197 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19198 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19199 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19200 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19201 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19202 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19203 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19204 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19205 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19206 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19207 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19208 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19209 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19210 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19211 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19212 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19213 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19214 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19215 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19216 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19217 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19218 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19219 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19220 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19221 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19222 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19223 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19224 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19225 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19226 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19227 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19228 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19229 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19230 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19231 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19232 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19233 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19234 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19235 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19236 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19237 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19238 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19239 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19240 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19241 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19242 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19243 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19244 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19245 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19246 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19247 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19248 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19249 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19250 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19251 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19252 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19253 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19254 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19255 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19256 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19257 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19258 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19259 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19260 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19261 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19262 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19263 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19264 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19265 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19266 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19267 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19268 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19269 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19270 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19271 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19272 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19273 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19274 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19275 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19276 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19277 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19278 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19279 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19280 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19281 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19282 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19283 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19284 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19285 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19286 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19287 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19288 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19289 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19290 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19291 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19292 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19293 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19294 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19295 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19296 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19297 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19298 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19299 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19300 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19301 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19302 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19303 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19304 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19305 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19306 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19307 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19308 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19309 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19310 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19311 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19312 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19313 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19314 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19315 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19316 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19317 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19318 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19319 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19320 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19321 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19322 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19323 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19324 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19325 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19326 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19327 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19328 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19329 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19330 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19331 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19332 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19333 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19334 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19335 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19336 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19337 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19338 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19339 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19340 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19341 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19342 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19343 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19344 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19345 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19346 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19347 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19348 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19349 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19350 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19351 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19352 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19353 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19354 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19355 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19356 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19357 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19358 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19359 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19360 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19361 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19362 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19363 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19364 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19365 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19366 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19367 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19368 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19369 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19370 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19371 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19372 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19373 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19374 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19375 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19376 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19377 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19378 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19379 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19380 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19381 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19382 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19383 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19384 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19385 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19386 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19387 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19388 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19389 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19390 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19391 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19392 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19393 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19394 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19395 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19396 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19397 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19398 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19399 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19400 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19401 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19402 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19403 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19404 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19405 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19406 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19407 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19408 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19409 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19410 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19411 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19412 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19413 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19414 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19415 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19416 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19417 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19418 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19419 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19420 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19421 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19422 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19423 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19424 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19425 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19426 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19427 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19428 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19429 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19430 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19431 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19432 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19433 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19434 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19435 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19436 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19437 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19438 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19439 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19440 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19441 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19442 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19443 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19444 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19445 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19446 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19447 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19448 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19449 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19450 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19451 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19452 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19453 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19454 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19455 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19456 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19457 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19458 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19459 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19460 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19461 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19462 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19463 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19464 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19465 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19466 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19467 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19468 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19469 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19470 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19471 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19687 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19688 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19689 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19690 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19691 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19692 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19693 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19694 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19695 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19696 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19697 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19698 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19699 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19700 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19701 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19702 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19703 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19704 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19705 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19706 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19707 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19708 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19709 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19710 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19711 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19712 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19713 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19714 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19715 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19716 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19717 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19718 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19719 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19720 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19721 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19722 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19723 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19724 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19725 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19726 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19727 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19728 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19729 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19730 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19731 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19732 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19733 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19734 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19735 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19736 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19737 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19738 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19739 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19740 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19741 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19742 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19743 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19744 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19745 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19746 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19747 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19748 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19749 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19750 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19751 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19752 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19753 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19754 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19755 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19756 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19757 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19758 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19759 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19760 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19761 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19762 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19763 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19764 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19765 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19766 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19767 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19768 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19769 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19770 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19771 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19772 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19773 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19774 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19775 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19776 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19777 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19778 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19779 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19780 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19781 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19782 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19783 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19815 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_19999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20127 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20128 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20129 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20130 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20131 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20132 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20133 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20134 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20135 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20136 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20137 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20138 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20139 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20140 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20141 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20142 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20143 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20144 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20145 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20146 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20147 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20148 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20149 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20150 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20151 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20152 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20153 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20154 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20155 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20156 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20157 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20158 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_20159 | idtac].
 destruct (in_nil HIn_S).
Qed.

