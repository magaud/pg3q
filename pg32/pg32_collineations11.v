Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG32.pg32_inductive PG32.pg32_spreads_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_automorphisms_inv.
Require Import PG32.pg32_is_col_fp.

Require Import Lists.List.
Import ListNotations.
Require Import Arith.

Lemma collineation_14784 : is_collineation2 fp_14784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14784.

Lemma collineation_14785 : is_collineation2 fp_14785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14785.

Lemma collineation_14786 : is_collineation2 fp_14786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14786.

Lemma collineation_14787 : is_collineation2 fp_14787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14787.

Lemma collineation_14788 : is_collineation2 fp_14788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14788.

Lemma collineation_14789 : is_collineation2 fp_14789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14789.

Lemma collineation_14790 : is_collineation2 fp_14790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14790.

Lemma collineation_14791 : is_collineation2 fp_14791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14791.

Lemma collineation_14792 : is_collineation2 fp_14792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14792.

Lemma collineation_14793 : is_collineation2 fp_14793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14793.

Lemma collineation_14794 : is_collineation2 fp_14794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14794.

Lemma collineation_14795 : is_collineation2 fp_14795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14795.

Lemma collineation_14796 : is_collineation2 fp_14796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14796.

Lemma collineation_14797 : is_collineation2 fp_14797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14797.

Lemma collineation_14798 : is_collineation2 fp_14798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14798.

Lemma collineation_14799 : is_collineation2 fp_14799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14799.

Lemma collineation_14800 : is_collineation2 fp_14800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14800.

Lemma collineation_14801 : is_collineation2 fp_14801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14801.

Lemma collineation_14802 : is_collineation2 fp_14802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14802.

Lemma collineation_14803 : is_collineation2 fp_14803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14803.

Lemma collineation_14804 : is_collineation2 fp_14804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14804.

Lemma collineation_14805 : is_collineation2 fp_14805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14805.

Lemma collineation_14806 : is_collineation2 fp_14806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14806.

Lemma collineation_14807 : is_collineation2 fp_14807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14807.

Lemma collineation_14808 : is_collineation2 fp_14808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14808.

Lemma collineation_14809 : is_collineation2 fp_14809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14809.

Lemma collineation_14810 : is_collineation2 fp_14810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14810.

Lemma collineation_14811 : is_collineation2 fp_14811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14811.

Lemma collineation_14812 : is_collineation2 fp_14812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14812.

Lemma collineation_14813 : is_collineation2 fp_14813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14813.

Lemma collineation_14814 : is_collineation2 fp_14814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14814.

Lemma collineation_14815 : is_collineation2 fp_14815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14815.

Lemma collineation_14816 : is_collineation2 fp_14816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14816.

Lemma collineation_14817 : is_collineation2 fp_14817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14817.

Lemma collineation_14818 : is_collineation2 fp_14818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14818.

Lemma collineation_14819 : is_collineation2 fp_14819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14819.

Lemma collineation_14820 : is_collineation2 fp_14820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14820.

Lemma collineation_14821 : is_collineation2 fp_14821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14821.

Lemma collineation_14822 : is_collineation2 fp_14822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14822.

Lemma collineation_14823 : is_collineation2 fp_14823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14823.

Lemma collineation_14824 : is_collineation2 fp_14824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14824.

Lemma collineation_14825 : is_collineation2 fp_14825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14825.

Lemma collineation_14826 : is_collineation2 fp_14826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14826.

Lemma collineation_14827 : is_collineation2 fp_14827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14827.

Lemma collineation_14828 : is_collineation2 fp_14828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14828.

Lemma collineation_14829 : is_collineation2 fp_14829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14829.

Lemma collineation_14830 : is_collineation2 fp_14830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14830.

Lemma collineation_14831 : is_collineation2 fp_14831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14831.

Lemma collineation_14832 : is_collineation2 fp_14832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14832.

Lemma collineation_14833 : is_collineation2 fp_14833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14833.

Lemma collineation_14834 : is_collineation2 fp_14834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14834.

Lemma collineation_14835 : is_collineation2 fp_14835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14835.

Lemma collineation_14836 : is_collineation2 fp_14836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14836.

Lemma collineation_14837 : is_collineation2 fp_14837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14837.

Lemma collineation_14838 : is_collineation2 fp_14838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14838.

Lemma collineation_14839 : is_collineation2 fp_14839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14839.

Lemma collineation_14840 : is_collineation2 fp_14840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14840.

Lemma collineation_14841 : is_collineation2 fp_14841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14841.

Lemma collineation_14842 : is_collineation2 fp_14842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14842.

Lemma collineation_14843 : is_collineation2 fp_14843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14843.

Lemma collineation_14844 : is_collineation2 fp_14844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14844.

Lemma collineation_14845 : is_collineation2 fp_14845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14845.

Lemma collineation_14846 : is_collineation2 fp_14846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14846.

Lemma collineation_14847 : is_collineation2 fp_14847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14847.

Lemma collineation_14848 : is_collineation2 fp_14848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14848.

Lemma collineation_14849 : is_collineation2 fp_14849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14849.

Lemma collineation_14850 : is_collineation2 fp_14850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14850.

Lemma collineation_14851 : is_collineation2 fp_14851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14851.

Lemma collineation_14852 : is_collineation2 fp_14852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14852.

Lemma collineation_14853 : is_collineation2 fp_14853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14853.

Lemma collineation_14854 : is_collineation2 fp_14854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14854.

Lemma collineation_14855 : is_collineation2 fp_14855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14855.

Lemma collineation_14856 : is_collineation2 fp_14856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14856.

Lemma collineation_14857 : is_collineation2 fp_14857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14857.

Lemma collineation_14858 : is_collineation2 fp_14858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14858.

Lemma collineation_14859 : is_collineation2 fp_14859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14859.

Lemma collineation_14860 : is_collineation2 fp_14860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14860.

Lemma collineation_14861 : is_collineation2 fp_14861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14861.

Lemma collineation_14862 : is_collineation2 fp_14862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14862.

Lemma collineation_14863 : is_collineation2 fp_14863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14863.

Lemma collineation_14864 : is_collineation2 fp_14864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14864.

Lemma collineation_14865 : is_collineation2 fp_14865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14865.

Lemma collineation_14866 : is_collineation2 fp_14866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14866.

Lemma collineation_14867 : is_collineation2 fp_14867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14867.

Lemma collineation_14868 : is_collineation2 fp_14868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14868.

Lemma collineation_14869 : is_collineation2 fp_14869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14869.

Lemma collineation_14870 : is_collineation2 fp_14870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14870.

Lemma collineation_14871 : is_collineation2 fp_14871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14871.

Lemma collineation_14872 : is_collineation2 fp_14872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14872.

Lemma collineation_14873 : is_collineation2 fp_14873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14873.

Lemma collineation_14874 : is_collineation2 fp_14874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14874.

Lemma collineation_14875 : is_collineation2 fp_14875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14875.

Lemma collineation_14876 : is_collineation2 fp_14876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14876.

Lemma collineation_14877 : is_collineation2 fp_14877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14877.

Lemma collineation_14878 : is_collineation2 fp_14878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14878.

Lemma collineation_14879 : is_collineation2 fp_14879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14879.

Lemma collineation_14880 : is_collineation2 fp_14880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14880.

Lemma collineation_14881 : is_collineation2 fp_14881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14881.

Lemma collineation_14882 : is_collineation2 fp_14882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14882.

Lemma collineation_14883 : is_collineation2 fp_14883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14883.

Lemma collineation_14884 : is_collineation2 fp_14884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14884.

Lemma collineation_14885 : is_collineation2 fp_14885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14885.

Lemma collineation_14886 : is_collineation2 fp_14886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14886.

Lemma collineation_14887 : is_collineation2 fp_14887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14887.

Lemma collineation_14888 : is_collineation2 fp_14888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14888.

Lemma collineation_14889 : is_collineation2 fp_14889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14889.

Lemma collineation_14890 : is_collineation2 fp_14890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14890.

Lemma collineation_14891 : is_collineation2 fp_14891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14891.

Lemma collineation_14892 : is_collineation2 fp_14892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14892.

Lemma collineation_14893 : is_collineation2 fp_14893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14893.

Lemma collineation_14894 : is_collineation2 fp_14894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14894.

Lemma collineation_14895 : is_collineation2 fp_14895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14895.

Lemma collineation_14896 : is_collineation2 fp_14896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14896.

Lemma collineation_14897 : is_collineation2 fp_14897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14897.

Lemma collineation_14898 : is_collineation2 fp_14898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14898.

Lemma collineation_14899 : is_collineation2 fp_14899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14899.

Lemma collineation_14900 : is_collineation2 fp_14900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14900.

Lemma collineation_14901 : is_collineation2 fp_14901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14901.

Lemma collineation_14902 : is_collineation2 fp_14902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14902.

Lemma collineation_14903 : is_collineation2 fp_14903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14903.

Lemma collineation_14904 : is_collineation2 fp_14904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14904.

Lemma collineation_14905 : is_collineation2 fp_14905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14905.

Lemma collineation_14906 : is_collineation2 fp_14906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14906.

Lemma collineation_14907 : is_collineation2 fp_14907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14907.

Lemma collineation_14908 : is_collineation2 fp_14908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14908.

Lemma collineation_14909 : is_collineation2 fp_14909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14909.

Lemma collineation_14910 : is_collineation2 fp_14910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14910.

Lemma collineation_14911 : is_collineation2 fp_14911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14911.

Lemma collineation_14912 : is_collineation2 fp_14912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14912.

Lemma collineation_14913 : is_collineation2 fp_14913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14913.

Lemma collineation_14914 : is_collineation2 fp_14914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14914.

Lemma collineation_14915 : is_collineation2 fp_14915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14915.

Lemma collineation_14916 : is_collineation2 fp_14916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14916.

Lemma collineation_14917 : is_collineation2 fp_14917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14917.

Lemma collineation_14918 : is_collineation2 fp_14918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14918.

Lemma collineation_14919 : is_collineation2 fp_14919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14919.

Lemma collineation_14920 : is_collineation2 fp_14920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14920.

Lemma collineation_14921 : is_collineation2 fp_14921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14921.

Lemma collineation_14922 : is_collineation2 fp_14922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14922.

Lemma collineation_14923 : is_collineation2 fp_14923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14923.

Lemma collineation_14924 : is_collineation2 fp_14924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14924.

Lemma collineation_14925 : is_collineation2 fp_14925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14925.

Lemma collineation_14926 : is_collineation2 fp_14926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14926.

Lemma collineation_14927 : is_collineation2 fp_14927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14927.

Lemma collineation_14928 : is_collineation2 fp_14928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14928.

Lemma collineation_14929 : is_collineation2 fp_14929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14929.

Lemma collineation_14930 : is_collineation2 fp_14930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14930.

Lemma collineation_14931 : is_collineation2 fp_14931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14931.

Lemma collineation_14932 : is_collineation2 fp_14932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14932.

Lemma collineation_14933 : is_collineation2 fp_14933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14933.

Lemma collineation_14934 : is_collineation2 fp_14934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14934.

Lemma collineation_14935 : is_collineation2 fp_14935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14935.

Lemma collineation_14936 : is_collineation2 fp_14936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14936.

Lemma collineation_14937 : is_collineation2 fp_14937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14937.

Lemma collineation_14938 : is_collineation2 fp_14938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14938.

Lemma collineation_14939 : is_collineation2 fp_14939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14939.

Lemma collineation_14940 : is_collineation2 fp_14940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14940.

Lemma collineation_14941 : is_collineation2 fp_14941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14941.

Lemma collineation_14942 : is_collineation2 fp_14942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14942.

Lemma collineation_14943 : is_collineation2 fp_14943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14943.

Lemma collineation_14944 : is_collineation2 fp_14944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14944.

Lemma collineation_14945 : is_collineation2 fp_14945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14945.

Lemma collineation_14946 : is_collineation2 fp_14946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14946.

Lemma collineation_14947 : is_collineation2 fp_14947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14947.

Lemma collineation_14948 : is_collineation2 fp_14948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14948.

Lemma collineation_14949 : is_collineation2 fp_14949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14949.

Lemma collineation_14950 : is_collineation2 fp_14950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14950.

Lemma collineation_14951 : is_collineation2 fp_14951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14951.

Lemma collineation_14952 : is_collineation2 fp_14952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14952.

Lemma collineation_14953 : is_collineation2 fp_14953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14953.

Lemma collineation_14954 : is_collineation2 fp_14954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14954.

Lemma collineation_14955 : is_collineation2 fp_14955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14955.

Lemma collineation_14956 : is_collineation2 fp_14956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14956.

Lemma collineation_14957 : is_collineation2 fp_14957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14957.

Lemma collineation_14958 : is_collineation2 fp_14958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14958.

Lemma collineation_14959 : is_collineation2 fp_14959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14959.

Lemma collineation_14960 : is_collineation2 fp_14960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14960.

Lemma collineation_14961 : is_collineation2 fp_14961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14961.

Lemma collineation_14962 : is_collineation2 fp_14962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14962.

Lemma collineation_14963 : is_collineation2 fp_14963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14963.

Lemma collineation_14964 : is_collineation2 fp_14964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14964.

Lemma collineation_14965 : is_collineation2 fp_14965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14965.

Lemma collineation_14966 : is_collineation2 fp_14966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14966.

Lemma collineation_14967 : is_collineation2 fp_14967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14967.

Lemma collineation_14968 : is_collineation2 fp_14968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14968.

Lemma collineation_14969 : is_collineation2 fp_14969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14969.

Lemma collineation_14970 : is_collineation2 fp_14970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14970.

Lemma collineation_14971 : is_collineation2 fp_14971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14971.

Lemma collineation_14972 : is_collineation2 fp_14972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14972.

Lemma collineation_14973 : is_collineation2 fp_14973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14973.

Lemma collineation_14974 : is_collineation2 fp_14974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14974.

Lemma collineation_14975 : is_collineation2 fp_14975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14975.

Lemma collineation_14976 : is_collineation2 fp_14976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14976.

Lemma collineation_14977 : is_collineation2 fp_14977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14977.

Lemma collineation_14978 : is_collineation2 fp_14978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14978.

Lemma collineation_14979 : is_collineation2 fp_14979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14979.

Lemma collineation_14980 : is_collineation2 fp_14980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14980.

Lemma collineation_14981 : is_collineation2 fp_14981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14981.

Lemma collineation_14982 : is_collineation2 fp_14982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14982.

Lemma collineation_14983 : is_collineation2 fp_14983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14983.

Lemma collineation_14984 : is_collineation2 fp_14984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14984.

Lemma collineation_14985 : is_collineation2 fp_14985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14985.

Lemma collineation_14986 : is_collineation2 fp_14986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14986.

Lemma collineation_14987 : is_collineation2 fp_14987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14987.

Lemma collineation_14988 : is_collineation2 fp_14988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14988.

Lemma collineation_14989 : is_collineation2 fp_14989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14989.

Lemma collineation_14990 : is_collineation2 fp_14990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14990.

Lemma collineation_14991 : is_collineation2 fp_14991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14991.

Lemma collineation_14992 : is_collineation2 fp_14992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14992.

Lemma collineation_14993 : is_collineation2 fp_14993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14993.

Lemma collineation_14994 : is_collineation2 fp_14994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14994.

Lemma collineation_14995 : is_collineation2 fp_14995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14995.

Lemma collineation_14996 : is_collineation2 fp_14996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14996.

Lemma collineation_14997 : is_collineation2 fp_14997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14997.

Lemma collineation_14998 : is_collineation2 fp_14998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14998.

Lemma collineation_14999 : is_collineation2 fp_14999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_14999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_14999.

Lemma collineation_15000 : is_collineation2 fp_15000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15000.

Lemma collineation_15001 : is_collineation2 fp_15001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15001.

Lemma collineation_15002 : is_collineation2 fp_15002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15002.

Lemma collineation_15003 : is_collineation2 fp_15003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15003.

Lemma collineation_15004 : is_collineation2 fp_15004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15004.

Lemma collineation_15005 : is_collineation2 fp_15005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15005.

Lemma collineation_15006 : is_collineation2 fp_15006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15006.

Lemma collineation_15007 : is_collineation2 fp_15007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15007.

Lemma collineation_15008 : is_collineation2 fp_15008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15008.

Lemma collineation_15009 : is_collineation2 fp_15009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15009.

Lemma collineation_15010 : is_collineation2 fp_15010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15010.

Lemma collineation_15011 : is_collineation2 fp_15011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15011.

Lemma collineation_15012 : is_collineation2 fp_15012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15012.

Lemma collineation_15013 : is_collineation2 fp_15013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15013.

Lemma collineation_15014 : is_collineation2 fp_15014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15014.

Lemma collineation_15015 : is_collineation2 fp_15015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15015.

Lemma collineation_15016 : is_collineation2 fp_15016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15016.

Lemma collineation_15017 : is_collineation2 fp_15017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15017.

Lemma collineation_15018 : is_collineation2 fp_15018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15018.

Lemma collineation_15019 : is_collineation2 fp_15019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15019.

Lemma collineation_15020 : is_collineation2 fp_15020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15020.

Lemma collineation_15021 : is_collineation2 fp_15021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15021.

Lemma collineation_15022 : is_collineation2 fp_15022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15022.

Lemma collineation_15023 : is_collineation2 fp_15023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15023.

Lemma collineation_15024 : is_collineation2 fp_15024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15024.

Lemma collineation_15025 : is_collineation2 fp_15025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15025.

Lemma collineation_15026 : is_collineation2 fp_15026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15026.

Lemma collineation_15027 : is_collineation2 fp_15027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15027.

Lemma collineation_15028 : is_collineation2 fp_15028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15028.

Lemma collineation_15029 : is_collineation2 fp_15029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15029.

Lemma collineation_15030 : is_collineation2 fp_15030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15030.

Lemma collineation_15031 : is_collineation2 fp_15031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15031.

Lemma collineation_15032 : is_collineation2 fp_15032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15032.

Lemma collineation_15033 : is_collineation2 fp_15033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15033.

Lemma collineation_15034 : is_collineation2 fp_15034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15034.

Lemma collineation_15035 : is_collineation2 fp_15035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15035.

Lemma collineation_15036 : is_collineation2 fp_15036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15036.

Lemma collineation_15037 : is_collineation2 fp_15037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15037.

Lemma collineation_15038 : is_collineation2 fp_15038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15038.

Lemma collineation_15039 : is_collineation2 fp_15039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15039.

Lemma collineation_15040 : is_collineation2 fp_15040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15040.

Lemma collineation_15041 : is_collineation2 fp_15041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15041.

Lemma collineation_15042 : is_collineation2 fp_15042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15042.

Lemma collineation_15043 : is_collineation2 fp_15043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15043.

Lemma collineation_15044 : is_collineation2 fp_15044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15044.

Lemma collineation_15045 : is_collineation2 fp_15045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15045.

Lemma collineation_15046 : is_collineation2 fp_15046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15046.

Lemma collineation_15047 : is_collineation2 fp_15047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15047.

Lemma collineation_15048 : is_collineation2 fp_15048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15048.

Lemma collineation_15049 : is_collineation2 fp_15049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15049.

Lemma collineation_15050 : is_collineation2 fp_15050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15050.

Lemma collineation_15051 : is_collineation2 fp_15051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15051.

Lemma collineation_15052 : is_collineation2 fp_15052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15052.

Lemma collineation_15053 : is_collineation2 fp_15053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15053.

Lemma collineation_15054 : is_collineation2 fp_15054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15054.

Lemma collineation_15055 : is_collineation2 fp_15055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15055.

Lemma collineation_15056 : is_collineation2 fp_15056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15056.

Lemma collineation_15057 : is_collineation2 fp_15057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15057.

Lemma collineation_15058 : is_collineation2 fp_15058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15058.

Lemma collineation_15059 : is_collineation2 fp_15059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15059.

Lemma collineation_15060 : is_collineation2 fp_15060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15060.

Lemma collineation_15061 : is_collineation2 fp_15061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15061.

Lemma collineation_15062 : is_collineation2 fp_15062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15062.

Lemma collineation_15063 : is_collineation2 fp_15063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15063.

Lemma collineation_15064 : is_collineation2 fp_15064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15064.

Lemma collineation_15065 : is_collineation2 fp_15065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15065.

Lemma collineation_15066 : is_collineation2 fp_15066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15066.

Lemma collineation_15067 : is_collineation2 fp_15067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15067.

Lemma collineation_15068 : is_collineation2 fp_15068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15068.

Lemma collineation_15069 : is_collineation2 fp_15069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15069.

Lemma collineation_15070 : is_collineation2 fp_15070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15070.

Lemma collineation_15071 : is_collineation2 fp_15071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15071.

Lemma collineation_15072 : is_collineation2 fp_15072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15072.

Lemma collineation_15073 : is_collineation2 fp_15073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15073.

Lemma collineation_15074 : is_collineation2 fp_15074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15074.

Lemma collineation_15075 : is_collineation2 fp_15075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15075.

Lemma collineation_15076 : is_collineation2 fp_15076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15076.

Lemma collineation_15077 : is_collineation2 fp_15077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15077.

Lemma collineation_15078 : is_collineation2 fp_15078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15078.

Lemma collineation_15079 : is_collineation2 fp_15079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15079.

Lemma collineation_15080 : is_collineation2 fp_15080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15080.

Lemma collineation_15081 : is_collineation2 fp_15081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15081.

Lemma collineation_15082 : is_collineation2 fp_15082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15082.

Lemma collineation_15083 : is_collineation2 fp_15083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15083.

Lemma collineation_15084 : is_collineation2 fp_15084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15084.

Lemma collineation_15085 : is_collineation2 fp_15085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15085.

Lemma collineation_15086 : is_collineation2 fp_15086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15086.

Lemma collineation_15087 : is_collineation2 fp_15087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15087.

Lemma collineation_15088 : is_collineation2 fp_15088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15088.

Lemma collineation_15089 : is_collineation2 fp_15089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15089.

Lemma collineation_15090 : is_collineation2 fp_15090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15090.

Lemma collineation_15091 : is_collineation2 fp_15091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15091.

Lemma collineation_15092 : is_collineation2 fp_15092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15092.

Lemma collineation_15093 : is_collineation2 fp_15093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15093.

Lemma collineation_15094 : is_collineation2 fp_15094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15094.

Lemma collineation_15095 : is_collineation2 fp_15095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15095.

Lemma collineation_15096 : is_collineation2 fp_15096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15096.

Lemma collineation_15097 : is_collineation2 fp_15097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15097.

Lemma collineation_15098 : is_collineation2 fp_15098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15098.

Lemma collineation_15099 : is_collineation2 fp_15099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15099.

Lemma collineation_15100 : is_collineation2 fp_15100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15100.

Lemma collineation_15101 : is_collineation2 fp_15101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15101.

Lemma collineation_15102 : is_collineation2 fp_15102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15102.

Lemma collineation_15103 : is_collineation2 fp_15103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15103.

Lemma collineation_15104 : is_collineation2 fp_15104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15104.

Lemma collineation_15105 : is_collineation2 fp_15105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15105.

Lemma collineation_15106 : is_collineation2 fp_15106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15106.

Lemma collineation_15107 : is_collineation2 fp_15107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15107.

Lemma collineation_15108 : is_collineation2 fp_15108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15108.

Lemma collineation_15109 : is_collineation2 fp_15109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15109.

Lemma collineation_15110 : is_collineation2 fp_15110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15110.

Lemma collineation_15111 : is_collineation2 fp_15111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15111.

Lemma collineation_15112 : is_collineation2 fp_15112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15112.

Lemma collineation_15113 : is_collineation2 fp_15113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15113.

Lemma collineation_15114 : is_collineation2 fp_15114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15114.

Lemma collineation_15115 : is_collineation2 fp_15115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15115.

Lemma collineation_15116 : is_collineation2 fp_15116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15116.

Lemma collineation_15117 : is_collineation2 fp_15117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15117.

Lemma collineation_15118 : is_collineation2 fp_15118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15118.

Lemma collineation_15119 : is_collineation2 fp_15119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15119.

Lemma collineation_15120 : is_collineation2 fp_15120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15120.

Lemma collineation_15121 : is_collineation2 fp_15121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15121.

Lemma collineation_15122 : is_collineation2 fp_15122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15122.

Lemma collineation_15123 : is_collineation2 fp_15123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15123.

Lemma collineation_15124 : is_collineation2 fp_15124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15124.

Lemma collineation_15125 : is_collineation2 fp_15125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15125.

Lemma collineation_15126 : is_collineation2 fp_15126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15126.

Lemma collineation_15127 : is_collineation2 fp_15127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15127.

Lemma collineation_15128 : is_collineation2 fp_15128.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15128 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15128.

Lemma collineation_15129 : is_collineation2 fp_15129.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15129 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15129.

Lemma collineation_15130 : is_collineation2 fp_15130.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15130 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15130.

Lemma collineation_15131 : is_collineation2 fp_15131.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15131 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15131.

Lemma collineation_15132 : is_collineation2 fp_15132.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15132 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15132.

Lemma collineation_15133 : is_collineation2 fp_15133.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15133 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15133.

Lemma collineation_15134 : is_collineation2 fp_15134.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15134 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15134.

Lemma collineation_15135 : is_collineation2 fp_15135.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15135 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15135.

Lemma collineation_15136 : is_collineation2 fp_15136.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15136 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15136.

Lemma collineation_15137 : is_collineation2 fp_15137.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15137 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15137.

Lemma collineation_15138 : is_collineation2 fp_15138.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15138 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15138.

Lemma collineation_15139 : is_collineation2 fp_15139.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15139 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15139.

Lemma collineation_15140 : is_collineation2 fp_15140.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15140 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15140.

Lemma collineation_15141 : is_collineation2 fp_15141.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15141 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15141.

Lemma collineation_15142 : is_collineation2 fp_15142.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15142 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15142.

Lemma collineation_15143 : is_collineation2 fp_15143.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15143 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15143.

Lemma collineation_15144 : is_collineation2 fp_15144.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15144 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15144.

Lemma collineation_15145 : is_collineation2 fp_15145.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15145 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15145.

Lemma collineation_15146 : is_collineation2 fp_15146.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15146 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15146.

Lemma collineation_15147 : is_collineation2 fp_15147.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15147 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15147.

Lemma collineation_15148 : is_collineation2 fp_15148.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15148 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15148.

Lemma collineation_15149 : is_collineation2 fp_15149.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15149 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15149.

Lemma collineation_15150 : is_collineation2 fp_15150.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15150 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15150.

Lemma collineation_15151 : is_collineation2 fp_15151.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15151 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15151.

Lemma collineation_15152 : is_collineation2 fp_15152.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15152 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15152.

Lemma collineation_15153 : is_collineation2 fp_15153.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15153 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15153.

Lemma collineation_15154 : is_collineation2 fp_15154.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15154 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15154.

Lemma collineation_15155 : is_collineation2 fp_15155.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15155 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15155.

Lemma collineation_15156 : is_collineation2 fp_15156.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15156 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15156.

Lemma collineation_15157 : is_collineation2 fp_15157.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15157 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15157.

Lemma collineation_15158 : is_collineation2 fp_15158.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15158 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15158.

Lemma collineation_15159 : is_collineation2 fp_15159.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15159 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15159.

Lemma collineation_15160 : is_collineation2 fp_15160.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15160 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15160.

Lemma collineation_15161 : is_collineation2 fp_15161.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15161 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15161.

Lemma collineation_15162 : is_collineation2 fp_15162.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15162 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15162.

Lemma collineation_15163 : is_collineation2 fp_15163.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15163 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15163.

Lemma collineation_15164 : is_collineation2 fp_15164.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15164 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15164.

Lemma collineation_15165 : is_collineation2 fp_15165.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15165 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15165.

Lemma collineation_15166 : is_collineation2 fp_15166.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15166 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15166.

Lemma collineation_15167 : is_collineation2 fp_15167.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15167 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15167.

Lemma collineation_15168 : is_collineation2 fp_15168.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15168 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15168.

Lemma collineation_15169 : is_collineation2 fp_15169.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15169 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15169.

Lemma collineation_15170 : is_collineation2 fp_15170.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15170 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15170.

Lemma collineation_15171 : is_collineation2 fp_15171.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15171 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15171.

Lemma collineation_15172 : is_collineation2 fp_15172.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15172 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15172.

Lemma collineation_15173 : is_collineation2 fp_15173.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15173 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15173.

Lemma collineation_15174 : is_collineation2 fp_15174.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15174 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15174.

Lemma collineation_15175 : is_collineation2 fp_15175.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15175 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15175.

Lemma collineation_15176 : is_collineation2 fp_15176.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15176 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15176.

Lemma collineation_15177 : is_collineation2 fp_15177.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15177 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15177.

Lemma collineation_15178 : is_collineation2 fp_15178.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15178 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15178.

Lemma collineation_15179 : is_collineation2 fp_15179.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15179 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15179.

Lemma collineation_15180 : is_collineation2 fp_15180.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15180 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15180.

Lemma collineation_15181 : is_collineation2 fp_15181.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15181 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15181.

Lemma collineation_15182 : is_collineation2 fp_15182.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15182 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15182.

Lemma collineation_15183 : is_collineation2 fp_15183.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15183 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15183.

Lemma collineation_15184 : is_collineation2 fp_15184.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15184 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15184.

Lemma collineation_15185 : is_collineation2 fp_15185.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15185 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15185.

Lemma collineation_15186 : is_collineation2 fp_15186.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15186 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15186.

Lemma collineation_15187 : is_collineation2 fp_15187.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15187 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15187.

Lemma collineation_15188 : is_collineation2 fp_15188.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15188 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15188.

Lemma collineation_15189 : is_collineation2 fp_15189.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15189 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15189.

Lemma collineation_15190 : is_collineation2 fp_15190.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15190 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15190.

Lemma collineation_15191 : is_collineation2 fp_15191.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15191 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15191.

Lemma collineation_15192 : is_collineation2 fp_15192.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15192 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15192.

Lemma collineation_15193 : is_collineation2 fp_15193.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15193 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15193.

Lemma collineation_15194 : is_collineation2 fp_15194.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15194 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15194.

Lemma collineation_15195 : is_collineation2 fp_15195.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15195 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15195.

Lemma collineation_15196 : is_collineation2 fp_15196.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15196 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15196.

Lemma collineation_15197 : is_collineation2 fp_15197.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15197 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15197.

Lemma collineation_15198 : is_collineation2 fp_15198.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15198 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15198.

Lemma collineation_15199 : is_collineation2 fp_15199.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15199 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15199.

Lemma collineation_15200 : is_collineation2 fp_15200.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15200 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15200.

Lemma collineation_15201 : is_collineation2 fp_15201.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15201 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15201.

Lemma collineation_15202 : is_collineation2 fp_15202.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15202 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15202.

Lemma collineation_15203 : is_collineation2 fp_15203.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15203 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15203.

Lemma collineation_15204 : is_collineation2 fp_15204.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15204 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15204.

Lemma collineation_15205 : is_collineation2 fp_15205.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15205 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15205.

Lemma collineation_15206 : is_collineation2 fp_15206.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15206 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15206.

Lemma collineation_15207 : is_collineation2 fp_15207.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15207 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15207.

Lemma collineation_15208 : is_collineation2 fp_15208.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15208 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15208.

Lemma collineation_15209 : is_collineation2 fp_15209.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15209 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15209.

Lemma collineation_15210 : is_collineation2 fp_15210.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15210 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15210.

Lemma collineation_15211 : is_collineation2 fp_15211.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15211 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15211.

Lemma collineation_15212 : is_collineation2 fp_15212.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15212 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15212.

Lemma collineation_15213 : is_collineation2 fp_15213.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15213 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15213.

Lemma collineation_15214 : is_collineation2 fp_15214.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15214 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15214.

Lemma collineation_15215 : is_collineation2 fp_15215.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15215 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15215.

Lemma collineation_15216 : is_collineation2 fp_15216.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15216 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15216.

Lemma collineation_15217 : is_collineation2 fp_15217.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15217 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15217.

Lemma collineation_15218 : is_collineation2 fp_15218.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15218 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15218.

Lemma collineation_15219 : is_collineation2 fp_15219.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15219 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15219.

Lemma collineation_15220 : is_collineation2 fp_15220.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15220 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15220.

Lemma collineation_15221 : is_collineation2 fp_15221.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15221 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15221.

Lemma collineation_15222 : is_collineation2 fp_15222.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15222 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15222.

Lemma collineation_15223 : is_collineation2 fp_15223.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15223 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15223.

Lemma collineation_15224 : is_collineation2 fp_15224.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15224 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15224.

Lemma collineation_15225 : is_collineation2 fp_15225.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15225 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15225.

Lemma collineation_15226 : is_collineation2 fp_15226.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15226 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15226.

Lemma collineation_15227 : is_collineation2 fp_15227.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15227 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15227.

Lemma collineation_15228 : is_collineation2 fp_15228.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15228 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15228.

Lemma collineation_15229 : is_collineation2 fp_15229.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15229 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15229.

Lemma collineation_15230 : is_collineation2 fp_15230.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15230 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15230.

Lemma collineation_15231 : is_collineation2 fp_15231.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15231 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15231.

Lemma collineation_15232 : is_collineation2 fp_15232.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15232 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15232.

Lemma collineation_15233 : is_collineation2 fp_15233.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15233 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15233.

Lemma collineation_15234 : is_collineation2 fp_15234.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15234 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15234.

Lemma collineation_15235 : is_collineation2 fp_15235.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15235 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15235.

Lemma collineation_15236 : is_collineation2 fp_15236.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15236 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15236.

Lemma collineation_15237 : is_collineation2 fp_15237.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15237 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15237.

Lemma collineation_15238 : is_collineation2 fp_15238.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15238 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15238.

Lemma collineation_15239 : is_collineation2 fp_15239.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15239 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15239.

Lemma collineation_15240 : is_collineation2 fp_15240.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15240 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15240.

Lemma collineation_15241 : is_collineation2 fp_15241.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15241 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15241.

Lemma collineation_15242 : is_collineation2 fp_15242.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15242 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15242.

Lemma collineation_15243 : is_collineation2 fp_15243.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15243 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15243.

Lemma collineation_15244 : is_collineation2 fp_15244.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15244 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15244.

Lemma collineation_15245 : is_collineation2 fp_15245.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15245 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15245.

Lemma collineation_15246 : is_collineation2 fp_15246.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15246 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15246.

Lemma collineation_15247 : is_collineation2 fp_15247.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15247 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15247.

Lemma collineation_15248 : is_collineation2 fp_15248.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15248 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15248.

Lemma collineation_15249 : is_collineation2 fp_15249.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15249 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15249.

Lemma collineation_15250 : is_collineation2 fp_15250.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15250 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15250.

Lemma collineation_15251 : is_collineation2 fp_15251.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15251 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15251.

Lemma collineation_15252 : is_collineation2 fp_15252.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15252 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15252.

Lemma collineation_15253 : is_collineation2 fp_15253.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15253 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15253.

Lemma collineation_15254 : is_collineation2 fp_15254.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15254 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15254.

Lemma collineation_15255 : is_collineation2 fp_15255.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15255 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15255.

Lemma collineation_15256 : is_collineation2 fp_15256.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15256 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15256.

Lemma collineation_15257 : is_collineation2 fp_15257.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15257 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15257.

Lemma collineation_15258 : is_collineation2 fp_15258.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15258 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15258.

Lemma collineation_15259 : is_collineation2 fp_15259.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15259 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15259.

Lemma collineation_15260 : is_collineation2 fp_15260.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15260 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15260.

Lemma collineation_15261 : is_collineation2 fp_15261.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15261 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15261.

Lemma collineation_15262 : is_collineation2 fp_15262.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15262 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15262.

Lemma collineation_15263 : is_collineation2 fp_15263.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15263 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15263.

Lemma collineation_15264 : is_collineation2 fp_15264.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15264 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15264.

Lemma collineation_15265 : is_collineation2 fp_15265.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15265 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15265.

Lemma collineation_15266 : is_collineation2 fp_15266.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15266 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15266.

Lemma collineation_15267 : is_collineation2 fp_15267.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15267 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15267.

Lemma collineation_15268 : is_collineation2 fp_15268.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15268 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15268.

Lemma collineation_15269 : is_collineation2 fp_15269.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15269 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15269.

Lemma collineation_15270 : is_collineation2 fp_15270.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15270 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15270.

Lemma collineation_15271 : is_collineation2 fp_15271.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15271 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15271.

Lemma collineation_15272 : is_collineation2 fp_15272.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15272 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15272.

Lemma collineation_15273 : is_collineation2 fp_15273.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15273 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15273.

Lemma collineation_15274 : is_collineation2 fp_15274.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15274 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15274.

Lemma collineation_15275 : is_collineation2 fp_15275.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15275 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15275.

Lemma collineation_15276 : is_collineation2 fp_15276.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15276 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15276.

Lemma collineation_15277 : is_collineation2 fp_15277.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15277 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15277.

Lemma collineation_15278 : is_collineation2 fp_15278.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15278 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15278.

Lemma collineation_15279 : is_collineation2 fp_15279.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15279 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15279.

Lemma collineation_15280 : is_collineation2 fp_15280.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15280 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15280.

Lemma collineation_15281 : is_collineation2 fp_15281.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15281 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15281.

Lemma collineation_15282 : is_collineation2 fp_15282.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15282 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15282.

Lemma collineation_15283 : is_collineation2 fp_15283.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15283 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15283.

Lemma collineation_15284 : is_collineation2 fp_15284.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15284 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15284.

Lemma collineation_15285 : is_collineation2 fp_15285.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15285 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15285.

Lemma collineation_15286 : is_collineation2 fp_15286.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15286 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15286.

Lemma collineation_15287 : is_collineation2 fp_15287.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15287 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15287.

Lemma collineation_15288 : is_collineation2 fp_15288.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15288 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15288.

Lemma collineation_15289 : is_collineation2 fp_15289.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15289 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15289.

Lemma collineation_15290 : is_collineation2 fp_15290.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15290 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15290.

Lemma collineation_15291 : is_collineation2 fp_15291.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15291 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15291.

Lemma collineation_15292 : is_collineation2 fp_15292.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15292 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15292.

Lemma collineation_15293 : is_collineation2 fp_15293.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15293 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15293.

Lemma collineation_15294 : is_collineation2 fp_15294.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15294 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15294.

Lemma collineation_15295 : is_collineation2 fp_15295.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15295 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15295.

Lemma collineation_15296 : is_collineation2 fp_15296.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15296 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15296.

Lemma collineation_15297 : is_collineation2 fp_15297.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15297 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15297.

Lemma collineation_15298 : is_collineation2 fp_15298.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15298 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15298.

Lemma collineation_15299 : is_collineation2 fp_15299.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15299 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15299.

Lemma collineation_15300 : is_collineation2 fp_15300.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15300 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15300.

Lemma collineation_15301 : is_collineation2 fp_15301.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15301 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15301.

Lemma collineation_15302 : is_collineation2 fp_15302.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15302 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15302.

Lemma collineation_15303 : is_collineation2 fp_15303.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15303 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15303.

Lemma collineation_15304 : is_collineation2 fp_15304.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15304 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15304.

Lemma collineation_15305 : is_collineation2 fp_15305.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15305 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15305.

Lemma collineation_15306 : is_collineation2 fp_15306.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15306 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15306.

Lemma collineation_15307 : is_collineation2 fp_15307.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15307 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15307.

Lemma collineation_15308 : is_collineation2 fp_15308.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15308 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15308.

Lemma collineation_15309 : is_collineation2 fp_15309.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15309 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15309.

Lemma collineation_15310 : is_collineation2 fp_15310.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15310 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15310.

Lemma collineation_15311 : is_collineation2 fp_15311.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15311 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15311.

Lemma collineation_15312 : is_collineation2 fp_15312.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15312 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15312.

Lemma collineation_15313 : is_collineation2 fp_15313.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15313 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15313.

Lemma collineation_15314 : is_collineation2 fp_15314.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15314 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15314.

Lemma collineation_15315 : is_collineation2 fp_15315.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15315 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15315.

Lemma collineation_15316 : is_collineation2 fp_15316.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15316 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15316.

Lemma collineation_15317 : is_collineation2 fp_15317.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15317 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15317.

Lemma collineation_15318 : is_collineation2 fp_15318.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15318 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15318.

Lemma collineation_15319 : is_collineation2 fp_15319.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15319 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15319.

Lemma collineation_15320 : is_collineation2 fp_15320.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15320 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15320.

Lemma collineation_15321 : is_collineation2 fp_15321.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15321 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15321.

Lemma collineation_15322 : is_collineation2 fp_15322.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15322 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15322.

Lemma collineation_15323 : is_collineation2 fp_15323.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15323 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15323.

Lemma collineation_15324 : is_collineation2 fp_15324.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15324 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15324.

Lemma collineation_15325 : is_collineation2 fp_15325.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15325 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15325.

Lemma collineation_15326 : is_collineation2 fp_15326.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15326 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15326.

Lemma collineation_15327 : is_collineation2 fp_15327.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15327 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15327.

Lemma collineation_15328 : is_collineation2 fp_15328.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15328 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15328.

Lemma collineation_15329 : is_collineation2 fp_15329.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15329 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15329.

Lemma collineation_15330 : is_collineation2 fp_15330.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15330 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15330.

Lemma collineation_15331 : is_collineation2 fp_15331.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15331 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15331.

Lemma collineation_15332 : is_collineation2 fp_15332.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15332 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15332.

Lemma collineation_15333 : is_collineation2 fp_15333.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15333 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15333.

Lemma collineation_15334 : is_collineation2 fp_15334.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15334 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15334.

Lemma collineation_15335 : is_collineation2 fp_15335.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15335 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15335.

Lemma collineation_15336 : is_collineation2 fp_15336.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15336 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15336.

Lemma collineation_15337 : is_collineation2 fp_15337.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15337 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15337.

Lemma collineation_15338 : is_collineation2 fp_15338.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15338 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15338.

Lemma collineation_15339 : is_collineation2 fp_15339.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15339 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15339.

Lemma collineation_15340 : is_collineation2 fp_15340.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15340 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15340.

Lemma collineation_15341 : is_collineation2 fp_15341.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15341 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15341.

Lemma collineation_15342 : is_collineation2 fp_15342.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15342 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15342.

Lemma collineation_15343 : is_collineation2 fp_15343.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15343 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15343.

Lemma collineation_15344 : is_collineation2 fp_15344.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15344 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15344.

Lemma collineation_15345 : is_collineation2 fp_15345.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15345 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15345.

Lemma collineation_15346 : is_collineation2 fp_15346.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15346 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15346.

Lemma collineation_15347 : is_collineation2 fp_15347.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15347 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15347.

Lemma collineation_15348 : is_collineation2 fp_15348.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15348 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15348.

Lemma collineation_15349 : is_collineation2 fp_15349.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15349 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15349.

Lemma collineation_15350 : is_collineation2 fp_15350.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15350 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15350.

Lemma collineation_15351 : is_collineation2 fp_15351.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15351 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15351.

Lemma collineation_15352 : is_collineation2 fp_15352.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15352 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15352.

Lemma collineation_15353 : is_collineation2 fp_15353.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15353 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15353.

Lemma collineation_15354 : is_collineation2 fp_15354.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15354 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15354.

Lemma collineation_15355 : is_collineation2 fp_15355.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15355 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15355.

Lemma collineation_15356 : is_collineation2 fp_15356.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15356 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15356.

Lemma collineation_15357 : is_collineation2 fp_15357.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15357 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15357.

Lemma collineation_15358 : is_collineation2 fp_15358.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15358 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15358.

Lemma collineation_15359 : is_collineation2 fp_15359.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15359 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15359.

Lemma collineation_15360 : is_collineation2 fp_15360.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15360 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15360.

Lemma collineation_15361 : is_collineation2 fp_15361.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15361 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15361.

Lemma collineation_15362 : is_collineation2 fp_15362.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15362 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15362.

Lemma collineation_15363 : is_collineation2 fp_15363.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15363 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15363.

Lemma collineation_15364 : is_collineation2 fp_15364.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15364 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15364.

Lemma collineation_15365 : is_collineation2 fp_15365.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15365 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15365.

Lemma collineation_15366 : is_collineation2 fp_15366.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15366 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15366.

Lemma collineation_15367 : is_collineation2 fp_15367.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15367 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15367.

Lemma collineation_15368 : is_collineation2 fp_15368.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15368 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15368.

Lemma collineation_15369 : is_collineation2 fp_15369.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15369 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15369.

Lemma collineation_15370 : is_collineation2 fp_15370.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15370 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15370.

Lemma collineation_15371 : is_collineation2 fp_15371.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15371 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15371.

Lemma collineation_15372 : is_collineation2 fp_15372.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15372 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15372.

Lemma collineation_15373 : is_collineation2 fp_15373.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15373 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15373.

Lemma collineation_15374 : is_collineation2 fp_15374.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15374 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15374.

Lemma collineation_15375 : is_collineation2 fp_15375.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15375 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15375.

Lemma collineation_15376 : is_collineation2 fp_15376.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15376 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15376.

Lemma collineation_15377 : is_collineation2 fp_15377.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15377 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15377.

Lemma collineation_15378 : is_collineation2 fp_15378.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15378 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15378.

Lemma collineation_15379 : is_collineation2 fp_15379.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15379 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15379.

Lemma collineation_15380 : is_collineation2 fp_15380.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15380 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15380.

Lemma collineation_15381 : is_collineation2 fp_15381.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15381 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15381.

Lemma collineation_15382 : is_collineation2 fp_15382.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15382 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15382.

Lemma collineation_15383 : is_collineation2 fp_15383.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15383 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15383.

Lemma collineation_15384 : is_collineation2 fp_15384.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15384 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15384.

Lemma collineation_15385 : is_collineation2 fp_15385.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15385 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15385.

Lemma collineation_15386 : is_collineation2 fp_15386.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15386 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15386.

Lemma collineation_15387 : is_collineation2 fp_15387.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15387 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15387.

Lemma collineation_15388 : is_collineation2 fp_15388.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15388 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15388.

Lemma collineation_15389 : is_collineation2 fp_15389.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15389 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15389.

Lemma collineation_15390 : is_collineation2 fp_15390.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15390 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15390.

Lemma collineation_15391 : is_collineation2 fp_15391.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15391 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15391.

Lemma collineation_15392 : is_collineation2 fp_15392.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15392 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15392.

Lemma collineation_15393 : is_collineation2 fp_15393.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15393 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15393.

Lemma collineation_15394 : is_collineation2 fp_15394.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15394 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15394.

Lemma collineation_15395 : is_collineation2 fp_15395.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15395 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15395.

Lemma collineation_15396 : is_collineation2 fp_15396.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15396 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15396.

Lemma collineation_15397 : is_collineation2 fp_15397.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15397 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15397.

Lemma collineation_15398 : is_collineation2 fp_15398.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15398 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15398.

Lemma collineation_15399 : is_collineation2 fp_15399.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15399 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15399.

Lemma collineation_15400 : is_collineation2 fp_15400.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15400 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15400.

Lemma collineation_15401 : is_collineation2 fp_15401.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15401 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15401.

Lemma collineation_15402 : is_collineation2 fp_15402.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15402 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15402.

Lemma collineation_15403 : is_collineation2 fp_15403.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15403 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15403.

Lemma collineation_15404 : is_collineation2 fp_15404.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15404 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15404.

Lemma collineation_15405 : is_collineation2 fp_15405.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15405 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15405.

Lemma collineation_15406 : is_collineation2 fp_15406.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15406 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15406.

Lemma collineation_15407 : is_collineation2 fp_15407.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15407 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15407.

Lemma collineation_15408 : is_collineation2 fp_15408.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15408 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15408.

Lemma collineation_15409 : is_collineation2 fp_15409.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15409 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15409.

Lemma collineation_15410 : is_collineation2 fp_15410.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15410 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15410.

Lemma collineation_15411 : is_collineation2 fp_15411.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15411 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15411.

Lemma collineation_15412 : is_collineation2 fp_15412.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15412 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15412.

Lemma collineation_15413 : is_collineation2 fp_15413.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15413 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15413.

Lemma collineation_15414 : is_collineation2 fp_15414.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15414 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15414.

Lemma collineation_15415 : is_collineation2 fp_15415.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15415 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15415.

Lemma collineation_15416 : is_collineation2 fp_15416.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15416 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15416.

Lemma collineation_15417 : is_collineation2 fp_15417.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15417 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15417.

Lemma collineation_15418 : is_collineation2 fp_15418.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15418 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15418.

Lemma collineation_15419 : is_collineation2 fp_15419.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15419 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15419.

Lemma collineation_15420 : is_collineation2 fp_15420.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15420 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15420.

Lemma collineation_15421 : is_collineation2 fp_15421.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15421 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15421.

Lemma collineation_15422 : is_collineation2 fp_15422.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15422 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15422.

Lemma collineation_15423 : is_collineation2 fp_15423.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15423 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15423.

Lemma collineation_15424 : is_collineation2 fp_15424.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15424 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15424.

Lemma collineation_15425 : is_collineation2 fp_15425.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15425 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15425.

Lemma collineation_15426 : is_collineation2 fp_15426.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15426 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15426.

Lemma collineation_15427 : is_collineation2 fp_15427.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15427 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15427.

Lemma collineation_15428 : is_collineation2 fp_15428.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15428 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15428.

Lemma collineation_15429 : is_collineation2 fp_15429.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15429 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15429.

Lemma collineation_15430 : is_collineation2 fp_15430.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15430 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15430.

Lemma collineation_15431 : is_collineation2 fp_15431.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15431 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15431.

Lemma collineation_15432 : is_collineation2 fp_15432.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15432 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15432.

Lemma collineation_15433 : is_collineation2 fp_15433.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15433 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15433.

Lemma collineation_15434 : is_collineation2 fp_15434.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15434 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15434.

Lemma collineation_15435 : is_collineation2 fp_15435.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15435 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15435.

Lemma collineation_15436 : is_collineation2 fp_15436.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15436 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15436.

Lemma collineation_15437 : is_collineation2 fp_15437.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15437 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15437.

Lemma collineation_15438 : is_collineation2 fp_15438.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15438 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15438.

Lemma collineation_15439 : is_collineation2 fp_15439.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15439 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15439.

Lemma collineation_15440 : is_collineation2 fp_15440.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15440 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15440.

Lemma collineation_15441 : is_collineation2 fp_15441.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15441 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15441.

Lemma collineation_15442 : is_collineation2 fp_15442.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15442 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15442.

Lemma collineation_15443 : is_collineation2 fp_15443.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15443 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15443.

Lemma collineation_15444 : is_collineation2 fp_15444.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15444 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15444.

Lemma collineation_15445 : is_collineation2 fp_15445.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15445 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15445.

Lemma collineation_15446 : is_collineation2 fp_15446.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15446 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15446.

Lemma collineation_15447 : is_collineation2 fp_15447.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15447 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15447.

Lemma collineation_15448 : is_collineation2 fp_15448.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15448 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15448.

Lemma collineation_15449 : is_collineation2 fp_15449.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15449 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15449.

Lemma collineation_15450 : is_collineation2 fp_15450.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15450 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15450.

Lemma collineation_15451 : is_collineation2 fp_15451.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15451 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15451.

Lemma collineation_15452 : is_collineation2 fp_15452.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15452 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15452.

Lemma collineation_15453 : is_collineation2 fp_15453.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15453 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15453.

Lemma collineation_15454 : is_collineation2 fp_15454.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15454 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15454.

Lemma collineation_15455 : is_collineation2 fp_15455.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15455 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15455.

Lemma collineation_15456 : is_collineation2 fp_15456.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15456 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15456.

Lemma collineation_15457 : is_collineation2 fp_15457.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15457 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15457.

Lemma collineation_15458 : is_collineation2 fp_15458.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15458 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15458.

Lemma collineation_15459 : is_collineation2 fp_15459.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15459 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15459.

Lemma collineation_15460 : is_collineation2 fp_15460.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15460 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15460.

Lemma collineation_15461 : is_collineation2 fp_15461.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15461 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15461.

Lemma collineation_15462 : is_collineation2 fp_15462.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15462 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15462.

Lemma collineation_15463 : is_collineation2 fp_15463.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15463 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15463.

Lemma collineation_15464 : is_collineation2 fp_15464.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15464 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15464.

Lemma collineation_15465 : is_collineation2 fp_15465.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15465 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15465.

Lemma collineation_15466 : is_collineation2 fp_15466.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15466 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15466.

Lemma collineation_15467 : is_collineation2 fp_15467.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15467 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15467.

Lemma collineation_15468 : is_collineation2 fp_15468.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15468 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15468.

Lemma collineation_15469 : is_collineation2 fp_15469.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15469 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15469.

Lemma collineation_15470 : is_collineation2 fp_15470.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15470 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15470.

Lemma collineation_15471 : is_collineation2 fp_15471.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15471 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15471.

Lemma collineation_15472 : is_collineation2 fp_15472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15472.

Lemma collineation_15473 : is_collineation2 fp_15473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15473.

Lemma collineation_15474 : is_collineation2 fp_15474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15474.

Lemma collineation_15475 : is_collineation2 fp_15475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15475.

Lemma collineation_15476 : is_collineation2 fp_15476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15476.

Lemma collineation_15477 : is_collineation2 fp_15477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15477.

Lemma collineation_15478 : is_collineation2 fp_15478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15478.

Lemma collineation_15479 : is_collineation2 fp_15479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15479.

Lemma collineation_15480 : is_collineation2 fp_15480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15480.

Lemma collineation_15481 : is_collineation2 fp_15481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15481.

Lemma collineation_15482 : is_collineation2 fp_15482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15482.

Lemma collineation_15483 : is_collineation2 fp_15483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15483.

Lemma collineation_15484 : is_collineation2 fp_15484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15484.

Lemma collineation_15485 : is_collineation2 fp_15485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15485.

Lemma collineation_15486 : is_collineation2 fp_15486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15486.

Lemma collineation_15487 : is_collineation2 fp_15487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15487.

Lemma collineation_15488 : is_collineation2 fp_15488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15488.

Lemma collineation_15489 : is_collineation2 fp_15489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15489.

Lemma collineation_15490 : is_collineation2 fp_15490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15490.

Lemma collineation_15491 : is_collineation2 fp_15491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15491.

Lemma collineation_15492 : is_collineation2 fp_15492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15492.

Lemma collineation_15493 : is_collineation2 fp_15493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15493.

Lemma collineation_15494 : is_collineation2 fp_15494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15494.

Lemma collineation_15495 : is_collineation2 fp_15495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15495.

Lemma collineation_15496 : is_collineation2 fp_15496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15496.

Lemma collineation_15497 : is_collineation2 fp_15497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15497.

Lemma collineation_15498 : is_collineation2 fp_15498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15498.

Lemma collineation_15499 : is_collineation2 fp_15499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15499.

Lemma collineation_15500 : is_collineation2 fp_15500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15500.

Lemma collineation_15501 : is_collineation2 fp_15501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15501.

Lemma collineation_15502 : is_collineation2 fp_15502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15502.

Lemma collineation_15503 : is_collineation2 fp_15503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15503.

Lemma collineation_15504 : is_collineation2 fp_15504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15504.

Lemma collineation_15505 : is_collineation2 fp_15505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15505.

Lemma collineation_15506 : is_collineation2 fp_15506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15506.

Lemma collineation_15507 : is_collineation2 fp_15507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15507.

Lemma collineation_15508 : is_collineation2 fp_15508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15508.

Lemma collineation_15509 : is_collineation2 fp_15509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15509.

Lemma collineation_15510 : is_collineation2 fp_15510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15510.

Lemma collineation_15511 : is_collineation2 fp_15511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15511.

Lemma collineation_15512 : is_collineation2 fp_15512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15512.

Lemma collineation_15513 : is_collineation2 fp_15513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15513.

Lemma collineation_15514 : is_collineation2 fp_15514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15514.

Lemma collineation_15515 : is_collineation2 fp_15515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15515.

Lemma collineation_15516 : is_collineation2 fp_15516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15516.

Lemma collineation_15517 : is_collineation2 fp_15517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15517.

Lemma collineation_15518 : is_collineation2 fp_15518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15518.

Lemma collineation_15519 : is_collineation2 fp_15519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15519.

Lemma collineation_15520 : is_collineation2 fp_15520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15520.

Lemma collineation_15521 : is_collineation2 fp_15521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15521.

Lemma collineation_15522 : is_collineation2 fp_15522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15522.

Lemma collineation_15523 : is_collineation2 fp_15523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15523.

Lemma collineation_15524 : is_collineation2 fp_15524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15524.

Lemma collineation_15525 : is_collineation2 fp_15525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15525.

Lemma collineation_15526 : is_collineation2 fp_15526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15526.

Lemma collineation_15527 : is_collineation2 fp_15527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15527.

Lemma collineation_15528 : is_collineation2 fp_15528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15528.

Lemma collineation_15529 : is_collineation2 fp_15529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15529.

Lemma collineation_15530 : is_collineation2 fp_15530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15530.

Lemma collineation_15531 : is_collineation2 fp_15531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15531.

Lemma collineation_15532 : is_collineation2 fp_15532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15532.

Lemma collineation_15533 : is_collineation2 fp_15533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15533.

Lemma collineation_15534 : is_collineation2 fp_15534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15534.

Lemma collineation_15535 : is_collineation2 fp_15535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15535.

Lemma collineation_15536 : is_collineation2 fp_15536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15536.

Lemma collineation_15537 : is_collineation2 fp_15537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15537.

Lemma collineation_15538 : is_collineation2 fp_15538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15538.

Lemma collineation_15539 : is_collineation2 fp_15539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15539.

Lemma collineation_15540 : is_collineation2 fp_15540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15540.

Lemma collineation_15541 : is_collineation2 fp_15541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15541.

Lemma collineation_15542 : is_collineation2 fp_15542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15542.

Lemma collineation_15543 : is_collineation2 fp_15543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15543.

Lemma collineation_15544 : is_collineation2 fp_15544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15544.

Lemma collineation_15545 : is_collineation2 fp_15545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15545.

Lemma collineation_15546 : is_collineation2 fp_15546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15546.

Lemma collineation_15547 : is_collineation2 fp_15547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15547.

Lemma collineation_15548 : is_collineation2 fp_15548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15548.

Lemma collineation_15549 : is_collineation2 fp_15549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15549.

Lemma collineation_15550 : is_collineation2 fp_15550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15550.

Lemma collineation_15551 : is_collineation2 fp_15551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15551.

Lemma collineation_15552 : is_collineation2 fp_15552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15552.

Lemma collineation_15553 : is_collineation2 fp_15553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15553.

Lemma collineation_15554 : is_collineation2 fp_15554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15554.

Lemma collineation_15555 : is_collineation2 fp_15555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15555.

Lemma collineation_15556 : is_collineation2 fp_15556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15556.

Lemma collineation_15557 : is_collineation2 fp_15557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15557.

Lemma collineation_15558 : is_collineation2 fp_15558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15558.

Lemma collineation_15559 : is_collineation2 fp_15559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15559.

Lemma collineation_15560 : is_collineation2 fp_15560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15560.

Lemma collineation_15561 : is_collineation2 fp_15561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15561.

Lemma collineation_15562 : is_collineation2 fp_15562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15562.

Lemma collineation_15563 : is_collineation2 fp_15563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15563.

Lemma collineation_15564 : is_collineation2 fp_15564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15564.

Lemma collineation_15565 : is_collineation2 fp_15565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15565.

Lemma collineation_15566 : is_collineation2 fp_15566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15566.

Lemma collineation_15567 : is_collineation2 fp_15567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15567.

Lemma collineation_15568 : is_collineation2 fp_15568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15568.

Lemma collineation_15569 : is_collineation2 fp_15569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15569.

Lemma collineation_15570 : is_collineation2 fp_15570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15570.

Lemma collineation_15571 : is_collineation2 fp_15571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15571.

Lemma collineation_15572 : is_collineation2 fp_15572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15572.

Lemma collineation_15573 : is_collineation2 fp_15573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15573.

Lemma collineation_15574 : is_collineation2 fp_15574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15574.

Lemma collineation_15575 : is_collineation2 fp_15575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15575.

Lemma collineation_15576 : is_collineation2 fp_15576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15576.

Lemma collineation_15577 : is_collineation2 fp_15577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15577.

Lemma collineation_15578 : is_collineation2 fp_15578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15578.

Lemma collineation_15579 : is_collineation2 fp_15579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15579.

Lemma collineation_15580 : is_collineation2 fp_15580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15580.

Lemma collineation_15581 : is_collineation2 fp_15581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15581.

Lemma collineation_15582 : is_collineation2 fp_15582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15582.

Lemma collineation_15583 : is_collineation2 fp_15583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15583.

Lemma collineation_15584 : is_collineation2 fp_15584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15584.

Lemma collineation_15585 : is_collineation2 fp_15585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15585.

Lemma collineation_15586 : is_collineation2 fp_15586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15586.

Lemma collineation_15587 : is_collineation2 fp_15587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15587.

Lemma collineation_15588 : is_collineation2 fp_15588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15588.

Lemma collineation_15589 : is_collineation2 fp_15589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15589.

Lemma collineation_15590 : is_collineation2 fp_15590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15590.

Lemma collineation_15591 : is_collineation2 fp_15591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15591.

Lemma collineation_15592 : is_collineation2 fp_15592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15592.

Lemma collineation_15593 : is_collineation2 fp_15593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15593.

Lemma collineation_15594 : is_collineation2 fp_15594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15594.

Lemma collineation_15595 : is_collineation2 fp_15595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15595.

Lemma collineation_15596 : is_collineation2 fp_15596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15596.

Lemma collineation_15597 : is_collineation2 fp_15597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15597.

Lemma collineation_15598 : is_collineation2 fp_15598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15598.

Lemma collineation_15599 : is_collineation2 fp_15599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15599.

Lemma collineation_15600 : is_collineation2 fp_15600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15600.

Lemma collineation_15601 : is_collineation2 fp_15601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15601.

Lemma collineation_15602 : is_collineation2 fp_15602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15602.

Lemma collineation_15603 : is_collineation2 fp_15603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15603.

Lemma collineation_15604 : is_collineation2 fp_15604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15604.

Lemma collineation_15605 : is_collineation2 fp_15605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15605.

Lemma collineation_15606 : is_collineation2 fp_15606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15606.

Lemma collineation_15607 : is_collineation2 fp_15607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15607.

Lemma collineation_15608 : is_collineation2 fp_15608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15608.

Lemma collineation_15609 : is_collineation2 fp_15609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15609.

Lemma collineation_15610 : is_collineation2 fp_15610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15610.

Lemma collineation_15611 : is_collineation2 fp_15611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15611.

Lemma collineation_15612 : is_collineation2 fp_15612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15612.

Lemma collineation_15613 : is_collineation2 fp_15613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15613.

Lemma collineation_15614 : is_collineation2 fp_15614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15614.

Lemma collineation_15615 : is_collineation2 fp_15615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15615.

Lemma collineation_15616 : is_collineation2 fp_15616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15616.

Lemma collineation_15617 : is_collineation2 fp_15617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15617.

Lemma collineation_15618 : is_collineation2 fp_15618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15618.

Lemma collineation_15619 : is_collineation2 fp_15619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15619.

Lemma collineation_15620 : is_collineation2 fp_15620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15620.

Lemma collineation_15621 : is_collineation2 fp_15621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15621.

Lemma collineation_15622 : is_collineation2 fp_15622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15622.

Lemma collineation_15623 : is_collineation2 fp_15623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15623.

Lemma collineation_15624 : is_collineation2 fp_15624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15624.

Lemma collineation_15625 : is_collineation2 fp_15625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15625.

Lemma collineation_15626 : is_collineation2 fp_15626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15626.

Lemma collineation_15627 : is_collineation2 fp_15627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15627.

Lemma collineation_15628 : is_collineation2 fp_15628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15628.

Lemma collineation_15629 : is_collineation2 fp_15629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15629.

Lemma collineation_15630 : is_collineation2 fp_15630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15630.

Lemma collineation_15631 : is_collineation2 fp_15631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15631.

Lemma collineation_15632 : is_collineation2 fp_15632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15632.

Lemma collineation_15633 : is_collineation2 fp_15633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15633.

Lemma collineation_15634 : is_collineation2 fp_15634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15634.

Lemma collineation_15635 : is_collineation2 fp_15635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15635.

Lemma collineation_15636 : is_collineation2 fp_15636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15636.

Lemma collineation_15637 : is_collineation2 fp_15637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15637.

Lemma collineation_15638 : is_collineation2 fp_15638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15638.

Lemma collineation_15639 : is_collineation2 fp_15639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15639.

Lemma collineation_15640 : is_collineation2 fp_15640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15640.

Lemma collineation_15641 : is_collineation2 fp_15641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15641.

Lemma collineation_15642 : is_collineation2 fp_15642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15642.

Lemma collineation_15643 : is_collineation2 fp_15643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15643.

Lemma collineation_15644 : is_collineation2 fp_15644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15644.

Lemma collineation_15645 : is_collineation2 fp_15645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15645.

Lemma collineation_15646 : is_collineation2 fp_15646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15646.

Lemma collineation_15647 : is_collineation2 fp_15647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15647.

Lemma collineation_15648 : is_collineation2 fp_15648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15648.

Lemma collineation_15649 : is_collineation2 fp_15649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15649.

Lemma collineation_15650 : is_collineation2 fp_15650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15650.

Lemma collineation_15651 : is_collineation2 fp_15651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15651.

Lemma collineation_15652 : is_collineation2 fp_15652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15652.

Lemma collineation_15653 : is_collineation2 fp_15653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15653.

Lemma collineation_15654 : is_collineation2 fp_15654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15654.

Lemma collineation_15655 : is_collineation2 fp_15655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15655.

Lemma collineation_15656 : is_collineation2 fp_15656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15656.

Lemma collineation_15657 : is_collineation2 fp_15657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15657.

Lemma collineation_15658 : is_collineation2 fp_15658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15658.

Lemma collineation_15659 : is_collineation2 fp_15659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15659.

Lemma collineation_15660 : is_collineation2 fp_15660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15660.

Lemma collineation_15661 : is_collineation2 fp_15661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15661.

Lemma collineation_15662 : is_collineation2 fp_15662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15662.

Lemma collineation_15663 : is_collineation2 fp_15663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15663.

Lemma collineation_15664 : is_collineation2 fp_15664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15664.

Lemma collineation_15665 : is_collineation2 fp_15665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15665.

Lemma collineation_15666 : is_collineation2 fp_15666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15666.

Lemma collineation_15667 : is_collineation2 fp_15667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15667.

Lemma collineation_15668 : is_collineation2 fp_15668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15668.

Lemma collineation_15669 : is_collineation2 fp_15669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15669.

Lemma collineation_15670 : is_collineation2 fp_15670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15670.

Lemma collineation_15671 : is_collineation2 fp_15671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15671.

Lemma collineation_15672 : is_collineation2 fp_15672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15672.

Lemma collineation_15673 : is_collineation2 fp_15673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15673.

Lemma collineation_15674 : is_collineation2 fp_15674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15674.

Lemma collineation_15675 : is_collineation2 fp_15675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15675.

Lemma collineation_15676 : is_collineation2 fp_15676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15676.

Lemma collineation_15677 : is_collineation2 fp_15677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15677.

Lemma collineation_15678 : is_collineation2 fp_15678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15678.

Lemma collineation_15679 : is_collineation2 fp_15679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15679.

Lemma collineation_15680 : is_collineation2 fp_15680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15680.

Lemma collineation_15681 : is_collineation2 fp_15681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15681.

Lemma collineation_15682 : is_collineation2 fp_15682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15682.

Lemma collineation_15683 : is_collineation2 fp_15683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15683.

Lemma collineation_15684 : is_collineation2 fp_15684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15684.

Lemma collineation_15685 : is_collineation2 fp_15685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15685.

Lemma collineation_15686 : is_collineation2 fp_15686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15686.

Lemma collineation_15687 : is_collineation2 fp_15687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15687.

Lemma collineation_15688 : is_collineation2 fp_15688.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15688 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15688.

Lemma collineation_15689 : is_collineation2 fp_15689.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15689 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15689.

Lemma collineation_15690 : is_collineation2 fp_15690.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15690 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15690.

Lemma collineation_15691 : is_collineation2 fp_15691.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15691 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15691.

Lemma collineation_15692 : is_collineation2 fp_15692.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15692 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15692.

Lemma collineation_15693 : is_collineation2 fp_15693.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15693 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15693.

Lemma collineation_15694 : is_collineation2 fp_15694.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15694 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15694.

Lemma collineation_15695 : is_collineation2 fp_15695.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15695 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15695.

Lemma collineation_15696 : is_collineation2 fp_15696.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15696 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15696.

Lemma collineation_15697 : is_collineation2 fp_15697.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15697 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15697.

Lemma collineation_15698 : is_collineation2 fp_15698.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15698 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15698.

Lemma collineation_15699 : is_collineation2 fp_15699.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15699 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15699.

Lemma collineation_15700 : is_collineation2 fp_15700.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15700 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15700.

Lemma collineation_15701 : is_collineation2 fp_15701.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15701 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15701.

Lemma collineation_15702 : is_collineation2 fp_15702.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15702 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15702.

Lemma collineation_15703 : is_collineation2 fp_15703.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15703 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15703.

Lemma collineation_15704 : is_collineation2 fp_15704.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15704 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15704.

Lemma collineation_15705 : is_collineation2 fp_15705.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15705 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15705.

Lemma collineation_15706 : is_collineation2 fp_15706.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15706 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15706.

Lemma collineation_15707 : is_collineation2 fp_15707.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15707 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15707.

Lemma collineation_15708 : is_collineation2 fp_15708.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15708 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15708.

Lemma collineation_15709 : is_collineation2 fp_15709.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15709 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15709.

Lemma collineation_15710 : is_collineation2 fp_15710.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15710 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15710.

Lemma collineation_15711 : is_collineation2 fp_15711.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15711 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15711.

Lemma collineation_15712 : is_collineation2 fp_15712.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15712 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15712.

Lemma collineation_15713 : is_collineation2 fp_15713.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15713 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15713.

Lemma collineation_15714 : is_collineation2 fp_15714.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15714 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15714.

Lemma collineation_15715 : is_collineation2 fp_15715.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15715 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15715.

Lemma collineation_15716 : is_collineation2 fp_15716.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15716 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15716.

Lemma collineation_15717 : is_collineation2 fp_15717.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15717 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15717.

Lemma collineation_15718 : is_collineation2 fp_15718.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15718 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15718.

Lemma collineation_15719 : is_collineation2 fp_15719.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15719 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15719.

Lemma collineation_15720 : is_collineation2 fp_15720.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15720 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15720.

Lemma collineation_15721 : is_collineation2 fp_15721.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15721 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15721.

Lemma collineation_15722 : is_collineation2 fp_15722.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15722 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15722.

Lemma collineation_15723 : is_collineation2 fp_15723.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15723 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15723.

Lemma collineation_15724 : is_collineation2 fp_15724.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15724 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15724.

Lemma collineation_15725 : is_collineation2 fp_15725.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15725 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15725.

Lemma collineation_15726 : is_collineation2 fp_15726.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15726 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15726.

Lemma collineation_15727 : is_collineation2 fp_15727.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15727 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15727.

Lemma collineation_15728 : is_collineation2 fp_15728.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15728 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15728.

Lemma collineation_15729 : is_collineation2 fp_15729.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15729 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15729.

Lemma collineation_15730 : is_collineation2 fp_15730.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15730 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15730.

Lemma collineation_15731 : is_collineation2 fp_15731.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15731 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15731.

Lemma collineation_15732 : is_collineation2 fp_15732.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15732 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15732.

Lemma collineation_15733 : is_collineation2 fp_15733.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15733 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15733.

Lemma collineation_15734 : is_collineation2 fp_15734.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15734 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15734.

Lemma collineation_15735 : is_collineation2 fp_15735.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15735 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15735.

Lemma collineation_15736 : is_collineation2 fp_15736.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15736 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15736.

Lemma collineation_15737 : is_collineation2 fp_15737.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15737 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15737.

Lemma collineation_15738 : is_collineation2 fp_15738.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15738 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15738.

Lemma collineation_15739 : is_collineation2 fp_15739.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15739 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15739.

Lemma collineation_15740 : is_collineation2 fp_15740.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15740 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15740.

Lemma collineation_15741 : is_collineation2 fp_15741.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15741 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15741.

Lemma collineation_15742 : is_collineation2 fp_15742.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15742 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15742.

Lemma collineation_15743 : is_collineation2 fp_15743.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15743 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15743.

Lemma collineation_15744 : is_collineation2 fp_15744.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15744 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15744.

Lemma collineation_15745 : is_collineation2 fp_15745.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15745 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15745.

Lemma collineation_15746 : is_collineation2 fp_15746.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15746 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15746.

Lemma collineation_15747 : is_collineation2 fp_15747.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15747 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15747.

Lemma collineation_15748 : is_collineation2 fp_15748.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15748 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15748.

Lemma collineation_15749 : is_collineation2 fp_15749.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15749 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15749.

Lemma collineation_15750 : is_collineation2 fp_15750.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15750 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15750.

Lemma collineation_15751 : is_collineation2 fp_15751.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15751 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15751.

Lemma collineation_15752 : is_collineation2 fp_15752.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15752 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15752.

Lemma collineation_15753 : is_collineation2 fp_15753.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15753 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15753.

Lemma collineation_15754 : is_collineation2 fp_15754.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15754 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15754.

Lemma collineation_15755 : is_collineation2 fp_15755.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15755 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15755.

Lemma collineation_15756 : is_collineation2 fp_15756.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15756 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15756.

Lemma collineation_15757 : is_collineation2 fp_15757.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15757 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15757.

Lemma collineation_15758 : is_collineation2 fp_15758.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15758 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15758.

Lemma collineation_15759 : is_collineation2 fp_15759.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15759 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15759.

Lemma collineation_15760 : is_collineation2 fp_15760.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15760 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15760.

Lemma collineation_15761 : is_collineation2 fp_15761.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15761 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15761.

Lemma collineation_15762 : is_collineation2 fp_15762.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15762 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15762.

Lemma collineation_15763 : is_collineation2 fp_15763.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15763 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15763.

Lemma collineation_15764 : is_collineation2 fp_15764.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15764 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15764.

Lemma collineation_15765 : is_collineation2 fp_15765.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15765 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15765.

Lemma collineation_15766 : is_collineation2 fp_15766.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15766 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15766.

Lemma collineation_15767 : is_collineation2 fp_15767.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15767 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15767.

Lemma collineation_15768 : is_collineation2 fp_15768.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15768 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15768.

Lemma collineation_15769 : is_collineation2 fp_15769.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15769 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15769.

Lemma collineation_15770 : is_collineation2 fp_15770.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15770 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15770.

Lemma collineation_15771 : is_collineation2 fp_15771.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15771 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15771.

Lemma collineation_15772 : is_collineation2 fp_15772.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15772 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15772.

Lemma collineation_15773 : is_collineation2 fp_15773.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15773 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15773.

Lemma collineation_15774 : is_collineation2 fp_15774.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15774 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15774.

Lemma collineation_15775 : is_collineation2 fp_15775.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15775 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15775.

Lemma collineation_15776 : is_collineation2 fp_15776.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15776 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15776.

Lemma collineation_15777 : is_collineation2 fp_15777.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15777 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15777.

Lemma collineation_15778 : is_collineation2 fp_15778.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15778 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15778.

Lemma collineation_15779 : is_collineation2 fp_15779.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15779 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15779.

Lemma collineation_15780 : is_collineation2 fp_15780.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15780 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15780.

Lemma collineation_15781 : is_collineation2 fp_15781.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15781 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15781.

Lemma collineation_15782 : is_collineation2 fp_15782.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15782 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15782.

Lemma collineation_15783 : is_collineation2 fp_15783.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15783 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15783.

Lemma collineation_15784 : is_collineation2 fp_15784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15784.

Lemma collineation_15785 : is_collineation2 fp_15785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15785.

Lemma collineation_15786 : is_collineation2 fp_15786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15786.

Lemma collineation_15787 : is_collineation2 fp_15787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15787.

Lemma collineation_15788 : is_collineation2 fp_15788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15788.

Lemma collineation_15789 : is_collineation2 fp_15789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15789.

Lemma collineation_15790 : is_collineation2 fp_15790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15790.

Lemma collineation_15791 : is_collineation2 fp_15791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15791.

Lemma collineation_15792 : is_collineation2 fp_15792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15792.

Lemma collineation_15793 : is_collineation2 fp_15793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15793.

Lemma collineation_15794 : is_collineation2 fp_15794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15794.

Lemma collineation_15795 : is_collineation2 fp_15795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15795.

Lemma collineation_15796 : is_collineation2 fp_15796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15796.

Lemma collineation_15797 : is_collineation2 fp_15797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15797.

Lemma collineation_15798 : is_collineation2 fp_15798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15798.

Lemma collineation_15799 : is_collineation2 fp_15799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15799.

Lemma collineation_15800 : is_collineation2 fp_15800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15800.

Lemma collineation_15801 : is_collineation2 fp_15801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15801.

Lemma collineation_15802 : is_collineation2 fp_15802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15802.

Lemma collineation_15803 : is_collineation2 fp_15803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15803.

Lemma collineation_15804 : is_collineation2 fp_15804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15804.

Lemma collineation_15805 : is_collineation2 fp_15805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15805.

Lemma collineation_15806 : is_collineation2 fp_15806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15806.

Lemma collineation_15807 : is_collineation2 fp_15807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15807.

Lemma collineation_15808 : is_collineation2 fp_15808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15808.

Lemma collineation_15809 : is_collineation2 fp_15809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15809.

Lemma collineation_15810 : is_collineation2 fp_15810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15810.

Lemma collineation_15811 : is_collineation2 fp_15811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15811.

Lemma collineation_15812 : is_collineation2 fp_15812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15812.

Lemma collineation_15813 : is_collineation2 fp_15813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15813.

Lemma collineation_15814 : is_collineation2 fp_15814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15814.

Lemma collineation_15815 : is_collineation2 fp_15815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15815.

Lemma collineation_15816 : is_collineation2 fp_15816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15816.

Lemma collineation_15817 : is_collineation2 fp_15817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15817.

Lemma collineation_15818 : is_collineation2 fp_15818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15818.

Lemma collineation_15819 : is_collineation2 fp_15819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15819.

Lemma collineation_15820 : is_collineation2 fp_15820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15820.

Lemma collineation_15821 : is_collineation2 fp_15821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15821.

Lemma collineation_15822 : is_collineation2 fp_15822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15822.

Lemma collineation_15823 : is_collineation2 fp_15823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15823.

Lemma collineation_15824 : is_collineation2 fp_15824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15824.

Lemma collineation_15825 : is_collineation2 fp_15825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15825.

Lemma collineation_15826 : is_collineation2 fp_15826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15826.

Lemma collineation_15827 : is_collineation2 fp_15827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15827.

Lemma collineation_15828 : is_collineation2 fp_15828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15828.

Lemma collineation_15829 : is_collineation2 fp_15829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15829.

Lemma collineation_15830 : is_collineation2 fp_15830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15830.

Lemma collineation_15831 : is_collineation2 fp_15831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15831.

Lemma collineation_15832 : is_collineation2 fp_15832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15832.

Lemma collineation_15833 : is_collineation2 fp_15833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15833.

Lemma collineation_15834 : is_collineation2 fp_15834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15834.

Lemma collineation_15835 : is_collineation2 fp_15835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15835.

Lemma collineation_15836 : is_collineation2 fp_15836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15836.

Lemma collineation_15837 : is_collineation2 fp_15837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15837.

Lemma collineation_15838 : is_collineation2 fp_15838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15838.

Lemma collineation_15839 : is_collineation2 fp_15839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15839.

Lemma collineation_15840 : is_collineation2 fp_15840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15840.

Lemma collineation_15841 : is_collineation2 fp_15841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15841.

Lemma collineation_15842 : is_collineation2 fp_15842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15842.

Lemma collineation_15843 : is_collineation2 fp_15843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15843.

Lemma collineation_15844 : is_collineation2 fp_15844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15844.

Lemma collineation_15845 : is_collineation2 fp_15845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15845.

Lemma collineation_15846 : is_collineation2 fp_15846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15846.

Lemma collineation_15847 : is_collineation2 fp_15847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15847.

Lemma collineation_15848 : is_collineation2 fp_15848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15848.

Lemma collineation_15849 : is_collineation2 fp_15849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15849.

Lemma collineation_15850 : is_collineation2 fp_15850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15850.

Lemma collineation_15851 : is_collineation2 fp_15851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15851.

Lemma collineation_15852 : is_collineation2 fp_15852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15852.

Lemma collineation_15853 : is_collineation2 fp_15853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15853.

Lemma collineation_15854 : is_collineation2 fp_15854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15854.

Lemma collineation_15855 : is_collineation2 fp_15855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15855.

Lemma collineation_15856 : is_collineation2 fp_15856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15856.

Lemma collineation_15857 : is_collineation2 fp_15857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15857.

Lemma collineation_15858 : is_collineation2 fp_15858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15858.

Lemma collineation_15859 : is_collineation2 fp_15859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15859.

Lemma collineation_15860 : is_collineation2 fp_15860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15860.

Lemma collineation_15861 : is_collineation2 fp_15861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15861.

Lemma collineation_15862 : is_collineation2 fp_15862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15862.

Lemma collineation_15863 : is_collineation2 fp_15863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15863.

Lemma collineation_15864 : is_collineation2 fp_15864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15864.

Lemma collineation_15865 : is_collineation2 fp_15865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15865.

Lemma collineation_15866 : is_collineation2 fp_15866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15866.

Lemma collineation_15867 : is_collineation2 fp_15867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15867.

Lemma collineation_15868 : is_collineation2 fp_15868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15868.

Lemma collineation_15869 : is_collineation2 fp_15869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15869.

Lemma collineation_15870 : is_collineation2 fp_15870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15870.

Lemma collineation_15871 : is_collineation2 fp_15871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15871.

Lemma collineation_15872 : is_collineation2 fp_15872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15872.

Lemma collineation_15873 : is_collineation2 fp_15873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15873.

Lemma collineation_15874 : is_collineation2 fp_15874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15874.

Lemma collineation_15875 : is_collineation2 fp_15875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15875.

Lemma collineation_15876 : is_collineation2 fp_15876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15876.

Lemma collineation_15877 : is_collineation2 fp_15877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15877.

Lemma collineation_15878 : is_collineation2 fp_15878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15878.

Lemma collineation_15879 : is_collineation2 fp_15879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15879.

Lemma collineation_15880 : is_collineation2 fp_15880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15880.

Lemma collineation_15881 : is_collineation2 fp_15881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15881.

Lemma collineation_15882 : is_collineation2 fp_15882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15882.

Lemma collineation_15883 : is_collineation2 fp_15883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15883.

Lemma collineation_15884 : is_collineation2 fp_15884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15884.

Lemma collineation_15885 : is_collineation2 fp_15885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15885.

Lemma collineation_15886 : is_collineation2 fp_15886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15886.

Lemma collineation_15887 : is_collineation2 fp_15887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15887.

Lemma collineation_15888 : is_collineation2 fp_15888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15888.

Lemma collineation_15889 : is_collineation2 fp_15889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15889.

Lemma collineation_15890 : is_collineation2 fp_15890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15890.

Lemma collineation_15891 : is_collineation2 fp_15891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15891.

Lemma collineation_15892 : is_collineation2 fp_15892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15892.

Lemma collineation_15893 : is_collineation2 fp_15893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15893.

Lemma collineation_15894 : is_collineation2 fp_15894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15894.

Lemma collineation_15895 : is_collineation2 fp_15895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15895.

Lemma collineation_15896 : is_collineation2 fp_15896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15896.

Lemma collineation_15897 : is_collineation2 fp_15897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15897.

Lemma collineation_15898 : is_collineation2 fp_15898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15898.

Lemma collineation_15899 : is_collineation2 fp_15899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15899.

Lemma collineation_15900 : is_collineation2 fp_15900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15900.

Lemma collineation_15901 : is_collineation2 fp_15901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15901.

Lemma collineation_15902 : is_collineation2 fp_15902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15902.

Lemma collineation_15903 : is_collineation2 fp_15903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15903.

Lemma collineation_15904 : is_collineation2 fp_15904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15904.

Lemma collineation_15905 : is_collineation2 fp_15905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15905.

Lemma collineation_15906 : is_collineation2 fp_15906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15906.

Lemma collineation_15907 : is_collineation2 fp_15907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15907.

Lemma collineation_15908 : is_collineation2 fp_15908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15908.

Lemma collineation_15909 : is_collineation2 fp_15909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15909.

Lemma collineation_15910 : is_collineation2 fp_15910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15910.

Lemma collineation_15911 : is_collineation2 fp_15911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15911.

Lemma collineation_15912 : is_collineation2 fp_15912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15912.

Lemma collineation_15913 : is_collineation2 fp_15913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15913.

Lemma collineation_15914 : is_collineation2 fp_15914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15914.

Lemma collineation_15915 : is_collineation2 fp_15915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15915.

Lemma collineation_15916 : is_collineation2 fp_15916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15916.

Lemma collineation_15917 : is_collineation2 fp_15917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15917.

Lemma collineation_15918 : is_collineation2 fp_15918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15918.

Lemma collineation_15919 : is_collineation2 fp_15919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15919.

Lemma collineation_15920 : is_collineation2 fp_15920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15920.

Lemma collineation_15921 : is_collineation2 fp_15921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15921.

Lemma collineation_15922 : is_collineation2 fp_15922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15922.

Lemma collineation_15923 : is_collineation2 fp_15923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15923.

Lemma collineation_15924 : is_collineation2 fp_15924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15924.

Lemma collineation_15925 : is_collineation2 fp_15925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15925.

Lemma collineation_15926 : is_collineation2 fp_15926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15926.

Lemma collineation_15927 : is_collineation2 fp_15927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15927.

Lemma collineation_15928 : is_collineation2 fp_15928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15928.

Lemma collineation_15929 : is_collineation2 fp_15929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15929.

Lemma collineation_15930 : is_collineation2 fp_15930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15930.

Lemma collineation_15931 : is_collineation2 fp_15931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15931.

Lemma collineation_15932 : is_collineation2 fp_15932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15932.

Lemma collineation_15933 : is_collineation2 fp_15933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15933.

Lemma collineation_15934 : is_collineation2 fp_15934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15934.

Lemma collineation_15935 : is_collineation2 fp_15935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15935.

Lemma collineation_15936 : is_collineation2 fp_15936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15936.

Lemma collineation_15937 : is_collineation2 fp_15937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15937.

Lemma collineation_15938 : is_collineation2 fp_15938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15938.

Lemma collineation_15939 : is_collineation2 fp_15939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15939.

Lemma collineation_15940 : is_collineation2 fp_15940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15940.

Lemma collineation_15941 : is_collineation2 fp_15941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15941.

Lemma collineation_15942 : is_collineation2 fp_15942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15942.

Lemma collineation_15943 : is_collineation2 fp_15943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15943.

Lemma collineation_15944 : is_collineation2 fp_15944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15944.

Lemma collineation_15945 : is_collineation2 fp_15945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15945.

Lemma collineation_15946 : is_collineation2 fp_15946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15946.

Lemma collineation_15947 : is_collineation2 fp_15947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15947.

Lemma collineation_15948 : is_collineation2 fp_15948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15948.

Lemma collineation_15949 : is_collineation2 fp_15949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15949.

Lemma collineation_15950 : is_collineation2 fp_15950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15950.

Lemma collineation_15951 : is_collineation2 fp_15951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15951.

Lemma collineation_15952 : is_collineation2 fp_15952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15952.

Lemma collineation_15953 : is_collineation2 fp_15953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15953.

Lemma collineation_15954 : is_collineation2 fp_15954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15954.

Lemma collineation_15955 : is_collineation2 fp_15955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15955.

Lemma collineation_15956 : is_collineation2 fp_15956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15956.

Lemma collineation_15957 : is_collineation2 fp_15957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15957.

Lemma collineation_15958 : is_collineation2 fp_15958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15958.

Lemma collineation_15959 : is_collineation2 fp_15959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15959.

Lemma collineation_15960 : is_collineation2 fp_15960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15960.

Lemma collineation_15961 : is_collineation2 fp_15961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15961.

Lemma collineation_15962 : is_collineation2 fp_15962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15962.

Lemma collineation_15963 : is_collineation2 fp_15963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15963.

Lemma collineation_15964 : is_collineation2 fp_15964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15964.

Lemma collineation_15965 : is_collineation2 fp_15965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15965.

Lemma collineation_15966 : is_collineation2 fp_15966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15966.

Lemma collineation_15967 : is_collineation2 fp_15967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15967.

Lemma collineation_15968 : is_collineation2 fp_15968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15968.

Lemma collineation_15969 : is_collineation2 fp_15969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15969.

Lemma collineation_15970 : is_collineation2 fp_15970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15970.

Lemma collineation_15971 : is_collineation2 fp_15971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15971.

Lemma collineation_15972 : is_collineation2 fp_15972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15972.

Lemma collineation_15973 : is_collineation2 fp_15973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15973.

Lemma collineation_15974 : is_collineation2 fp_15974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15974.

Lemma collineation_15975 : is_collineation2 fp_15975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15975.

Lemma collineation_15976 : is_collineation2 fp_15976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15976.

Lemma collineation_15977 : is_collineation2 fp_15977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15977.

Lemma collineation_15978 : is_collineation2 fp_15978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15978.

Lemma collineation_15979 : is_collineation2 fp_15979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15979.

Lemma collineation_15980 : is_collineation2 fp_15980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15980.

Lemma collineation_15981 : is_collineation2 fp_15981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15981.

Lemma collineation_15982 : is_collineation2 fp_15982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15982.

Lemma collineation_15983 : is_collineation2 fp_15983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15983.

Lemma collineation_15984 : is_collineation2 fp_15984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15984.

Lemma collineation_15985 : is_collineation2 fp_15985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15985.

Lemma collineation_15986 : is_collineation2 fp_15986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15986.

Lemma collineation_15987 : is_collineation2 fp_15987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15987.

Lemma collineation_15988 : is_collineation2 fp_15988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15988.

Lemma collineation_15989 : is_collineation2 fp_15989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15989.

Lemma collineation_15990 : is_collineation2 fp_15990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15990.

Lemma collineation_15991 : is_collineation2 fp_15991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15991.

Lemma collineation_15992 : is_collineation2 fp_15992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15992.

Lemma collineation_15993 : is_collineation2 fp_15993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15993.

Lemma collineation_15994 : is_collineation2 fp_15994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15994.

Lemma collineation_15995 : is_collineation2 fp_15995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15995.

Lemma collineation_15996 : is_collineation2 fp_15996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15996.

Lemma collineation_15997 : is_collineation2 fp_15997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15997.

Lemma collineation_15998 : is_collineation2 fp_15998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15998.

Lemma collineation_15999 : is_collineation2 fp_15999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_15999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_15999.

Lemma collineation_16000 : is_collineation2 fp_16000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16000.

Lemma collineation_16001 : is_collineation2 fp_16001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16001.

Lemma collineation_16002 : is_collineation2 fp_16002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16002.

Lemma collineation_16003 : is_collineation2 fp_16003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16003.

Lemma collineation_16004 : is_collineation2 fp_16004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16004.

Lemma collineation_16005 : is_collineation2 fp_16005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16005.

Lemma collineation_16006 : is_collineation2 fp_16006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16006.

Lemma collineation_16007 : is_collineation2 fp_16007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16007.

Lemma collineation_16008 : is_collineation2 fp_16008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16008.

Lemma collineation_16009 : is_collineation2 fp_16009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16009.

Lemma collineation_16010 : is_collineation2 fp_16010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16010.

Lemma collineation_16011 : is_collineation2 fp_16011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16011.

Lemma collineation_16012 : is_collineation2 fp_16012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16012.

Lemma collineation_16013 : is_collineation2 fp_16013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16013.

Lemma collineation_16014 : is_collineation2 fp_16014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16014.

Lemma collineation_16015 : is_collineation2 fp_16015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16015.

Lemma collineation_16016 : is_collineation2 fp_16016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16016.

Lemma collineation_16017 : is_collineation2 fp_16017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16017.

Lemma collineation_16018 : is_collineation2 fp_16018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16018.

Lemma collineation_16019 : is_collineation2 fp_16019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16019.

Lemma collineation_16020 : is_collineation2 fp_16020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16020.

Lemma collineation_16021 : is_collineation2 fp_16021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16021.

Lemma collineation_16022 : is_collineation2 fp_16022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16022.

Lemma collineation_16023 : is_collineation2 fp_16023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16023.

Lemma collineation_16024 : is_collineation2 fp_16024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16024.

Lemma collineation_16025 : is_collineation2 fp_16025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16025.

Lemma collineation_16026 : is_collineation2 fp_16026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16026.

Lemma collineation_16027 : is_collineation2 fp_16027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16027.

Lemma collineation_16028 : is_collineation2 fp_16028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16028.

Lemma collineation_16029 : is_collineation2 fp_16029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16029.

Lemma collineation_16030 : is_collineation2 fp_16030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16030.

Lemma collineation_16031 : is_collineation2 fp_16031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16031.

Lemma collineation_16032 : is_collineation2 fp_16032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16032.

Lemma collineation_16033 : is_collineation2 fp_16033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16033.

Lemma collineation_16034 : is_collineation2 fp_16034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16034.

Lemma collineation_16035 : is_collineation2 fp_16035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16035.

Lemma collineation_16036 : is_collineation2 fp_16036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16036.

Lemma collineation_16037 : is_collineation2 fp_16037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16037.

Lemma collineation_16038 : is_collineation2 fp_16038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16038.

Lemma collineation_16039 : is_collineation2 fp_16039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16039.

Lemma collineation_16040 : is_collineation2 fp_16040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16040.

Lemma collineation_16041 : is_collineation2 fp_16041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16041.

Lemma collineation_16042 : is_collineation2 fp_16042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16042.

Lemma collineation_16043 : is_collineation2 fp_16043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16043.

Lemma collineation_16044 : is_collineation2 fp_16044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16044.

Lemma collineation_16045 : is_collineation2 fp_16045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16045.

Lemma collineation_16046 : is_collineation2 fp_16046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16046.

Lemma collineation_16047 : is_collineation2 fp_16047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16047.

Lemma collineation_16048 : is_collineation2 fp_16048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16048.

Lemma collineation_16049 : is_collineation2 fp_16049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16049.

Lemma collineation_16050 : is_collineation2 fp_16050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16050.

Lemma collineation_16051 : is_collineation2 fp_16051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16051.

Lemma collineation_16052 : is_collineation2 fp_16052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16052.

Lemma collineation_16053 : is_collineation2 fp_16053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16053.

Lemma collineation_16054 : is_collineation2 fp_16054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16054.

Lemma collineation_16055 : is_collineation2 fp_16055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16055.

Lemma collineation_16056 : is_collineation2 fp_16056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16056.

Lemma collineation_16057 : is_collineation2 fp_16057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16057.

Lemma collineation_16058 : is_collineation2 fp_16058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16058.

Lemma collineation_16059 : is_collineation2 fp_16059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16059.

Lemma collineation_16060 : is_collineation2 fp_16060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16060.

Lemma collineation_16061 : is_collineation2 fp_16061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16061.

Lemma collineation_16062 : is_collineation2 fp_16062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16062.

Lemma collineation_16063 : is_collineation2 fp_16063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16063.

Lemma collineation_16064 : is_collineation2 fp_16064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16064.

Lemma collineation_16065 : is_collineation2 fp_16065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16065.

Lemma collineation_16066 : is_collineation2 fp_16066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16066.

Lemma collineation_16067 : is_collineation2 fp_16067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16067.

Lemma collineation_16068 : is_collineation2 fp_16068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16068.

Lemma collineation_16069 : is_collineation2 fp_16069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16069.

Lemma collineation_16070 : is_collineation2 fp_16070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16070.

Lemma collineation_16071 : is_collineation2 fp_16071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16071.

Lemma collineation_16072 : is_collineation2 fp_16072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16072.

Lemma collineation_16073 : is_collineation2 fp_16073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16073.

Lemma collineation_16074 : is_collineation2 fp_16074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16074.

Lemma collineation_16075 : is_collineation2 fp_16075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16075.

Lemma collineation_16076 : is_collineation2 fp_16076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16076.

Lemma collineation_16077 : is_collineation2 fp_16077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16077.

Lemma collineation_16078 : is_collineation2 fp_16078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16078.

Lemma collineation_16079 : is_collineation2 fp_16079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16079.

Lemma collineation_16080 : is_collineation2 fp_16080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16080.

Lemma collineation_16081 : is_collineation2 fp_16081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16081.

Lemma collineation_16082 : is_collineation2 fp_16082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16082.

Lemma collineation_16083 : is_collineation2 fp_16083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16083.

Lemma collineation_16084 : is_collineation2 fp_16084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16084.

Lemma collineation_16085 : is_collineation2 fp_16085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16085.

Lemma collineation_16086 : is_collineation2 fp_16086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16086.

Lemma collineation_16087 : is_collineation2 fp_16087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16087.

Lemma collineation_16088 : is_collineation2 fp_16088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16088.

Lemma collineation_16089 : is_collineation2 fp_16089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16089.

Lemma collineation_16090 : is_collineation2 fp_16090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16090.

Lemma collineation_16091 : is_collineation2 fp_16091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16091.

Lemma collineation_16092 : is_collineation2 fp_16092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16092.

Lemma collineation_16093 : is_collineation2 fp_16093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16093.

Lemma collineation_16094 : is_collineation2 fp_16094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16094.

Lemma collineation_16095 : is_collineation2 fp_16095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16095.

Lemma collineation_16096 : is_collineation2 fp_16096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16096.

Lemma collineation_16097 : is_collineation2 fp_16097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16097.

Lemma collineation_16098 : is_collineation2 fp_16098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16098.

Lemma collineation_16099 : is_collineation2 fp_16099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16099.

Lemma collineation_16100 : is_collineation2 fp_16100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16100.

Lemma collineation_16101 : is_collineation2 fp_16101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16101.

Lemma collineation_16102 : is_collineation2 fp_16102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16102.

Lemma collineation_16103 : is_collineation2 fp_16103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16103.

Lemma collineation_16104 : is_collineation2 fp_16104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16104.

Lemma collineation_16105 : is_collineation2 fp_16105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16105.

Lemma collineation_16106 : is_collineation2 fp_16106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16106.

Lemma collineation_16107 : is_collineation2 fp_16107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16107.

Lemma collineation_16108 : is_collineation2 fp_16108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16108.

Lemma collineation_16109 : is_collineation2 fp_16109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16109.

Lemma collineation_16110 : is_collineation2 fp_16110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16110.

Lemma collineation_16111 : is_collineation2 fp_16111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16111.

Lemma collineation_16112 : is_collineation2 fp_16112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16112.

Lemma collineation_16113 : is_collineation2 fp_16113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16113.

Lemma collineation_16114 : is_collineation2 fp_16114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16114.

Lemma collineation_16115 : is_collineation2 fp_16115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16115.

Lemma collineation_16116 : is_collineation2 fp_16116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16116.

Lemma collineation_16117 : is_collineation2 fp_16117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16117.

Lemma collineation_16118 : is_collineation2 fp_16118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16118.

Lemma collineation_16119 : is_collineation2 fp_16119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16119.

Lemma collineation_16120 : is_collineation2 fp_16120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16120.

Lemma collineation_16121 : is_collineation2 fp_16121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16121.

Lemma collineation_16122 : is_collineation2 fp_16122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16122.

Lemma collineation_16123 : is_collineation2 fp_16123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16123.

Lemma collineation_16124 : is_collineation2 fp_16124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16124.

Lemma collineation_16125 : is_collineation2 fp_16125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16125.

Lemma collineation_16126 : is_collineation2 fp_16126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16126.

Lemma collineation_16127 : is_collineation2 fp_16127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_16127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_16127.

Lemma is_col_all_c154 : forall fp, In fp (all_c154++all_c155++all_c156++all_c157++all_c158++all_c159++all_c160++all_c161++all_c162++all_c163++all_c164++all_c165++all_c166++all_c167) -> is_collineation2 fp.
Proof.
 intros fp HIn_S.
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14815 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_14999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15127 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15128 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15129 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15130 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15131 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15132 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15133 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15134 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15135 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15136 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15137 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15138 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15139 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15140 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15141 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15142 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15143 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15144 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15145 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15146 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15147 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15148 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15149 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15150 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15151 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15152 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15153 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15154 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15155 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15156 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15157 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15158 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15159 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15160 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15161 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15162 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15163 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15164 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15165 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15166 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15167 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15168 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15169 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15170 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15171 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15172 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15173 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15174 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15175 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15176 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15177 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15178 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15179 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15180 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15181 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15182 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15183 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15184 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15185 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15186 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15187 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15188 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15189 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15190 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15191 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15192 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15193 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15194 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15195 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15196 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15197 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15198 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15199 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15200 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15201 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15202 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15203 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15204 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15205 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15206 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15207 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15208 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15209 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15210 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15211 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15212 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15213 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15214 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15215 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15216 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15217 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15218 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15219 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15220 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15221 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15222 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15223 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15224 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15225 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15226 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15227 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15228 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15229 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15230 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15231 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15232 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15233 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15234 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15235 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15236 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15237 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15238 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15239 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15240 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15241 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15242 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15243 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15244 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15245 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15246 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15247 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15248 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15249 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15250 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15251 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15252 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15253 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15254 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15255 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15256 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15257 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15258 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15259 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15260 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15261 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15262 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15263 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15264 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15265 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15266 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15267 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15268 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15269 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15270 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15271 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15272 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15273 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15274 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15275 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15276 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15277 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15278 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15279 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15280 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15281 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15282 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15283 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15284 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15285 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15286 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15287 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15288 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15289 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15290 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15291 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15292 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15293 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15294 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15295 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15296 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15297 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15298 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15299 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15300 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15301 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15302 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15303 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15304 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15305 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15306 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15307 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15308 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15309 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15310 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15311 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15312 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15313 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15314 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15315 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15316 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15317 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15318 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15319 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15320 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15321 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15322 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15323 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15324 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15325 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15326 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15327 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15328 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15329 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15330 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15331 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15332 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15333 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15334 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15335 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15336 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15337 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15338 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15339 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15340 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15341 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15342 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15343 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15344 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15345 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15346 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15347 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15348 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15349 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15350 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15351 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15352 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15353 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15354 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15355 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15356 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15357 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15358 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15359 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15360 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15361 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15362 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15363 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15364 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15365 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15366 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15367 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15368 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15369 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15370 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15371 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15372 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15373 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15374 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15375 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15376 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15377 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15378 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15379 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15380 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15381 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15382 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15383 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15384 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15385 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15386 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15387 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15388 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15389 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15390 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15391 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15392 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15393 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15394 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15395 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15396 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15397 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15398 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15399 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15400 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15401 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15402 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15403 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15404 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15405 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15406 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15407 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15408 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15409 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15410 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15411 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15412 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15413 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15414 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15415 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15416 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15417 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15418 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15419 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15420 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15421 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15422 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15423 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15424 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15425 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15426 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15427 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15428 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15429 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15430 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15431 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15432 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15433 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15434 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15435 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15436 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15437 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15438 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15439 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15440 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15441 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15442 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15443 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15444 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15445 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15446 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15447 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15448 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15449 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15450 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15451 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15452 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15453 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15454 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15455 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15456 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15457 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15458 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15459 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15460 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15461 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15462 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15463 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15464 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15465 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15466 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15467 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15468 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15469 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15470 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15471 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15687 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15688 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15689 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15690 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15691 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15692 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15693 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15694 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15695 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15696 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15697 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15698 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15699 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15700 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15701 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15702 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15703 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15704 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15705 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15706 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15707 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15708 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15709 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15710 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15711 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15712 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15713 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15714 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15715 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15716 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15717 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15718 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15719 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15720 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15721 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15722 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15723 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15724 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15725 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15726 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15727 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15728 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15729 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15730 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15731 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15732 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15733 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15734 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15735 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15736 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15737 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15738 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15739 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15740 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15741 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15742 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15743 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15744 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15745 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15746 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15747 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15748 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15749 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15750 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15751 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15752 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15753 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15754 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15755 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15756 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15757 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15758 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15759 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15760 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15761 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15762 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15763 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15764 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15765 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15766 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15767 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15768 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15769 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15770 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15771 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15772 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15773 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15774 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15775 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15776 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15777 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15778 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15779 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15780 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15781 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15782 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15783 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15815 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_15999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_16127 | idtac].
 destruct (in_nil HIn_S).
Qed.

