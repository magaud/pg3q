Require Import ssreflect ssrfun ssrbool.

Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads.
Require Import PG32.pg32_spreads_collineations.
Require Import PG32.pg32_spreads_packings.
Require Import PG32.pg32_packings_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_is_col_fp.
Require Import PG32.pg32_decomp_prelude.

Require Import List.
Import ListNotations.

Lemma is_collineations_descr_B_P10_P0 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P0 -> In fp all_c140.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P1 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P1 -> In fp all_c141.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.
 
Lemma is_collineations_descr_B_P10_P2 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P2 -> In fp all_c142.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P3 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P3 -> In fp all_c143.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P4 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P4 -> In fp all_c144.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P5 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P5 -> In fp all_c145.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P6 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P6 -> In fp all_c146.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P7 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P7 -> In fp all_c147.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P8 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P8 -> In fp all_c148.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P9 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P9 -> In fp all_c149.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P11 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P11 -> In fp all_c150.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P12 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P12 -> In fp all_c151.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P13 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P13 -> In fp all_c152.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10_P14 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> fp P1 = P14 -> In fp all_c153.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P10 :
  forall fp, is_collineation2 fp -> fp P0 = P10 -> In fp (all_c140++all_c141++all_c142++all_c143++all_c144++all_c145++all_c146++all_c147++all_c148++all_c149++all_c150++all_c151++all_c152++all_c153).
Proof.
  intros fp Hfpfl HP0.
  repeat rewrite in_app_iff.
  case_eq (fp P1); intros HP1;
    try (apply False_ind; match goal with H:?fp ?P = ?Q, H':?fp ?R = ?Q |- _ =>
                                          eapply (is_c_diffP fp Hfpfl P R Q); solve [assumption | discriminate] end).
  left; apply is_collineations_descr_B_P10_P0; assumption.
  do 1 right; left; apply is_collineations_descr_B_P10_P1; assumption.
  do 2 right; left; apply is_collineations_descr_B_P10_P2; assumption.
  do 3 right; left; apply is_collineations_descr_B_P10_P3; assumption.
  do 4 right; left; apply is_collineations_descr_B_P10_P4; assumption.
  do 5 right; left; apply is_collineations_descr_B_P10_P5; assumption.
  do 6 right; left; apply is_collineations_descr_B_P10_P6; assumption.
  do 7 right; left; apply is_collineations_descr_B_P10_P7; assumption.
  do 8 right; left; apply is_collineations_descr_B_P10_P8; assumption.
  do 9 right; left; apply is_collineations_descr_B_P10_P9; assumption.
  do 10 right; left; apply is_collineations_descr_B_P10_P11; assumption.
  do 11 right; left; apply is_collineations_descr_B_P10_P12; assumption.
  do 12 right; left; apply is_collineations_descr_B_P10_P13; assumption.
  do 13 right; apply is_collineations_descr_B_P10_P14; assumption.
Qed.
  
Check is_collineations_descr_B_P10.
