Require Import ssreflect ssrfun ssrbool.

Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads.
Require Import PG32.pg32_spreads_collineations.
Require Import PG32.pg32_spreads_packings.
Require Import PG32.pg32_packings_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_is_col_fp.
Require Import PG32.pg32_decomp_prelude.

Require Import List.
Import ListNotations.

Lemma is_collineations_descr_B_P14_P0 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P0 -> In fp all_c196.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P1 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P1 -> In fp all_c197.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.
 
Lemma is_collineations_descr_B_P14_P2 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P2 -> In fp all_c198.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P3 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P3 -> In fp all_c199.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P4 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P4 -> In fp all_c200.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P5 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P5 -> In fp all_c201.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P6 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P6 -> In fp all_c202.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P7 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P7 -> In fp all_c203.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P8 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P8 -> In fp all_c204.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P9 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P9 -> In fp all_c205.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P10 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P10 -> In fp all_c206.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P11 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P11 -> In fp all_c207.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P12 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P12 -> In fp all_c208.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14_P13 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> fp P1 = P13 -> In fp all_c209.
Proof.
  intros fp Hfpfl HP0 HP1; build_hyps fp Hfpfl HP0 HP1.
  par: in_list HP0 HP1 HP2 HP3 HP4 HP5 HP6 HP7 HP8 HP9 HP10 HP11 HP12 HP13 HP14.
Qed.

Lemma is_collineations_descr_B_P14 :
  forall fp, is_collineation2 fp -> fp P0 = P14 -> In fp (all_c196++all_c197++all_c198++all_c199++all_c200++all_c201++all_c202++all_c203++all_c204++all_c205++all_c206++all_c207++all_c208++all_c209).
Proof.
  intros fp Hfpfl HP0.
  repeat rewrite in_app_iff.
  case_eq (fp P1); intros HP1;
    try (apply False_ind; match goal with H:?fp ?P = ?Q, H':?fp ?R = ?Q |- _ =>
                                          eapply (is_c_diffP fp Hfpfl P R Q); solve [assumption | discriminate] end).
  left; apply is_collineations_descr_B_P14_P0; assumption.
  do 1 right; left; apply is_collineations_descr_B_P14_P1; assumption.
  do 2 right; left; apply is_collineations_descr_B_P14_P2; assumption.
  do 3 right; left; apply is_collineations_descr_B_P14_P3; assumption.
  do 4 right; left; apply is_collineations_descr_B_P14_P4; assumption.
  do 5 right; left; apply is_collineations_descr_B_P14_P5; assumption.
  do 6 right; left; apply is_collineations_descr_B_P14_P6; assumption.
  do 7 right; left; apply is_collineations_descr_B_P14_P7; assumption.
  do 8 right; left; apply is_collineations_descr_B_P14_P8; assumption.
  do 9 right; left; apply is_collineations_descr_B_P14_P9; assumption.
  do 10 right; left; apply is_collineations_descr_B_P14_P10; assumption.
  do 11 right; left; apply is_collineations_descr_B_P14_P11; assumption.
  do 12 right; left; apply is_collineations_descr_B_P14_P12; assumption.
  do 13 right; apply is_collineations_descr_B_P14_P13; assumption.
Qed.
  
Check is_collineations_descr_B_P14.
