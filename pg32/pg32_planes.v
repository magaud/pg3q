Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG32.pg32_inductive PG32.pg32_proofs.
Require Import PG32.pg32_spreads.

Require Import List.
Import ListNotations.

Definition all_points := [P0; P1; P2; P3; P4; P5; P6; P7; P8; P9; P10;P11; P12; P13; P14].

Definition all_lines := [L0; L1; L2; L3; L4; L5 ; L6; L7; L8; L9; L10; L11; L12; L13; L14; L15; L16; L17; L18; L19; L20; L21; L22; L23; L24; L25; L26; L27; L28 ; L29; L30; L31; L32; L33; L34].

Definition lines_through_point (x:Point) := filter (fun (l:Line) => incid_lp x l) all_lines. 

Definition points_incident_to_line (l:Line) := filter (fun (x:Point) => incid_lp x l) all_points.

Lemma a_point_is_incident_to_7_lines : forall x, length (lines_through_point x) = 7.
Proof.
destruct x; apply erefl.
Qed.

Lemma a_line_contains_3_points : forall l, length (points_incident_to_line l) = 3.
Proof.
  destruct l; apply erefl.
Qed.

(*
Definition exists_Line (f:Line -> bool) : bool :=
  f L0 || f L1 || f L2 || f L3 || f L4 || f L5 || f L6 || f L7 || f L8 || f L9 ||
  f L10 || f L11 || f L12 || f L13 || f L14 || f L15 || f L16 || f L17 || f L18 || f L19 || 
  f L20 || f L21 || f L22 || f L23 || f L24 || f L25 || f L26 || f L27 || f L28 || f L29 ||
  f L30 || f L31 || f L32 || f L33 || f L34.

Lemma exists_exists_Line : forall P:Line->bool, (exists p:Line, P p) <-> exists_Line P.
Proof.
  intros P; split.
  intros Hexists; unfold exists_Line; repeat (rewrite or_bool).
  destruct Hexists as [x Hx]; destruct x; solve [intuition].
  unfold exists_Line; repeat (rewrite or_bool); intros Hexists.
  par:firstorder.
Qed.  
*)

Definition inter (l1 l2 : Line) : list Point :=
  List.filter (fun n => List.existsb (eqP n) (all_points_of_line l2)) (all_points_of_line l1).

Definition intersect (l1:Line) (l2:Line) := match (inter l1 l2) with [] => false | _ => true end.

Definition plane (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2) :=
  let (xy1,z1) := points_from_line l1 in
  let (x1,y1) := xy1 in
  let (xy2,z2) := points_from_line l2 in
  let (x2,y2) := xy2 in
  let i := match (inter l1 l2) with [a] => a | _ => P0 end in
  
  filter (fun (y:Point) => incid_lp y l1 || incid_lp y l2 ||
                           if (Point_dec i x1) then
                             incid_lp y (l_from_points y1 y2) || incid_lp y (l_from_points z1 z2) ||
                             incid_lp y (l_from_points y1 z2) || incid_lp y (l_from_points y2 z1)
                           else if (Point_dec i y1) then 
                                  incid_lp y (l_from_points z1 y2) || incid_lp y (l_from_points x1 z2) ||
                                  incid_lp y (l_from_points x1 y2) || incid_lp y (l_from_points z1 z2)
                                else
                                  if (Point_dec i z1) then 
                                    incid_lp y (l_from_points y1 y2) || incid_lp y (l_from_points x1 z2) ||
                                    incid_lp y (l_from_points x1 y2) || incid_lp y (l_from_points y1 z2)
                                  else false) all_points.

Lemma a_plane_has_7_points : forall  (l1 : Line) (l2 : Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2), length (plane l1 l2 Hdiff Hinter) = 7.
Proof.
  intros l1 l2; destruct l1; destruct l2; intros Hdiff Hinter; try solve [(apply False_ind; apply Hdiff; apply erefl) | apply (degen_bool _ Hinter) | apply erefl].
Qed.

Definition list_planes := [
                           [P0; P1; P2; P3; P4; P5; P6]; 
                           [P0; P1; P2; P7; P8; P9; P10];
                           [P0; P1; P2; P11; P12; P13; P14];
                           [P0; P3; P4; P7; P8; P11; P12];
                           [P0; P3; P4; P9; P10; P13; P14];
                           [P0; P5; P6; P7; P8; P13; P14];
                           [P0; P5; P6; P9; P10; P11; P12];
                           [P1; P3; P5; P7; P9; P11; P13];
                           [P1; P3; P5; P8; P10; P12; P14];
                           [P1; P4; P6; P8; P10; P11; P13];
                           [P1; P4; P6; P7; P9; P12; P14];
                           [P2; P3; P6; P7; P10; P11; P14];
                           [P2; P3; P6; P8; P9; P12; P13];
                           [P2; P4; P5; P7; P10; P12; P13];
                           [P2; P4; P5; P8; P9; P11; P14]
                         ].

Lemma plane_equiv_1 : forall (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2),
    In (plane l1 l2 Hdiff Hinter) list_planes.
Proof.
  intros l1 l2; destruct l1; destruct l2; intros; solve [(apply False_ind; apply Hdiff; apply erefl) | apply (degen_bool _ Hinter) | time (unfold  plane; simpl; intuition)]. 
Qed.

Ltac witnesses l m := let Hdiff := fresh  in assert (Hdiff:l<>m) by discriminate;
                      let Hinter := fresh in assert(Hinter:intersect l m) by apply erefl;
                             exists l; exists m; exists Hdiff; exists Hinter; unfold plane; simpl; apply erefl.

Lemma plane_equiv_2 : forall l:list Point,
    In l list_planes ->
    exists l1, exists l2, exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  unfold list_planes; intros.
  repeat (match goal with H: In l (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x l xs H); clear H; intros H'; destruct H' end).
1-15:match goal with H: [?x0; ?x1; ?x2; ?x3; ?x4; ?x5; ?x6] = ?l |- _ => rewrite <- H; witnesses (l_from_points x0 x1) (l_from_points x5 x6) end.
apply False_ind; eapply in_nil; eassumption.
Qed.
  
Lemma plane_equiv : forall l:list Point,
    In l list_planes <-> exists l1, exists l2,
        exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  intros; split.
  apply plane_equiv_2.
  intros [l1 [l2 [Hdiff [Hinter H]]]]; rewrite H.
  apply plane_equiv_1.
Qed.

Definition belongs p l := if (in_dec Point_dec p l) then true else false.

Lemma a_point_belongs_to_7_planes :
  forall p:Point, 
    length (filter (fun (l:list Point) => belongs p l) list_planes) = 7.
Proof.
intros p; destruct p; apply erefl.
Qed.

Definition lines_of_plane (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2) :=
  filter (fun (l:Line) =>
            let (xy,z) := points_from_line l in
            let (x,y) := xy in belongs x (plane l1 l2 Hdiff Hinter) && belongs y (plane l1 l2 Hdiff Hinter) && belongs z (plane l1 l2 Hdiff Hinter)) all_lines.

Lemma a_plane_has_7_lines : forall (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2),
    length (lines_of_plane l1 l2 Hdiff Hinter) = 7.
Proof.
intros l1 l2; destruct l1; destruct l2; intros; try solve [(apply False_ind; apply Hdiff; apply erefl) | apply (degen_bool _ Hinter) | unfold lines_of_plane; simpl; apply erefl]. 
Qed.

Lemma a_line_belongs_to_exactly_3_planes : forall l:Line,
    length (filter (fun (p:list Point) => let (xy,z) := points_from_line l in
            let (x,y) := xy in belongs x p && belongs y p && belongs z p) list_planes) = 3.
Proof.
  intros l; destruct l; intros; unfold list_planes; simpl; apply erefl.
Qed.

Ltac ex_l t := solve [
                   exists L0; t | exists L1; t | exists L2; t | exists L3; t | exists L4; t | 
                          exists L5; t | exists L6; t | exists L7; t | exists L8; t | exists L9; t |
                          exists L10; t | exists L11; t | exists L12; t | exists L13; t | exists L14; t |
                          exists L15; t | exists L16; t | exists L17; t | exists L18; t | exists L19; t |
                          exists L20; t | exists L21; t | exists L22; t | exists L23; t | exists L24; t | 
                          exists L25; t | exists L26; t | exists L27; t | exists L28; t | exists L29; t |
                          exists L30; t | exists L31; t | exists L32; t | exists L33; t | exists L34; t ].

Lemma two_distinct_planes_intersect_along_a_line :
  forall p1 p2, In p1 list_planes -> In p2 list_planes -> p1<>p2 ->
                exists l:Line,
                  let (xy,z) := points_from_line l in
                  let (x,y) := xy in belongs x p1 && belongs y p1 && belongs z p1 &&
                                     belongs x p2 && belongs y p2 && belongs z p2.
Proof.
  unfold list_planes; intros p1 p2 Hp1 Hp2 Hp1p2.
repeat (match goal with H: In p1 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p1 xs H); clear H; intros H'; destruct H' end);
  repeat (match goal with H: In p2 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p2 xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption | rewrite H in H0; apply False_ind; apply Hp1p2; assumption]; rewrite <- H; clear H; rewrite <- H0; clear H0.
par: ex_l ltac:(simpl; apply erefl).
Qed.

Lemma and_only_one_line :
  forall p1 p2, In p1 list_planes -> In p2 list_planes -> p1<>p2 ->
                forall l l', 
                  (let (xy,z) := points_from_line l in
                  let (x,y) := xy in belongs x p1 && belongs y p1 && belongs z p1 &&
                                     belongs x p2 && belongs y p2 && belongs z p2) ->

                  (let (xy,z) := points_from_line l' in
                  let (x,y) := xy in belongs x p1 && belongs y p1 && belongs z p1 &&
                                     belongs x p2 && belongs y p2 && belongs z p2)

   -> l=l'.
Proof.
  unfold list_planes; intros.
repeat (match goal with H: In p1 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p1 xs H); clear H; intros H'; destruct H' end);
  repeat (match goal with H: In p2 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p2 xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption | rewrite H in H0; apply False_ind; apply H1; assumption]; rewrite <- H in H2,H3; rewrite <- H0 in H2,H3.
par: time(destruct l; destruct l'; try solve [apply erefl | apply (degen_bool _ H2) | apply (degen_bool _ H3)]).
Qed.

Lemma incid_lp_points : forall p l, incid_lp p l -> (let (xy,z) := points_from_line l in
                  let (x,y) := xy in p=x \/ p=y \/ p=z).
Proof.
intros p l Hincid; destruct l.
par:simpl; destruct p; try solve [apply (degen_bool _ Hincid) | tauto].
Qed.

Lemma plane_line :
  forall s:list Point,
    In s list_planes ->
    forall l:Line, (exists p:Point, incid_lp p l && negb (belongs p s)) -> exists q:Point, incid_lp q l && (belongs q s).
Proof.
unfold list_planes; intros.
repeat (match goal with H: In s (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x s xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption];
  rewrite <- H in H0; rewrite <- H; clear H; destruct H0 as [r Hr].
par:revert Hr; rewrite and_bool; intros Hr; destruct Hr as [Hir Hbr];
generalize (incid_lp_points r l Hir); intros Hi; destruct l;simpl in Hi; intuition; subst;
  try solve [  apply (degen_bool _ Hbr) | apply (degen_bool _ Hir) | match goal with |- exists q : Point, is_true ((incid_lp q ?l) && _) =>
solve [exists (fst (fst (points_from_line l))); apply erefl | exists (snd (fst (points_from_line l))); apply erefl | exists (snd (points_from_line l)); apply erefl] end].
Qed.

Lemma plane_line_unique :
    forall s:list Point,
    In s list_planes ->
    forall l:Line,  (exists p:Point, incid_lp p l && negb (belongs p s)) ->
                    forall (p q:Point), incid_lp p l && (belongs p s) -> incid_lp q l && (belongs q s) -> p=q.
Proof.
unfold list_planes; intros.
repeat (match goal with H: In s (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x s xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption]; rewrite <-H in H0,H1,H2; clear H.
par: time (destruct H0 as [r Hr];
revert H1 H2 Hr; do 3 rewrite and_bool; intros [H1 H1'] [H2 H2'] [Hr Hr'];generalize (incid_lp_points p l H1);
generalize (incid_lp_points q l H2);
generalize (incid_lp_points r l Hr); intros Hip Hiq Hir; destruct l; simpl in Hip, Hiq, Hir; intuition; subst; 
  solve [apply erefl | apply (degen_bool _ H1) | apply (degen_bool _ H1') 
             | apply (degen_bool _ H2) |  apply (degen_bool _ H2')
             | apply (degen_bool _ Hr) |  apply (degen_bool _ Hr')]).
Qed.

(*
Each point is contained in 7 lines and 7 planes 
Each line is contained in 3 planes and contains 3 points
Each plane contains 7 points and 7 lines

Each plane is isomorphic to the Fano plane
Every pair of distinct planes intersect in a line
A line and a plane not containing the line intersect in exactly one point
*)
