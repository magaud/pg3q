Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG32.pg32_inductive PG32.pg32_spreads_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_automorphisms_inv.
Require Import PG32.pg32_is_col_fp.

Require Import Lists.List.
Import ListNotations.
Require Import Arith.

Lemma collineation_17472 : is_collineation2 fp_17472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17472.

Lemma collineation_17473 : is_collineation2 fp_17473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17473.

Lemma collineation_17474 : is_collineation2 fp_17474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17474.

Lemma collineation_17475 : is_collineation2 fp_17475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17475.

Lemma collineation_17476 : is_collineation2 fp_17476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17476.

Lemma collineation_17477 : is_collineation2 fp_17477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17477.

Lemma collineation_17478 : is_collineation2 fp_17478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17478.

Lemma collineation_17479 : is_collineation2 fp_17479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17479.

Lemma collineation_17480 : is_collineation2 fp_17480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17480.

Lemma collineation_17481 : is_collineation2 fp_17481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17481.

Lemma collineation_17482 : is_collineation2 fp_17482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17482.

Lemma collineation_17483 : is_collineation2 fp_17483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17483.

Lemma collineation_17484 : is_collineation2 fp_17484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17484.

Lemma collineation_17485 : is_collineation2 fp_17485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17485.

Lemma collineation_17486 : is_collineation2 fp_17486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17486.

Lemma collineation_17487 : is_collineation2 fp_17487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17487.

Lemma collineation_17488 : is_collineation2 fp_17488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17488.

Lemma collineation_17489 : is_collineation2 fp_17489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17489.

Lemma collineation_17490 : is_collineation2 fp_17490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17490.

Lemma collineation_17491 : is_collineation2 fp_17491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17491.

Lemma collineation_17492 : is_collineation2 fp_17492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17492.

Lemma collineation_17493 : is_collineation2 fp_17493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17493.

Lemma collineation_17494 : is_collineation2 fp_17494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17494.

Lemma collineation_17495 : is_collineation2 fp_17495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17495.

Lemma collineation_17496 : is_collineation2 fp_17496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17496.

Lemma collineation_17497 : is_collineation2 fp_17497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17497.

Lemma collineation_17498 : is_collineation2 fp_17498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17498.

Lemma collineation_17499 : is_collineation2 fp_17499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17499.

Lemma collineation_17500 : is_collineation2 fp_17500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17500.

Lemma collineation_17501 : is_collineation2 fp_17501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17501.

Lemma collineation_17502 : is_collineation2 fp_17502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17502.

Lemma collineation_17503 : is_collineation2 fp_17503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17503.

Lemma collineation_17504 : is_collineation2 fp_17504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17504.

Lemma collineation_17505 : is_collineation2 fp_17505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17505.

Lemma collineation_17506 : is_collineation2 fp_17506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17506.

Lemma collineation_17507 : is_collineation2 fp_17507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17507.

Lemma collineation_17508 : is_collineation2 fp_17508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17508.

Lemma collineation_17509 : is_collineation2 fp_17509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17509.

Lemma collineation_17510 : is_collineation2 fp_17510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17510.

Lemma collineation_17511 : is_collineation2 fp_17511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17511.

Lemma collineation_17512 : is_collineation2 fp_17512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17512.

Lemma collineation_17513 : is_collineation2 fp_17513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17513.

Lemma collineation_17514 : is_collineation2 fp_17514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17514.

Lemma collineation_17515 : is_collineation2 fp_17515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17515.

Lemma collineation_17516 : is_collineation2 fp_17516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17516.

Lemma collineation_17517 : is_collineation2 fp_17517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17517.

Lemma collineation_17518 : is_collineation2 fp_17518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17518.

Lemma collineation_17519 : is_collineation2 fp_17519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17519.

Lemma collineation_17520 : is_collineation2 fp_17520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17520.

Lemma collineation_17521 : is_collineation2 fp_17521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17521.

Lemma collineation_17522 : is_collineation2 fp_17522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17522.

Lemma collineation_17523 : is_collineation2 fp_17523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17523.

Lemma collineation_17524 : is_collineation2 fp_17524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17524.

Lemma collineation_17525 : is_collineation2 fp_17525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17525.

Lemma collineation_17526 : is_collineation2 fp_17526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17526.

Lemma collineation_17527 : is_collineation2 fp_17527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17527.

Lemma collineation_17528 : is_collineation2 fp_17528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17528.

Lemma collineation_17529 : is_collineation2 fp_17529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17529.

Lemma collineation_17530 : is_collineation2 fp_17530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17530.

Lemma collineation_17531 : is_collineation2 fp_17531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17531.

Lemma collineation_17532 : is_collineation2 fp_17532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17532.

Lemma collineation_17533 : is_collineation2 fp_17533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17533.

Lemma collineation_17534 : is_collineation2 fp_17534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17534.

Lemma collineation_17535 : is_collineation2 fp_17535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17535.

Lemma collineation_17536 : is_collineation2 fp_17536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17536.

Lemma collineation_17537 : is_collineation2 fp_17537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17537.

Lemma collineation_17538 : is_collineation2 fp_17538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17538.

Lemma collineation_17539 : is_collineation2 fp_17539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17539.

Lemma collineation_17540 : is_collineation2 fp_17540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17540.

Lemma collineation_17541 : is_collineation2 fp_17541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17541.

Lemma collineation_17542 : is_collineation2 fp_17542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17542.

Lemma collineation_17543 : is_collineation2 fp_17543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17543.

Lemma collineation_17544 : is_collineation2 fp_17544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17544.

Lemma collineation_17545 : is_collineation2 fp_17545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17545.

Lemma collineation_17546 : is_collineation2 fp_17546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17546.

Lemma collineation_17547 : is_collineation2 fp_17547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17547.

Lemma collineation_17548 : is_collineation2 fp_17548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17548.

Lemma collineation_17549 : is_collineation2 fp_17549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17549.

Lemma collineation_17550 : is_collineation2 fp_17550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17550.

Lemma collineation_17551 : is_collineation2 fp_17551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17551.

Lemma collineation_17552 : is_collineation2 fp_17552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17552.

Lemma collineation_17553 : is_collineation2 fp_17553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17553.

Lemma collineation_17554 : is_collineation2 fp_17554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17554.

Lemma collineation_17555 : is_collineation2 fp_17555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17555.

Lemma collineation_17556 : is_collineation2 fp_17556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17556.

Lemma collineation_17557 : is_collineation2 fp_17557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17557.

Lemma collineation_17558 : is_collineation2 fp_17558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17558.

Lemma collineation_17559 : is_collineation2 fp_17559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17559.

Lemma collineation_17560 : is_collineation2 fp_17560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17560.

Lemma collineation_17561 : is_collineation2 fp_17561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17561.

Lemma collineation_17562 : is_collineation2 fp_17562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17562.

Lemma collineation_17563 : is_collineation2 fp_17563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17563.

Lemma collineation_17564 : is_collineation2 fp_17564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17564.

Lemma collineation_17565 : is_collineation2 fp_17565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17565.

Lemma collineation_17566 : is_collineation2 fp_17566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17566.

Lemma collineation_17567 : is_collineation2 fp_17567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17567.

Lemma collineation_17568 : is_collineation2 fp_17568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17568.

Lemma collineation_17569 : is_collineation2 fp_17569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17569.

Lemma collineation_17570 : is_collineation2 fp_17570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17570.

Lemma collineation_17571 : is_collineation2 fp_17571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17571.

Lemma collineation_17572 : is_collineation2 fp_17572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17572.

Lemma collineation_17573 : is_collineation2 fp_17573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17573.

Lemma collineation_17574 : is_collineation2 fp_17574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17574.

Lemma collineation_17575 : is_collineation2 fp_17575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17575.

Lemma collineation_17576 : is_collineation2 fp_17576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17576.

Lemma collineation_17577 : is_collineation2 fp_17577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17577.

Lemma collineation_17578 : is_collineation2 fp_17578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17578.

Lemma collineation_17579 : is_collineation2 fp_17579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17579.

Lemma collineation_17580 : is_collineation2 fp_17580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17580.

Lemma collineation_17581 : is_collineation2 fp_17581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17581.

Lemma collineation_17582 : is_collineation2 fp_17582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17582.

Lemma collineation_17583 : is_collineation2 fp_17583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17583.

Lemma collineation_17584 : is_collineation2 fp_17584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17584.

Lemma collineation_17585 : is_collineation2 fp_17585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17585.

Lemma collineation_17586 : is_collineation2 fp_17586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17586.

Lemma collineation_17587 : is_collineation2 fp_17587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17587.

Lemma collineation_17588 : is_collineation2 fp_17588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17588.

Lemma collineation_17589 : is_collineation2 fp_17589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17589.

Lemma collineation_17590 : is_collineation2 fp_17590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17590.

Lemma collineation_17591 : is_collineation2 fp_17591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17591.

Lemma collineation_17592 : is_collineation2 fp_17592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17592.

Lemma collineation_17593 : is_collineation2 fp_17593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17593.

Lemma collineation_17594 : is_collineation2 fp_17594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17594.

Lemma collineation_17595 : is_collineation2 fp_17595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17595.

Lemma collineation_17596 : is_collineation2 fp_17596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17596.

Lemma collineation_17597 : is_collineation2 fp_17597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17597.

Lemma collineation_17598 : is_collineation2 fp_17598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17598.

Lemma collineation_17599 : is_collineation2 fp_17599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17599.

Lemma collineation_17600 : is_collineation2 fp_17600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17600.

Lemma collineation_17601 : is_collineation2 fp_17601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17601.

Lemma collineation_17602 : is_collineation2 fp_17602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17602.

Lemma collineation_17603 : is_collineation2 fp_17603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17603.

Lemma collineation_17604 : is_collineation2 fp_17604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17604.

Lemma collineation_17605 : is_collineation2 fp_17605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17605.

Lemma collineation_17606 : is_collineation2 fp_17606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17606.

Lemma collineation_17607 : is_collineation2 fp_17607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17607.

Lemma collineation_17608 : is_collineation2 fp_17608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17608.

Lemma collineation_17609 : is_collineation2 fp_17609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17609.

Lemma collineation_17610 : is_collineation2 fp_17610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17610.

Lemma collineation_17611 : is_collineation2 fp_17611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17611.

Lemma collineation_17612 : is_collineation2 fp_17612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17612.

Lemma collineation_17613 : is_collineation2 fp_17613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17613.

Lemma collineation_17614 : is_collineation2 fp_17614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17614.

Lemma collineation_17615 : is_collineation2 fp_17615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17615.

Lemma collineation_17616 : is_collineation2 fp_17616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17616.

Lemma collineation_17617 : is_collineation2 fp_17617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17617.

Lemma collineation_17618 : is_collineation2 fp_17618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17618.

Lemma collineation_17619 : is_collineation2 fp_17619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17619.

Lemma collineation_17620 : is_collineation2 fp_17620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17620.

Lemma collineation_17621 : is_collineation2 fp_17621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17621.

Lemma collineation_17622 : is_collineation2 fp_17622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17622.

Lemma collineation_17623 : is_collineation2 fp_17623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17623.

Lemma collineation_17624 : is_collineation2 fp_17624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17624.

Lemma collineation_17625 : is_collineation2 fp_17625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17625.

Lemma collineation_17626 : is_collineation2 fp_17626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17626.

Lemma collineation_17627 : is_collineation2 fp_17627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17627.

Lemma collineation_17628 : is_collineation2 fp_17628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17628.

Lemma collineation_17629 : is_collineation2 fp_17629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17629.

Lemma collineation_17630 : is_collineation2 fp_17630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17630.

Lemma collineation_17631 : is_collineation2 fp_17631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17631.

Lemma collineation_17632 : is_collineation2 fp_17632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17632.

Lemma collineation_17633 : is_collineation2 fp_17633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17633.

Lemma collineation_17634 : is_collineation2 fp_17634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17634.

Lemma collineation_17635 : is_collineation2 fp_17635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17635.

Lemma collineation_17636 : is_collineation2 fp_17636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17636.

Lemma collineation_17637 : is_collineation2 fp_17637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17637.

Lemma collineation_17638 : is_collineation2 fp_17638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17638.

Lemma collineation_17639 : is_collineation2 fp_17639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17639.

Lemma collineation_17640 : is_collineation2 fp_17640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17640.

Lemma collineation_17641 : is_collineation2 fp_17641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17641.

Lemma collineation_17642 : is_collineation2 fp_17642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17642.

Lemma collineation_17643 : is_collineation2 fp_17643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17643.

Lemma collineation_17644 : is_collineation2 fp_17644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17644.

Lemma collineation_17645 : is_collineation2 fp_17645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17645.

Lemma collineation_17646 : is_collineation2 fp_17646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17646.

Lemma collineation_17647 : is_collineation2 fp_17647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17647.

Lemma collineation_17648 : is_collineation2 fp_17648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17648.

Lemma collineation_17649 : is_collineation2 fp_17649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17649.

Lemma collineation_17650 : is_collineation2 fp_17650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17650.

Lemma collineation_17651 : is_collineation2 fp_17651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17651.

Lemma collineation_17652 : is_collineation2 fp_17652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17652.

Lemma collineation_17653 : is_collineation2 fp_17653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17653.

Lemma collineation_17654 : is_collineation2 fp_17654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17654.

Lemma collineation_17655 : is_collineation2 fp_17655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17655.

Lemma collineation_17656 : is_collineation2 fp_17656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17656.

Lemma collineation_17657 : is_collineation2 fp_17657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17657.

Lemma collineation_17658 : is_collineation2 fp_17658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17658.

Lemma collineation_17659 : is_collineation2 fp_17659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17659.

Lemma collineation_17660 : is_collineation2 fp_17660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17660.

Lemma collineation_17661 : is_collineation2 fp_17661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17661.

Lemma collineation_17662 : is_collineation2 fp_17662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17662.

Lemma collineation_17663 : is_collineation2 fp_17663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17663.

Lemma collineation_17664 : is_collineation2 fp_17664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17664.

Lemma collineation_17665 : is_collineation2 fp_17665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17665.

Lemma collineation_17666 : is_collineation2 fp_17666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17666.

Lemma collineation_17667 : is_collineation2 fp_17667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17667.

Lemma collineation_17668 : is_collineation2 fp_17668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17668.

Lemma collineation_17669 : is_collineation2 fp_17669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17669.

Lemma collineation_17670 : is_collineation2 fp_17670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17670.

Lemma collineation_17671 : is_collineation2 fp_17671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17671.

Lemma collineation_17672 : is_collineation2 fp_17672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17672.

Lemma collineation_17673 : is_collineation2 fp_17673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17673.

Lemma collineation_17674 : is_collineation2 fp_17674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17674.

Lemma collineation_17675 : is_collineation2 fp_17675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17675.

Lemma collineation_17676 : is_collineation2 fp_17676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17676.

Lemma collineation_17677 : is_collineation2 fp_17677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17677.

Lemma collineation_17678 : is_collineation2 fp_17678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17678.

Lemma collineation_17679 : is_collineation2 fp_17679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17679.

Lemma collineation_17680 : is_collineation2 fp_17680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17680.

Lemma collineation_17681 : is_collineation2 fp_17681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17681.

Lemma collineation_17682 : is_collineation2 fp_17682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17682.

Lemma collineation_17683 : is_collineation2 fp_17683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17683.

Lemma collineation_17684 : is_collineation2 fp_17684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17684.

Lemma collineation_17685 : is_collineation2 fp_17685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17685.

Lemma collineation_17686 : is_collineation2 fp_17686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17686.

Lemma collineation_17687 : is_collineation2 fp_17687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17687.

Lemma collineation_17688 : is_collineation2 fp_17688.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17688 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17688.

Lemma collineation_17689 : is_collineation2 fp_17689.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17689 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17689.

Lemma collineation_17690 : is_collineation2 fp_17690.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17690 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17690.

Lemma collineation_17691 : is_collineation2 fp_17691.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17691 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17691.

Lemma collineation_17692 : is_collineation2 fp_17692.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17692 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17692.

Lemma collineation_17693 : is_collineation2 fp_17693.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17693 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17693.

Lemma collineation_17694 : is_collineation2 fp_17694.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17694 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17694.

Lemma collineation_17695 : is_collineation2 fp_17695.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17695 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17695.

Lemma collineation_17696 : is_collineation2 fp_17696.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17696 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17696.

Lemma collineation_17697 : is_collineation2 fp_17697.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17697 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17697.

Lemma collineation_17698 : is_collineation2 fp_17698.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17698 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17698.

Lemma collineation_17699 : is_collineation2 fp_17699.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17699 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17699.

Lemma collineation_17700 : is_collineation2 fp_17700.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17700 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17700.

Lemma collineation_17701 : is_collineation2 fp_17701.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17701 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17701.

Lemma collineation_17702 : is_collineation2 fp_17702.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17702 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17702.

Lemma collineation_17703 : is_collineation2 fp_17703.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17703 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17703.

Lemma collineation_17704 : is_collineation2 fp_17704.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17704 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17704.

Lemma collineation_17705 : is_collineation2 fp_17705.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17705 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17705.

Lemma collineation_17706 : is_collineation2 fp_17706.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17706 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17706.

Lemma collineation_17707 : is_collineation2 fp_17707.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17707 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17707.

Lemma collineation_17708 : is_collineation2 fp_17708.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17708 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17708.

Lemma collineation_17709 : is_collineation2 fp_17709.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17709 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17709.

Lemma collineation_17710 : is_collineation2 fp_17710.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17710 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17710.

Lemma collineation_17711 : is_collineation2 fp_17711.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17711 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17711.

Lemma collineation_17712 : is_collineation2 fp_17712.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17712 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17712.

Lemma collineation_17713 : is_collineation2 fp_17713.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17713 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17713.

Lemma collineation_17714 : is_collineation2 fp_17714.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17714 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17714.

Lemma collineation_17715 : is_collineation2 fp_17715.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17715 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17715.

Lemma collineation_17716 : is_collineation2 fp_17716.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17716 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17716.

Lemma collineation_17717 : is_collineation2 fp_17717.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17717 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17717.

Lemma collineation_17718 : is_collineation2 fp_17718.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17718 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17718.

Lemma collineation_17719 : is_collineation2 fp_17719.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17719 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17719.

Lemma collineation_17720 : is_collineation2 fp_17720.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17720 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17720.

Lemma collineation_17721 : is_collineation2 fp_17721.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17721 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17721.

Lemma collineation_17722 : is_collineation2 fp_17722.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17722 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17722.

Lemma collineation_17723 : is_collineation2 fp_17723.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17723 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17723.

Lemma collineation_17724 : is_collineation2 fp_17724.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17724 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17724.

Lemma collineation_17725 : is_collineation2 fp_17725.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17725 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17725.

Lemma collineation_17726 : is_collineation2 fp_17726.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17726 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17726.

Lemma collineation_17727 : is_collineation2 fp_17727.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17727 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17727.

Lemma collineation_17728 : is_collineation2 fp_17728.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17728 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17728.

Lemma collineation_17729 : is_collineation2 fp_17729.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17729 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17729.

Lemma collineation_17730 : is_collineation2 fp_17730.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17730 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17730.

Lemma collineation_17731 : is_collineation2 fp_17731.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17731 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17731.

Lemma collineation_17732 : is_collineation2 fp_17732.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17732 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17732.

Lemma collineation_17733 : is_collineation2 fp_17733.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17733 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17733.

Lemma collineation_17734 : is_collineation2 fp_17734.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17734 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17734.

Lemma collineation_17735 : is_collineation2 fp_17735.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17735 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17735.

Lemma collineation_17736 : is_collineation2 fp_17736.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17736 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17736.

Lemma collineation_17737 : is_collineation2 fp_17737.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17737 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17737.

Lemma collineation_17738 : is_collineation2 fp_17738.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17738 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17738.

Lemma collineation_17739 : is_collineation2 fp_17739.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17739 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17739.

Lemma collineation_17740 : is_collineation2 fp_17740.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17740 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17740.

Lemma collineation_17741 : is_collineation2 fp_17741.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17741 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17741.

Lemma collineation_17742 : is_collineation2 fp_17742.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17742 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17742.

Lemma collineation_17743 : is_collineation2 fp_17743.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17743 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17743.

Lemma collineation_17744 : is_collineation2 fp_17744.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17744 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17744.

Lemma collineation_17745 : is_collineation2 fp_17745.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17745 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17745.

Lemma collineation_17746 : is_collineation2 fp_17746.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17746 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17746.

Lemma collineation_17747 : is_collineation2 fp_17747.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17747 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17747.

Lemma collineation_17748 : is_collineation2 fp_17748.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17748 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17748.

Lemma collineation_17749 : is_collineation2 fp_17749.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17749 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17749.

Lemma collineation_17750 : is_collineation2 fp_17750.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17750 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17750.

Lemma collineation_17751 : is_collineation2 fp_17751.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17751 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17751.

Lemma collineation_17752 : is_collineation2 fp_17752.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17752 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17752.

Lemma collineation_17753 : is_collineation2 fp_17753.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17753 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17753.

Lemma collineation_17754 : is_collineation2 fp_17754.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17754 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17754.

Lemma collineation_17755 : is_collineation2 fp_17755.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17755 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17755.

Lemma collineation_17756 : is_collineation2 fp_17756.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17756 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17756.

Lemma collineation_17757 : is_collineation2 fp_17757.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17757 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17757.

Lemma collineation_17758 : is_collineation2 fp_17758.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17758 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17758.

Lemma collineation_17759 : is_collineation2 fp_17759.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17759 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17759.

Lemma collineation_17760 : is_collineation2 fp_17760.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17760 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17760.

Lemma collineation_17761 : is_collineation2 fp_17761.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17761 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17761.

Lemma collineation_17762 : is_collineation2 fp_17762.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17762 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17762.

Lemma collineation_17763 : is_collineation2 fp_17763.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17763 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17763.

Lemma collineation_17764 : is_collineation2 fp_17764.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17764 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17764.

Lemma collineation_17765 : is_collineation2 fp_17765.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17765 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17765.

Lemma collineation_17766 : is_collineation2 fp_17766.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17766 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17766.

Lemma collineation_17767 : is_collineation2 fp_17767.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17767 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17767.

Lemma collineation_17768 : is_collineation2 fp_17768.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17768 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17768.

Lemma collineation_17769 : is_collineation2 fp_17769.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17769 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17769.

Lemma collineation_17770 : is_collineation2 fp_17770.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17770 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17770.

Lemma collineation_17771 : is_collineation2 fp_17771.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17771 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17771.

Lemma collineation_17772 : is_collineation2 fp_17772.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17772 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17772.

Lemma collineation_17773 : is_collineation2 fp_17773.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17773 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17773.

Lemma collineation_17774 : is_collineation2 fp_17774.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17774 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17774.

Lemma collineation_17775 : is_collineation2 fp_17775.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17775 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17775.

Lemma collineation_17776 : is_collineation2 fp_17776.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17776 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17776.

Lemma collineation_17777 : is_collineation2 fp_17777.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17777 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17777.

Lemma collineation_17778 : is_collineation2 fp_17778.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17778 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17778.

Lemma collineation_17779 : is_collineation2 fp_17779.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17779 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17779.

Lemma collineation_17780 : is_collineation2 fp_17780.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17780 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17780.

Lemma collineation_17781 : is_collineation2 fp_17781.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17781 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17781.

Lemma collineation_17782 : is_collineation2 fp_17782.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17782 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17782.

Lemma collineation_17783 : is_collineation2 fp_17783.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17783 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17783.

Lemma collineation_17784 : is_collineation2 fp_17784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17784.

Lemma collineation_17785 : is_collineation2 fp_17785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17785.

Lemma collineation_17786 : is_collineation2 fp_17786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17786.

Lemma collineation_17787 : is_collineation2 fp_17787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17787.

Lemma collineation_17788 : is_collineation2 fp_17788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17788.

Lemma collineation_17789 : is_collineation2 fp_17789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17789.

Lemma collineation_17790 : is_collineation2 fp_17790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17790.

Lemma collineation_17791 : is_collineation2 fp_17791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17791.

Lemma collineation_17792 : is_collineation2 fp_17792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17792.

Lemma collineation_17793 : is_collineation2 fp_17793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17793.

Lemma collineation_17794 : is_collineation2 fp_17794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17794.

Lemma collineation_17795 : is_collineation2 fp_17795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17795.

Lemma collineation_17796 : is_collineation2 fp_17796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17796.

Lemma collineation_17797 : is_collineation2 fp_17797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17797.

Lemma collineation_17798 : is_collineation2 fp_17798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17798.

Lemma collineation_17799 : is_collineation2 fp_17799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17799.

Lemma collineation_17800 : is_collineation2 fp_17800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17800.

Lemma collineation_17801 : is_collineation2 fp_17801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17801.

Lemma collineation_17802 : is_collineation2 fp_17802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17802.

Lemma collineation_17803 : is_collineation2 fp_17803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17803.

Lemma collineation_17804 : is_collineation2 fp_17804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17804.

Lemma collineation_17805 : is_collineation2 fp_17805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17805.

Lemma collineation_17806 : is_collineation2 fp_17806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17806.

Lemma collineation_17807 : is_collineation2 fp_17807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17807.

Lemma collineation_17808 : is_collineation2 fp_17808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17808.

Lemma collineation_17809 : is_collineation2 fp_17809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17809.

Lemma collineation_17810 : is_collineation2 fp_17810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17810.

Lemma collineation_17811 : is_collineation2 fp_17811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17811.

Lemma collineation_17812 : is_collineation2 fp_17812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17812.

Lemma collineation_17813 : is_collineation2 fp_17813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17813.

Lemma collineation_17814 : is_collineation2 fp_17814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17814.

Lemma collineation_17815 : is_collineation2 fp_17815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17815.

Lemma collineation_17816 : is_collineation2 fp_17816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17816.

Lemma collineation_17817 : is_collineation2 fp_17817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17817.

Lemma collineation_17818 : is_collineation2 fp_17818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17818.

Lemma collineation_17819 : is_collineation2 fp_17819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17819.

Lemma collineation_17820 : is_collineation2 fp_17820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17820.

Lemma collineation_17821 : is_collineation2 fp_17821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17821.

Lemma collineation_17822 : is_collineation2 fp_17822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17822.

Lemma collineation_17823 : is_collineation2 fp_17823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17823.

Lemma collineation_17824 : is_collineation2 fp_17824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17824.

Lemma collineation_17825 : is_collineation2 fp_17825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17825.

Lemma collineation_17826 : is_collineation2 fp_17826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17826.

Lemma collineation_17827 : is_collineation2 fp_17827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17827.

Lemma collineation_17828 : is_collineation2 fp_17828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17828.

Lemma collineation_17829 : is_collineation2 fp_17829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17829.

Lemma collineation_17830 : is_collineation2 fp_17830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17830.

Lemma collineation_17831 : is_collineation2 fp_17831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17831.

Lemma collineation_17832 : is_collineation2 fp_17832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17832.

Lemma collineation_17833 : is_collineation2 fp_17833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17833.

Lemma collineation_17834 : is_collineation2 fp_17834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17834.

Lemma collineation_17835 : is_collineation2 fp_17835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17835.

Lemma collineation_17836 : is_collineation2 fp_17836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17836.

Lemma collineation_17837 : is_collineation2 fp_17837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17837.

Lemma collineation_17838 : is_collineation2 fp_17838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17838.

Lemma collineation_17839 : is_collineation2 fp_17839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17839.

Lemma collineation_17840 : is_collineation2 fp_17840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17840.

Lemma collineation_17841 : is_collineation2 fp_17841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17841.

Lemma collineation_17842 : is_collineation2 fp_17842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17842.

Lemma collineation_17843 : is_collineation2 fp_17843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17843.

Lemma collineation_17844 : is_collineation2 fp_17844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17844.

Lemma collineation_17845 : is_collineation2 fp_17845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17845.

Lemma collineation_17846 : is_collineation2 fp_17846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17846.

Lemma collineation_17847 : is_collineation2 fp_17847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17847.

Lemma collineation_17848 : is_collineation2 fp_17848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17848.

Lemma collineation_17849 : is_collineation2 fp_17849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17849.

Lemma collineation_17850 : is_collineation2 fp_17850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17850.

Lemma collineation_17851 : is_collineation2 fp_17851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17851.

Lemma collineation_17852 : is_collineation2 fp_17852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17852.

Lemma collineation_17853 : is_collineation2 fp_17853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17853.

Lemma collineation_17854 : is_collineation2 fp_17854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17854.

Lemma collineation_17855 : is_collineation2 fp_17855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17855.

Lemma collineation_17856 : is_collineation2 fp_17856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17856.

Lemma collineation_17857 : is_collineation2 fp_17857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17857.

Lemma collineation_17858 : is_collineation2 fp_17858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17858.

Lemma collineation_17859 : is_collineation2 fp_17859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17859.

Lemma collineation_17860 : is_collineation2 fp_17860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17860.

Lemma collineation_17861 : is_collineation2 fp_17861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17861.

Lemma collineation_17862 : is_collineation2 fp_17862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17862.

Lemma collineation_17863 : is_collineation2 fp_17863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17863.

Lemma collineation_17864 : is_collineation2 fp_17864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17864.

Lemma collineation_17865 : is_collineation2 fp_17865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17865.

Lemma collineation_17866 : is_collineation2 fp_17866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17866.

Lemma collineation_17867 : is_collineation2 fp_17867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17867.

Lemma collineation_17868 : is_collineation2 fp_17868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17868.

Lemma collineation_17869 : is_collineation2 fp_17869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17869.

Lemma collineation_17870 : is_collineation2 fp_17870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17870.

Lemma collineation_17871 : is_collineation2 fp_17871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17871.

Lemma collineation_17872 : is_collineation2 fp_17872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17872.

Lemma collineation_17873 : is_collineation2 fp_17873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17873.

Lemma collineation_17874 : is_collineation2 fp_17874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17874.

Lemma collineation_17875 : is_collineation2 fp_17875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17875.

Lemma collineation_17876 : is_collineation2 fp_17876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17876.

Lemma collineation_17877 : is_collineation2 fp_17877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17877.

Lemma collineation_17878 : is_collineation2 fp_17878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17878.

Lemma collineation_17879 : is_collineation2 fp_17879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17879.

Lemma collineation_17880 : is_collineation2 fp_17880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17880.

Lemma collineation_17881 : is_collineation2 fp_17881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17881.

Lemma collineation_17882 : is_collineation2 fp_17882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17882.

Lemma collineation_17883 : is_collineation2 fp_17883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17883.

Lemma collineation_17884 : is_collineation2 fp_17884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17884.

Lemma collineation_17885 : is_collineation2 fp_17885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17885.

Lemma collineation_17886 : is_collineation2 fp_17886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17886.

Lemma collineation_17887 : is_collineation2 fp_17887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17887.

Lemma collineation_17888 : is_collineation2 fp_17888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17888.

Lemma collineation_17889 : is_collineation2 fp_17889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17889.

Lemma collineation_17890 : is_collineation2 fp_17890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17890.

Lemma collineation_17891 : is_collineation2 fp_17891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17891.

Lemma collineation_17892 : is_collineation2 fp_17892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17892.

Lemma collineation_17893 : is_collineation2 fp_17893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17893.

Lemma collineation_17894 : is_collineation2 fp_17894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17894.

Lemma collineation_17895 : is_collineation2 fp_17895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17895.

Lemma collineation_17896 : is_collineation2 fp_17896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17896.

Lemma collineation_17897 : is_collineation2 fp_17897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17897.

Lemma collineation_17898 : is_collineation2 fp_17898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17898.

Lemma collineation_17899 : is_collineation2 fp_17899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17899.

Lemma collineation_17900 : is_collineation2 fp_17900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17900.

Lemma collineation_17901 : is_collineation2 fp_17901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17901.

Lemma collineation_17902 : is_collineation2 fp_17902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17902.

Lemma collineation_17903 : is_collineation2 fp_17903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17903.

Lemma collineation_17904 : is_collineation2 fp_17904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17904.

Lemma collineation_17905 : is_collineation2 fp_17905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17905.

Lemma collineation_17906 : is_collineation2 fp_17906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17906.

Lemma collineation_17907 : is_collineation2 fp_17907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17907.

Lemma collineation_17908 : is_collineation2 fp_17908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17908.

Lemma collineation_17909 : is_collineation2 fp_17909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17909.

Lemma collineation_17910 : is_collineation2 fp_17910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17910.

Lemma collineation_17911 : is_collineation2 fp_17911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17911.

Lemma collineation_17912 : is_collineation2 fp_17912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17912.

Lemma collineation_17913 : is_collineation2 fp_17913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17913.

Lemma collineation_17914 : is_collineation2 fp_17914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17914.

Lemma collineation_17915 : is_collineation2 fp_17915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17915.

Lemma collineation_17916 : is_collineation2 fp_17916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17916.

Lemma collineation_17917 : is_collineation2 fp_17917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17917.

Lemma collineation_17918 : is_collineation2 fp_17918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17918.

Lemma collineation_17919 : is_collineation2 fp_17919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17919.

Lemma collineation_17920 : is_collineation2 fp_17920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17920.

Lemma collineation_17921 : is_collineation2 fp_17921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17921.

Lemma collineation_17922 : is_collineation2 fp_17922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17922.

Lemma collineation_17923 : is_collineation2 fp_17923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17923.

Lemma collineation_17924 : is_collineation2 fp_17924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17924.

Lemma collineation_17925 : is_collineation2 fp_17925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17925.

Lemma collineation_17926 : is_collineation2 fp_17926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17926.

Lemma collineation_17927 : is_collineation2 fp_17927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17927.

Lemma collineation_17928 : is_collineation2 fp_17928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17928.

Lemma collineation_17929 : is_collineation2 fp_17929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17929.

Lemma collineation_17930 : is_collineation2 fp_17930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17930.

Lemma collineation_17931 : is_collineation2 fp_17931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17931.

Lemma collineation_17932 : is_collineation2 fp_17932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17932.

Lemma collineation_17933 : is_collineation2 fp_17933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17933.

Lemma collineation_17934 : is_collineation2 fp_17934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17934.

Lemma collineation_17935 : is_collineation2 fp_17935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17935.

Lemma collineation_17936 : is_collineation2 fp_17936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17936.

Lemma collineation_17937 : is_collineation2 fp_17937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17937.

Lemma collineation_17938 : is_collineation2 fp_17938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17938.

Lemma collineation_17939 : is_collineation2 fp_17939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17939.

Lemma collineation_17940 : is_collineation2 fp_17940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17940.

Lemma collineation_17941 : is_collineation2 fp_17941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17941.

Lemma collineation_17942 : is_collineation2 fp_17942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17942.

Lemma collineation_17943 : is_collineation2 fp_17943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17943.

Lemma collineation_17944 : is_collineation2 fp_17944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17944.

Lemma collineation_17945 : is_collineation2 fp_17945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17945.

Lemma collineation_17946 : is_collineation2 fp_17946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17946.

Lemma collineation_17947 : is_collineation2 fp_17947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17947.

Lemma collineation_17948 : is_collineation2 fp_17948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17948.

Lemma collineation_17949 : is_collineation2 fp_17949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17949.

Lemma collineation_17950 : is_collineation2 fp_17950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17950.

Lemma collineation_17951 : is_collineation2 fp_17951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17951.

Lemma collineation_17952 : is_collineation2 fp_17952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17952.

Lemma collineation_17953 : is_collineation2 fp_17953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17953.

Lemma collineation_17954 : is_collineation2 fp_17954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17954.

Lemma collineation_17955 : is_collineation2 fp_17955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17955.

Lemma collineation_17956 : is_collineation2 fp_17956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17956.

Lemma collineation_17957 : is_collineation2 fp_17957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17957.

Lemma collineation_17958 : is_collineation2 fp_17958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17958.

Lemma collineation_17959 : is_collineation2 fp_17959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17959.

Lemma collineation_17960 : is_collineation2 fp_17960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17960.

Lemma collineation_17961 : is_collineation2 fp_17961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17961.

Lemma collineation_17962 : is_collineation2 fp_17962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17962.

Lemma collineation_17963 : is_collineation2 fp_17963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17963.

Lemma collineation_17964 : is_collineation2 fp_17964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17964.

Lemma collineation_17965 : is_collineation2 fp_17965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17965.

Lemma collineation_17966 : is_collineation2 fp_17966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17966.

Lemma collineation_17967 : is_collineation2 fp_17967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17967.

Lemma collineation_17968 : is_collineation2 fp_17968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17968.

Lemma collineation_17969 : is_collineation2 fp_17969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17969.

Lemma collineation_17970 : is_collineation2 fp_17970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17970.

Lemma collineation_17971 : is_collineation2 fp_17971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17971.

Lemma collineation_17972 : is_collineation2 fp_17972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17972.

Lemma collineation_17973 : is_collineation2 fp_17973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17973.

Lemma collineation_17974 : is_collineation2 fp_17974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17974.

Lemma collineation_17975 : is_collineation2 fp_17975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17975.

Lemma collineation_17976 : is_collineation2 fp_17976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17976.

Lemma collineation_17977 : is_collineation2 fp_17977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17977.

Lemma collineation_17978 : is_collineation2 fp_17978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17978.

Lemma collineation_17979 : is_collineation2 fp_17979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17979.

Lemma collineation_17980 : is_collineation2 fp_17980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17980.

Lemma collineation_17981 : is_collineation2 fp_17981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17981.

Lemma collineation_17982 : is_collineation2 fp_17982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17982.

Lemma collineation_17983 : is_collineation2 fp_17983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17983.

Lemma collineation_17984 : is_collineation2 fp_17984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17984.

Lemma collineation_17985 : is_collineation2 fp_17985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17985.

Lemma collineation_17986 : is_collineation2 fp_17986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17986.

Lemma collineation_17987 : is_collineation2 fp_17987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17987.

Lemma collineation_17988 : is_collineation2 fp_17988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17988.

Lemma collineation_17989 : is_collineation2 fp_17989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17989.

Lemma collineation_17990 : is_collineation2 fp_17990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17990.

Lemma collineation_17991 : is_collineation2 fp_17991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17991.

Lemma collineation_17992 : is_collineation2 fp_17992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17992.

Lemma collineation_17993 : is_collineation2 fp_17993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17993.

Lemma collineation_17994 : is_collineation2 fp_17994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17994.

Lemma collineation_17995 : is_collineation2 fp_17995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17995.

Lemma collineation_17996 : is_collineation2 fp_17996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17996.

Lemma collineation_17997 : is_collineation2 fp_17997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17997.

Lemma collineation_17998 : is_collineation2 fp_17998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17998.

Lemma collineation_17999 : is_collineation2 fp_17999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_17999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_17999.

Lemma collineation_18000 : is_collineation2 fp_18000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18000.

Lemma collineation_18001 : is_collineation2 fp_18001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18001.

Lemma collineation_18002 : is_collineation2 fp_18002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18002.

Lemma collineation_18003 : is_collineation2 fp_18003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18003.

Lemma collineation_18004 : is_collineation2 fp_18004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18004.

Lemma collineation_18005 : is_collineation2 fp_18005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18005.

Lemma collineation_18006 : is_collineation2 fp_18006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18006.

Lemma collineation_18007 : is_collineation2 fp_18007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18007.

Lemma collineation_18008 : is_collineation2 fp_18008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18008.

Lemma collineation_18009 : is_collineation2 fp_18009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18009.

Lemma collineation_18010 : is_collineation2 fp_18010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18010.

Lemma collineation_18011 : is_collineation2 fp_18011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18011.

Lemma collineation_18012 : is_collineation2 fp_18012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18012.

Lemma collineation_18013 : is_collineation2 fp_18013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18013.

Lemma collineation_18014 : is_collineation2 fp_18014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18014.

Lemma collineation_18015 : is_collineation2 fp_18015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18015.

Lemma collineation_18016 : is_collineation2 fp_18016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18016.

Lemma collineation_18017 : is_collineation2 fp_18017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18017.

Lemma collineation_18018 : is_collineation2 fp_18018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18018.

Lemma collineation_18019 : is_collineation2 fp_18019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18019.

Lemma collineation_18020 : is_collineation2 fp_18020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18020.

Lemma collineation_18021 : is_collineation2 fp_18021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18021.

Lemma collineation_18022 : is_collineation2 fp_18022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18022.

Lemma collineation_18023 : is_collineation2 fp_18023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18023.

Lemma collineation_18024 : is_collineation2 fp_18024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18024.

Lemma collineation_18025 : is_collineation2 fp_18025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18025.

Lemma collineation_18026 : is_collineation2 fp_18026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18026.

Lemma collineation_18027 : is_collineation2 fp_18027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18027.

Lemma collineation_18028 : is_collineation2 fp_18028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18028.

Lemma collineation_18029 : is_collineation2 fp_18029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18029.

Lemma collineation_18030 : is_collineation2 fp_18030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18030.

Lemma collineation_18031 : is_collineation2 fp_18031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18031.

Lemma collineation_18032 : is_collineation2 fp_18032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18032.

Lemma collineation_18033 : is_collineation2 fp_18033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18033.

Lemma collineation_18034 : is_collineation2 fp_18034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18034.

Lemma collineation_18035 : is_collineation2 fp_18035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18035.

Lemma collineation_18036 : is_collineation2 fp_18036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18036.

Lemma collineation_18037 : is_collineation2 fp_18037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18037.

Lemma collineation_18038 : is_collineation2 fp_18038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18038.

Lemma collineation_18039 : is_collineation2 fp_18039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18039.

Lemma collineation_18040 : is_collineation2 fp_18040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18040.

Lemma collineation_18041 : is_collineation2 fp_18041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18041.

Lemma collineation_18042 : is_collineation2 fp_18042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18042.

Lemma collineation_18043 : is_collineation2 fp_18043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18043.

Lemma collineation_18044 : is_collineation2 fp_18044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18044.

Lemma collineation_18045 : is_collineation2 fp_18045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18045.

Lemma collineation_18046 : is_collineation2 fp_18046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18046.

Lemma collineation_18047 : is_collineation2 fp_18047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18047.

Lemma collineation_18048 : is_collineation2 fp_18048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18048.

Lemma collineation_18049 : is_collineation2 fp_18049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18049.

Lemma collineation_18050 : is_collineation2 fp_18050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18050.

Lemma collineation_18051 : is_collineation2 fp_18051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18051.

Lemma collineation_18052 : is_collineation2 fp_18052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18052.

Lemma collineation_18053 : is_collineation2 fp_18053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18053.

Lemma collineation_18054 : is_collineation2 fp_18054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18054.

Lemma collineation_18055 : is_collineation2 fp_18055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18055.

Lemma collineation_18056 : is_collineation2 fp_18056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18056.

Lemma collineation_18057 : is_collineation2 fp_18057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18057.

Lemma collineation_18058 : is_collineation2 fp_18058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18058.

Lemma collineation_18059 : is_collineation2 fp_18059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18059.

Lemma collineation_18060 : is_collineation2 fp_18060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18060.

Lemma collineation_18061 : is_collineation2 fp_18061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18061.

Lemma collineation_18062 : is_collineation2 fp_18062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18062.

Lemma collineation_18063 : is_collineation2 fp_18063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18063.

Lemma collineation_18064 : is_collineation2 fp_18064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18064.

Lemma collineation_18065 : is_collineation2 fp_18065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18065.

Lemma collineation_18066 : is_collineation2 fp_18066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18066.

Lemma collineation_18067 : is_collineation2 fp_18067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18067.

Lemma collineation_18068 : is_collineation2 fp_18068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18068.

Lemma collineation_18069 : is_collineation2 fp_18069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18069.

Lemma collineation_18070 : is_collineation2 fp_18070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18070.

Lemma collineation_18071 : is_collineation2 fp_18071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18071.

Lemma collineation_18072 : is_collineation2 fp_18072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18072.

Lemma collineation_18073 : is_collineation2 fp_18073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18073.

Lemma collineation_18074 : is_collineation2 fp_18074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18074.

Lemma collineation_18075 : is_collineation2 fp_18075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18075.

Lemma collineation_18076 : is_collineation2 fp_18076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18076.

Lemma collineation_18077 : is_collineation2 fp_18077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18077.

Lemma collineation_18078 : is_collineation2 fp_18078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18078.

Lemma collineation_18079 : is_collineation2 fp_18079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18079.

Lemma collineation_18080 : is_collineation2 fp_18080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18080.

Lemma collineation_18081 : is_collineation2 fp_18081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18081.

Lemma collineation_18082 : is_collineation2 fp_18082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18082.

Lemma collineation_18083 : is_collineation2 fp_18083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18083.

Lemma collineation_18084 : is_collineation2 fp_18084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18084.

Lemma collineation_18085 : is_collineation2 fp_18085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18085.

Lemma collineation_18086 : is_collineation2 fp_18086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18086.

Lemma collineation_18087 : is_collineation2 fp_18087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18087.

Lemma collineation_18088 : is_collineation2 fp_18088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18088.

Lemma collineation_18089 : is_collineation2 fp_18089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18089.

Lemma collineation_18090 : is_collineation2 fp_18090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18090.

Lemma collineation_18091 : is_collineation2 fp_18091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18091.

Lemma collineation_18092 : is_collineation2 fp_18092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18092.

Lemma collineation_18093 : is_collineation2 fp_18093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18093.

Lemma collineation_18094 : is_collineation2 fp_18094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18094.

Lemma collineation_18095 : is_collineation2 fp_18095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18095.

Lemma collineation_18096 : is_collineation2 fp_18096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18096.

Lemma collineation_18097 : is_collineation2 fp_18097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18097.

Lemma collineation_18098 : is_collineation2 fp_18098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18098.

Lemma collineation_18099 : is_collineation2 fp_18099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18099.

Lemma collineation_18100 : is_collineation2 fp_18100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18100.

Lemma collineation_18101 : is_collineation2 fp_18101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18101.

Lemma collineation_18102 : is_collineation2 fp_18102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18102.

Lemma collineation_18103 : is_collineation2 fp_18103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18103.

Lemma collineation_18104 : is_collineation2 fp_18104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18104.

Lemma collineation_18105 : is_collineation2 fp_18105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18105.

Lemma collineation_18106 : is_collineation2 fp_18106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18106.

Lemma collineation_18107 : is_collineation2 fp_18107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18107.

Lemma collineation_18108 : is_collineation2 fp_18108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18108.

Lemma collineation_18109 : is_collineation2 fp_18109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18109.

Lemma collineation_18110 : is_collineation2 fp_18110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18110.

Lemma collineation_18111 : is_collineation2 fp_18111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18111.

Lemma collineation_18112 : is_collineation2 fp_18112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18112.

Lemma collineation_18113 : is_collineation2 fp_18113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18113.

Lemma collineation_18114 : is_collineation2 fp_18114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18114.

Lemma collineation_18115 : is_collineation2 fp_18115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18115.

Lemma collineation_18116 : is_collineation2 fp_18116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18116.

Lemma collineation_18117 : is_collineation2 fp_18117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18117.

Lemma collineation_18118 : is_collineation2 fp_18118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18118.

Lemma collineation_18119 : is_collineation2 fp_18119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18119.

Lemma collineation_18120 : is_collineation2 fp_18120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18120.

Lemma collineation_18121 : is_collineation2 fp_18121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18121.

Lemma collineation_18122 : is_collineation2 fp_18122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18122.

Lemma collineation_18123 : is_collineation2 fp_18123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18123.

Lemma collineation_18124 : is_collineation2 fp_18124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18124.

Lemma collineation_18125 : is_collineation2 fp_18125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18125.

Lemma collineation_18126 : is_collineation2 fp_18126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18126.

Lemma collineation_18127 : is_collineation2 fp_18127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18127.

Lemma collineation_18128 : is_collineation2 fp_18128.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18128 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18128.

Lemma collineation_18129 : is_collineation2 fp_18129.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18129 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18129.

Lemma collineation_18130 : is_collineation2 fp_18130.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18130 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18130.

Lemma collineation_18131 : is_collineation2 fp_18131.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18131 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18131.

Lemma collineation_18132 : is_collineation2 fp_18132.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18132 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18132.

Lemma collineation_18133 : is_collineation2 fp_18133.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18133 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18133.

Lemma collineation_18134 : is_collineation2 fp_18134.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18134 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18134.

Lemma collineation_18135 : is_collineation2 fp_18135.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18135 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18135.

Lemma collineation_18136 : is_collineation2 fp_18136.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18136 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18136.

Lemma collineation_18137 : is_collineation2 fp_18137.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18137 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18137.

Lemma collineation_18138 : is_collineation2 fp_18138.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18138 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18138.

Lemma collineation_18139 : is_collineation2 fp_18139.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18139 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18139.

Lemma collineation_18140 : is_collineation2 fp_18140.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18140 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18140.

Lemma collineation_18141 : is_collineation2 fp_18141.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18141 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18141.

Lemma collineation_18142 : is_collineation2 fp_18142.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18142 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18142.

Lemma collineation_18143 : is_collineation2 fp_18143.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18143 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18143.

Lemma collineation_18144 : is_collineation2 fp_18144.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18144 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18144.

Lemma collineation_18145 : is_collineation2 fp_18145.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18145 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18145.

Lemma collineation_18146 : is_collineation2 fp_18146.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18146 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18146.

Lemma collineation_18147 : is_collineation2 fp_18147.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18147 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18147.

Lemma collineation_18148 : is_collineation2 fp_18148.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18148 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18148.

Lemma collineation_18149 : is_collineation2 fp_18149.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18149 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18149.

Lemma collineation_18150 : is_collineation2 fp_18150.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18150 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18150.

Lemma collineation_18151 : is_collineation2 fp_18151.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18151 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18151.

Lemma collineation_18152 : is_collineation2 fp_18152.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18152 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18152.

Lemma collineation_18153 : is_collineation2 fp_18153.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18153 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18153.

Lemma collineation_18154 : is_collineation2 fp_18154.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18154 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18154.

Lemma collineation_18155 : is_collineation2 fp_18155.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18155 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18155.

Lemma collineation_18156 : is_collineation2 fp_18156.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18156 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18156.

Lemma collineation_18157 : is_collineation2 fp_18157.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18157 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18157.

Lemma collineation_18158 : is_collineation2 fp_18158.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18158 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18158.

Lemma collineation_18159 : is_collineation2 fp_18159.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18159 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18159.

Lemma collineation_18160 : is_collineation2 fp_18160.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18160 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18160.

Lemma collineation_18161 : is_collineation2 fp_18161.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18161 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18161.

Lemma collineation_18162 : is_collineation2 fp_18162.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18162 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18162.

Lemma collineation_18163 : is_collineation2 fp_18163.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18163 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18163.

Lemma collineation_18164 : is_collineation2 fp_18164.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18164 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18164.

Lemma collineation_18165 : is_collineation2 fp_18165.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18165 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18165.

Lemma collineation_18166 : is_collineation2 fp_18166.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18166 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18166.

Lemma collineation_18167 : is_collineation2 fp_18167.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18167 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18167.

Lemma collineation_18168 : is_collineation2 fp_18168.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18168 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18168.

Lemma collineation_18169 : is_collineation2 fp_18169.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18169 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18169.

Lemma collineation_18170 : is_collineation2 fp_18170.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18170 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18170.

Lemma collineation_18171 : is_collineation2 fp_18171.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18171 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18171.

Lemma collineation_18172 : is_collineation2 fp_18172.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18172 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18172.

Lemma collineation_18173 : is_collineation2 fp_18173.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18173 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18173.

Lemma collineation_18174 : is_collineation2 fp_18174.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18174 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18174.

Lemma collineation_18175 : is_collineation2 fp_18175.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18175 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18175.

Lemma collineation_18176 : is_collineation2 fp_18176.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18176 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18176.

Lemma collineation_18177 : is_collineation2 fp_18177.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18177 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18177.

Lemma collineation_18178 : is_collineation2 fp_18178.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18178 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18178.

Lemma collineation_18179 : is_collineation2 fp_18179.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18179 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18179.

Lemma collineation_18180 : is_collineation2 fp_18180.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18180 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18180.

Lemma collineation_18181 : is_collineation2 fp_18181.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18181 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18181.

Lemma collineation_18182 : is_collineation2 fp_18182.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18182 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18182.

Lemma collineation_18183 : is_collineation2 fp_18183.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18183 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18183.

Lemma collineation_18184 : is_collineation2 fp_18184.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18184 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18184.

Lemma collineation_18185 : is_collineation2 fp_18185.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18185 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18185.

Lemma collineation_18186 : is_collineation2 fp_18186.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18186 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18186.

Lemma collineation_18187 : is_collineation2 fp_18187.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18187 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18187.

Lemma collineation_18188 : is_collineation2 fp_18188.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18188 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18188.

Lemma collineation_18189 : is_collineation2 fp_18189.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18189 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18189.

Lemma collineation_18190 : is_collineation2 fp_18190.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18190 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18190.

Lemma collineation_18191 : is_collineation2 fp_18191.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18191 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18191.

Lemma collineation_18192 : is_collineation2 fp_18192.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18192 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18192.

Lemma collineation_18193 : is_collineation2 fp_18193.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18193 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18193.

Lemma collineation_18194 : is_collineation2 fp_18194.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18194 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18194.

Lemma collineation_18195 : is_collineation2 fp_18195.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18195 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18195.

Lemma collineation_18196 : is_collineation2 fp_18196.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18196 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18196.

Lemma collineation_18197 : is_collineation2 fp_18197.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18197 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18197.

Lemma collineation_18198 : is_collineation2 fp_18198.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18198 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18198.

Lemma collineation_18199 : is_collineation2 fp_18199.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18199 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18199.

Lemma collineation_18200 : is_collineation2 fp_18200.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18200 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18200.

Lemma collineation_18201 : is_collineation2 fp_18201.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18201 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18201.

Lemma collineation_18202 : is_collineation2 fp_18202.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18202 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18202.

Lemma collineation_18203 : is_collineation2 fp_18203.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18203 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18203.

Lemma collineation_18204 : is_collineation2 fp_18204.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18204 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18204.

Lemma collineation_18205 : is_collineation2 fp_18205.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18205 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18205.

Lemma collineation_18206 : is_collineation2 fp_18206.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18206 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18206.

Lemma collineation_18207 : is_collineation2 fp_18207.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18207 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18207.

Lemma collineation_18208 : is_collineation2 fp_18208.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18208 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18208.

Lemma collineation_18209 : is_collineation2 fp_18209.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18209 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18209.

Lemma collineation_18210 : is_collineation2 fp_18210.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18210 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18210.

Lemma collineation_18211 : is_collineation2 fp_18211.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18211 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18211.

Lemma collineation_18212 : is_collineation2 fp_18212.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18212 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18212.

Lemma collineation_18213 : is_collineation2 fp_18213.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18213 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18213.

Lemma collineation_18214 : is_collineation2 fp_18214.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18214 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18214.

Lemma collineation_18215 : is_collineation2 fp_18215.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18215 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18215.

Lemma collineation_18216 : is_collineation2 fp_18216.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18216 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18216.

Lemma collineation_18217 : is_collineation2 fp_18217.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18217 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18217.

Lemma collineation_18218 : is_collineation2 fp_18218.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18218 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18218.

Lemma collineation_18219 : is_collineation2 fp_18219.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18219 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18219.

Lemma collineation_18220 : is_collineation2 fp_18220.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18220 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18220.

Lemma collineation_18221 : is_collineation2 fp_18221.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18221 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18221.

Lemma collineation_18222 : is_collineation2 fp_18222.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18222 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18222.

Lemma collineation_18223 : is_collineation2 fp_18223.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18223 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18223.

Lemma collineation_18224 : is_collineation2 fp_18224.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18224 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18224.

Lemma collineation_18225 : is_collineation2 fp_18225.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18225 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18225.

Lemma collineation_18226 : is_collineation2 fp_18226.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18226 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18226.

Lemma collineation_18227 : is_collineation2 fp_18227.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18227 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18227.

Lemma collineation_18228 : is_collineation2 fp_18228.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18228 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18228.

Lemma collineation_18229 : is_collineation2 fp_18229.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18229 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18229.

Lemma collineation_18230 : is_collineation2 fp_18230.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18230 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18230.

Lemma collineation_18231 : is_collineation2 fp_18231.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18231 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18231.

Lemma collineation_18232 : is_collineation2 fp_18232.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18232 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18232.

Lemma collineation_18233 : is_collineation2 fp_18233.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18233 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18233.

Lemma collineation_18234 : is_collineation2 fp_18234.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18234 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18234.

Lemma collineation_18235 : is_collineation2 fp_18235.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18235 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18235.

Lemma collineation_18236 : is_collineation2 fp_18236.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18236 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18236.

Lemma collineation_18237 : is_collineation2 fp_18237.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18237 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18237.

Lemma collineation_18238 : is_collineation2 fp_18238.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18238 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18238.

Lemma collineation_18239 : is_collineation2 fp_18239.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18239 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18239.

Lemma collineation_18240 : is_collineation2 fp_18240.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18240 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18240.

Lemma collineation_18241 : is_collineation2 fp_18241.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18241 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18241.

Lemma collineation_18242 : is_collineation2 fp_18242.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18242 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18242.

Lemma collineation_18243 : is_collineation2 fp_18243.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18243 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18243.

Lemma collineation_18244 : is_collineation2 fp_18244.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18244 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18244.

Lemma collineation_18245 : is_collineation2 fp_18245.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18245 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18245.

Lemma collineation_18246 : is_collineation2 fp_18246.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18246 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18246.

Lemma collineation_18247 : is_collineation2 fp_18247.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18247 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18247.

Lemma collineation_18248 : is_collineation2 fp_18248.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18248 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18248.

Lemma collineation_18249 : is_collineation2 fp_18249.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18249 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18249.

Lemma collineation_18250 : is_collineation2 fp_18250.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18250 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18250.

Lemma collineation_18251 : is_collineation2 fp_18251.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18251 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18251.

Lemma collineation_18252 : is_collineation2 fp_18252.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18252 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18252.

Lemma collineation_18253 : is_collineation2 fp_18253.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18253 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18253.

Lemma collineation_18254 : is_collineation2 fp_18254.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18254 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18254.

Lemma collineation_18255 : is_collineation2 fp_18255.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18255 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18255.

Lemma collineation_18256 : is_collineation2 fp_18256.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18256 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18256.

Lemma collineation_18257 : is_collineation2 fp_18257.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18257 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18257.

Lemma collineation_18258 : is_collineation2 fp_18258.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18258 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18258.

Lemma collineation_18259 : is_collineation2 fp_18259.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18259 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18259.

Lemma collineation_18260 : is_collineation2 fp_18260.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18260 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18260.

Lemma collineation_18261 : is_collineation2 fp_18261.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18261 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18261.

Lemma collineation_18262 : is_collineation2 fp_18262.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18262 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18262.

Lemma collineation_18263 : is_collineation2 fp_18263.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18263 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18263.

Lemma collineation_18264 : is_collineation2 fp_18264.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18264 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18264.

Lemma collineation_18265 : is_collineation2 fp_18265.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18265 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18265.

Lemma collineation_18266 : is_collineation2 fp_18266.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18266 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18266.

Lemma collineation_18267 : is_collineation2 fp_18267.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18267 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18267.

Lemma collineation_18268 : is_collineation2 fp_18268.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18268 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18268.

Lemma collineation_18269 : is_collineation2 fp_18269.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18269 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18269.

Lemma collineation_18270 : is_collineation2 fp_18270.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18270 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18270.

Lemma collineation_18271 : is_collineation2 fp_18271.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18271 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18271.

Lemma collineation_18272 : is_collineation2 fp_18272.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18272 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18272.

Lemma collineation_18273 : is_collineation2 fp_18273.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18273 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18273.

Lemma collineation_18274 : is_collineation2 fp_18274.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18274 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18274.

Lemma collineation_18275 : is_collineation2 fp_18275.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18275 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18275.

Lemma collineation_18276 : is_collineation2 fp_18276.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18276 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18276.

Lemma collineation_18277 : is_collineation2 fp_18277.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18277 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18277.

Lemma collineation_18278 : is_collineation2 fp_18278.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18278 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18278.

Lemma collineation_18279 : is_collineation2 fp_18279.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18279 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18279.

Lemma collineation_18280 : is_collineation2 fp_18280.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18280 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18280.

Lemma collineation_18281 : is_collineation2 fp_18281.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18281 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18281.

Lemma collineation_18282 : is_collineation2 fp_18282.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18282 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18282.

Lemma collineation_18283 : is_collineation2 fp_18283.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18283 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18283.

Lemma collineation_18284 : is_collineation2 fp_18284.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18284 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18284.

Lemma collineation_18285 : is_collineation2 fp_18285.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18285 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18285.

Lemma collineation_18286 : is_collineation2 fp_18286.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18286 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18286.

Lemma collineation_18287 : is_collineation2 fp_18287.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18287 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18287.

Lemma collineation_18288 : is_collineation2 fp_18288.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18288 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18288.

Lemma collineation_18289 : is_collineation2 fp_18289.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18289 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18289.

Lemma collineation_18290 : is_collineation2 fp_18290.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18290 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18290.

Lemma collineation_18291 : is_collineation2 fp_18291.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18291 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18291.

Lemma collineation_18292 : is_collineation2 fp_18292.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18292 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18292.

Lemma collineation_18293 : is_collineation2 fp_18293.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18293 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18293.

Lemma collineation_18294 : is_collineation2 fp_18294.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18294 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18294.

Lemma collineation_18295 : is_collineation2 fp_18295.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18295 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18295.

Lemma collineation_18296 : is_collineation2 fp_18296.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18296 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18296.

Lemma collineation_18297 : is_collineation2 fp_18297.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18297 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18297.

Lemma collineation_18298 : is_collineation2 fp_18298.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18298 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18298.

Lemma collineation_18299 : is_collineation2 fp_18299.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18299 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18299.

Lemma collineation_18300 : is_collineation2 fp_18300.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18300 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18300.

Lemma collineation_18301 : is_collineation2 fp_18301.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18301 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18301.

Lemma collineation_18302 : is_collineation2 fp_18302.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18302 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18302.

Lemma collineation_18303 : is_collineation2 fp_18303.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18303 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18303.

Lemma collineation_18304 : is_collineation2 fp_18304.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18304 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18304.

Lemma collineation_18305 : is_collineation2 fp_18305.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18305 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18305.

Lemma collineation_18306 : is_collineation2 fp_18306.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18306 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18306.

Lemma collineation_18307 : is_collineation2 fp_18307.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18307 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18307.

Lemma collineation_18308 : is_collineation2 fp_18308.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18308 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18308.

Lemma collineation_18309 : is_collineation2 fp_18309.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18309 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18309.

Lemma collineation_18310 : is_collineation2 fp_18310.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18310 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18310.

Lemma collineation_18311 : is_collineation2 fp_18311.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18311 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18311.

Lemma collineation_18312 : is_collineation2 fp_18312.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18312 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18312.

Lemma collineation_18313 : is_collineation2 fp_18313.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18313 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18313.

Lemma collineation_18314 : is_collineation2 fp_18314.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18314 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18314.

Lemma collineation_18315 : is_collineation2 fp_18315.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18315 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18315.

Lemma collineation_18316 : is_collineation2 fp_18316.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18316 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18316.

Lemma collineation_18317 : is_collineation2 fp_18317.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18317 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18317.

Lemma collineation_18318 : is_collineation2 fp_18318.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18318 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18318.

Lemma collineation_18319 : is_collineation2 fp_18319.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18319 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18319.

Lemma collineation_18320 : is_collineation2 fp_18320.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18320 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18320.

Lemma collineation_18321 : is_collineation2 fp_18321.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18321 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18321.

Lemma collineation_18322 : is_collineation2 fp_18322.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18322 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18322.

Lemma collineation_18323 : is_collineation2 fp_18323.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18323 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18323.

Lemma collineation_18324 : is_collineation2 fp_18324.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18324 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18324.

Lemma collineation_18325 : is_collineation2 fp_18325.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18325 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18325.

Lemma collineation_18326 : is_collineation2 fp_18326.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18326 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18326.

Lemma collineation_18327 : is_collineation2 fp_18327.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18327 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18327.

Lemma collineation_18328 : is_collineation2 fp_18328.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18328 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18328.

Lemma collineation_18329 : is_collineation2 fp_18329.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18329 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18329.

Lemma collineation_18330 : is_collineation2 fp_18330.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18330 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18330.

Lemma collineation_18331 : is_collineation2 fp_18331.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18331 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18331.

Lemma collineation_18332 : is_collineation2 fp_18332.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18332 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18332.

Lemma collineation_18333 : is_collineation2 fp_18333.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18333 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18333.

Lemma collineation_18334 : is_collineation2 fp_18334.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18334 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18334.

Lemma collineation_18335 : is_collineation2 fp_18335.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18335 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18335.

Lemma collineation_18336 : is_collineation2 fp_18336.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18336 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18336.

Lemma collineation_18337 : is_collineation2 fp_18337.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18337 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18337.

Lemma collineation_18338 : is_collineation2 fp_18338.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18338 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18338.

Lemma collineation_18339 : is_collineation2 fp_18339.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18339 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18339.

Lemma collineation_18340 : is_collineation2 fp_18340.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18340 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18340.

Lemma collineation_18341 : is_collineation2 fp_18341.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18341 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18341.

Lemma collineation_18342 : is_collineation2 fp_18342.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18342 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18342.

Lemma collineation_18343 : is_collineation2 fp_18343.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18343 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18343.

Lemma collineation_18344 : is_collineation2 fp_18344.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18344 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18344.

Lemma collineation_18345 : is_collineation2 fp_18345.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18345 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18345.

Lemma collineation_18346 : is_collineation2 fp_18346.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18346 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18346.

Lemma collineation_18347 : is_collineation2 fp_18347.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18347 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18347.

Lemma collineation_18348 : is_collineation2 fp_18348.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18348 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18348.

Lemma collineation_18349 : is_collineation2 fp_18349.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18349 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18349.

Lemma collineation_18350 : is_collineation2 fp_18350.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18350 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18350.

Lemma collineation_18351 : is_collineation2 fp_18351.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18351 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18351.

Lemma collineation_18352 : is_collineation2 fp_18352.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18352 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18352.

Lemma collineation_18353 : is_collineation2 fp_18353.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18353 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18353.

Lemma collineation_18354 : is_collineation2 fp_18354.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18354 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18354.

Lemma collineation_18355 : is_collineation2 fp_18355.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18355 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18355.

Lemma collineation_18356 : is_collineation2 fp_18356.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18356 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18356.

Lemma collineation_18357 : is_collineation2 fp_18357.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18357 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18357.

Lemma collineation_18358 : is_collineation2 fp_18358.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18358 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18358.

Lemma collineation_18359 : is_collineation2 fp_18359.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18359 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18359.

Lemma collineation_18360 : is_collineation2 fp_18360.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18360 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18360.

Lemma collineation_18361 : is_collineation2 fp_18361.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18361 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18361.

Lemma collineation_18362 : is_collineation2 fp_18362.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18362 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18362.

Lemma collineation_18363 : is_collineation2 fp_18363.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18363 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18363.

Lemma collineation_18364 : is_collineation2 fp_18364.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18364 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18364.

Lemma collineation_18365 : is_collineation2 fp_18365.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18365 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18365.

Lemma collineation_18366 : is_collineation2 fp_18366.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18366 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18366.

Lemma collineation_18367 : is_collineation2 fp_18367.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18367 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18367.

Lemma collineation_18368 : is_collineation2 fp_18368.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18368 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18368.

Lemma collineation_18369 : is_collineation2 fp_18369.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18369 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18369.

Lemma collineation_18370 : is_collineation2 fp_18370.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18370 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18370.

Lemma collineation_18371 : is_collineation2 fp_18371.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18371 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18371.

Lemma collineation_18372 : is_collineation2 fp_18372.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18372 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18372.

Lemma collineation_18373 : is_collineation2 fp_18373.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18373 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18373.

Lemma collineation_18374 : is_collineation2 fp_18374.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18374 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18374.

Lemma collineation_18375 : is_collineation2 fp_18375.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18375 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18375.

Lemma collineation_18376 : is_collineation2 fp_18376.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18376 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18376.

Lemma collineation_18377 : is_collineation2 fp_18377.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18377 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18377.

Lemma collineation_18378 : is_collineation2 fp_18378.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18378 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18378.

Lemma collineation_18379 : is_collineation2 fp_18379.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18379 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18379.

Lemma collineation_18380 : is_collineation2 fp_18380.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18380 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18380.

Lemma collineation_18381 : is_collineation2 fp_18381.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18381 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18381.

Lemma collineation_18382 : is_collineation2 fp_18382.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18382 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18382.

Lemma collineation_18383 : is_collineation2 fp_18383.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18383 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18383.

Lemma collineation_18384 : is_collineation2 fp_18384.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18384 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18384.

Lemma collineation_18385 : is_collineation2 fp_18385.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18385 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18385.

Lemma collineation_18386 : is_collineation2 fp_18386.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18386 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18386.

Lemma collineation_18387 : is_collineation2 fp_18387.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18387 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18387.

Lemma collineation_18388 : is_collineation2 fp_18388.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18388 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18388.

Lemma collineation_18389 : is_collineation2 fp_18389.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18389 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18389.

Lemma collineation_18390 : is_collineation2 fp_18390.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18390 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18390.

Lemma collineation_18391 : is_collineation2 fp_18391.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18391 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18391.

Lemma collineation_18392 : is_collineation2 fp_18392.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18392 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18392.

Lemma collineation_18393 : is_collineation2 fp_18393.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18393 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18393.

Lemma collineation_18394 : is_collineation2 fp_18394.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18394 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18394.

Lemma collineation_18395 : is_collineation2 fp_18395.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18395 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18395.

Lemma collineation_18396 : is_collineation2 fp_18396.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18396 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18396.

Lemma collineation_18397 : is_collineation2 fp_18397.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18397 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18397.

Lemma collineation_18398 : is_collineation2 fp_18398.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18398 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18398.

Lemma collineation_18399 : is_collineation2 fp_18399.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18399 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18399.

Lemma collineation_18400 : is_collineation2 fp_18400.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18400 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18400.

Lemma collineation_18401 : is_collineation2 fp_18401.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18401 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18401.

Lemma collineation_18402 : is_collineation2 fp_18402.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18402 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18402.

Lemma collineation_18403 : is_collineation2 fp_18403.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18403 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18403.

Lemma collineation_18404 : is_collineation2 fp_18404.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18404 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18404.

Lemma collineation_18405 : is_collineation2 fp_18405.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18405 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18405.

Lemma collineation_18406 : is_collineation2 fp_18406.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18406 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18406.

Lemma collineation_18407 : is_collineation2 fp_18407.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18407 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18407.

Lemma collineation_18408 : is_collineation2 fp_18408.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18408 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18408.

Lemma collineation_18409 : is_collineation2 fp_18409.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18409 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18409.

Lemma collineation_18410 : is_collineation2 fp_18410.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18410 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18410.

Lemma collineation_18411 : is_collineation2 fp_18411.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18411 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18411.

Lemma collineation_18412 : is_collineation2 fp_18412.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18412 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18412.

Lemma collineation_18413 : is_collineation2 fp_18413.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18413 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18413.

Lemma collineation_18414 : is_collineation2 fp_18414.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18414 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18414.

Lemma collineation_18415 : is_collineation2 fp_18415.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18415 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18415.

Lemma collineation_18416 : is_collineation2 fp_18416.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18416 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18416.

Lemma collineation_18417 : is_collineation2 fp_18417.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18417 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18417.

Lemma collineation_18418 : is_collineation2 fp_18418.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18418 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18418.

Lemma collineation_18419 : is_collineation2 fp_18419.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18419 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18419.

Lemma collineation_18420 : is_collineation2 fp_18420.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18420 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18420.

Lemma collineation_18421 : is_collineation2 fp_18421.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18421 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18421.

Lemma collineation_18422 : is_collineation2 fp_18422.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18422 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18422.

Lemma collineation_18423 : is_collineation2 fp_18423.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18423 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18423.

Lemma collineation_18424 : is_collineation2 fp_18424.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18424 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18424.

Lemma collineation_18425 : is_collineation2 fp_18425.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18425 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18425.

Lemma collineation_18426 : is_collineation2 fp_18426.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18426 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18426.

Lemma collineation_18427 : is_collineation2 fp_18427.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18427 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18427.

Lemma collineation_18428 : is_collineation2 fp_18428.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18428 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18428.

Lemma collineation_18429 : is_collineation2 fp_18429.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18429 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18429.

Lemma collineation_18430 : is_collineation2 fp_18430.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18430 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18430.

Lemma collineation_18431 : is_collineation2 fp_18431.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18431 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18431.

Lemma collineation_18432 : is_collineation2 fp_18432.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18432 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18432.

Lemma collineation_18433 : is_collineation2 fp_18433.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18433 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18433.

Lemma collineation_18434 : is_collineation2 fp_18434.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18434 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18434.

Lemma collineation_18435 : is_collineation2 fp_18435.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18435 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18435.

Lemma collineation_18436 : is_collineation2 fp_18436.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18436 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18436.

Lemma collineation_18437 : is_collineation2 fp_18437.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18437 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18437.

Lemma collineation_18438 : is_collineation2 fp_18438.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18438 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18438.

Lemma collineation_18439 : is_collineation2 fp_18439.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18439 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18439.

Lemma collineation_18440 : is_collineation2 fp_18440.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18440 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18440.

Lemma collineation_18441 : is_collineation2 fp_18441.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18441 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18441.

Lemma collineation_18442 : is_collineation2 fp_18442.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18442 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18442.

Lemma collineation_18443 : is_collineation2 fp_18443.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18443 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18443.

Lemma collineation_18444 : is_collineation2 fp_18444.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18444 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18444.

Lemma collineation_18445 : is_collineation2 fp_18445.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18445 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18445.

Lemma collineation_18446 : is_collineation2 fp_18446.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18446 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18446.

Lemma collineation_18447 : is_collineation2 fp_18447.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18447 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18447.

Lemma collineation_18448 : is_collineation2 fp_18448.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18448 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18448.

Lemma collineation_18449 : is_collineation2 fp_18449.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18449 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18449.

Lemma collineation_18450 : is_collineation2 fp_18450.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18450 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18450.

Lemma collineation_18451 : is_collineation2 fp_18451.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18451 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18451.

Lemma collineation_18452 : is_collineation2 fp_18452.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18452 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18452.

Lemma collineation_18453 : is_collineation2 fp_18453.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18453 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18453.

Lemma collineation_18454 : is_collineation2 fp_18454.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18454 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18454.

Lemma collineation_18455 : is_collineation2 fp_18455.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18455 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18455.

Lemma collineation_18456 : is_collineation2 fp_18456.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18456 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18456.

Lemma collineation_18457 : is_collineation2 fp_18457.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18457 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18457.

Lemma collineation_18458 : is_collineation2 fp_18458.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18458 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18458.

Lemma collineation_18459 : is_collineation2 fp_18459.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18459 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18459.

Lemma collineation_18460 : is_collineation2 fp_18460.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18460 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18460.

Lemma collineation_18461 : is_collineation2 fp_18461.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18461 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18461.

Lemma collineation_18462 : is_collineation2 fp_18462.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18462 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18462.

Lemma collineation_18463 : is_collineation2 fp_18463.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18463 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18463.

Lemma collineation_18464 : is_collineation2 fp_18464.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18464 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18464.

Lemma collineation_18465 : is_collineation2 fp_18465.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18465 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18465.

Lemma collineation_18466 : is_collineation2 fp_18466.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18466 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18466.

Lemma collineation_18467 : is_collineation2 fp_18467.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18467 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18467.

Lemma collineation_18468 : is_collineation2 fp_18468.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18468 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18468.

Lemma collineation_18469 : is_collineation2 fp_18469.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18469 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18469.

Lemma collineation_18470 : is_collineation2 fp_18470.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18470 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18470.

Lemma collineation_18471 : is_collineation2 fp_18471.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18471 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18471.

Lemma collineation_18472 : is_collineation2 fp_18472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18472.

Lemma collineation_18473 : is_collineation2 fp_18473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18473.

Lemma collineation_18474 : is_collineation2 fp_18474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18474.

Lemma collineation_18475 : is_collineation2 fp_18475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18475.

Lemma collineation_18476 : is_collineation2 fp_18476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18476.

Lemma collineation_18477 : is_collineation2 fp_18477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18477.

Lemma collineation_18478 : is_collineation2 fp_18478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18478.

Lemma collineation_18479 : is_collineation2 fp_18479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18479.

Lemma collineation_18480 : is_collineation2 fp_18480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18480.

Lemma collineation_18481 : is_collineation2 fp_18481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18481.

Lemma collineation_18482 : is_collineation2 fp_18482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18482.

Lemma collineation_18483 : is_collineation2 fp_18483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18483.

Lemma collineation_18484 : is_collineation2 fp_18484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18484.

Lemma collineation_18485 : is_collineation2 fp_18485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18485.

Lemma collineation_18486 : is_collineation2 fp_18486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18486.

Lemma collineation_18487 : is_collineation2 fp_18487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18487.

Lemma collineation_18488 : is_collineation2 fp_18488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18488.

Lemma collineation_18489 : is_collineation2 fp_18489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18489.

Lemma collineation_18490 : is_collineation2 fp_18490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18490.

Lemma collineation_18491 : is_collineation2 fp_18491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18491.

Lemma collineation_18492 : is_collineation2 fp_18492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18492.

Lemma collineation_18493 : is_collineation2 fp_18493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18493.

Lemma collineation_18494 : is_collineation2 fp_18494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18494.

Lemma collineation_18495 : is_collineation2 fp_18495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18495.

Lemma collineation_18496 : is_collineation2 fp_18496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18496.

Lemma collineation_18497 : is_collineation2 fp_18497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18497.

Lemma collineation_18498 : is_collineation2 fp_18498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18498.

Lemma collineation_18499 : is_collineation2 fp_18499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18499.

Lemma collineation_18500 : is_collineation2 fp_18500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18500.

Lemma collineation_18501 : is_collineation2 fp_18501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18501.

Lemma collineation_18502 : is_collineation2 fp_18502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18502.

Lemma collineation_18503 : is_collineation2 fp_18503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18503.

Lemma collineation_18504 : is_collineation2 fp_18504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18504.

Lemma collineation_18505 : is_collineation2 fp_18505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18505.

Lemma collineation_18506 : is_collineation2 fp_18506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18506.

Lemma collineation_18507 : is_collineation2 fp_18507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18507.

Lemma collineation_18508 : is_collineation2 fp_18508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18508.

Lemma collineation_18509 : is_collineation2 fp_18509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18509.

Lemma collineation_18510 : is_collineation2 fp_18510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18510.

Lemma collineation_18511 : is_collineation2 fp_18511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18511.

Lemma collineation_18512 : is_collineation2 fp_18512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18512.

Lemma collineation_18513 : is_collineation2 fp_18513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18513.

Lemma collineation_18514 : is_collineation2 fp_18514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18514.

Lemma collineation_18515 : is_collineation2 fp_18515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18515.

Lemma collineation_18516 : is_collineation2 fp_18516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18516.

Lemma collineation_18517 : is_collineation2 fp_18517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18517.

Lemma collineation_18518 : is_collineation2 fp_18518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18518.

Lemma collineation_18519 : is_collineation2 fp_18519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18519.

Lemma collineation_18520 : is_collineation2 fp_18520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18520.

Lemma collineation_18521 : is_collineation2 fp_18521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18521.

Lemma collineation_18522 : is_collineation2 fp_18522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18522.

Lemma collineation_18523 : is_collineation2 fp_18523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18523.

Lemma collineation_18524 : is_collineation2 fp_18524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18524.

Lemma collineation_18525 : is_collineation2 fp_18525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18525.

Lemma collineation_18526 : is_collineation2 fp_18526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18526.

Lemma collineation_18527 : is_collineation2 fp_18527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18527.

Lemma collineation_18528 : is_collineation2 fp_18528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18528.

Lemma collineation_18529 : is_collineation2 fp_18529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18529.

Lemma collineation_18530 : is_collineation2 fp_18530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18530.

Lemma collineation_18531 : is_collineation2 fp_18531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18531.

Lemma collineation_18532 : is_collineation2 fp_18532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18532.

Lemma collineation_18533 : is_collineation2 fp_18533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18533.

Lemma collineation_18534 : is_collineation2 fp_18534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18534.

Lemma collineation_18535 : is_collineation2 fp_18535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18535.

Lemma collineation_18536 : is_collineation2 fp_18536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18536.

Lemma collineation_18537 : is_collineation2 fp_18537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18537.

Lemma collineation_18538 : is_collineation2 fp_18538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18538.

Lemma collineation_18539 : is_collineation2 fp_18539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18539.

Lemma collineation_18540 : is_collineation2 fp_18540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18540.

Lemma collineation_18541 : is_collineation2 fp_18541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18541.

Lemma collineation_18542 : is_collineation2 fp_18542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18542.

Lemma collineation_18543 : is_collineation2 fp_18543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18543.

Lemma collineation_18544 : is_collineation2 fp_18544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18544.

Lemma collineation_18545 : is_collineation2 fp_18545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18545.

Lemma collineation_18546 : is_collineation2 fp_18546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18546.

Lemma collineation_18547 : is_collineation2 fp_18547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18547.

Lemma collineation_18548 : is_collineation2 fp_18548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18548.

Lemma collineation_18549 : is_collineation2 fp_18549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18549.

Lemma collineation_18550 : is_collineation2 fp_18550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18550.

Lemma collineation_18551 : is_collineation2 fp_18551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18551.

Lemma collineation_18552 : is_collineation2 fp_18552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18552.

Lemma collineation_18553 : is_collineation2 fp_18553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18553.

Lemma collineation_18554 : is_collineation2 fp_18554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18554.

Lemma collineation_18555 : is_collineation2 fp_18555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18555.

Lemma collineation_18556 : is_collineation2 fp_18556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18556.

Lemma collineation_18557 : is_collineation2 fp_18557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18557.

Lemma collineation_18558 : is_collineation2 fp_18558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18558.

Lemma collineation_18559 : is_collineation2 fp_18559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18559.

Lemma collineation_18560 : is_collineation2 fp_18560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18560.

Lemma collineation_18561 : is_collineation2 fp_18561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18561.

Lemma collineation_18562 : is_collineation2 fp_18562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18562.

Lemma collineation_18563 : is_collineation2 fp_18563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18563.

Lemma collineation_18564 : is_collineation2 fp_18564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18564.

Lemma collineation_18565 : is_collineation2 fp_18565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18565.

Lemma collineation_18566 : is_collineation2 fp_18566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18566.

Lemma collineation_18567 : is_collineation2 fp_18567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18567.

Lemma collineation_18568 : is_collineation2 fp_18568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18568.

Lemma collineation_18569 : is_collineation2 fp_18569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18569.

Lemma collineation_18570 : is_collineation2 fp_18570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18570.

Lemma collineation_18571 : is_collineation2 fp_18571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18571.

Lemma collineation_18572 : is_collineation2 fp_18572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18572.

Lemma collineation_18573 : is_collineation2 fp_18573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18573.

Lemma collineation_18574 : is_collineation2 fp_18574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18574.

Lemma collineation_18575 : is_collineation2 fp_18575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18575.

Lemma collineation_18576 : is_collineation2 fp_18576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18576.

Lemma collineation_18577 : is_collineation2 fp_18577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18577.

Lemma collineation_18578 : is_collineation2 fp_18578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18578.

Lemma collineation_18579 : is_collineation2 fp_18579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18579.

Lemma collineation_18580 : is_collineation2 fp_18580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18580.

Lemma collineation_18581 : is_collineation2 fp_18581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18581.

Lemma collineation_18582 : is_collineation2 fp_18582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18582.

Lemma collineation_18583 : is_collineation2 fp_18583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18583.

Lemma collineation_18584 : is_collineation2 fp_18584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18584.

Lemma collineation_18585 : is_collineation2 fp_18585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18585.

Lemma collineation_18586 : is_collineation2 fp_18586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18586.

Lemma collineation_18587 : is_collineation2 fp_18587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18587.

Lemma collineation_18588 : is_collineation2 fp_18588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18588.

Lemma collineation_18589 : is_collineation2 fp_18589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18589.

Lemma collineation_18590 : is_collineation2 fp_18590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18590.

Lemma collineation_18591 : is_collineation2 fp_18591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18591.

Lemma collineation_18592 : is_collineation2 fp_18592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18592.

Lemma collineation_18593 : is_collineation2 fp_18593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18593.

Lemma collineation_18594 : is_collineation2 fp_18594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18594.

Lemma collineation_18595 : is_collineation2 fp_18595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18595.

Lemma collineation_18596 : is_collineation2 fp_18596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18596.

Lemma collineation_18597 : is_collineation2 fp_18597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18597.

Lemma collineation_18598 : is_collineation2 fp_18598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18598.

Lemma collineation_18599 : is_collineation2 fp_18599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18599.

Lemma collineation_18600 : is_collineation2 fp_18600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18600.

Lemma collineation_18601 : is_collineation2 fp_18601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18601.

Lemma collineation_18602 : is_collineation2 fp_18602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18602.

Lemma collineation_18603 : is_collineation2 fp_18603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18603.

Lemma collineation_18604 : is_collineation2 fp_18604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18604.

Lemma collineation_18605 : is_collineation2 fp_18605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18605.

Lemma collineation_18606 : is_collineation2 fp_18606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18606.

Lemma collineation_18607 : is_collineation2 fp_18607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18607.

Lemma collineation_18608 : is_collineation2 fp_18608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18608.

Lemma collineation_18609 : is_collineation2 fp_18609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18609.

Lemma collineation_18610 : is_collineation2 fp_18610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18610.

Lemma collineation_18611 : is_collineation2 fp_18611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18611.

Lemma collineation_18612 : is_collineation2 fp_18612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18612.

Lemma collineation_18613 : is_collineation2 fp_18613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18613.

Lemma collineation_18614 : is_collineation2 fp_18614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18614.

Lemma collineation_18615 : is_collineation2 fp_18615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18615.

Lemma collineation_18616 : is_collineation2 fp_18616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18616.

Lemma collineation_18617 : is_collineation2 fp_18617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18617.

Lemma collineation_18618 : is_collineation2 fp_18618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18618.

Lemma collineation_18619 : is_collineation2 fp_18619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18619.

Lemma collineation_18620 : is_collineation2 fp_18620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18620.

Lemma collineation_18621 : is_collineation2 fp_18621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18621.

Lemma collineation_18622 : is_collineation2 fp_18622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18622.

Lemma collineation_18623 : is_collineation2 fp_18623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18623.

Lemma collineation_18624 : is_collineation2 fp_18624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18624.

Lemma collineation_18625 : is_collineation2 fp_18625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18625.

Lemma collineation_18626 : is_collineation2 fp_18626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18626.

Lemma collineation_18627 : is_collineation2 fp_18627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18627.

Lemma collineation_18628 : is_collineation2 fp_18628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18628.

Lemma collineation_18629 : is_collineation2 fp_18629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18629.

Lemma collineation_18630 : is_collineation2 fp_18630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18630.

Lemma collineation_18631 : is_collineation2 fp_18631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18631.

Lemma collineation_18632 : is_collineation2 fp_18632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18632.

Lemma collineation_18633 : is_collineation2 fp_18633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18633.

Lemma collineation_18634 : is_collineation2 fp_18634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18634.

Lemma collineation_18635 : is_collineation2 fp_18635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18635.

Lemma collineation_18636 : is_collineation2 fp_18636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18636.

Lemma collineation_18637 : is_collineation2 fp_18637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18637.

Lemma collineation_18638 : is_collineation2 fp_18638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18638.

Lemma collineation_18639 : is_collineation2 fp_18639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18639.

Lemma collineation_18640 : is_collineation2 fp_18640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18640.

Lemma collineation_18641 : is_collineation2 fp_18641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18641.

Lemma collineation_18642 : is_collineation2 fp_18642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18642.

Lemma collineation_18643 : is_collineation2 fp_18643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18643.

Lemma collineation_18644 : is_collineation2 fp_18644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18644.

Lemma collineation_18645 : is_collineation2 fp_18645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18645.

Lemma collineation_18646 : is_collineation2 fp_18646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18646.

Lemma collineation_18647 : is_collineation2 fp_18647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18647.

Lemma collineation_18648 : is_collineation2 fp_18648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18648.

Lemma collineation_18649 : is_collineation2 fp_18649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18649.

Lemma collineation_18650 : is_collineation2 fp_18650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18650.

Lemma collineation_18651 : is_collineation2 fp_18651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18651.

Lemma collineation_18652 : is_collineation2 fp_18652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18652.

Lemma collineation_18653 : is_collineation2 fp_18653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18653.

Lemma collineation_18654 : is_collineation2 fp_18654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18654.

Lemma collineation_18655 : is_collineation2 fp_18655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18655.

Lemma collineation_18656 : is_collineation2 fp_18656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18656.

Lemma collineation_18657 : is_collineation2 fp_18657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18657.

Lemma collineation_18658 : is_collineation2 fp_18658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18658.

Lemma collineation_18659 : is_collineation2 fp_18659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18659.

Lemma collineation_18660 : is_collineation2 fp_18660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18660.

Lemma collineation_18661 : is_collineation2 fp_18661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18661.

Lemma collineation_18662 : is_collineation2 fp_18662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18662.

Lemma collineation_18663 : is_collineation2 fp_18663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18663.

Lemma collineation_18664 : is_collineation2 fp_18664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18664.

Lemma collineation_18665 : is_collineation2 fp_18665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18665.

Lemma collineation_18666 : is_collineation2 fp_18666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18666.

Lemma collineation_18667 : is_collineation2 fp_18667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18667.

Lemma collineation_18668 : is_collineation2 fp_18668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18668.

Lemma collineation_18669 : is_collineation2 fp_18669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18669.

Lemma collineation_18670 : is_collineation2 fp_18670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18670.

Lemma collineation_18671 : is_collineation2 fp_18671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18671.

Lemma collineation_18672 : is_collineation2 fp_18672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18672.

Lemma collineation_18673 : is_collineation2 fp_18673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18673.

Lemma collineation_18674 : is_collineation2 fp_18674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18674.

Lemma collineation_18675 : is_collineation2 fp_18675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18675.

Lemma collineation_18676 : is_collineation2 fp_18676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18676.

Lemma collineation_18677 : is_collineation2 fp_18677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18677.

Lemma collineation_18678 : is_collineation2 fp_18678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18678.

Lemma collineation_18679 : is_collineation2 fp_18679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18679.

Lemma collineation_18680 : is_collineation2 fp_18680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18680.

Lemma collineation_18681 : is_collineation2 fp_18681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18681.

Lemma collineation_18682 : is_collineation2 fp_18682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18682.

Lemma collineation_18683 : is_collineation2 fp_18683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18683.

Lemma collineation_18684 : is_collineation2 fp_18684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18684.

Lemma collineation_18685 : is_collineation2 fp_18685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18685.

Lemma collineation_18686 : is_collineation2 fp_18686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18686.

Lemma collineation_18687 : is_collineation2 fp_18687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18687.

Lemma collineation_18688 : is_collineation2 fp_18688.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18688 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18688.

Lemma collineation_18689 : is_collineation2 fp_18689.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18689 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18689.

Lemma collineation_18690 : is_collineation2 fp_18690.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18690 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18690.

Lemma collineation_18691 : is_collineation2 fp_18691.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18691 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18691.

Lemma collineation_18692 : is_collineation2 fp_18692.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18692 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18692.

Lemma collineation_18693 : is_collineation2 fp_18693.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18693 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18693.

Lemma collineation_18694 : is_collineation2 fp_18694.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18694 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18694.

Lemma collineation_18695 : is_collineation2 fp_18695.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18695 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18695.

Lemma collineation_18696 : is_collineation2 fp_18696.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18696 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18696.

Lemma collineation_18697 : is_collineation2 fp_18697.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18697 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18697.

Lemma collineation_18698 : is_collineation2 fp_18698.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18698 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18698.

Lemma collineation_18699 : is_collineation2 fp_18699.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18699 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18699.

Lemma collineation_18700 : is_collineation2 fp_18700.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18700 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18700.

Lemma collineation_18701 : is_collineation2 fp_18701.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18701 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18701.

Lemma collineation_18702 : is_collineation2 fp_18702.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18702 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18702.

Lemma collineation_18703 : is_collineation2 fp_18703.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18703 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18703.

Lemma collineation_18704 : is_collineation2 fp_18704.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18704 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18704.

Lemma collineation_18705 : is_collineation2 fp_18705.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18705 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18705.

Lemma collineation_18706 : is_collineation2 fp_18706.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18706 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18706.

Lemma collineation_18707 : is_collineation2 fp_18707.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18707 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18707.

Lemma collineation_18708 : is_collineation2 fp_18708.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18708 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18708.

Lemma collineation_18709 : is_collineation2 fp_18709.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18709 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18709.

Lemma collineation_18710 : is_collineation2 fp_18710.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18710 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18710.

Lemma collineation_18711 : is_collineation2 fp_18711.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18711 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18711.

Lemma collineation_18712 : is_collineation2 fp_18712.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18712 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18712.

Lemma collineation_18713 : is_collineation2 fp_18713.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18713 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18713.

Lemma collineation_18714 : is_collineation2 fp_18714.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18714 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18714.

Lemma collineation_18715 : is_collineation2 fp_18715.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18715 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18715.

Lemma collineation_18716 : is_collineation2 fp_18716.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18716 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18716.

Lemma collineation_18717 : is_collineation2 fp_18717.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18717 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18717.

Lemma collineation_18718 : is_collineation2 fp_18718.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18718 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18718.

Lemma collineation_18719 : is_collineation2 fp_18719.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18719 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18719.

Lemma collineation_18720 : is_collineation2 fp_18720.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18720 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18720.

Lemma collineation_18721 : is_collineation2 fp_18721.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18721 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18721.

Lemma collineation_18722 : is_collineation2 fp_18722.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18722 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18722.

Lemma collineation_18723 : is_collineation2 fp_18723.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18723 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18723.

Lemma collineation_18724 : is_collineation2 fp_18724.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18724 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18724.

Lemma collineation_18725 : is_collineation2 fp_18725.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18725 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18725.

Lemma collineation_18726 : is_collineation2 fp_18726.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18726 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18726.

Lemma collineation_18727 : is_collineation2 fp_18727.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18727 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18727.

Lemma collineation_18728 : is_collineation2 fp_18728.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18728 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18728.

Lemma collineation_18729 : is_collineation2 fp_18729.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18729 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18729.

Lemma collineation_18730 : is_collineation2 fp_18730.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18730 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18730.

Lemma collineation_18731 : is_collineation2 fp_18731.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18731 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18731.

Lemma collineation_18732 : is_collineation2 fp_18732.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18732 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18732.

Lemma collineation_18733 : is_collineation2 fp_18733.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18733 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18733.

Lemma collineation_18734 : is_collineation2 fp_18734.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18734 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18734.

Lemma collineation_18735 : is_collineation2 fp_18735.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18735 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18735.

Lemma collineation_18736 : is_collineation2 fp_18736.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18736 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18736.

Lemma collineation_18737 : is_collineation2 fp_18737.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18737 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18737.

Lemma collineation_18738 : is_collineation2 fp_18738.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18738 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18738.

Lemma collineation_18739 : is_collineation2 fp_18739.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18739 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18739.

Lemma collineation_18740 : is_collineation2 fp_18740.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18740 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18740.

Lemma collineation_18741 : is_collineation2 fp_18741.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18741 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18741.

Lemma collineation_18742 : is_collineation2 fp_18742.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18742 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18742.

Lemma collineation_18743 : is_collineation2 fp_18743.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18743 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18743.

Lemma collineation_18744 : is_collineation2 fp_18744.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18744 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18744.

Lemma collineation_18745 : is_collineation2 fp_18745.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18745 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18745.

Lemma collineation_18746 : is_collineation2 fp_18746.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18746 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18746.

Lemma collineation_18747 : is_collineation2 fp_18747.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18747 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18747.

Lemma collineation_18748 : is_collineation2 fp_18748.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18748 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18748.

Lemma collineation_18749 : is_collineation2 fp_18749.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18749 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18749.

Lemma collineation_18750 : is_collineation2 fp_18750.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18750 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18750.

Lemma collineation_18751 : is_collineation2 fp_18751.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18751 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18751.

Lemma collineation_18752 : is_collineation2 fp_18752.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18752 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18752.

Lemma collineation_18753 : is_collineation2 fp_18753.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18753 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18753.

Lemma collineation_18754 : is_collineation2 fp_18754.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18754 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18754.

Lemma collineation_18755 : is_collineation2 fp_18755.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18755 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18755.

Lemma collineation_18756 : is_collineation2 fp_18756.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18756 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18756.

Lemma collineation_18757 : is_collineation2 fp_18757.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18757 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18757.

Lemma collineation_18758 : is_collineation2 fp_18758.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18758 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18758.

Lemma collineation_18759 : is_collineation2 fp_18759.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18759 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18759.

Lemma collineation_18760 : is_collineation2 fp_18760.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18760 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18760.

Lemma collineation_18761 : is_collineation2 fp_18761.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18761 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18761.

Lemma collineation_18762 : is_collineation2 fp_18762.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18762 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18762.

Lemma collineation_18763 : is_collineation2 fp_18763.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18763 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18763.

Lemma collineation_18764 : is_collineation2 fp_18764.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18764 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18764.

Lemma collineation_18765 : is_collineation2 fp_18765.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18765 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18765.

Lemma collineation_18766 : is_collineation2 fp_18766.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18766 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18766.

Lemma collineation_18767 : is_collineation2 fp_18767.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18767 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18767.

Lemma collineation_18768 : is_collineation2 fp_18768.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18768 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18768.

Lemma collineation_18769 : is_collineation2 fp_18769.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18769 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18769.

Lemma collineation_18770 : is_collineation2 fp_18770.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18770 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18770.

Lemma collineation_18771 : is_collineation2 fp_18771.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18771 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18771.

Lemma collineation_18772 : is_collineation2 fp_18772.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18772 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18772.

Lemma collineation_18773 : is_collineation2 fp_18773.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18773 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18773.

Lemma collineation_18774 : is_collineation2 fp_18774.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18774 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18774.

Lemma collineation_18775 : is_collineation2 fp_18775.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18775 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18775.

Lemma collineation_18776 : is_collineation2 fp_18776.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18776 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18776.

Lemma collineation_18777 : is_collineation2 fp_18777.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18777 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18777.

Lemma collineation_18778 : is_collineation2 fp_18778.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18778 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18778.

Lemma collineation_18779 : is_collineation2 fp_18779.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18779 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18779.

Lemma collineation_18780 : is_collineation2 fp_18780.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18780 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18780.

Lemma collineation_18781 : is_collineation2 fp_18781.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18781 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18781.

Lemma collineation_18782 : is_collineation2 fp_18782.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18782 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18782.

Lemma collineation_18783 : is_collineation2 fp_18783.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18783 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18783.

Lemma collineation_18784 : is_collineation2 fp_18784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18784.

Lemma collineation_18785 : is_collineation2 fp_18785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18785.

Lemma collineation_18786 : is_collineation2 fp_18786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18786.

Lemma collineation_18787 : is_collineation2 fp_18787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18787.

Lemma collineation_18788 : is_collineation2 fp_18788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18788.

Lemma collineation_18789 : is_collineation2 fp_18789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18789.

Lemma collineation_18790 : is_collineation2 fp_18790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18790.

Lemma collineation_18791 : is_collineation2 fp_18791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18791.

Lemma collineation_18792 : is_collineation2 fp_18792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18792.

Lemma collineation_18793 : is_collineation2 fp_18793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18793.

Lemma collineation_18794 : is_collineation2 fp_18794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18794.

Lemma collineation_18795 : is_collineation2 fp_18795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18795.

Lemma collineation_18796 : is_collineation2 fp_18796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18796.

Lemma collineation_18797 : is_collineation2 fp_18797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18797.

Lemma collineation_18798 : is_collineation2 fp_18798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18798.

Lemma collineation_18799 : is_collineation2 fp_18799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18799.

Lemma collineation_18800 : is_collineation2 fp_18800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18800.

Lemma collineation_18801 : is_collineation2 fp_18801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18801.

Lemma collineation_18802 : is_collineation2 fp_18802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18802.

Lemma collineation_18803 : is_collineation2 fp_18803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18803.

Lemma collineation_18804 : is_collineation2 fp_18804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18804.

Lemma collineation_18805 : is_collineation2 fp_18805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18805.

Lemma collineation_18806 : is_collineation2 fp_18806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18806.

Lemma collineation_18807 : is_collineation2 fp_18807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18807.

Lemma collineation_18808 : is_collineation2 fp_18808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18808.

Lemma collineation_18809 : is_collineation2 fp_18809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18809.

Lemma collineation_18810 : is_collineation2 fp_18810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18810.

Lemma collineation_18811 : is_collineation2 fp_18811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18811.

Lemma collineation_18812 : is_collineation2 fp_18812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18812.

Lemma collineation_18813 : is_collineation2 fp_18813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18813.

Lemma collineation_18814 : is_collineation2 fp_18814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18814.

Lemma collineation_18815 : is_collineation2 fp_18815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_18815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_18815.

Lemma is_col_all_c182 : forall fp, In fp (all_c182++all_c183++all_c184++all_c185++all_c186++all_c187++all_c188++all_c189++all_c190++all_c191++all_c192++all_c193++all_c194++all_c195) -> is_collineation2 fp.
Proof.
 intros fp HIn_S.
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17687 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17688 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17689 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17690 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17691 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17692 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17693 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17694 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17695 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17696 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17697 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17698 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17699 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17700 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17701 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17702 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17703 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17704 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17705 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17706 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17707 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17708 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17709 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17710 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17711 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17712 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17713 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17714 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17715 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17716 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17717 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17718 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17719 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17720 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17721 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17722 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17723 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17724 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17725 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17726 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17727 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17728 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17729 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17730 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17731 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17732 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17733 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17734 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17735 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17736 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17737 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17738 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17739 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17740 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17741 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17742 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17743 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17744 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17745 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17746 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17747 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17748 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17749 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17750 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17751 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17752 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17753 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17754 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17755 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17756 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17757 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17758 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17759 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17760 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17761 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17762 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17763 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17764 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17765 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17766 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17767 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17768 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17769 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17770 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17771 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17772 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17773 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17774 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17775 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17776 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17777 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17778 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17779 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17780 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17781 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17782 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17783 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17815 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_17999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18127 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18128 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18129 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18130 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18131 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18132 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18133 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18134 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18135 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18136 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18137 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18138 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18139 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18140 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18141 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18142 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18143 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18144 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18145 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18146 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18147 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18148 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18149 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18150 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18151 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18152 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18153 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18154 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18155 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18156 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18157 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18158 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18159 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18160 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18161 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18162 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18163 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18164 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18165 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18166 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18167 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18168 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18169 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18170 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18171 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18172 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18173 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18174 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18175 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18176 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18177 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18178 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18179 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18180 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18181 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18182 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18183 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18184 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18185 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18186 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18187 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18188 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18189 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18190 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18191 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18192 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18193 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18194 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18195 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18196 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18197 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18198 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18199 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18200 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18201 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18202 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18203 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18204 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18205 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18206 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18207 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18208 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18209 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18210 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18211 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18212 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18213 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18214 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18215 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18216 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18217 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18218 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18219 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18220 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18221 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18222 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18223 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18224 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18225 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18226 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18227 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18228 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18229 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18230 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18231 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18232 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18233 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18234 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18235 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18236 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18237 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18238 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18239 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18240 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18241 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18242 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18243 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18244 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18245 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18246 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18247 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18248 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18249 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18250 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18251 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18252 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18253 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18254 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18255 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18256 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18257 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18258 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18259 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18260 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18261 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18262 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18263 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18264 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18265 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18266 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18267 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18268 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18269 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18270 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18271 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18272 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18273 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18274 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18275 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18276 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18277 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18278 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18279 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18280 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18281 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18282 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18283 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18284 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18285 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18286 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18287 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18288 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18289 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18290 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18291 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18292 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18293 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18294 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18295 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18296 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18297 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18298 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18299 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18300 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18301 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18302 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18303 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18304 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18305 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18306 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18307 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18308 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18309 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18310 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18311 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18312 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18313 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18314 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18315 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18316 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18317 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18318 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18319 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18320 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18321 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18322 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18323 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18324 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18325 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18326 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18327 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18328 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18329 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18330 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18331 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18332 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18333 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18334 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18335 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18336 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18337 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18338 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18339 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18340 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18341 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18342 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18343 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18344 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18345 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18346 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18347 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18348 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18349 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18350 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18351 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18352 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18353 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18354 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18355 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18356 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18357 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18358 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18359 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18360 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18361 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18362 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18363 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18364 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18365 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18366 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18367 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18368 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18369 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18370 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18371 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18372 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18373 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18374 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18375 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18376 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18377 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18378 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18379 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18380 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18381 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18382 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18383 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18384 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18385 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18386 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18387 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18388 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18389 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18390 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18391 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18392 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18393 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18394 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18395 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18396 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18397 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18398 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18399 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18400 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18401 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18402 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18403 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18404 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18405 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18406 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18407 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18408 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18409 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18410 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18411 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18412 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18413 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18414 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18415 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18416 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18417 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18418 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18419 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18420 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18421 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18422 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18423 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18424 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18425 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18426 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18427 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18428 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18429 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18430 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18431 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18432 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18433 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18434 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18435 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18436 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18437 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18438 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18439 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18440 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18441 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18442 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18443 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18444 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18445 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18446 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18447 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18448 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18449 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18450 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18451 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18452 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18453 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18454 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18455 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18456 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18457 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18458 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18459 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18460 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18461 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18462 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18463 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18464 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18465 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18466 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18467 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18468 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18469 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18470 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18471 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18687 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18688 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18689 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18690 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18691 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18692 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18693 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18694 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18695 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18696 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18697 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18698 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18699 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18700 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18701 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18702 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18703 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18704 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18705 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18706 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18707 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18708 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18709 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18710 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18711 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18712 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18713 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18714 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18715 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18716 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18717 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18718 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18719 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18720 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18721 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18722 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18723 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18724 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18725 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18726 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18727 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18728 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18729 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18730 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18731 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18732 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18733 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18734 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18735 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18736 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18737 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18738 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18739 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18740 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18741 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18742 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18743 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18744 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18745 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18746 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18747 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18748 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18749 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18750 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18751 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18752 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18753 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18754 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18755 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18756 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18757 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18758 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18759 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18760 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18761 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18762 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18763 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18764 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18765 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18766 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18767 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18768 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18769 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18770 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18771 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18772 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18773 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18774 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18775 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18776 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18777 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18778 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18779 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18780 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18781 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18782 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18783 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_18815 | idtac].
 destruct (in_nil HIn_S).
Qed.

