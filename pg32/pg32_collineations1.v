Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG32.pg32_inductive PG32.pg32_spreads_collineations.
Require Import PG32.pg32_automorphisms.
Require Import PG32.pg32_automorphisms_inv.
Require Import PG32.pg32_is_col_fp.

Require Import Lists.List.
Import ListNotations.
Require Import Arith.

Lemma collineation_1344 : is_collineation2 fp_1344.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1344 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1344.

Lemma collineation_1345 : is_collineation2 fp_1345.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1345 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1345.

Lemma collineation_1346 : is_collineation2 fp_1346.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1346 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1346.

Lemma collineation_1347 : is_collineation2 fp_1347.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1347 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1347.

Lemma collineation_1348 : is_collineation2 fp_1348.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1348 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1348.

Lemma collineation_1349 : is_collineation2 fp_1349.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1349 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1349.

Lemma collineation_1350 : is_collineation2 fp_1350.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1350 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1350.

Lemma collineation_1351 : is_collineation2 fp_1351.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1351 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1351.

Lemma collineation_1352 : is_collineation2 fp_1352.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1352 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1352.

Lemma collineation_1353 : is_collineation2 fp_1353.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1353 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1353.

Lemma collineation_1354 : is_collineation2 fp_1354.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1354 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1354.

Lemma collineation_1355 : is_collineation2 fp_1355.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1355 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1355.

Lemma collineation_1356 : is_collineation2 fp_1356.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1356 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1356.

Lemma collineation_1357 : is_collineation2 fp_1357.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1357 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1357.

Lemma collineation_1358 : is_collineation2 fp_1358.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1358 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1358.

Lemma collineation_1359 : is_collineation2 fp_1359.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1359 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1359.

Lemma collineation_1360 : is_collineation2 fp_1360.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1360 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1360.

Lemma collineation_1361 : is_collineation2 fp_1361.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1361 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1361.

Lemma collineation_1362 : is_collineation2 fp_1362.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1362 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1362.

Lemma collineation_1363 : is_collineation2 fp_1363.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1363 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1363.

Lemma collineation_1364 : is_collineation2 fp_1364.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1364 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1364.

Lemma collineation_1365 : is_collineation2 fp_1365.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1365 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1365.

Lemma collineation_1366 : is_collineation2 fp_1366.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1366 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1366.

Lemma collineation_1367 : is_collineation2 fp_1367.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1367 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1367.

Lemma collineation_1368 : is_collineation2 fp_1368.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1368 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1368.

Lemma collineation_1369 : is_collineation2 fp_1369.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1369 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1369.

Lemma collineation_1370 : is_collineation2 fp_1370.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1370 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1370.

Lemma collineation_1371 : is_collineation2 fp_1371.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1371 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1371.

Lemma collineation_1372 : is_collineation2 fp_1372.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1372 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1372.

Lemma collineation_1373 : is_collineation2 fp_1373.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1373 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1373.

Lemma collineation_1374 : is_collineation2 fp_1374.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1374 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1374.

Lemma collineation_1375 : is_collineation2 fp_1375.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1375 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1375.

Lemma collineation_1376 : is_collineation2 fp_1376.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1376 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1376.

Lemma collineation_1377 : is_collineation2 fp_1377.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1377 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1377.

Lemma collineation_1378 : is_collineation2 fp_1378.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1378 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1378.

Lemma collineation_1379 : is_collineation2 fp_1379.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1379 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1379.

Lemma collineation_1380 : is_collineation2 fp_1380.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1380 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1380.

Lemma collineation_1381 : is_collineation2 fp_1381.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1381 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1381.

Lemma collineation_1382 : is_collineation2 fp_1382.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1382 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1382.

Lemma collineation_1383 : is_collineation2 fp_1383.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1383 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1383.

Lemma collineation_1384 : is_collineation2 fp_1384.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1384 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1384.

Lemma collineation_1385 : is_collineation2 fp_1385.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1385 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1385.

Lemma collineation_1386 : is_collineation2 fp_1386.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1386 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1386.

Lemma collineation_1387 : is_collineation2 fp_1387.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1387 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1387.

Lemma collineation_1388 : is_collineation2 fp_1388.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1388 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1388.

Lemma collineation_1389 : is_collineation2 fp_1389.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1389 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1389.

Lemma collineation_1390 : is_collineation2 fp_1390.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1390 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1390.

Lemma collineation_1391 : is_collineation2 fp_1391.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1391 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1391.

Lemma collineation_1392 : is_collineation2 fp_1392.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1392 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1392.

Lemma collineation_1393 : is_collineation2 fp_1393.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1393 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1393.

Lemma collineation_1394 : is_collineation2 fp_1394.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1394 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1394.

Lemma collineation_1395 : is_collineation2 fp_1395.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1395 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1395.

Lemma collineation_1396 : is_collineation2 fp_1396.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1396 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1396.

Lemma collineation_1397 : is_collineation2 fp_1397.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1397 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1397.

Lemma collineation_1398 : is_collineation2 fp_1398.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1398 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1398.

Lemma collineation_1399 : is_collineation2 fp_1399.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1399 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1399.

Lemma collineation_1400 : is_collineation2 fp_1400.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1400 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1400.

Lemma collineation_1401 : is_collineation2 fp_1401.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1401 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1401.

Lemma collineation_1402 : is_collineation2 fp_1402.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1402 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1402.

Lemma collineation_1403 : is_collineation2 fp_1403.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1403 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1403.

Lemma collineation_1404 : is_collineation2 fp_1404.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1404 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1404.

Lemma collineation_1405 : is_collineation2 fp_1405.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1405 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1405.

Lemma collineation_1406 : is_collineation2 fp_1406.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1406 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1406.

Lemma collineation_1407 : is_collineation2 fp_1407.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1407 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1407.

Lemma collineation_1408 : is_collineation2 fp_1408.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1408 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1408.

Lemma collineation_1409 : is_collineation2 fp_1409.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1409 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1409.

Lemma collineation_1410 : is_collineation2 fp_1410.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1410 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1410.

Lemma collineation_1411 : is_collineation2 fp_1411.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1411 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1411.

Lemma collineation_1412 : is_collineation2 fp_1412.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1412 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1412.

Lemma collineation_1413 : is_collineation2 fp_1413.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1413 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1413.

Lemma collineation_1414 : is_collineation2 fp_1414.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1414 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1414.

Lemma collineation_1415 : is_collineation2 fp_1415.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1415 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1415.

Lemma collineation_1416 : is_collineation2 fp_1416.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1416 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1416.

Lemma collineation_1417 : is_collineation2 fp_1417.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1417 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1417.

Lemma collineation_1418 : is_collineation2 fp_1418.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1418 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1418.

Lemma collineation_1419 : is_collineation2 fp_1419.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1419 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1419.

Lemma collineation_1420 : is_collineation2 fp_1420.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1420 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1420.

Lemma collineation_1421 : is_collineation2 fp_1421.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1421 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1421.

Lemma collineation_1422 : is_collineation2 fp_1422.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1422 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1422.

Lemma collineation_1423 : is_collineation2 fp_1423.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1423 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1423.

Lemma collineation_1424 : is_collineation2 fp_1424.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1424 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1424.

Lemma collineation_1425 : is_collineation2 fp_1425.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1425 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1425.

Lemma collineation_1426 : is_collineation2 fp_1426.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1426 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1426.

Lemma collineation_1427 : is_collineation2 fp_1427.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1427 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1427.

Lemma collineation_1428 : is_collineation2 fp_1428.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1428 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1428.

Lemma collineation_1429 : is_collineation2 fp_1429.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1429 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1429.

Lemma collineation_1430 : is_collineation2 fp_1430.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1430 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1430.

Lemma collineation_1431 : is_collineation2 fp_1431.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1431 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1431.

Lemma collineation_1432 : is_collineation2 fp_1432.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1432 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1432.

Lemma collineation_1433 : is_collineation2 fp_1433.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1433 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1433.

Lemma collineation_1434 : is_collineation2 fp_1434.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1434 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1434.

Lemma collineation_1435 : is_collineation2 fp_1435.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1435 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1435.

Lemma collineation_1436 : is_collineation2 fp_1436.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1436 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1436.

Lemma collineation_1437 : is_collineation2 fp_1437.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1437 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1437.

Lemma collineation_1438 : is_collineation2 fp_1438.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1438 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1438.

Lemma collineation_1439 : is_collineation2 fp_1439.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1439 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1439.

Lemma collineation_1440 : is_collineation2 fp_1440.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1440 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1440.

Lemma collineation_1441 : is_collineation2 fp_1441.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1441 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1441.

Lemma collineation_1442 : is_collineation2 fp_1442.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1442 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1442.

Lemma collineation_1443 : is_collineation2 fp_1443.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1443 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1443.

Lemma collineation_1444 : is_collineation2 fp_1444.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1444 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1444.

Lemma collineation_1445 : is_collineation2 fp_1445.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1445 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1445.

Lemma collineation_1446 : is_collineation2 fp_1446.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1446 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1446.

Lemma collineation_1447 : is_collineation2 fp_1447.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1447 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1447.

Lemma collineation_1448 : is_collineation2 fp_1448.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1448 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1448.

Lemma collineation_1449 : is_collineation2 fp_1449.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1449 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1449.

Lemma collineation_1450 : is_collineation2 fp_1450.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1450 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1450.

Lemma collineation_1451 : is_collineation2 fp_1451.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1451 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1451.

Lemma collineation_1452 : is_collineation2 fp_1452.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1452 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1452.

Lemma collineation_1453 : is_collineation2 fp_1453.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1453 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1453.

Lemma collineation_1454 : is_collineation2 fp_1454.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1454 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1454.

Lemma collineation_1455 : is_collineation2 fp_1455.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1455 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1455.

Lemma collineation_1456 : is_collineation2 fp_1456.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1456 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1456.

Lemma collineation_1457 : is_collineation2 fp_1457.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1457 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1457.

Lemma collineation_1458 : is_collineation2 fp_1458.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1458 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1458.

Lemma collineation_1459 : is_collineation2 fp_1459.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1459 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1459.

Lemma collineation_1460 : is_collineation2 fp_1460.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1460 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1460.

Lemma collineation_1461 : is_collineation2 fp_1461.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1461 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1461.

Lemma collineation_1462 : is_collineation2 fp_1462.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1462 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1462.

Lemma collineation_1463 : is_collineation2 fp_1463.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1463 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1463.

Lemma collineation_1464 : is_collineation2 fp_1464.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1464 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1464.

Lemma collineation_1465 : is_collineation2 fp_1465.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1465 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1465.

Lemma collineation_1466 : is_collineation2 fp_1466.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1466 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1466.

Lemma collineation_1467 : is_collineation2 fp_1467.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1467 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1467.

Lemma collineation_1468 : is_collineation2 fp_1468.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1468 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1468.

Lemma collineation_1469 : is_collineation2 fp_1469.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1469 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1469.

Lemma collineation_1470 : is_collineation2 fp_1470.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1470 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1470.

Lemma collineation_1471 : is_collineation2 fp_1471.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1471 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1471.

Lemma collineation_1472 : is_collineation2 fp_1472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1472.

Lemma collineation_1473 : is_collineation2 fp_1473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1473.

Lemma collineation_1474 : is_collineation2 fp_1474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1474.

Lemma collineation_1475 : is_collineation2 fp_1475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1475.

Lemma collineation_1476 : is_collineation2 fp_1476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1476.

Lemma collineation_1477 : is_collineation2 fp_1477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1477.

Lemma collineation_1478 : is_collineation2 fp_1478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1478.

Lemma collineation_1479 : is_collineation2 fp_1479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1479.

Lemma collineation_1480 : is_collineation2 fp_1480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1480.

Lemma collineation_1481 : is_collineation2 fp_1481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1481.

Lemma collineation_1482 : is_collineation2 fp_1482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1482.

Lemma collineation_1483 : is_collineation2 fp_1483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1483.

Lemma collineation_1484 : is_collineation2 fp_1484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1484.

Lemma collineation_1485 : is_collineation2 fp_1485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1485.

Lemma collineation_1486 : is_collineation2 fp_1486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1486.

Lemma collineation_1487 : is_collineation2 fp_1487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1487.

Lemma collineation_1488 : is_collineation2 fp_1488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1488.

Lemma collineation_1489 : is_collineation2 fp_1489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1489.

Lemma collineation_1490 : is_collineation2 fp_1490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1490.

Lemma collineation_1491 : is_collineation2 fp_1491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1491.

Lemma collineation_1492 : is_collineation2 fp_1492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1492.

Lemma collineation_1493 : is_collineation2 fp_1493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1493.

Lemma collineation_1494 : is_collineation2 fp_1494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1494.

Lemma collineation_1495 : is_collineation2 fp_1495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1495.

Lemma collineation_1496 : is_collineation2 fp_1496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1496.

Lemma collineation_1497 : is_collineation2 fp_1497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1497.

Lemma collineation_1498 : is_collineation2 fp_1498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1498.

Lemma collineation_1499 : is_collineation2 fp_1499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1499.

Lemma collineation_1500 : is_collineation2 fp_1500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1500.

Lemma collineation_1501 : is_collineation2 fp_1501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1501.

Lemma collineation_1502 : is_collineation2 fp_1502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1502.

Lemma collineation_1503 : is_collineation2 fp_1503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1503.

Lemma collineation_1504 : is_collineation2 fp_1504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1504.

Lemma collineation_1505 : is_collineation2 fp_1505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1505.

Lemma collineation_1506 : is_collineation2 fp_1506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1506.

Lemma collineation_1507 : is_collineation2 fp_1507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1507.

Lemma collineation_1508 : is_collineation2 fp_1508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1508.

Lemma collineation_1509 : is_collineation2 fp_1509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1509.

Lemma collineation_1510 : is_collineation2 fp_1510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1510.

Lemma collineation_1511 : is_collineation2 fp_1511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1511.

Lemma collineation_1512 : is_collineation2 fp_1512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1512.

Lemma collineation_1513 : is_collineation2 fp_1513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1513.

Lemma collineation_1514 : is_collineation2 fp_1514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1514.

Lemma collineation_1515 : is_collineation2 fp_1515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1515.

Lemma collineation_1516 : is_collineation2 fp_1516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1516.

Lemma collineation_1517 : is_collineation2 fp_1517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1517.

Lemma collineation_1518 : is_collineation2 fp_1518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1518.

Lemma collineation_1519 : is_collineation2 fp_1519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1519.

Lemma collineation_1520 : is_collineation2 fp_1520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1520.

Lemma collineation_1521 : is_collineation2 fp_1521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1521.

Lemma collineation_1522 : is_collineation2 fp_1522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1522.

Lemma collineation_1523 : is_collineation2 fp_1523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1523.

Lemma collineation_1524 : is_collineation2 fp_1524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1524.

Lemma collineation_1525 : is_collineation2 fp_1525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1525.

Lemma collineation_1526 : is_collineation2 fp_1526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1526.

Lemma collineation_1527 : is_collineation2 fp_1527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1527.

Lemma collineation_1528 : is_collineation2 fp_1528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1528.

Lemma collineation_1529 : is_collineation2 fp_1529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1529.

Lemma collineation_1530 : is_collineation2 fp_1530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1530.

Lemma collineation_1531 : is_collineation2 fp_1531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1531.

Lemma collineation_1532 : is_collineation2 fp_1532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1532.

Lemma collineation_1533 : is_collineation2 fp_1533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1533.

Lemma collineation_1534 : is_collineation2 fp_1534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1534.

Lemma collineation_1535 : is_collineation2 fp_1535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1535.

Lemma collineation_1536 : is_collineation2 fp_1536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1536.

Lemma collineation_1537 : is_collineation2 fp_1537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1537.

Lemma collineation_1538 : is_collineation2 fp_1538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1538.

Lemma collineation_1539 : is_collineation2 fp_1539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1539.

Lemma collineation_1540 : is_collineation2 fp_1540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1540.

Lemma collineation_1541 : is_collineation2 fp_1541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1541.

Lemma collineation_1542 : is_collineation2 fp_1542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1542.

Lemma collineation_1543 : is_collineation2 fp_1543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1543.

Lemma collineation_1544 : is_collineation2 fp_1544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1544.

Lemma collineation_1545 : is_collineation2 fp_1545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1545.

Lemma collineation_1546 : is_collineation2 fp_1546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1546.

Lemma collineation_1547 : is_collineation2 fp_1547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1547.

Lemma collineation_1548 : is_collineation2 fp_1548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1548.

Lemma collineation_1549 : is_collineation2 fp_1549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1549.

Lemma collineation_1550 : is_collineation2 fp_1550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1550.

Lemma collineation_1551 : is_collineation2 fp_1551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1551.

Lemma collineation_1552 : is_collineation2 fp_1552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1552.

Lemma collineation_1553 : is_collineation2 fp_1553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1553.

Lemma collineation_1554 : is_collineation2 fp_1554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1554.

Lemma collineation_1555 : is_collineation2 fp_1555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1555.

Lemma collineation_1556 : is_collineation2 fp_1556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1556.

Lemma collineation_1557 : is_collineation2 fp_1557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1557.

Lemma collineation_1558 : is_collineation2 fp_1558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1558.

Lemma collineation_1559 : is_collineation2 fp_1559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1559.

Lemma collineation_1560 : is_collineation2 fp_1560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1560.

Lemma collineation_1561 : is_collineation2 fp_1561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1561.

Lemma collineation_1562 : is_collineation2 fp_1562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1562.

Lemma collineation_1563 : is_collineation2 fp_1563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1563.

Lemma collineation_1564 : is_collineation2 fp_1564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1564.

Lemma collineation_1565 : is_collineation2 fp_1565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1565.

Lemma collineation_1566 : is_collineation2 fp_1566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1566.

Lemma collineation_1567 : is_collineation2 fp_1567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1567.

Lemma collineation_1568 : is_collineation2 fp_1568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1568.

Lemma collineation_1569 : is_collineation2 fp_1569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1569.

Lemma collineation_1570 : is_collineation2 fp_1570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1570.

Lemma collineation_1571 : is_collineation2 fp_1571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1571.

Lemma collineation_1572 : is_collineation2 fp_1572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1572.

Lemma collineation_1573 : is_collineation2 fp_1573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1573.

Lemma collineation_1574 : is_collineation2 fp_1574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1574.

Lemma collineation_1575 : is_collineation2 fp_1575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1575.

Lemma collineation_1576 : is_collineation2 fp_1576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1576.

Lemma collineation_1577 : is_collineation2 fp_1577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1577.

Lemma collineation_1578 : is_collineation2 fp_1578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1578.

Lemma collineation_1579 : is_collineation2 fp_1579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1579.

Lemma collineation_1580 : is_collineation2 fp_1580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1580.

Lemma collineation_1581 : is_collineation2 fp_1581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1581.

Lemma collineation_1582 : is_collineation2 fp_1582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1582.

Lemma collineation_1583 : is_collineation2 fp_1583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1583.

Lemma collineation_1584 : is_collineation2 fp_1584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1584.

Lemma collineation_1585 : is_collineation2 fp_1585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1585.

Lemma collineation_1586 : is_collineation2 fp_1586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1586.

Lemma collineation_1587 : is_collineation2 fp_1587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1587.

Lemma collineation_1588 : is_collineation2 fp_1588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1588.

Lemma collineation_1589 : is_collineation2 fp_1589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1589.

Lemma collineation_1590 : is_collineation2 fp_1590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1590.

Lemma collineation_1591 : is_collineation2 fp_1591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1591.

Lemma collineation_1592 : is_collineation2 fp_1592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1592.

Lemma collineation_1593 : is_collineation2 fp_1593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1593.

Lemma collineation_1594 : is_collineation2 fp_1594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1594.

Lemma collineation_1595 : is_collineation2 fp_1595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1595.

Lemma collineation_1596 : is_collineation2 fp_1596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1596.

Lemma collineation_1597 : is_collineation2 fp_1597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1597.

Lemma collineation_1598 : is_collineation2 fp_1598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1598.

Lemma collineation_1599 : is_collineation2 fp_1599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1599.

Lemma collineation_1600 : is_collineation2 fp_1600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1600.

Lemma collineation_1601 : is_collineation2 fp_1601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1601.

Lemma collineation_1602 : is_collineation2 fp_1602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1602.

Lemma collineation_1603 : is_collineation2 fp_1603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1603.

Lemma collineation_1604 : is_collineation2 fp_1604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1604.

Lemma collineation_1605 : is_collineation2 fp_1605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1605.

Lemma collineation_1606 : is_collineation2 fp_1606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1606.

Lemma collineation_1607 : is_collineation2 fp_1607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1607.

Lemma collineation_1608 : is_collineation2 fp_1608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1608.

Lemma collineation_1609 : is_collineation2 fp_1609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1609.

Lemma collineation_1610 : is_collineation2 fp_1610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1610.

Lemma collineation_1611 : is_collineation2 fp_1611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1611.

Lemma collineation_1612 : is_collineation2 fp_1612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1612.

Lemma collineation_1613 : is_collineation2 fp_1613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1613.

Lemma collineation_1614 : is_collineation2 fp_1614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1614.

Lemma collineation_1615 : is_collineation2 fp_1615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1615.

Lemma collineation_1616 : is_collineation2 fp_1616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1616.

Lemma collineation_1617 : is_collineation2 fp_1617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1617.

Lemma collineation_1618 : is_collineation2 fp_1618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1618.

Lemma collineation_1619 : is_collineation2 fp_1619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1619.

Lemma collineation_1620 : is_collineation2 fp_1620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1620.

Lemma collineation_1621 : is_collineation2 fp_1621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1621.

Lemma collineation_1622 : is_collineation2 fp_1622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1622.

Lemma collineation_1623 : is_collineation2 fp_1623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1623.

Lemma collineation_1624 : is_collineation2 fp_1624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1624.

Lemma collineation_1625 : is_collineation2 fp_1625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1625.

Lemma collineation_1626 : is_collineation2 fp_1626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1626.

Lemma collineation_1627 : is_collineation2 fp_1627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1627.

Lemma collineation_1628 : is_collineation2 fp_1628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1628.

Lemma collineation_1629 : is_collineation2 fp_1629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1629.

Lemma collineation_1630 : is_collineation2 fp_1630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1630.

Lemma collineation_1631 : is_collineation2 fp_1631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1631.

Lemma collineation_1632 : is_collineation2 fp_1632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1632.

Lemma collineation_1633 : is_collineation2 fp_1633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1633.

Lemma collineation_1634 : is_collineation2 fp_1634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1634.

Lemma collineation_1635 : is_collineation2 fp_1635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1635.

Lemma collineation_1636 : is_collineation2 fp_1636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1636.

Lemma collineation_1637 : is_collineation2 fp_1637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1637.

Lemma collineation_1638 : is_collineation2 fp_1638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1638.

Lemma collineation_1639 : is_collineation2 fp_1639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1639.

Lemma collineation_1640 : is_collineation2 fp_1640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1640.

Lemma collineation_1641 : is_collineation2 fp_1641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1641.

Lemma collineation_1642 : is_collineation2 fp_1642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1642.

Lemma collineation_1643 : is_collineation2 fp_1643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1643.

Lemma collineation_1644 : is_collineation2 fp_1644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1644.

Lemma collineation_1645 : is_collineation2 fp_1645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1645.

Lemma collineation_1646 : is_collineation2 fp_1646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1646.

Lemma collineation_1647 : is_collineation2 fp_1647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1647.

Lemma collineation_1648 : is_collineation2 fp_1648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1648.

Lemma collineation_1649 : is_collineation2 fp_1649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1649.

Lemma collineation_1650 : is_collineation2 fp_1650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1650.

Lemma collineation_1651 : is_collineation2 fp_1651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1651.

Lemma collineation_1652 : is_collineation2 fp_1652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1652.

Lemma collineation_1653 : is_collineation2 fp_1653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1653.

Lemma collineation_1654 : is_collineation2 fp_1654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1654.

Lemma collineation_1655 : is_collineation2 fp_1655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1655.

Lemma collineation_1656 : is_collineation2 fp_1656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1656.

Lemma collineation_1657 : is_collineation2 fp_1657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1657.

Lemma collineation_1658 : is_collineation2 fp_1658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1658.

Lemma collineation_1659 : is_collineation2 fp_1659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1659.

Lemma collineation_1660 : is_collineation2 fp_1660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1660.

Lemma collineation_1661 : is_collineation2 fp_1661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1661.

Lemma collineation_1662 : is_collineation2 fp_1662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1662.

Lemma collineation_1663 : is_collineation2 fp_1663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1663.

Lemma collineation_1664 : is_collineation2 fp_1664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1664.

Lemma collineation_1665 : is_collineation2 fp_1665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1665.

Lemma collineation_1666 : is_collineation2 fp_1666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1666.

Lemma collineation_1667 : is_collineation2 fp_1667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1667.

Lemma collineation_1668 : is_collineation2 fp_1668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1668.

Lemma collineation_1669 : is_collineation2 fp_1669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1669.

Lemma collineation_1670 : is_collineation2 fp_1670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1670.

Lemma collineation_1671 : is_collineation2 fp_1671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1671.

Lemma collineation_1672 : is_collineation2 fp_1672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1672.

Lemma collineation_1673 : is_collineation2 fp_1673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1673.

Lemma collineation_1674 : is_collineation2 fp_1674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1674.

Lemma collineation_1675 : is_collineation2 fp_1675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1675.

Lemma collineation_1676 : is_collineation2 fp_1676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1676.

Lemma collineation_1677 : is_collineation2 fp_1677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1677.

Lemma collineation_1678 : is_collineation2 fp_1678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1678.

Lemma collineation_1679 : is_collineation2 fp_1679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1679.

Lemma collineation_1680 : is_collineation2 fp_1680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1680.

Lemma collineation_1681 : is_collineation2 fp_1681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1681.

Lemma collineation_1682 : is_collineation2 fp_1682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1682.

Lemma collineation_1683 : is_collineation2 fp_1683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1683.

Lemma collineation_1684 : is_collineation2 fp_1684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1684.

Lemma collineation_1685 : is_collineation2 fp_1685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1685.

Lemma collineation_1686 : is_collineation2 fp_1686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1686.

Lemma collineation_1687 : is_collineation2 fp_1687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1687.

Lemma collineation_1688 : is_collineation2 fp_1688.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1688 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1688.

Lemma collineation_1689 : is_collineation2 fp_1689.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1689 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1689.

Lemma collineation_1690 : is_collineation2 fp_1690.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1690 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1690.

Lemma collineation_1691 : is_collineation2 fp_1691.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1691 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1691.

Lemma collineation_1692 : is_collineation2 fp_1692.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1692 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1692.

Lemma collineation_1693 : is_collineation2 fp_1693.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1693 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1693.

Lemma collineation_1694 : is_collineation2 fp_1694.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1694 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1694.

Lemma collineation_1695 : is_collineation2 fp_1695.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1695 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1695.

Lemma collineation_1696 : is_collineation2 fp_1696.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1696 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1696.

Lemma collineation_1697 : is_collineation2 fp_1697.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1697 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1697.

Lemma collineation_1698 : is_collineation2 fp_1698.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1698 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1698.

Lemma collineation_1699 : is_collineation2 fp_1699.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1699 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1699.

Lemma collineation_1700 : is_collineation2 fp_1700.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1700 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1700.

Lemma collineation_1701 : is_collineation2 fp_1701.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1701 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1701.

Lemma collineation_1702 : is_collineation2 fp_1702.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1702 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1702.

Lemma collineation_1703 : is_collineation2 fp_1703.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1703 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1703.

Lemma collineation_1704 : is_collineation2 fp_1704.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1704 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1704.

Lemma collineation_1705 : is_collineation2 fp_1705.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1705 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1705.

Lemma collineation_1706 : is_collineation2 fp_1706.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1706 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1706.

Lemma collineation_1707 : is_collineation2 fp_1707.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1707 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1707.

Lemma collineation_1708 : is_collineation2 fp_1708.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1708 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1708.

Lemma collineation_1709 : is_collineation2 fp_1709.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1709 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1709.

Lemma collineation_1710 : is_collineation2 fp_1710.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1710 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1710.

Lemma collineation_1711 : is_collineation2 fp_1711.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1711 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1711.

Lemma collineation_1712 : is_collineation2 fp_1712.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1712 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1712.

Lemma collineation_1713 : is_collineation2 fp_1713.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1713 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1713.

Lemma collineation_1714 : is_collineation2 fp_1714.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1714 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1714.

Lemma collineation_1715 : is_collineation2 fp_1715.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1715 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1715.

Lemma collineation_1716 : is_collineation2 fp_1716.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1716 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1716.

Lemma collineation_1717 : is_collineation2 fp_1717.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1717 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1717.

Lemma collineation_1718 : is_collineation2 fp_1718.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1718 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1718.

Lemma collineation_1719 : is_collineation2 fp_1719.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1719 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1719.

Lemma collineation_1720 : is_collineation2 fp_1720.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1720 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1720.

Lemma collineation_1721 : is_collineation2 fp_1721.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1721 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1721.

Lemma collineation_1722 : is_collineation2 fp_1722.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1722 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1722.

Lemma collineation_1723 : is_collineation2 fp_1723.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1723 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1723.

Lemma collineation_1724 : is_collineation2 fp_1724.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1724 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1724.

Lemma collineation_1725 : is_collineation2 fp_1725.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1725 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1725.

Lemma collineation_1726 : is_collineation2 fp_1726.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1726 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1726.

Lemma collineation_1727 : is_collineation2 fp_1727.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1727 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1727.

Lemma collineation_1728 : is_collineation2 fp_1728.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1728 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1728.

Lemma collineation_1729 : is_collineation2 fp_1729.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1729 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1729.

Lemma collineation_1730 : is_collineation2 fp_1730.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1730 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1730.

Lemma collineation_1731 : is_collineation2 fp_1731.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1731 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1731.

Lemma collineation_1732 : is_collineation2 fp_1732.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1732 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1732.

Lemma collineation_1733 : is_collineation2 fp_1733.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1733 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1733.

Lemma collineation_1734 : is_collineation2 fp_1734.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1734 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1734.

Lemma collineation_1735 : is_collineation2 fp_1735.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1735 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1735.

Lemma collineation_1736 : is_collineation2 fp_1736.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1736 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1736.

Lemma collineation_1737 : is_collineation2 fp_1737.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1737 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1737.

Lemma collineation_1738 : is_collineation2 fp_1738.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1738 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1738.

Lemma collineation_1739 : is_collineation2 fp_1739.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1739 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1739.

Lemma collineation_1740 : is_collineation2 fp_1740.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1740 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1740.

Lemma collineation_1741 : is_collineation2 fp_1741.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1741 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1741.

Lemma collineation_1742 : is_collineation2 fp_1742.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1742 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1742.

Lemma collineation_1743 : is_collineation2 fp_1743.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1743 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1743.

Lemma collineation_1744 : is_collineation2 fp_1744.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1744 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1744.

Lemma collineation_1745 : is_collineation2 fp_1745.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1745 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1745.

Lemma collineation_1746 : is_collineation2 fp_1746.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1746 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1746.

Lemma collineation_1747 : is_collineation2 fp_1747.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1747 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1747.

Lemma collineation_1748 : is_collineation2 fp_1748.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1748 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1748.

Lemma collineation_1749 : is_collineation2 fp_1749.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1749 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1749.

Lemma collineation_1750 : is_collineation2 fp_1750.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1750 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1750.

Lemma collineation_1751 : is_collineation2 fp_1751.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1751 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1751.

Lemma collineation_1752 : is_collineation2 fp_1752.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1752 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1752.

Lemma collineation_1753 : is_collineation2 fp_1753.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1753 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1753.

Lemma collineation_1754 : is_collineation2 fp_1754.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1754 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1754.

Lemma collineation_1755 : is_collineation2 fp_1755.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1755 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1755.

Lemma collineation_1756 : is_collineation2 fp_1756.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1756 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1756.

Lemma collineation_1757 : is_collineation2 fp_1757.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1757 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1757.

Lemma collineation_1758 : is_collineation2 fp_1758.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1758 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1758.

Lemma collineation_1759 : is_collineation2 fp_1759.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1759 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1759.

Lemma collineation_1760 : is_collineation2 fp_1760.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1760 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1760.

Lemma collineation_1761 : is_collineation2 fp_1761.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1761 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1761.

Lemma collineation_1762 : is_collineation2 fp_1762.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1762 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1762.

Lemma collineation_1763 : is_collineation2 fp_1763.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1763 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1763.

Lemma collineation_1764 : is_collineation2 fp_1764.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1764 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1764.

Lemma collineation_1765 : is_collineation2 fp_1765.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1765 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1765.

Lemma collineation_1766 : is_collineation2 fp_1766.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1766 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1766.

Lemma collineation_1767 : is_collineation2 fp_1767.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1767 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1767.

Lemma collineation_1768 : is_collineation2 fp_1768.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1768 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1768.

Lemma collineation_1769 : is_collineation2 fp_1769.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1769 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1769.

Lemma collineation_1770 : is_collineation2 fp_1770.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1770 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1770.

Lemma collineation_1771 : is_collineation2 fp_1771.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1771 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1771.

Lemma collineation_1772 : is_collineation2 fp_1772.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1772 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1772.

Lemma collineation_1773 : is_collineation2 fp_1773.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1773 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1773.

Lemma collineation_1774 : is_collineation2 fp_1774.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1774 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1774.

Lemma collineation_1775 : is_collineation2 fp_1775.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1775 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1775.

Lemma collineation_1776 : is_collineation2 fp_1776.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1776 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1776.

Lemma collineation_1777 : is_collineation2 fp_1777.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1777 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1777.

Lemma collineation_1778 : is_collineation2 fp_1778.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1778 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1778.

Lemma collineation_1779 : is_collineation2 fp_1779.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1779 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1779.

Lemma collineation_1780 : is_collineation2 fp_1780.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1780 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1780.

Lemma collineation_1781 : is_collineation2 fp_1781.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1781 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1781.

Lemma collineation_1782 : is_collineation2 fp_1782.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1782 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1782.

Lemma collineation_1783 : is_collineation2 fp_1783.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1783 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1783.

Lemma collineation_1784 : is_collineation2 fp_1784.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1784 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1784.

Lemma collineation_1785 : is_collineation2 fp_1785.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1785 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1785.

Lemma collineation_1786 : is_collineation2 fp_1786.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1786 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1786.

Lemma collineation_1787 : is_collineation2 fp_1787.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1787 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1787.

Lemma collineation_1788 : is_collineation2 fp_1788.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1788 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1788.

Lemma collineation_1789 : is_collineation2 fp_1789.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1789 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1789.

Lemma collineation_1790 : is_collineation2 fp_1790.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1790 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1790.

Lemma collineation_1791 : is_collineation2 fp_1791.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1791 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1791.

Lemma collineation_1792 : is_collineation2 fp_1792.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1792 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1792.

Lemma collineation_1793 : is_collineation2 fp_1793.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1793 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1793.

Lemma collineation_1794 : is_collineation2 fp_1794.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1794 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1794.

Lemma collineation_1795 : is_collineation2 fp_1795.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1795 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1795.

Lemma collineation_1796 : is_collineation2 fp_1796.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1796 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1796.

Lemma collineation_1797 : is_collineation2 fp_1797.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1797 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1797.

Lemma collineation_1798 : is_collineation2 fp_1798.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1798 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1798.

Lemma collineation_1799 : is_collineation2 fp_1799.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1799 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1799.

Lemma collineation_1800 : is_collineation2 fp_1800.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1800 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1800.

Lemma collineation_1801 : is_collineation2 fp_1801.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1801 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1801.

Lemma collineation_1802 : is_collineation2 fp_1802.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1802 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1802.

Lemma collineation_1803 : is_collineation2 fp_1803.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1803 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1803.

Lemma collineation_1804 : is_collineation2 fp_1804.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1804 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1804.

Lemma collineation_1805 : is_collineation2 fp_1805.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1805 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1805.

Lemma collineation_1806 : is_collineation2 fp_1806.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1806 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1806.

Lemma collineation_1807 : is_collineation2 fp_1807.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1807 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1807.

Lemma collineation_1808 : is_collineation2 fp_1808.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1808 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1808.

Lemma collineation_1809 : is_collineation2 fp_1809.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1809 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1809.

Lemma collineation_1810 : is_collineation2 fp_1810.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1810 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1810.

Lemma collineation_1811 : is_collineation2 fp_1811.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1811 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1811.

Lemma collineation_1812 : is_collineation2 fp_1812.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1812 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1812.

Lemma collineation_1813 : is_collineation2 fp_1813.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1813 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1813.

Lemma collineation_1814 : is_collineation2 fp_1814.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1814 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1814.

Lemma collineation_1815 : is_collineation2 fp_1815.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1815 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1815.

Lemma collineation_1816 : is_collineation2 fp_1816.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1816 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1816.

Lemma collineation_1817 : is_collineation2 fp_1817.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1817 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1817.

Lemma collineation_1818 : is_collineation2 fp_1818.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1818 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1818.

Lemma collineation_1819 : is_collineation2 fp_1819.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1819 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1819.

Lemma collineation_1820 : is_collineation2 fp_1820.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1820 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1820.

Lemma collineation_1821 : is_collineation2 fp_1821.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1821 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1821.

Lemma collineation_1822 : is_collineation2 fp_1822.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1822 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1822.

Lemma collineation_1823 : is_collineation2 fp_1823.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1823 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1823.

Lemma collineation_1824 : is_collineation2 fp_1824.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1824 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1824.

Lemma collineation_1825 : is_collineation2 fp_1825.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1825 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1825.

Lemma collineation_1826 : is_collineation2 fp_1826.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1826 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1826.

Lemma collineation_1827 : is_collineation2 fp_1827.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1827 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1827.

Lemma collineation_1828 : is_collineation2 fp_1828.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1828 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1828.

Lemma collineation_1829 : is_collineation2 fp_1829.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1829 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1829.

Lemma collineation_1830 : is_collineation2 fp_1830.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1830 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1830.

Lemma collineation_1831 : is_collineation2 fp_1831.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1831 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1831.

Lemma collineation_1832 : is_collineation2 fp_1832.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1832 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1832.

Lemma collineation_1833 : is_collineation2 fp_1833.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1833 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1833.

Lemma collineation_1834 : is_collineation2 fp_1834.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1834 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1834.

Lemma collineation_1835 : is_collineation2 fp_1835.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1835 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1835.

Lemma collineation_1836 : is_collineation2 fp_1836.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1836 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1836.

Lemma collineation_1837 : is_collineation2 fp_1837.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1837 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1837.

Lemma collineation_1838 : is_collineation2 fp_1838.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1838 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1838.

Lemma collineation_1839 : is_collineation2 fp_1839.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1839 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1839.

Lemma collineation_1840 : is_collineation2 fp_1840.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1840 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1840.

Lemma collineation_1841 : is_collineation2 fp_1841.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1841 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1841.

Lemma collineation_1842 : is_collineation2 fp_1842.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1842 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1842.

Lemma collineation_1843 : is_collineation2 fp_1843.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1843 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1843.

Lemma collineation_1844 : is_collineation2 fp_1844.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1844 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1844.

Lemma collineation_1845 : is_collineation2 fp_1845.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1845 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1845.

Lemma collineation_1846 : is_collineation2 fp_1846.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1846 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1846.

Lemma collineation_1847 : is_collineation2 fp_1847.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1847 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1847.

Lemma collineation_1848 : is_collineation2 fp_1848.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1848 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1848.

Lemma collineation_1849 : is_collineation2 fp_1849.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1849 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1849.

Lemma collineation_1850 : is_collineation2 fp_1850.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1850 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1850.

Lemma collineation_1851 : is_collineation2 fp_1851.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1851 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1851.

Lemma collineation_1852 : is_collineation2 fp_1852.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1852 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1852.

Lemma collineation_1853 : is_collineation2 fp_1853.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1853 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1853.

Lemma collineation_1854 : is_collineation2 fp_1854.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1854 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1854.

Lemma collineation_1855 : is_collineation2 fp_1855.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1855 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1855.

Lemma collineation_1856 : is_collineation2 fp_1856.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1856 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1856.

Lemma collineation_1857 : is_collineation2 fp_1857.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1857 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1857.

Lemma collineation_1858 : is_collineation2 fp_1858.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1858 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1858.

Lemma collineation_1859 : is_collineation2 fp_1859.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1859 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1859.

Lemma collineation_1860 : is_collineation2 fp_1860.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1860 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1860.

Lemma collineation_1861 : is_collineation2 fp_1861.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1861 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1861.

Lemma collineation_1862 : is_collineation2 fp_1862.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1862 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1862.

Lemma collineation_1863 : is_collineation2 fp_1863.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1863 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1863.

Lemma collineation_1864 : is_collineation2 fp_1864.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1864 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1864.

Lemma collineation_1865 : is_collineation2 fp_1865.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1865 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1865.

Lemma collineation_1866 : is_collineation2 fp_1866.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1866 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1866.

Lemma collineation_1867 : is_collineation2 fp_1867.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1867 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1867.

Lemma collineation_1868 : is_collineation2 fp_1868.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1868 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1868.

Lemma collineation_1869 : is_collineation2 fp_1869.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1869 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1869.

Lemma collineation_1870 : is_collineation2 fp_1870.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1870 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1870.

Lemma collineation_1871 : is_collineation2 fp_1871.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1871 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1871.

Lemma collineation_1872 : is_collineation2 fp_1872.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1872 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1872.

Lemma collineation_1873 : is_collineation2 fp_1873.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1873 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1873.

Lemma collineation_1874 : is_collineation2 fp_1874.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1874 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1874.

Lemma collineation_1875 : is_collineation2 fp_1875.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1875 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1875.

Lemma collineation_1876 : is_collineation2 fp_1876.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1876 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1876.

Lemma collineation_1877 : is_collineation2 fp_1877.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1877 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1877.

Lemma collineation_1878 : is_collineation2 fp_1878.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1878 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1878.

Lemma collineation_1879 : is_collineation2 fp_1879.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1879 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1879.

Lemma collineation_1880 : is_collineation2 fp_1880.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1880 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1880.

Lemma collineation_1881 : is_collineation2 fp_1881.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1881 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1881.

Lemma collineation_1882 : is_collineation2 fp_1882.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1882 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1882.

Lemma collineation_1883 : is_collineation2 fp_1883.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1883 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1883.

Lemma collineation_1884 : is_collineation2 fp_1884.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1884 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1884.

Lemma collineation_1885 : is_collineation2 fp_1885.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1885 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1885.

Lemma collineation_1886 : is_collineation2 fp_1886.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1886 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1886.

Lemma collineation_1887 : is_collineation2 fp_1887.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1887 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1887.

Lemma collineation_1888 : is_collineation2 fp_1888.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1888 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1888.

Lemma collineation_1889 : is_collineation2 fp_1889.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1889 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1889.

Lemma collineation_1890 : is_collineation2 fp_1890.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1890 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1890.

Lemma collineation_1891 : is_collineation2 fp_1891.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1891 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1891.

Lemma collineation_1892 : is_collineation2 fp_1892.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1892 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1892.

Lemma collineation_1893 : is_collineation2 fp_1893.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1893 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1893.

Lemma collineation_1894 : is_collineation2 fp_1894.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1894 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1894.

Lemma collineation_1895 : is_collineation2 fp_1895.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1895 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1895.

Lemma collineation_1896 : is_collineation2 fp_1896.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1896 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1896.

Lemma collineation_1897 : is_collineation2 fp_1897.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1897 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1897.

Lemma collineation_1898 : is_collineation2 fp_1898.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1898 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1898.

Lemma collineation_1899 : is_collineation2 fp_1899.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1899 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1899.

Lemma collineation_1900 : is_collineation2 fp_1900.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1900 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1900.

Lemma collineation_1901 : is_collineation2 fp_1901.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1901 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1901.

Lemma collineation_1902 : is_collineation2 fp_1902.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1902 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1902.

Lemma collineation_1903 : is_collineation2 fp_1903.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1903 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1903.

Lemma collineation_1904 : is_collineation2 fp_1904.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1904 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1904.

Lemma collineation_1905 : is_collineation2 fp_1905.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1905 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1905.

Lemma collineation_1906 : is_collineation2 fp_1906.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1906 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1906.

Lemma collineation_1907 : is_collineation2 fp_1907.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1907 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1907.

Lemma collineation_1908 : is_collineation2 fp_1908.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1908 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1908.

Lemma collineation_1909 : is_collineation2 fp_1909.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1909 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1909.

Lemma collineation_1910 : is_collineation2 fp_1910.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1910 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1910.

Lemma collineation_1911 : is_collineation2 fp_1911.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1911 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1911.

Lemma collineation_1912 : is_collineation2 fp_1912.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1912 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1912.

Lemma collineation_1913 : is_collineation2 fp_1913.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1913 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1913.

Lemma collineation_1914 : is_collineation2 fp_1914.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1914 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1914.

Lemma collineation_1915 : is_collineation2 fp_1915.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1915 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1915.

Lemma collineation_1916 : is_collineation2 fp_1916.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1916 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1916.

Lemma collineation_1917 : is_collineation2 fp_1917.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1917 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1917.

Lemma collineation_1918 : is_collineation2 fp_1918.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1918 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1918.

Lemma collineation_1919 : is_collineation2 fp_1919.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1919 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1919.

Lemma collineation_1920 : is_collineation2 fp_1920.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1920 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1920.

Lemma collineation_1921 : is_collineation2 fp_1921.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1921 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1921.

Lemma collineation_1922 : is_collineation2 fp_1922.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1922 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1922.

Lemma collineation_1923 : is_collineation2 fp_1923.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1923 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1923.

Lemma collineation_1924 : is_collineation2 fp_1924.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1924 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1924.

Lemma collineation_1925 : is_collineation2 fp_1925.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1925 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1925.

Lemma collineation_1926 : is_collineation2 fp_1926.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1926 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1926.

Lemma collineation_1927 : is_collineation2 fp_1927.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1927 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1927.

Lemma collineation_1928 : is_collineation2 fp_1928.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1928 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1928.

Lemma collineation_1929 : is_collineation2 fp_1929.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1929 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1929.

Lemma collineation_1930 : is_collineation2 fp_1930.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1930 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1930.

Lemma collineation_1931 : is_collineation2 fp_1931.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1931 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1931.

Lemma collineation_1932 : is_collineation2 fp_1932.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1932 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1932.

Lemma collineation_1933 : is_collineation2 fp_1933.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1933 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1933.

Lemma collineation_1934 : is_collineation2 fp_1934.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1934 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1934.

Lemma collineation_1935 : is_collineation2 fp_1935.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1935 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1935.

Lemma collineation_1936 : is_collineation2 fp_1936.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1936 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1936.

Lemma collineation_1937 : is_collineation2 fp_1937.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1937 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1937.

Lemma collineation_1938 : is_collineation2 fp_1938.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1938 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1938.

Lemma collineation_1939 : is_collineation2 fp_1939.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1939 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1939.

Lemma collineation_1940 : is_collineation2 fp_1940.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1940 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1940.

Lemma collineation_1941 : is_collineation2 fp_1941.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1941 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1941.

Lemma collineation_1942 : is_collineation2 fp_1942.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1942 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1942.

Lemma collineation_1943 : is_collineation2 fp_1943.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1943 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1943.

Lemma collineation_1944 : is_collineation2 fp_1944.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1944 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1944.

Lemma collineation_1945 : is_collineation2 fp_1945.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1945 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1945.

Lemma collineation_1946 : is_collineation2 fp_1946.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1946 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1946.

Lemma collineation_1947 : is_collineation2 fp_1947.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1947 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1947.

Lemma collineation_1948 : is_collineation2 fp_1948.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1948 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1948.

Lemma collineation_1949 : is_collineation2 fp_1949.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1949 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1949.

Lemma collineation_1950 : is_collineation2 fp_1950.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1950 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1950.

Lemma collineation_1951 : is_collineation2 fp_1951.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1951 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1951.

Lemma collineation_1952 : is_collineation2 fp_1952.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1952 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1952.

Lemma collineation_1953 : is_collineation2 fp_1953.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1953 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1953.

Lemma collineation_1954 : is_collineation2 fp_1954.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1954 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1954.

Lemma collineation_1955 : is_collineation2 fp_1955.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1955 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1955.

Lemma collineation_1956 : is_collineation2 fp_1956.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1956 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1956.

Lemma collineation_1957 : is_collineation2 fp_1957.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1957 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1957.

Lemma collineation_1958 : is_collineation2 fp_1958.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1958 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1958.

Lemma collineation_1959 : is_collineation2 fp_1959.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1959 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1959.

Lemma collineation_1960 : is_collineation2 fp_1960.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1960 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1960.

Lemma collineation_1961 : is_collineation2 fp_1961.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1961 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1961.

Lemma collineation_1962 : is_collineation2 fp_1962.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1962 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1962.

Lemma collineation_1963 : is_collineation2 fp_1963.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1963 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1963.

Lemma collineation_1964 : is_collineation2 fp_1964.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1964 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1964.

Lemma collineation_1965 : is_collineation2 fp_1965.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1965 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1965.

Lemma collineation_1966 : is_collineation2 fp_1966.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1966 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1966.

Lemma collineation_1967 : is_collineation2 fp_1967.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1967 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1967.

Lemma collineation_1968 : is_collineation2 fp_1968.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1968 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1968.

Lemma collineation_1969 : is_collineation2 fp_1969.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1969 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1969.

Lemma collineation_1970 : is_collineation2 fp_1970.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1970 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1970.

Lemma collineation_1971 : is_collineation2 fp_1971.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1971 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1971.

Lemma collineation_1972 : is_collineation2 fp_1972.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1972 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1972.

Lemma collineation_1973 : is_collineation2 fp_1973.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1973 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1973.

Lemma collineation_1974 : is_collineation2 fp_1974.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1974 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1974.

Lemma collineation_1975 : is_collineation2 fp_1975.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1975 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1975.

Lemma collineation_1976 : is_collineation2 fp_1976.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1976 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1976.

Lemma collineation_1977 : is_collineation2 fp_1977.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1977 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1977.

Lemma collineation_1978 : is_collineation2 fp_1978.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1978 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1978.

Lemma collineation_1979 : is_collineation2 fp_1979.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1979 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1979.

Lemma collineation_1980 : is_collineation2 fp_1980.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1980 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1980.

Lemma collineation_1981 : is_collineation2 fp_1981.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1981 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1981.

Lemma collineation_1982 : is_collineation2 fp_1982.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1982 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1982.

Lemma collineation_1983 : is_collineation2 fp_1983.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1983 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1983.

Lemma collineation_1984 : is_collineation2 fp_1984.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1984 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1984.

Lemma collineation_1985 : is_collineation2 fp_1985.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1985 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1985.

Lemma collineation_1986 : is_collineation2 fp_1986.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1986 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1986.

Lemma collineation_1987 : is_collineation2 fp_1987.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1987 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1987.

Lemma collineation_1988 : is_collineation2 fp_1988.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1988 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1988.

Lemma collineation_1989 : is_collineation2 fp_1989.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1989 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1989.

Lemma collineation_1990 : is_collineation2 fp_1990.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1990 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1990.

Lemma collineation_1991 : is_collineation2 fp_1991.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1991 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1991.

Lemma collineation_1992 : is_collineation2 fp_1992.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1992 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1992.

Lemma collineation_1993 : is_collineation2 fp_1993.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1993 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1993.

Lemma collineation_1994 : is_collineation2 fp_1994.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1994 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1994.

Lemma collineation_1995 : is_collineation2 fp_1995.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1995 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1995.

Lemma collineation_1996 : is_collineation2 fp_1996.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1996 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1996.

Lemma collineation_1997 : is_collineation2 fp_1997.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1997 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1997.

Lemma collineation_1998 : is_collineation2 fp_1998.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1998 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1998.

Lemma collineation_1999 : is_collineation2 fp_1999.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_1999 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_1999.

Lemma collineation_2000 : is_collineation2 fp_2000.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2000 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2000.

Lemma collineation_2001 : is_collineation2 fp_2001.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2001 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2001.

Lemma collineation_2002 : is_collineation2 fp_2002.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2002 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2002.

Lemma collineation_2003 : is_collineation2 fp_2003.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2003 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2003.

Lemma collineation_2004 : is_collineation2 fp_2004.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2004 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2004.

Lemma collineation_2005 : is_collineation2 fp_2005.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2005 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2005.

Lemma collineation_2006 : is_collineation2 fp_2006.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2006 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2006.

Lemma collineation_2007 : is_collineation2 fp_2007.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2007 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2007.

Lemma collineation_2008 : is_collineation2 fp_2008.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2008 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2008.

Lemma collineation_2009 : is_collineation2 fp_2009.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2009 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2009.

Lemma collineation_2010 : is_collineation2 fp_2010.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2010 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2010.

Lemma collineation_2011 : is_collineation2 fp_2011.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2011 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2011.

Lemma collineation_2012 : is_collineation2 fp_2012.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2012 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2012.

Lemma collineation_2013 : is_collineation2 fp_2013.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2013 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2013.

Lemma collineation_2014 : is_collineation2 fp_2014.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2014 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2014.

Lemma collineation_2015 : is_collineation2 fp_2015.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2015 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2015.

Lemma collineation_2016 : is_collineation2 fp_2016.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2016 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2016.

Lemma collineation_2017 : is_collineation2 fp_2017.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2017 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2017.

Lemma collineation_2018 : is_collineation2 fp_2018.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2018 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2018.

Lemma collineation_2019 : is_collineation2 fp_2019.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2019 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2019.

Lemma collineation_2020 : is_collineation2 fp_2020.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2020 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2020.

Lemma collineation_2021 : is_collineation2 fp_2021.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2021 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2021.

Lemma collineation_2022 : is_collineation2 fp_2022.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2022 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2022.

Lemma collineation_2023 : is_collineation2 fp_2023.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2023 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2023.

Lemma collineation_2024 : is_collineation2 fp_2024.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2024 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2024.

Lemma collineation_2025 : is_collineation2 fp_2025.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2025 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2025.

Lemma collineation_2026 : is_collineation2 fp_2026.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2026 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2026.

Lemma collineation_2027 : is_collineation2 fp_2027.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2027 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2027.

Lemma collineation_2028 : is_collineation2 fp_2028.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2028 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2028.

Lemma collineation_2029 : is_collineation2 fp_2029.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2029 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2029.

Lemma collineation_2030 : is_collineation2 fp_2030.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2030 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2030.

Lemma collineation_2031 : is_collineation2 fp_2031.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2031 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2031.

Lemma collineation_2032 : is_collineation2 fp_2032.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2032 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2032.

Lemma collineation_2033 : is_collineation2 fp_2033.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2033 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2033.

Lemma collineation_2034 : is_collineation2 fp_2034.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2034 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2034.

Lemma collineation_2035 : is_collineation2 fp_2035.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2035 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2035.

Lemma collineation_2036 : is_collineation2 fp_2036.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2036 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2036.

Lemma collineation_2037 : is_collineation2 fp_2037.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2037 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2037.

Lemma collineation_2038 : is_collineation2 fp_2038.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2038 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2038.

Lemma collineation_2039 : is_collineation2 fp_2039.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2039 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2039.

Lemma collineation_2040 : is_collineation2 fp_2040.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2040 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2040.

Lemma collineation_2041 : is_collineation2 fp_2041.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2041 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2041.

Lemma collineation_2042 : is_collineation2 fp_2042.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2042 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2042.

Lemma collineation_2043 : is_collineation2 fp_2043.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2043 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2043.

Lemma collineation_2044 : is_collineation2 fp_2044.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2044 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2044.

Lemma collineation_2045 : is_collineation2 fp_2045.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2045 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2045.

Lemma collineation_2046 : is_collineation2 fp_2046.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2046 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2046.

Lemma collineation_2047 : is_collineation2 fp_2047.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2047 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2047.

Lemma collineation_2048 : is_collineation2 fp_2048.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2048 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2048.

Lemma collineation_2049 : is_collineation2 fp_2049.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2049 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2049.

Lemma collineation_2050 : is_collineation2 fp_2050.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2050 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2050.

Lemma collineation_2051 : is_collineation2 fp_2051.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2051 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2051.

Lemma collineation_2052 : is_collineation2 fp_2052.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2052 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2052.

Lemma collineation_2053 : is_collineation2 fp_2053.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2053 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2053.

Lemma collineation_2054 : is_collineation2 fp_2054.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2054 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2054.

Lemma collineation_2055 : is_collineation2 fp_2055.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2055 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2055.

Lemma collineation_2056 : is_collineation2 fp_2056.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2056 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2056.

Lemma collineation_2057 : is_collineation2 fp_2057.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2057 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2057.

Lemma collineation_2058 : is_collineation2 fp_2058.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2058 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2058.

Lemma collineation_2059 : is_collineation2 fp_2059.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2059 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2059.

Lemma collineation_2060 : is_collineation2 fp_2060.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2060 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2060.

Lemma collineation_2061 : is_collineation2 fp_2061.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2061 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2061.

Lemma collineation_2062 : is_collineation2 fp_2062.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2062 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2062.

Lemma collineation_2063 : is_collineation2 fp_2063.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2063 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2063.

Lemma collineation_2064 : is_collineation2 fp_2064.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2064 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2064.

Lemma collineation_2065 : is_collineation2 fp_2065.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2065 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2065.

Lemma collineation_2066 : is_collineation2 fp_2066.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2066 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2066.

Lemma collineation_2067 : is_collineation2 fp_2067.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2067 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2067.

Lemma collineation_2068 : is_collineation2 fp_2068.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2068 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2068.

Lemma collineation_2069 : is_collineation2 fp_2069.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2069 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2069.

Lemma collineation_2070 : is_collineation2 fp_2070.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2070 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2070.

Lemma collineation_2071 : is_collineation2 fp_2071.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2071 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2071.

Lemma collineation_2072 : is_collineation2 fp_2072.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2072 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2072.

Lemma collineation_2073 : is_collineation2 fp_2073.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2073 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2073.

Lemma collineation_2074 : is_collineation2 fp_2074.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2074 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2074.

Lemma collineation_2075 : is_collineation2 fp_2075.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2075 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2075.

Lemma collineation_2076 : is_collineation2 fp_2076.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2076 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2076.

Lemma collineation_2077 : is_collineation2 fp_2077.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2077 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2077.

Lemma collineation_2078 : is_collineation2 fp_2078.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2078 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2078.

Lemma collineation_2079 : is_collineation2 fp_2079.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2079 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2079.

Lemma collineation_2080 : is_collineation2 fp_2080.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2080 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2080.

Lemma collineation_2081 : is_collineation2 fp_2081.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2081 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2081.

Lemma collineation_2082 : is_collineation2 fp_2082.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2082 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2082.

Lemma collineation_2083 : is_collineation2 fp_2083.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2083 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2083.

Lemma collineation_2084 : is_collineation2 fp_2084.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2084 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2084.

Lemma collineation_2085 : is_collineation2 fp_2085.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2085 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2085.

Lemma collineation_2086 : is_collineation2 fp_2086.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2086 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2086.

Lemma collineation_2087 : is_collineation2 fp_2087.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2087 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2087.

Lemma collineation_2088 : is_collineation2 fp_2088.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2088 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2088.

Lemma collineation_2089 : is_collineation2 fp_2089.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2089 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2089.

Lemma collineation_2090 : is_collineation2 fp_2090.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2090 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2090.

Lemma collineation_2091 : is_collineation2 fp_2091.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2091 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2091.

Lemma collineation_2092 : is_collineation2 fp_2092.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2092 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2092.

Lemma collineation_2093 : is_collineation2 fp_2093.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2093 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2093.

Lemma collineation_2094 : is_collineation2 fp_2094.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2094 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2094.

Lemma collineation_2095 : is_collineation2 fp_2095.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2095 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2095.

Lemma collineation_2096 : is_collineation2 fp_2096.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2096 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2096.

Lemma collineation_2097 : is_collineation2 fp_2097.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2097 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2097.

Lemma collineation_2098 : is_collineation2 fp_2098.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2098 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2098.

Lemma collineation_2099 : is_collineation2 fp_2099.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2099 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2099.

Lemma collineation_2100 : is_collineation2 fp_2100.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2100 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2100.

Lemma collineation_2101 : is_collineation2 fp_2101.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2101 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2101.

Lemma collineation_2102 : is_collineation2 fp_2102.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2102 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2102.

Lemma collineation_2103 : is_collineation2 fp_2103.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2103 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2103.

Lemma collineation_2104 : is_collineation2 fp_2104.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2104 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2104.

Lemma collineation_2105 : is_collineation2 fp_2105.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2105 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2105.

Lemma collineation_2106 : is_collineation2 fp_2106.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2106 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2106.

Lemma collineation_2107 : is_collineation2 fp_2107.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2107 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2107.

Lemma collineation_2108 : is_collineation2 fp_2108.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2108 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2108.

Lemma collineation_2109 : is_collineation2 fp_2109.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2109 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2109.

Lemma collineation_2110 : is_collineation2 fp_2110.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2110 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2110.

Lemma collineation_2111 : is_collineation2 fp_2111.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2111 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2111.

Lemma collineation_2112 : is_collineation2 fp_2112.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2112 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2112.

Lemma collineation_2113 : is_collineation2 fp_2113.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2113 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2113.

Lemma collineation_2114 : is_collineation2 fp_2114.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2114 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2114.

Lemma collineation_2115 : is_collineation2 fp_2115.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2115 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2115.

Lemma collineation_2116 : is_collineation2 fp_2116.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2116 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2116.

Lemma collineation_2117 : is_collineation2 fp_2117.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2117 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2117.

Lemma collineation_2118 : is_collineation2 fp_2118.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2118 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2118.

Lemma collineation_2119 : is_collineation2 fp_2119.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2119 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2119.

Lemma collineation_2120 : is_collineation2 fp_2120.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2120 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2120.

Lemma collineation_2121 : is_collineation2 fp_2121.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2121 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2121.

Lemma collineation_2122 : is_collineation2 fp_2122.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2122 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2122.

Lemma collineation_2123 : is_collineation2 fp_2123.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2123 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2123.

Lemma collineation_2124 : is_collineation2 fp_2124.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2124 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2124.

Lemma collineation_2125 : is_collineation2 fp_2125.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2125 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2125.

Lemma collineation_2126 : is_collineation2 fp_2126.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2126 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2126.

Lemma collineation_2127 : is_collineation2 fp_2127.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2127 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2127.

Lemma collineation_2128 : is_collineation2 fp_2128.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2128 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2128.

Lemma collineation_2129 : is_collineation2 fp_2129.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2129 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2129.

Lemma collineation_2130 : is_collineation2 fp_2130.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2130 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2130.

Lemma collineation_2131 : is_collineation2 fp_2131.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2131 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2131.

Lemma collineation_2132 : is_collineation2 fp_2132.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2132 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2132.

Lemma collineation_2133 : is_collineation2 fp_2133.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2133 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2133.

Lemma collineation_2134 : is_collineation2 fp_2134.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2134 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2134.

Lemma collineation_2135 : is_collineation2 fp_2135.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2135 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2135.

Lemma collineation_2136 : is_collineation2 fp_2136.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2136 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2136.

Lemma collineation_2137 : is_collineation2 fp_2137.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2137 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2137.

Lemma collineation_2138 : is_collineation2 fp_2138.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2138 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2138.

Lemma collineation_2139 : is_collineation2 fp_2139.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2139 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2139.

Lemma collineation_2140 : is_collineation2 fp_2140.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2140 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2140.

Lemma collineation_2141 : is_collineation2 fp_2141.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2141 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2141.

Lemma collineation_2142 : is_collineation2 fp_2142.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2142 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2142.

Lemma collineation_2143 : is_collineation2 fp_2143.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2143 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2143.

Lemma collineation_2144 : is_collineation2 fp_2144.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2144 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2144.

Lemma collineation_2145 : is_collineation2 fp_2145.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2145 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2145.

Lemma collineation_2146 : is_collineation2 fp_2146.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2146 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2146.

Lemma collineation_2147 : is_collineation2 fp_2147.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2147 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2147.

Lemma collineation_2148 : is_collineation2 fp_2148.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2148 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2148.

Lemma collineation_2149 : is_collineation2 fp_2149.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2149 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2149.

Lemma collineation_2150 : is_collineation2 fp_2150.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2150 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2150.

Lemma collineation_2151 : is_collineation2 fp_2151.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2151 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2151.

Lemma collineation_2152 : is_collineation2 fp_2152.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2152 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2152.

Lemma collineation_2153 : is_collineation2 fp_2153.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2153 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2153.

Lemma collineation_2154 : is_collineation2 fp_2154.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2154 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2154.

Lemma collineation_2155 : is_collineation2 fp_2155.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2155 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2155.

Lemma collineation_2156 : is_collineation2 fp_2156.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2156 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2156.

Lemma collineation_2157 : is_collineation2 fp_2157.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2157 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2157.

Lemma collineation_2158 : is_collineation2 fp_2158.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2158 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2158.

Lemma collineation_2159 : is_collineation2 fp_2159.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2159 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2159.

Lemma collineation_2160 : is_collineation2 fp_2160.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2160 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2160.

Lemma collineation_2161 : is_collineation2 fp_2161.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2161 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2161.

Lemma collineation_2162 : is_collineation2 fp_2162.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2162 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2162.

Lemma collineation_2163 : is_collineation2 fp_2163.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2163 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2163.

Lemma collineation_2164 : is_collineation2 fp_2164.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2164 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2164.

Lemma collineation_2165 : is_collineation2 fp_2165.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2165 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2165.

Lemma collineation_2166 : is_collineation2 fp_2166.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2166 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2166.

Lemma collineation_2167 : is_collineation2 fp_2167.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2167 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2167.

Lemma collineation_2168 : is_collineation2 fp_2168.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2168 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2168.

Lemma collineation_2169 : is_collineation2 fp_2169.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2169 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2169.

Lemma collineation_2170 : is_collineation2 fp_2170.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2170 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2170.

Lemma collineation_2171 : is_collineation2 fp_2171.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2171 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2171.

Lemma collineation_2172 : is_collineation2 fp_2172.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2172 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2172.

Lemma collineation_2173 : is_collineation2 fp_2173.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2173 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2173.

Lemma collineation_2174 : is_collineation2 fp_2174.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2174 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2174.

Lemma collineation_2175 : is_collineation2 fp_2175.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2175 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2175.

Lemma collineation_2176 : is_collineation2 fp_2176.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2176 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2176.

Lemma collineation_2177 : is_collineation2 fp_2177.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2177 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2177.

Lemma collineation_2178 : is_collineation2 fp_2178.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2178 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2178.

Lemma collineation_2179 : is_collineation2 fp_2179.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2179 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2179.

Lemma collineation_2180 : is_collineation2 fp_2180.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2180 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2180.

Lemma collineation_2181 : is_collineation2 fp_2181.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2181 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2181.

Lemma collineation_2182 : is_collineation2 fp_2182.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2182 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2182.

Lemma collineation_2183 : is_collineation2 fp_2183.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2183 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2183.

Lemma collineation_2184 : is_collineation2 fp_2184.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2184 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2184.

Lemma collineation_2185 : is_collineation2 fp_2185.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2185 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2185.

Lemma collineation_2186 : is_collineation2 fp_2186.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2186 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2186.

Lemma collineation_2187 : is_collineation2 fp_2187.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2187 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2187.

Lemma collineation_2188 : is_collineation2 fp_2188.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2188 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2188.

Lemma collineation_2189 : is_collineation2 fp_2189.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2189 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2189.

Lemma collineation_2190 : is_collineation2 fp_2190.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2190 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2190.

Lemma collineation_2191 : is_collineation2 fp_2191.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2191 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2191.

Lemma collineation_2192 : is_collineation2 fp_2192.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2192 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2192.

Lemma collineation_2193 : is_collineation2 fp_2193.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2193 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2193.

Lemma collineation_2194 : is_collineation2 fp_2194.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2194 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2194.

Lemma collineation_2195 : is_collineation2 fp_2195.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2195 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2195.

Lemma collineation_2196 : is_collineation2 fp_2196.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2196 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2196.

Lemma collineation_2197 : is_collineation2 fp_2197.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2197 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2197.

Lemma collineation_2198 : is_collineation2 fp_2198.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2198 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2198.

Lemma collineation_2199 : is_collineation2 fp_2199.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2199 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2199.

Lemma collineation_2200 : is_collineation2 fp_2200.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2200 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2200.

Lemma collineation_2201 : is_collineation2 fp_2201.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2201 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2201.

Lemma collineation_2202 : is_collineation2 fp_2202.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2202 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2202.

Lemma collineation_2203 : is_collineation2 fp_2203.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2203 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2203.

Lemma collineation_2204 : is_collineation2 fp_2204.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2204 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2204.

Lemma collineation_2205 : is_collineation2 fp_2205.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2205 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2205.

Lemma collineation_2206 : is_collineation2 fp_2206.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2206 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2206.

Lemma collineation_2207 : is_collineation2 fp_2207.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2207 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2207.

Lemma collineation_2208 : is_collineation2 fp_2208.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2208 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2208.

Lemma collineation_2209 : is_collineation2 fp_2209.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2209 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2209.

Lemma collineation_2210 : is_collineation2 fp_2210.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2210 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2210.

Lemma collineation_2211 : is_collineation2 fp_2211.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2211 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2211.

Lemma collineation_2212 : is_collineation2 fp_2212.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2212 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2212.

Lemma collineation_2213 : is_collineation2 fp_2213.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2213 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2213.

Lemma collineation_2214 : is_collineation2 fp_2214.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2214 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2214.

Lemma collineation_2215 : is_collineation2 fp_2215.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2215 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2215.

Lemma collineation_2216 : is_collineation2 fp_2216.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2216 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2216.

Lemma collineation_2217 : is_collineation2 fp_2217.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2217 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2217.

Lemma collineation_2218 : is_collineation2 fp_2218.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2218 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2218.

Lemma collineation_2219 : is_collineation2 fp_2219.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2219 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2219.

Lemma collineation_2220 : is_collineation2 fp_2220.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2220 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2220.

Lemma collineation_2221 : is_collineation2 fp_2221.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2221 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2221.

Lemma collineation_2222 : is_collineation2 fp_2222.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2222 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2222.

Lemma collineation_2223 : is_collineation2 fp_2223.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2223 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2223.

Lemma collineation_2224 : is_collineation2 fp_2224.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2224 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2224.

Lemma collineation_2225 : is_collineation2 fp_2225.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2225 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2225.

Lemma collineation_2226 : is_collineation2 fp_2226.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2226 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2226.

Lemma collineation_2227 : is_collineation2 fp_2227.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2227 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2227.

Lemma collineation_2228 : is_collineation2 fp_2228.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2228 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2228.

Lemma collineation_2229 : is_collineation2 fp_2229.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2229 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2229.

Lemma collineation_2230 : is_collineation2 fp_2230.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2230 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2230.

Lemma collineation_2231 : is_collineation2 fp_2231.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2231 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2231.

Lemma collineation_2232 : is_collineation2 fp_2232.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2232 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2232.

Lemma collineation_2233 : is_collineation2 fp_2233.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2233 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2233.

Lemma collineation_2234 : is_collineation2 fp_2234.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2234 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2234.

Lemma collineation_2235 : is_collineation2 fp_2235.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2235 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2235.

Lemma collineation_2236 : is_collineation2 fp_2236.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2236 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2236.

Lemma collineation_2237 : is_collineation2 fp_2237.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2237 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2237.

Lemma collineation_2238 : is_collineation2 fp_2238.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2238 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2238.

Lemma collineation_2239 : is_collineation2 fp_2239.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2239 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2239.

Lemma collineation_2240 : is_collineation2 fp_2240.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2240 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2240.

Lemma collineation_2241 : is_collineation2 fp_2241.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2241 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2241.

Lemma collineation_2242 : is_collineation2 fp_2242.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2242 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2242.

Lemma collineation_2243 : is_collineation2 fp_2243.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2243 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2243.

Lemma collineation_2244 : is_collineation2 fp_2244.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2244 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2244.

Lemma collineation_2245 : is_collineation2 fp_2245.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2245 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2245.

Lemma collineation_2246 : is_collineation2 fp_2246.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2246 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2246.

Lemma collineation_2247 : is_collineation2 fp_2247.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2247 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2247.

Lemma collineation_2248 : is_collineation2 fp_2248.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2248 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2248.

Lemma collineation_2249 : is_collineation2 fp_2249.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2249 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2249.

Lemma collineation_2250 : is_collineation2 fp_2250.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2250 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2250.

Lemma collineation_2251 : is_collineation2 fp_2251.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2251 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2251.

Lemma collineation_2252 : is_collineation2 fp_2252.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2252 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2252.

Lemma collineation_2253 : is_collineation2 fp_2253.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2253 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2253.

Lemma collineation_2254 : is_collineation2 fp_2254.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2254 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2254.

Lemma collineation_2255 : is_collineation2 fp_2255.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2255 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2255.

Lemma collineation_2256 : is_collineation2 fp_2256.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2256 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2256.

Lemma collineation_2257 : is_collineation2 fp_2257.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2257 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2257.

Lemma collineation_2258 : is_collineation2 fp_2258.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2258 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2258.

Lemma collineation_2259 : is_collineation2 fp_2259.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2259 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2259.

Lemma collineation_2260 : is_collineation2 fp_2260.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2260 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2260.

Lemma collineation_2261 : is_collineation2 fp_2261.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2261 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2261.

Lemma collineation_2262 : is_collineation2 fp_2262.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2262 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2262.

Lemma collineation_2263 : is_collineation2 fp_2263.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2263 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2263.

Lemma collineation_2264 : is_collineation2 fp_2264.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2264 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2264.

Lemma collineation_2265 : is_collineation2 fp_2265.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2265 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2265.

Lemma collineation_2266 : is_collineation2 fp_2266.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2266 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2266.

Lemma collineation_2267 : is_collineation2 fp_2267.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2267 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2267.

Lemma collineation_2268 : is_collineation2 fp_2268.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2268 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2268.

Lemma collineation_2269 : is_collineation2 fp_2269.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2269 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2269.

Lemma collineation_2270 : is_collineation2 fp_2270.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2270 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2270.

Lemma collineation_2271 : is_collineation2 fp_2271.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2271 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2271.

Lemma collineation_2272 : is_collineation2 fp_2272.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2272 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2272.

Lemma collineation_2273 : is_collineation2 fp_2273.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2273 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2273.

Lemma collineation_2274 : is_collineation2 fp_2274.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2274 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2274.

Lemma collineation_2275 : is_collineation2 fp_2275.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2275 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2275.

Lemma collineation_2276 : is_collineation2 fp_2276.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2276 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2276.

Lemma collineation_2277 : is_collineation2 fp_2277.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2277 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2277.

Lemma collineation_2278 : is_collineation2 fp_2278.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2278 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2278.

Lemma collineation_2279 : is_collineation2 fp_2279.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2279 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2279.

Lemma collineation_2280 : is_collineation2 fp_2280.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2280 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2280.

Lemma collineation_2281 : is_collineation2 fp_2281.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2281 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2281.

Lemma collineation_2282 : is_collineation2 fp_2282.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2282 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2282.

Lemma collineation_2283 : is_collineation2 fp_2283.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2283 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2283.

Lemma collineation_2284 : is_collineation2 fp_2284.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2284 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2284.

Lemma collineation_2285 : is_collineation2 fp_2285.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2285 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2285.

Lemma collineation_2286 : is_collineation2 fp_2286.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2286 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2286.

Lemma collineation_2287 : is_collineation2 fp_2287.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2287 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2287.

Lemma collineation_2288 : is_collineation2 fp_2288.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2288 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2288.

Lemma collineation_2289 : is_collineation2 fp_2289.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2289 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2289.

Lemma collineation_2290 : is_collineation2 fp_2290.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2290 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2290.

Lemma collineation_2291 : is_collineation2 fp_2291.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2291 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2291.

Lemma collineation_2292 : is_collineation2 fp_2292.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2292 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2292.

Lemma collineation_2293 : is_collineation2 fp_2293.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2293 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2293.

Lemma collineation_2294 : is_collineation2 fp_2294.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2294 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2294.

Lemma collineation_2295 : is_collineation2 fp_2295.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2295 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2295.

Lemma collineation_2296 : is_collineation2 fp_2296.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2296 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2296.

Lemma collineation_2297 : is_collineation2 fp_2297.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2297 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2297.

Lemma collineation_2298 : is_collineation2 fp_2298.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2298 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2298.

Lemma collineation_2299 : is_collineation2 fp_2299.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2299 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2299.

Lemma collineation_2300 : is_collineation2 fp_2300.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2300 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2300.

Lemma collineation_2301 : is_collineation2 fp_2301.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2301 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2301.

Lemma collineation_2302 : is_collineation2 fp_2302.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2302 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2302.

Lemma collineation_2303 : is_collineation2 fp_2303.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2303 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2303.

Lemma collineation_2304 : is_collineation2 fp_2304.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2304 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2304.

Lemma collineation_2305 : is_collineation2 fp_2305.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2305 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2305.

Lemma collineation_2306 : is_collineation2 fp_2306.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2306 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2306.

Lemma collineation_2307 : is_collineation2 fp_2307.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2307 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2307.

Lemma collineation_2308 : is_collineation2 fp_2308.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2308 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2308.

Lemma collineation_2309 : is_collineation2 fp_2309.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2309 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2309.

Lemma collineation_2310 : is_collineation2 fp_2310.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2310 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2310.

Lemma collineation_2311 : is_collineation2 fp_2311.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2311 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2311.

Lemma collineation_2312 : is_collineation2 fp_2312.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2312 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2312.

Lemma collineation_2313 : is_collineation2 fp_2313.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2313 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2313.

Lemma collineation_2314 : is_collineation2 fp_2314.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2314 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2314.

Lemma collineation_2315 : is_collineation2 fp_2315.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2315 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2315.

Lemma collineation_2316 : is_collineation2 fp_2316.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2316 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2316.

Lemma collineation_2317 : is_collineation2 fp_2317.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2317 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2317.

Lemma collineation_2318 : is_collineation2 fp_2318.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2318 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2318.

Lemma collineation_2319 : is_collineation2 fp_2319.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2319 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2319.

Lemma collineation_2320 : is_collineation2 fp_2320.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2320 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2320.

Lemma collineation_2321 : is_collineation2 fp_2321.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2321 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2321.

Lemma collineation_2322 : is_collineation2 fp_2322.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2322 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2322.

Lemma collineation_2323 : is_collineation2 fp_2323.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2323 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2323.

Lemma collineation_2324 : is_collineation2 fp_2324.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2324 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2324.

Lemma collineation_2325 : is_collineation2 fp_2325.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2325 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2325.

Lemma collineation_2326 : is_collineation2 fp_2326.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2326 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2326.

Lemma collineation_2327 : is_collineation2 fp_2327.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2327 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2327.

Lemma collineation_2328 : is_collineation2 fp_2328.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2328 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2328.

Lemma collineation_2329 : is_collineation2 fp_2329.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2329 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2329.

Lemma collineation_2330 : is_collineation2 fp_2330.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2330 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2330.

Lemma collineation_2331 : is_collineation2 fp_2331.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2331 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2331.

Lemma collineation_2332 : is_collineation2 fp_2332.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2332 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2332.

Lemma collineation_2333 : is_collineation2 fp_2333.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2333 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2333.

Lemma collineation_2334 : is_collineation2 fp_2334.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2334 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2334.

Lemma collineation_2335 : is_collineation2 fp_2335.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2335 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2335.

Lemma collineation_2336 : is_collineation2 fp_2336.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2336 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2336.

Lemma collineation_2337 : is_collineation2 fp_2337.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2337 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2337.

Lemma collineation_2338 : is_collineation2 fp_2338.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2338 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2338.

Lemma collineation_2339 : is_collineation2 fp_2339.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2339 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2339.

Lemma collineation_2340 : is_collineation2 fp_2340.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2340 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2340.

Lemma collineation_2341 : is_collineation2 fp_2341.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2341 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2341.

Lemma collineation_2342 : is_collineation2 fp_2342.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2342 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2342.

Lemma collineation_2343 : is_collineation2 fp_2343.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2343 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2343.

Lemma collineation_2344 : is_collineation2 fp_2344.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2344 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2344.

Lemma collineation_2345 : is_collineation2 fp_2345.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2345 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2345.

Lemma collineation_2346 : is_collineation2 fp_2346.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2346 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2346.

Lemma collineation_2347 : is_collineation2 fp_2347.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2347 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2347.

Lemma collineation_2348 : is_collineation2 fp_2348.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2348 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2348.

Lemma collineation_2349 : is_collineation2 fp_2349.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2349 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2349.

Lemma collineation_2350 : is_collineation2 fp_2350.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2350 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2350.

Lemma collineation_2351 : is_collineation2 fp_2351.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2351 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2351.

Lemma collineation_2352 : is_collineation2 fp_2352.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2352 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2352.

Lemma collineation_2353 : is_collineation2 fp_2353.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2353 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2353.

Lemma collineation_2354 : is_collineation2 fp_2354.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2354 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2354.

Lemma collineation_2355 : is_collineation2 fp_2355.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2355 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2355.

Lemma collineation_2356 : is_collineation2 fp_2356.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2356 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2356.

Lemma collineation_2357 : is_collineation2 fp_2357.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2357 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2357.

Lemma collineation_2358 : is_collineation2 fp_2358.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2358 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2358.

Lemma collineation_2359 : is_collineation2 fp_2359.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2359 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2359.

Lemma collineation_2360 : is_collineation2 fp_2360.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2360 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2360.

Lemma collineation_2361 : is_collineation2 fp_2361.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2361 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2361.

Lemma collineation_2362 : is_collineation2 fp_2362.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2362 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2362.

Lemma collineation_2363 : is_collineation2 fp_2363.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2363 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2363.

Lemma collineation_2364 : is_collineation2 fp_2364.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2364 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2364.

Lemma collineation_2365 : is_collineation2 fp_2365.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2365 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2365.

Lemma collineation_2366 : is_collineation2 fp_2366.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2366 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2366.

Lemma collineation_2367 : is_collineation2 fp_2367.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2367 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2367.

Lemma collineation_2368 : is_collineation2 fp_2368.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2368 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2368.

Lemma collineation_2369 : is_collineation2 fp_2369.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2369 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2369.

Lemma collineation_2370 : is_collineation2 fp_2370.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2370 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2370.

Lemma collineation_2371 : is_collineation2 fp_2371.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2371 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2371.

Lemma collineation_2372 : is_collineation2 fp_2372.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2372 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2372.

Lemma collineation_2373 : is_collineation2 fp_2373.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2373 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2373.

Lemma collineation_2374 : is_collineation2 fp_2374.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2374 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2374.

Lemma collineation_2375 : is_collineation2 fp_2375.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2375 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2375.

Lemma collineation_2376 : is_collineation2 fp_2376.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2376 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2376.

Lemma collineation_2377 : is_collineation2 fp_2377.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2377 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2377.

Lemma collineation_2378 : is_collineation2 fp_2378.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2378 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2378.

Lemma collineation_2379 : is_collineation2 fp_2379.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2379 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2379.

Lemma collineation_2380 : is_collineation2 fp_2380.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2380 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2380.

Lemma collineation_2381 : is_collineation2 fp_2381.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2381 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2381.

Lemma collineation_2382 : is_collineation2 fp_2382.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2382 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2382.

Lemma collineation_2383 : is_collineation2 fp_2383.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2383 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2383.

Lemma collineation_2384 : is_collineation2 fp_2384.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2384 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2384.

Lemma collineation_2385 : is_collineation2 fp_2385.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2385 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2385.

Lemma collineation_2386 : is_collineation2 fp_2386.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2386 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2386.

Lemma collineation_2387 : is_collineation2 fp_2387.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2387 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2387.

Lemma collineation_2388 : is_collineation2 fp_2388.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2388 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2388.

Lemma collineation_2389 : is_collineation2 fp_2389.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2389 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2389.

Lemma collineation_2390 : is_collineation2 fp_2390.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2390 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2390.

Lemma collineation_2391 : is_collineation2 fp_2391.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2391 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2391.

Lemma collineation_2392 : is_collineation2 fp_2392.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2392 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2392.

Lemma collineation_2393 : is_collineation2 fp_2393.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2393 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2393.

Lemma collineation_2394 : is_collineation2 fp_2394.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2394 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2394.

Lemma collineation_2395 : is_collineation2 fp_2395.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2395 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2395.

Lemma collineation_2396 : is_collineation2 fp_2396.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2396 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2396.

Lemma collineation_2397 : is_collineation2 fp_2397.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2397 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2397.

Lemma collineation_2398 : is_collineation2 fp_2398.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2398 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2398.

Lemma collineation_2399 : is_collineation2 fp_2399.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2399 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2399.

Lemma collineation_2400 : is_collineation2 fp_2400.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2400 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2400.

Lemma collineation_2401 : is_collineation2 fp_2401.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2401 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2401.

Lemma collineation_2402 : is_collineation2 fp_2402.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2402 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2402.

Lemma collineation_2403 : is_collineation2 fp_2403.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2403 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2403.

Lemma collineation_2404 : is_collineation2 fp_2404.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2404 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2404.

Lemma collineation_2405 : is_collineation2 fp_2405.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2405 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2405.

Lemma collineation_2406 : is_collineation2 fp_2406.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2406 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2406.

Lemma collineation_2407 : is_collineation2 fp_2407.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2407 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2407.

Lemma collineation_2408 : is_collineation2 fp_2408.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2408 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2408.

Lemma collineation_2409 : is_collineation2 fp_2409.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2409 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2409.

Lemma collineation_2410 : is_collineation2 fp_2410.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2410 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2410.

Lemma collineation_2411 : is_collineation2 fp_2411.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2411 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2411.

Lemma collineation_2412 : is_collineation2 fp_2412.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2412 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2412.

Lemma collineation_2413 : is_collineation2 fp_2413.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2413 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2413.

Lemma collineation_2414 : is_collineation2 fp_2414.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2414 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2414.

Lemma collineation_2415 : is_collineation2 fp_2415.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2415 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2415.

Lemma collineation_2416 : is_collineation2 fp_2416.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2416 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2416.

Lemma collineation_2417 : is_collineation2 fp_2417.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2417 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2417.

Lemma collineation_2418 : is_collineation2 fp_2418.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2418 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2418.

Lemma collineation_2419 : is_collineation2 fp_2419.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2419 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2419.

Lemma collineation_2420 : is_collineation2 fp_2420.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2420 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2420.

Lemma collineation_2421 : is_collineation2 fp_2421.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2421 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2421.

Lemma collineation_2422 : is_collineation2 fp_2422.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2422 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2422.

Lemma collineation_2423 : is_collineation2 fp_2423.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2423 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2423.

Lemma collineation_2424 : is_collineation2 fp_2424.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2424 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2424.

Lemma collineation_2425 : is_collineation2 fp_2425.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2425 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2425.

Lemma collineation_2426 : is_collineation2 fp_2426.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2426 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2426.

Lemma collineation_2427 : is_collineation2 fp_2427.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2427 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2427.

Lemma collineation_2428 : is_collineation2 fp_2428.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2428 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2428.

Lemma collineation_2429 : is_collineation2 fp_2429.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2429 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2429.

Lemma collineation_2430 : is_collineation2 fp_2430.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2430 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2430.

Lemma collineation_2431 : is_collineation2 fp_2431.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2431 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2431.

Lemma collineation_2432 : is_collineation2 fp_2432.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2432 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2432.

Lemma collineation_2433 : is_collineation2 fp_2433.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2433 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2433.

Lemma collineation_2434 : is_collineation2 fp_2434.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2434 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2434.

Lemma collineation_2435 : is_collineation2 fp_2435.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2435 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2435.

Lemma collineation_2436 : is_collineation2 fp_2436.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2436 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2436.

Lemma collineation_2437 : is_collineation2 fp_2437.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2437 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2437.

Lemma collineation_2438 : is_collineation2 fp_2438.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2438 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2438.

Lemma collineation_2439 : is_collineation2 fp_2439.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2439 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2439.

Lemma collineation_2440 : is_collineation2 fp_2440.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2440 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2440.

Lemma collineation_2441 : is_collineation2 fp_2441.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2441 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2441.

Lemma collineation_2442 : is_collineation2 fp_2442.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2442 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2442.

Lemma collineation_2443 : is_collineation2 fp_2443.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2443 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2443.

Lemma collineation_2444 : is_collineation2 fp_2444.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2444 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2444.

Lemma collineation_2445 : is_collineation2 fp_2445.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2445 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2445.

Lemma collineation_2446 : is_collineation2 fp_2446.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2446 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2446.

Lemma collineation_2447 : is_collineation2 fp_2447.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2447 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2447.

Lemma collineation_2448 : is_collineation2 fp_2448.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2448 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2448.

Lemma collineation_2449 : is_collineation2 fp_2449.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2449 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2449.

Lemma collineation_2450 : is_collineation2 fp_2450.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2450 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2450.

Lemma collineation_2451 : is_collineation2 fp_2451.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2451 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2451.

Lemma collineation_2452 : is_collineation2 fp_2452.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2452 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2452.

Lemma collineation_2453 : is_collineation2 fp_2453.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2453 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2453.

Lemma collineation_2454 : is_collineation2 fp_2454.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2454 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2454.

Lemma collineation_2455 : is_collineation2 fp_2455.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2455 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2455.

Lemma collineation_2456 : is_collineation2 fp_2456.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2456 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2456.

Lemma collineation_2457 : is_collineation2 fp_2457.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2457 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2457.

Lemma collineation_2458 : is_collineation2 fp_2458.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2458 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2458.

Lemma collineation_2459 : is_collineation2 fp_2459.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2459 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2459.

Lemma collineation_2460 : is_collineation2 fp_2460.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2460 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2460.

Lemma collineation_2461 : is_collineation2 fp_2461.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2461 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2461.

Lemma collineation_2462 : is_collineation2 fp_2462.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2462 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2462.

Lemma collineation_2463 : is_collineation2 fp_2463.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2463 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2463.

Lemma collineation_2464 : is_collineation2 fp_2464.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2464 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2464.

Lemma collineation_2465 : is_collineation2 fp_2465.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2465 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2465.

Lemma collineation_2466 : is_collineation2 fp_2466.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2466 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2466.

Lemma collineation_2467 : is_collineation2 fp_2467.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2467 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2467.

Lemma collineation_2468 : is_collineation2 fp_2468.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2468 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2468.

Lemma collineation_2469 : is_collineation2 fp_2469.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2469 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2469.

Lemma collineation_2470 : is_collineation2 fp_2470.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2470 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2470.

Lemma collineation_2471 : is_collineation2 fp_2471.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2471 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2471.

Lemma collineation_2472 : is_collineation2 fp_2472.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2472 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2472.

Lemma collineation_2473 : is_collineation2 fp_2473.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2473 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2473.

Lemma collineation_2474 : is_collineation2 fp_2474.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2474 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2474.

Lemma collineation_2475 : is_collineation2 fp_2475.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2475 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2475.

Lemma collineation_2476 : is_collineation2 fp_2476.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2476 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2476.

Lemma collineation_2477 : is_collineation2 fp_2477.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2477 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2477.

Lemma collineation_2478 : is_collineation2 fp_2478.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2478 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2478.

Lemma collineation_2479 : is_collineation2 fp_2479.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2479 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2479.

Lemma collineation_2480 : is_collineation2 fp_2480.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2480 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2480.

Lemma collineation_2481 : is_collineation2 fp_2481.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2481 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2481.

Lemma collineation_2482 : is_collineation2 fp_2482.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2482 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2482.

Lemma collineation_2483 : is_collineation2 fp_2483.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2483 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2483.

Lemma collineation_2484 : is_collineation2 fp_2484.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2484 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2484.

Lemma collineation_2485 : is_collineation2 fp_2485.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2485 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2485.

Lemma collineation_2486 : is_collineation2 fp_2486.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2486 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2486.

Lemma collineation_2487 : is_collineation2 fp_2487.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2487 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2487.

Lemma collineation_2488 : is_collineation2 fp_2488.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2488 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2488.

Lemma collineation_2489 : is_collineation2 fp_2489.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2489 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2489.

Lemma collineation_2490 : is_collineation2 fp_2490.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2490 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2490.

Lemma collineation_2491 : is_collineation2 fp_2491.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2491 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2491.

Lemma collineation_2492 : is_collineation2 fp_2492.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2492 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2492.

Lemma collineation_2493 : is_collineation2 fp_2493.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2493 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2493.

Lemma collineation_2494 : is_collineation2 fp_2494.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2494 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2494.

Lemma collineation_2495 : is_collineation2 fp_2495.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2495 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2495.

Lemma collineation_2496 : is_collineation2 fp_2496.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2496 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2496.

Lemma collineation_2497 : is_collineation2 fp_2497.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2497 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2497.

Lemma collineation_2498 : is_collineation2 fp_2498.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2498 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2498.

Lemma collineation_2499 : is_collineation2 fp_2499.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2499 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2499.

Lemma collineation_2500 : is_collineation2 fp_2500.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2500 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2500.

Lemma collineation_2501 : is_collineation2 fp_2501.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2501 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2501.

Lemma collineation_2502 : is_collineation2 fp_2502.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2502 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2502.

Lemma collineation_2503 : is_collineation2 fp_2503.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2503 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2503.

Lemma collineation_2504 : is_collineation2 fp_2504.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2504 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2504.

Lemma collineation_2505 : is_collineation2 fp_2505.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2505 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2505.

Lemma collineation_2506 : is_collineation2 fp_2506.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2506 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2506.

Lemma collineation_2507 : is_collineation2 fp_2507.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2507 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2507.

Lemma collineation_2508 : is_collineation2 fp_2508.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2508 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2508.

Lemma collineation_2509 : is_collineation2 fp_2509.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2509 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2509.

Lemma collineation_2510 : is_collineation2 fp_2510.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2510 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2510.

Lemma collineation_2511 : is_collineation2 fp_2511.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2511 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2511.

Lemma collineation_2512 : is_collineation2 fp_2512.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2512 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2512.

Lemma collineation_2513 : is_collineation2 fp_2513.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2513 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2513.

Lemma collineation_2514 : is_collineation2 fp_2514.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2514 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2514.

Lemma collineation_2515 : is_collineation2 fp_2515.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2515 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2515.

Lemma collineation_2516 : is_collineation2 fp_2516.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2516 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2516.

Lemma collineation_2517 : is_collineation2 fp_2517.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2517 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2517.

Lemma collineation_2518 : is_collineation2 fp_2518.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2518 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2518.

Lemma collineation_2519 : is_collineation2 fp_2519.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2519 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2519.

Lemma collineation_2520 : is_collineation2 fp_2520.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2520 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2520.

Lemma collineation_2521 : is_collineation2 fp_2521.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2521 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2521.

Lemma collineation_2522 : is_collineation2 fp_2522.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2522 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2522.

Lemma collineation_2523 : is_collineation2 fp_2523.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2523 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2523.

Lemma collineation_2524 : is_collineation2 fp_2524.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2524 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2524.

Lemma collineation_2525 : is_collineation2 fp_2525.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2525 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2525.

Lemma collineation_2526 : is_collineation2 fp_2526.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2526 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2526.

Lemma collineation_2527 : is_collineation2 fp_2527.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2527 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2527.

Lemma collineation_2528 : is_collineation2 fp_2528.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2528 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2528.

Lemma collineation_2529 : is_collineation2 fp_2529.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2529 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2529.

Lemma collineation_2530 : is_collineation2 fp_2530.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2530 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2530.

Lemma collineation_2531 : is_collineation2 fp_2531.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2531 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2531.

Lemma collineation_2532 : is_collineation2 fp_2532.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2532 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2532.

Lemma collineation_2533 : is_collineation2 fp_2533.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2533 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2533.

Lemma collineation_2534 : is_collineation2 fp_2534.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2534 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2534.

Lemma collineation_2535 : is_collineation2 fp_2535.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2535 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2535.

Lemma collineation_2536 : is_collineation2 fp_2536.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2536 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2536.

Lemma collineation_2537 : is_collineation2 fp_2537.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2537 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2537.

Lemma collineation_2538 : is_collineation2 fp_2538.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2538 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2538.

Lemma collineation_2539 : is_collineation2 fp_2539.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2539 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2539.

Lemma collineation_2540 : is_collineation2 fp_2540.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2540 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2540.

Lemma collineation_2541 : is_collineation2 fp_2541.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2541 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2541.

Lemma collineation_2542 : is_collineation2 fp_2542.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2542 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2542.

Lemma collineation_2543 : is_collineation2 fp_2543.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2543 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2543.

Lemma collineation_2544 : is_collineation2 fp_2544.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2544 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2544.

Lemma collineation_2545 : is_collineation2 fp_2545.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2545 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2545.

Lemma collineation_2546 : is_collineation2 fp_2546.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2546 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2546.

Lemma collineation_2547 : is_collineation2 fp_2547.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2547 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2547.

Lemma collineation_2548 : is_collineation2 fp_2548.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2548 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2548.

Lemma collineation_2549 : is_collineation2 fp_2549.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2549 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2549.

Lemma collineation_2550 : is_collineation2 fp_2550.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2550 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2550.

Lemma collineation_2551 : is_collineation2 fp_2551.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2551 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2551.

Lemma collineation_2552 : is_collineation2 fp_2552.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2552 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2552.

Lemma collineation_2553 : is_collineation2 fp_2553.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2553 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2553.

Lemma collineation_2554 : is_collineation2 fp_2554.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2554 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2554.

Lemma collineation_2555 : is_collineation2 fp_2555.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2555 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2555.

Lemma collineation_2556 : is_collineation2 fp_2556.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2556 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2556.

Lemma collineation_2557 : is_collineation2 fp_2557.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2557 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2557.

Lemma collineation_2558 : is_collineation2 fp_2558.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2558 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2558.

Lemma collineation_2559 : is_collineation2 fp_2559.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2559 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2559.

Lemma collineation_2560 : is_collineation2 fp_2560.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2560 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2560.

Lemma collineation_2561 : is_collineation2 fp_2561.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2561 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2561.

Lemma collineation_2562 : is_collineation2 fp_2562.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2562 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2562.

Lemma collineation_2563 : is_collineation2 fp_2563.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2563 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2563.

Lemma collineation_2564 : is_collineation2 fp_2564.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2564 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2564.

Lemma collineation_2565 : is_collineation2 fp_2565.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2565 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2565.

Lemma collineation_2566 : is_collineation2 fp_2566.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2566 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2566.

Lemma collineation_2567 : is_collineation2 fp_2567.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2567 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2567.

Lemma collineation_2568 : is_collineation2 fp_2568.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2568 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2568.

Lemma collineation_2569 : is_collineation2 fp_2569.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2569 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2569.

Lemma collineation_2570 : is_collineation2 fp_2570.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2570 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2570.

Lemma collineation_2571 : is_collineation2 fp_2571.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2571 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2571.

Lemma collineation_2572 : is_collineation2 fp_2572.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2572 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2572.

Lemma collineation_2573 : is_collineation2 fp_2573.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2573 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2573.

Lemma collineation_2574 : is_collineation2 fp_2574.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2574 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2574.

Lemma collineation_2575 : is_collineation2 fp_2575.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2575 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2575.

Lemma collineation_2576 : is_collineation2 fp_2576.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2576 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2576.

Lemma collineation_2577 : is_collineation2 fp_2577.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2577 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2577.

Lemma collineation_2578 : is_collineation2 fp_2578.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2578 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2578.

Lemma collineation_2579 : is_collineation2 fp_2579.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2579 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2579.

Lemma collineation_2580 : is_collineation2 fp_2580.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2580 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2580.

Lemma collineation_2581 : is_collineation2 fp_2581.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2581 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2581.

Lemma collineation_2582 : is_collineation2 fp_2582.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2582 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2582.

Lemma collineation_2583 : is_collineation2 fp_2583.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2583 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2583.

Lemma collineation_2584 : is_collineation2 fp_2584.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2584 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2584.

Lemma collineation_2585 : is_collineation2 fp_2585.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2585 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2585.

Lemma collineation_2586 : is_collineation2 fp_2586.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2586 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2586.

Lemma collineation_2587 : is_collineation2 fp_2587.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2587 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2587.

Lemma collineation_2588 : is_collineation2 fp_2588.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2588 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2588.

Lemma collineation_2589 : is_collineation2 fp_2589.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2589 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2589.

Lemma collineation_2590 : is_collineation2 fp_2590.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2590 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2590.

Lemma collineation_2591 : is_collineation2 fp_2591.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2591 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2591.

Lemma collineation_2592 : is_collineation2 fp_2592.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2592 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2592.

Lemma collineation_2593 : is_collineation2 fp_2593.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2593 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2593.

Lemma collineation_2594 : is_collineation2 fp_2594.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2594 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2594.

Lemma collineation_2595 : is_collineation2 fp_2595.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2595 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2595.

Lemma collineation_2596 : is_collineation2 fp_2596.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2596 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2596.

Lemma collineation_2597 : is_collineation2 fp_2597.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2597 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2597.

Lemma collineation_2598 : is_collineation2 fp_2598.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2598 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2598.

Lemma collineation_2599 : is_collineation2 fp_2599.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2599 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2599.

Lemma collineation_2600 : is_collineation2 fp_2600.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2600 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2600.

Lemma collineation_2601 : is_collineation2 fp_2601.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2601 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2601.

Lemma collineation_2602 : is_collineation2 fp_2602.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2602 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2602.

Lemma collineation_2603 : is_collineation2 fp_2603.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2603 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2603.

Lemma collineation_2604 : is_collineation2 fp_2604.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2604 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2604.

Lemma collineation_2605 : is_collineation2 fp_2605.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2605 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2605.

Lemma collineation_2606 : is_collineation2 fp_2606.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2606 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2606.

Lemma collineation_2607 : is_collineation2 fp_2607.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2607 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2607.

Lemma collineation_2608 : is_collineation2 fp_2608.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2608 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2608.

Lemma collineation_2609 : is_collineation2 fp_2609.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2609 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2609.

Lemma collineation_2610 : is_collineation2 fp_2610.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2610 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2610.

Lemma collineation_2611 : is_collineation2 fp_2611.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2611 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2611.

Lemma collineation_2612 : is_collineation2 fp_2612.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2612 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2612.

Lemma collineation_2613 : is_collineation2 fp_2613.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2613 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2613.

Lemma collineation_2614 : is_collineation2 fp_2614.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2614 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2614.

Lemma collineation_2615 : is_collineation2 fp_2615.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2615 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2615.

Lemma collineation_2616 : is_collineation2 fp_2616.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2616 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2616.

Lemma collineation_2617 : is_collineation2 fp_2617.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2617 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2617.

Lemma collineation_2618 : is_collineation2 fp_2618.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2618 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2618.

Lemma collineation_2619 : is_collineation2 fp_2619.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2619 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2619.

Lemma collineation_2620 : is_collineation2 fp_2620.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2620 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2620.

Lemma collineation_2621 : is_collineation2 fp_2621.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2621 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2621.

Lemma collineation_2622 : is_collineation2 fp_2622.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2622 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2622.

Lemma collineation_2623 : is_collineation2 fp_2623.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2623 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2623.

Lemma collineation_2624 : is_collineation2 fp_2624.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2624 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2624.

Lemma collineation_2625 : is_collineation2 fp_2625.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2625 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2625.

Lemma collineation_2626 : is_collineation2 fp_2626.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2626 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2626.

Lemma collineation_2627 : is_collineation2 fp_2627.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2627 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2627.

Lemma collineation_2628 : is_collineation2 fp_2628.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2628 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2628.

Lemma collineation_2629 : is_collineation2 fp_2629.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2629 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2629.

Lemma collineation_2630 : is_collineation2 fp_2630.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2630 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2630.

Lemma collineation_2631 : is_collineation2 fp_2631.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2631 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2631.

Lemma collineation_2632 : is_collineation2 fp_2632.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2632 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2632.

Lemma collineation_2633 : is_collineation2 fp_2633.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2633 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2633.

Lemma collineation_2634 : is_collineation2 fp_2634.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2634 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2634.

Lemma collineation_2635 : is_collineation2 fp_2635.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2635 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2635.

Lemma collineation_2636 : is_collineation2 fp_2636.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2636 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2636.

Lemma collineation_2637 : is_collineation2 fp_2637.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2637 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2637.

Lemma collineation_2638 : is_collineation2 fp_2638.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2638 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2638.

Lemma collineation_2639 : is_collineation2 fp_2639.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2639 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2639.

Lemma collineation_2640 : is_collineation2 fp_2640.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2640 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2640.

Lemma collineation_2641 : is_collineation2 fp_2641.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2641 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2641.

Lemma collineation_2642 : is_collineation2 fp_2642.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2642 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2642.

Lemma collineation_2643 : is_collineation2 fp_2643.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2643 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2643.

Lemma collineation_2644 : is_collineation2 fp_2644.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2644 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2644.

Lemma collineation_2645 : is_collineation2 fp_2645.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2645 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2645.

Lemma collineation_2646 : is_collineation2 fp_2646.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2646 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2646.

Lemma collineation_2647 : is_collineation2 fp_2647.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2647 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2647.

Lemma collineation_2648 : is_collineation2 fp_2648.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2648 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2648.

Lemma collineation_2649 : is_collineation2 fp_2649.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2649 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2649.

Lemma collineation_2650 : is_collineation2 fp_2650.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2650 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2650.

Lemma collineation_2651 : is_collineation2 fp_2651.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2651 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2651.

Lemma collineation_2652 : is_collineation2 fp_2652.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2652 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2652.

Lemma collineation_2653 : is_collineation2 fp_2653.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2653 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2653.

Lemma collineation_2654 : is_collineation2 fp_2654.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2654 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2654.

Lemma collineation_2655 : is_collineation2 fp_2655.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2655 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2655.

Lemma collineation_2656 : is_collineation2 fp_2656.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2656 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2656.

Lemma collineation_2657 : is_collineation2 fp_2657.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2657 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2657.

Lemma collineation_2658 : is_collineation2 fp_2658.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2658 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2658.

Lemma collineation_2659 : is_collineation2 fp_2659.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2659 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2659.

Lemma collineation_2660 : is_collineation2 fp_2660.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2660 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2660.

Lemma collineation_2661 : is_collineation2 fp_2661.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2661 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2661.

Lemma collineation_2662 : is_collineation2 fp_2662.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2662 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2662.

Lemma collineation_2663 : is_collineation2 fp_2663.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2663 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2663.

Lemma collineation_2664 : is_collineation2 fp_2664.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2664 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2664.

Lemma collineation_2665 : is_collineation2 fp_2665.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2665 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2665.

Lemma collineation_2666 : is_collineation2 fp_2666.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2666 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2666.

Lemma collineation_2667 : is_collineation2 fp_2667.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2667 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2667.

Lemma collineation_2668 : is_collineation2 fp_2668.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2668 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2668.

Lemma collineation_2669 : is_collineation2 fp_2669.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2669 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2669.

Lemma collineation_2670 : is_collineation2 fp_2670.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2670 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2670.

Lemma collineation_2671 : is_collineation2 fp_2671.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2671 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2671.

Lemma collineation_2672 : is_collineation2 fp_2672.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2672 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2672.

Lemma collineation_2673 : is_collineation2 fp_2673.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2673 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2673.

Lemma collineation_2674 : is_collineation2 fp_2674.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2674 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2674.

Lemma collineation_2675 : is_collineation2 fp_2675.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2675 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2675.

Lemma collineation_2676 : is_collineation2 fp_2676.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2676 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2676.

Lemma collineation_2677 : is_collineation2 fp_2677.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2677 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2677.

Lemma collineation_2678 : is_collineation2 fp_2678.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2678 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2678.

Lemma collineation_2679 : is_collineation2 fp_2679.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2679 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2679.

Lemma collineation_2680 : is_collineation2 fp_2680.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2680 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2680.

Lemma collineation_2681 : is_collineation2 fp_2681.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2681 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2681.

Lemma collineation_2682 : is_collineation2 fp_2682.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2682 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2682.

Lemma collineation_2683 : is_collineation2 fp_2683.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2683 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2683.

Lemma collineation_2684 : is_collineation2 fp_2684.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2684 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2684.

Lemma collineation_2685 : is_collineation2 fp_2685.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2685 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2685.

Lemma collineation_2686 : is_collineation2 fp_2686.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2686 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2686.

Lemma collineation_2687 : is_collineation2 fp_2687.
Proof.
  time (split ;
    [split;
    [intros x y; destruct x; destruct y; intros [=]; apply erefl | intros y; exists (inv_fp_2687 y); destruct y; apply erefl]
     | intros x l; destruct x; destruct l;intros H; solve [apply (degen_bool _ H) | apply is_true_true]]).
Qed.

Check collineation_2687.

Lemma is_col_all_c14 : forall fp, In fp (all_c14++all_c15++all_c16++all_c17++all_c18++all_c19++all_c20++all_c21++all_c22++all_c23++all_c24++all_c25++all_c26++all_c27) -> is_collineation2 fp.
Proof.
 intros fp HIn_S.
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1344 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1345 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1346 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1347 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1348 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1349 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1350 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1351 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1352 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1353 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1354 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1355 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1356 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1357 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1358 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1359 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1360 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1361 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1362 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1363 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1364 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1365 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1366 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1367 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1368 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1369 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1370 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1371 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1372 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1373 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1374 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1375 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1376 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1377 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1378 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1379 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1380 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1381 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1382 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1383 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1384 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1385 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1386 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1387 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1388 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1389 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1390 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1391 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1392 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1393 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1394 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1395 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1396 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1397 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1398 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1399 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1400 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1401 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1402 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1403 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1404 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1405 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1406 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1407 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1408 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1409 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1410 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1411 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1412 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1413 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1414 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1415 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1416 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1417 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1418 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1419 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1420 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1421 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1422 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1423 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1424 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1425 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1426 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1427 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1428 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1429 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1430 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1431 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1432 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1433 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1434 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1435 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1436 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1437 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1438 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1439 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1440 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1441 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1442 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1443 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1444 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1445 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1446 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1447 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1448 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1449 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1450 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1451 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1452 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1453 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1454 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1455 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1456 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1457 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1458 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1459 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1460 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1461 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1462 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1463 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1464 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1465 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1466 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1467 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1468 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1469 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1470 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1471 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1687 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1688 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1689 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1690 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1691 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1692 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1693 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1694 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1695 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1696 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1697 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1698 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1699 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1700 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1701 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1702 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1703 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1704 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1705 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1706 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1707 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1708 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1709 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1710 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1711 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1712 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1713 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1714 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1715 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1716 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1717 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1718 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1719 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1720 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1721 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1722 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1723 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1724 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1725 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1726 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1727 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1728 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1729 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1730 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1731 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1732 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1733 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1734 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1735 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1736 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1737 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1738 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1739 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1740 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1741 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1742 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1743 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1744 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1745 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1746 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1747 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1748 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1749 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1750 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1751 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1752 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1753 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1754 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1755 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1756 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1757 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1758 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1759 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1760 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1761 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1762 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1763 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1764 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1765 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1766 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1767 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1768 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1769 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1770 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1771 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1772 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1773 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1774 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1775 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1776 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1777 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1778 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1779 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1780 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1781 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1782 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1783 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1784 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1785 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1786 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1787 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1788 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1789 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1790 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1791 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1792 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1793 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1794 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1795 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1796 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1797 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1798 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1799 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1800 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1801 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1802 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1803 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1804 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1805 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1806 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1807 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1808 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1809 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1810 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1811 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1812 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1813 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1814 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1815 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1816 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1817 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1818 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1819 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1820 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1821 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1822 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1823 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1824 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1825 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1826 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1827 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1828 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1829 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1830 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1831 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1832 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1833 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1834 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1835 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1836 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1837 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1838 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1839 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1840 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1841 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1842 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1843 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1844 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1845 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1846 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1847 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1848 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1849 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1850 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1851 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1852 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1853 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1854 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1855 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1856 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1857 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1858 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1859 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1860 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1861 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1862 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1863 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1864 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1865 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1866 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1867 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1868 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1869 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1870 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1871 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1872 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1873 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1874 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1875 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1876 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1877 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1878 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1879 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1880 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1881 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1882 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1883 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1884 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1885 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1886 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1887 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1888 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1889 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1890 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1891 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1892 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1893 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1894 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1895 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1896 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1897 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1898 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1899 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1900 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1901 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1902 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1903 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1904 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1905 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1906 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1907 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1908 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1909 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1910 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1911 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1912 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1913 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1914 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1915 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1916 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1917 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1918 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1919 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1920 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1921 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1922 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1923 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1924 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1925 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1926 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1927 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1928 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1929 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1930 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1931 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1932 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1933 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1934 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1935 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1936 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1937 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1938 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1939 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1940 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1941 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1942 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1943 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1944 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1945 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1946 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1947 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1948 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1949 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1950 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1951 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1952 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1953 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1954 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1955 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1956 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1957 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1958 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1959 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1960 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1961 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1962 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1963 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1964 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1965 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1966 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1967 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1968 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1969 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1970 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1971 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1972 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1973 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1974 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1975 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1976 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1977 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1978 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1979 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1980 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1981 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1982 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1983 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1984 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1985 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1986 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1987 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1988 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1989 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1990 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1991 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1992 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1993 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1994 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1995 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1996 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1997 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1998 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_1999 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2000 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2001 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2002 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2003 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2004 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2005 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2006 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2007 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2008 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2009 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2010 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2011 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2012 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2013 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2014 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2015 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2016 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2017 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2018 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2019 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2020 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2021 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2022 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2023 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2024 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2025 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2026 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2027 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2028 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2029 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2030 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2031 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2032 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2033 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2034 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2035 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2036 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2037 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2038 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2039 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2040 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2041 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2042 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2043 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2044 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2045 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2046 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2047 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2048 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2049 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2050 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2051 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2052 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2053 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2054 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2055 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2056 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2057 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2058 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2059 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2060 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2061 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2062 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2063 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2064 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2065 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2066 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2067 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2068 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2069 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2070 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2071 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2072 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2073 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2074 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2075 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2076 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2077 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2078 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2079 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2080 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2081 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2082 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2083 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2084 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2085 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2086 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2087 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2088 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2089 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2090 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2091 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2092 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2093 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2094 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2095 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2096 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2097 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2098 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2099 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2100 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2101 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2102 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2103 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2104 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2105 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2106 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2107 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2108 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2109 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2110 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2111 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2112 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2113 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2114 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2115 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2116 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2117 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2118 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2119 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2120 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2121 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2122 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2123 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2124 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2125 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2126 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2127 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2128 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2129 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2130 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2131 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2132 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2133 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2134 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2135 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2136 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2137 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2138 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2139 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2140 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2141 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2142 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2143 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2144 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2145 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2146 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2147 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2148 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2149 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2150 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2151 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2152 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2153 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2154 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2155 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2156 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2157 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2158 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2159 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2160 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2161 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2162 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2163 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2164 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2165 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2166 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2167 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2168 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2169 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2170 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2171 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2172 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2173 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2174 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2175 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2176 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2177 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2178 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2179 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2180 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2181 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2182 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2183 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2184 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2185 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2186 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2187 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2188 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2189 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2190 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2191 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2192 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2193 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2194 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2195 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2196 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2197 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2198 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2199 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2200 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2201 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2202 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2203 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2204 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2205 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2206 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2207 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2208 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2209 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2210 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2211 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2212 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2213 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2214 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2215 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2216 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2217 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2218 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2219 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2220 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2221 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2222 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2223 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2224 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2225 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2226 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2227 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2228 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2229 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2230 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2231 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2232 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2233 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2234 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2235 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2236 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2237 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2238 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2239 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2240 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2241 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2242 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2243 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2244 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2245 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2246 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2247 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2248 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2249 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2250 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2251 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2252 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2253 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2254 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2255 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2256 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2257 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2258 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2259 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2260 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2261 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2262 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2263 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2264 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2265 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2266 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2267 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2268 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2269 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2270 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2271 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2272 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2273 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2274 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2275 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2276 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2277 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2278 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2279 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2280 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2281 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2282 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2283 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2284 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2285 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2286 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2287 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2288 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2289 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2290 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2291 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2292 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2293 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2294 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2295 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2296 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2297 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2298 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2299 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2300 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2301 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2302 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2303 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2304 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2305 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2306 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2307 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2308 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2309 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2310 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2311 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2312 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2313 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2314 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2315 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2316 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2317 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2318 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2319 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2320 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2321 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2322 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2323 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2324 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2325 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2326 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2327 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2328 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2329 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2330 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2331 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2332 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2333 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2334 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2335 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2336 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2337 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2338 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2339 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2340 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2341 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2342 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2343 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2344 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2345 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2346 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2347 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2348 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2349 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2350 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2351 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2352 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2353 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2354 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2355 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2356 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2357 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2358 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2359 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2360 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2361 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2362 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2363 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2364 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2365 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2366 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2367 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2368 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2369 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2370 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2371 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2372 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2373 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2374 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2375 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2376 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2377 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2378 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2379 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2380 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2381 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2382 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2383 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2384 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2385 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2386 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2387 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2388 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2389 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2390 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2391 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2392 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2393 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2394 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2395 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2396 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2397 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2398 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2399 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2400 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2401 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2402 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2403 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2404 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2405 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2406 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2407 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2408 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2409 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2410 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2411 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2412 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2413 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2414 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2415 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2416 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2417 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2418 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2419 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2420 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2421 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2422 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2423 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2424 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2425 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2426 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2427 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2428 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2429 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2430 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2431 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2432 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2433 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2434 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2435 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2436 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2437 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2438 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2439 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2440 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2441 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2442 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2443 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2444 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2445 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2446 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2447 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2448 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2449 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2450 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2451 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2452 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2453 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2454 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2455 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2456 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2457 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2458 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2459 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2460 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2461 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2462 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2463 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2464 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2465 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2466 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2467 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2468 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2469 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2470 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2471 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2472 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2473 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2474 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2475 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2476 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2477 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2478 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2479 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2480 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2481 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2482 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2483 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2484 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2485 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2486 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2487 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2488 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2489 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2490 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2491 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2492 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2493 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2494 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2495 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2496 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2497 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2498 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2499 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2500 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2501 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2502 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2503 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2504 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2505 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2506 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2507 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2508 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2509 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2510 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2511 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2512 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2513 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2514 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2515 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2516 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2517 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2518 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2519 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2520 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2521 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2522 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2523 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2524 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2525 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2526 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2527 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2528 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2529 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2530 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2531 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2532 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2533 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2534 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2535 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2536 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2537 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2538 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2539 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2540 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2541 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2542 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2543 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2544 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2545 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2546 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2547 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2548 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2549 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2550 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2551 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2552 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2553 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2554 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2555 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2556 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2557 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2558 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2559 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2560 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2561 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2562 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2563 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2564 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2565 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2566 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2567 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2568 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2569 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2570 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2571 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2572 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2573 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2574 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2575 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2576 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2577 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2578 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2579 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2580 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2581 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2582 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2583 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2584 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2585 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2586 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2587 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2588 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2589 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2590 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2591 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2592 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2593 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2594 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2595 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2596 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2597 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2598 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2599 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2600 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2601 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2602 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2603 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2604 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2605 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2606 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2607 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2608 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2609 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2610 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2611 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2612 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2613 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2614 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2615 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2616 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2617 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2618 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2619 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2620 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2621 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2622 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2623 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2624 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2625 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2626 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2627 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2628 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2629 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2630 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2631 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2632 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2633 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2634 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2635 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2636 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2637 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2638 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2639 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2640 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2641 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2642 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2643 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2644 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2645 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2646 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2647 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2648 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2649 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2650 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2651 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2652 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2653 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2654 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2655 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2656 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2657 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2658 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2659 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2660 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2661 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2662 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2663 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2664 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2665 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2666 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2667 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2668 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2669 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2670 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2671 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2672 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2673 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2674 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2675 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2676 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2677 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2678 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2679 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2680 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2681 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2682 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2683 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2684 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2685 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2686 | idtac].
 destruct HIn_S as [HeqL | HIn_S]; [inversion HeqL; subst fp; apply collineation_2687 | idtac].
 destruct (in_nil HIn_S).
Qed.

