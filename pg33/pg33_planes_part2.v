Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG33.pg33_inductive.
Require Import PG33.pg33_planes PG33.pg33_planes_part1.

Require Import List.
Import ListNotations.

Ltac ex_l t := solve [
                   exists L0; t | exists L1; t | exists L2; t | exists L3; t | exists L4; t | 
                          exists L5; t | exists L6; t | exists L7; t | exists L8; t | exists L9; t |
                          exists L10; t | exists L11; t | exists L12; t | exists L13; t | exists L14; t |
                          exists L15; t | exists L16; t | exists L17; t | exists L18; t | exists L19; t |
                          exists L20; t | exists L21; t | exists L22; t | exists L23; t | exists L24; t | 
                          exists L25; t | exists L26; t | exists L27; t | exists L28; t | exists L29; t |
                          exists L30; t | exists L31; t | exists L32; t | exists L33; t | exists L34; t |
                          exists L35; t | exists L36; t | exists L37; t | exists L38; t | exists L39; t | 
                          exists L40; t | exists L41; t | exists L42; t | exists L43; t | exists L44; t |
                          exists L45; t | exists L46; t | exists L47; t | exists L48; t | exists L49; t |
                          exists L50; t | exists L51; t | exists L52; t | exists L53; t | exists L54; t | 
                          exists L55; t | exists L56; t | exists L57; t | exists L58; t | exists L59; t |
                          exists L60; t | exists L61; t | exists L62; t | exists L63; t | exists L64; t |
                          exists L65; t | exists L66; t | exists L67; t | exists L68; t | exists L69; t | 
                          exists L70; t | exists L71; t | exists L72; t | exists L73; t | exists L74; t |
                          exists L75; t | exists L76; t | exists L77; t | exists L78; t | exists L79; t |
                          exists L80; t | exists L81; t | exists L82; t | exists L83; t | exists L84; t | 
                          exists L85; t | exists L86; t | exists L87; t | exists L88; t | exists L89; t |
                          exists L90; t | exists L91; t | exists L92; t | exists L93; t | exists L94; t |
                          exists L95; t | exists L96; t | exists L97; t | exists L98; t | exists L99; t | 
                          
                          exists L100; t | exists L101; t | exists L102; t | exists L103; t | exists L104; t | 
                          exists L105; t | exists L106; t | exists L107; t | exists L108; t | exists L109; t |
                          exists L110; t | exists L111; t | exists L112; t | exists L113; t | exists L114; t |
                          exists L115; t | exists L116; t | exists L117; t | exists L118; t | exists L119; t |
                          exists L120; t | exists L121; t | exists L122; t | exists L123; t | exists L124; t | 
                          exists L125; t | exists L126; t | exists L127; t | exists L128; t | exists L129; t ].

Lemma two_distinct_planes_intersect_along_a_line :
  forall p1 p2, In p1 list_planes -> In p2 list_planes -> p1<>p2 ->
                exists l:Line,
                  let l' := all_points_of_line l in
                      let x := match l' with [a;b;c;d] => a | _ => P0 end in
                      let y := match l' with [a;b;c;d] => b | _ => P0 end in
                      let z := match l' with [a;b;c;d] => c | _ => P0 end in
                      let t := match l' with [a;b;c;d] => d | _ => P0 end in
                      let (xy,z) := points_from_line l in
                      belongs x p1 && belongs y p1 && belongs z p1 && belongs t p1 &&
                      belongs x p2 && belongs y p2 && belongs z p2 && belongs t p2.
Proof.
  unfold list_planes; intros p1 p2 Hp1 Hp2 Hp1p2.
repeat (match goal with H: In p1 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p1 xs H); clear H; intros H'; destruct H' end);
  repeat (match goal with H: In p2 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p2 xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption | rewrite H in H0; apply False_ind; apply Hp1p2; assumption]; rewrite <- H; clear H; rewrite <- H0; clear H0.
par: time(ex_l ltac:((*simpl;*) reflexivity)).
Qed.

Ltac dest x y := destruct (Point_dec x y); try reflexivity.

Lemma belongs_descr : forall t x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12,
    belongs t [x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12] <->
    t=x0 \/ t=x1 \/ t=x2 \/ t=x3 \/ t=x4 \/ t=x5 \/ t=x6 \/ t=x7 \/ t=x8 \/ t=x9 \/ t=x10 \/ t=x11 \/ t=x12.
Proof.
  intros; split.
  unfold belongs; intros Hb.
  destruct (in_dec Point_dec t [x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12]).
  inversion i.
  intuition.
  inversion H.
  intuition.
  inversion H0.
  intuition.
  inversion H1.
  intuition.
  inversion H2.
  intuition.
  inversion H3.
  intuition.
  inversion H4.
  intuition.
  inversion H5.
  intuition.
  inversion H6.
  intuition.
  inversion H7.
  intuition.
  inversion H8.
  intuition.
  inversion H9.
  intuition.
  inversion H10.
  intuition.
  apply False_ind; eapply in_nil; eassumption.
  apply (degen_bool _ Hb).
  
  intros Hor; decompose [or] Hor; clear Hor; subst; unfold belongs; simpl in_dec. 
  destruct (Point_dec x0 x0) ; [reflexivity | apply False_ind; apply n; reflexivity].
  destruct (Point_dec x1 x1);
    [ destruct (Point_dec x0 x1); [reflexivity | reflexivity]| apply False_ind; apply n; reflexivity ].
  dest x2 x2; dest x0 x2; dest x1 x2; apply False_ind; apply n; reflexivity.
  dest x3 x3; dest x0 x3; dest x1 x3; dest x2 x3; apply False_ind; apply n; reflexivity.
  dest x4 x4; dest x0 x4; dest x1 x4; dest x2 x4; dest x3 x4;  apply False_ind; apply n; reflexivity.
  dest x5 x5; dest x0 x5; dest x1 x5; dest x2 x5; dest x3 x5; dest x4 x5; apply False_ind; apply n; reflexivity.
  dest x6 x6; dest x0 x6; dest x1 x6; dest x2 x6; dest x3 x6; dest x4 x6; dest x5 x6; apply False_ind; apply n; reflexivity.
  dest x7 x7; dest x0 x7; dest x1 x7; dest x2 x7; dest x3 x7; dest x4 x7; dest x5 x7; dest x6 x7; apply False_ind; apply n; reflexivity.
  dest x8 x8; dest x0 x8; dest x1 x8; dest x2 x8; dest x3 x8; dest x4 x8; dest x5 x8; dest x6 x8; dest x7 x8; apply False_ind; apply n; reflexivity.
  dest x9 x9; dest x0 x9; dest x1 x9; dest x2 x9; dest x3 x9; dest x4 x9; dest x5 x9; dest x6 x9; dest x7 x9; dest x8 x9; apply False_ind; apply n; reflexivity.
  dest x10 x10; dest x0 x10; dest x1 x10; dest x2 x10; dest x3 x10; dest x4 x10; dest x5 x10; dest x6 x10; dest x7 x10; dest x8 x10; dest x9 x10; apply False_ind; apply n; reflexivity.
 dest x11 x11; dest x0 x11; dest x1 x11; dest x2 x11; dest x3 x11; dest x4 x11; dest x5 x11; dest x6 x11; dest x7 x11; dest x8 x11; dest x9 x11; dest x10 x11; apply False_ind; apply n; reflexivity.
 dest x12 x12; dest x0 x12; dest x1 x12; dest x2 x12; dest x3 x12; dest x4 x12; dest x5 x12; dest x6 x12; dest x7 x12; dest x8 x12; dest x9 x12; dest x10 x12; dest x11 x12; apply False_ind; apply n; reflexivity.
Qed.

Lemma and_only_one_line :
  forall p1 p2, In p1 list_planes -> In p2 list_planes -> p1<>p2 ->
                forall l l', 
                  (let l2 := all_points_of_line l in
                      let x := match l2 with [a;b;c;d] => a | _ => P0 end in
                      let y := match l2 with [a;b;c;d] => b | _ => P0 end in
                      let z := match l2 with [a;b;c;d] => c | _ => P0 end in
                      let t := match l2 with [a;b;c;d] => d | _ => P0 end in
                      belongs x p1 && belongs y p1 && belongs z p1 && belongs t p1 &&
                      belongs x p2 && belongs y p2 && belongs z p2 && belongs t p2) ->
                  (let l2' := all_points_of_line l' in
                      let x := match l2' with [a;b;c;d] => a | _ => P0 end in
                      let y := match l2' with [a;b;c;d] => b | _ => P0 end in
                      let z := match l2' with [a;b;c;d] => c | _ => P0 end in
                      let t := match l2' with [a;b;c;d] => d | _ => P0 end in
                      belongs x p1 && belongs y p1 && belongs z p1 && belongs t p1 &&
                      belongs x p2 && belongs y p2 && belongs z p2 && belongs t p2)
   -> l=l'.
Proof.
  unfold list_planes; intros.
repeat (match goal with H: In p1 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p1 xs H); clear H; intros H'; destruct H' end);
  repeat (match goal with H: In p2 (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x p2 xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption | rewrite H in H0; apply False_ind; apply H1; assumption]; rewrite <- H in H2,H3; rewrite <- H0 in H2,H3.
(*idtac.
clear H H0; revert H2 H3; do 14 rewrite  and_bool; intros H2 H3.
intuition.
 *)
par: time(clear H H0; destruct l; solve [apply (degen_bool _ H2) | destruct l'; solve [apply (degen_bool _ H3) | reflexivity]]).
Qed.

Lemma incid_lp_points : forall p l, incid_lp p l ->
                                    let l' := all_points_of_line l in
                      let x := match l' with [a;b;c;d] => a | _ => P0 end in
                      let y := match l' with [a;b;c;d] => b | _ => P0 end in
                      let z := match l' with [a;b;c;d] => c | _ => P0 end in
                      let t := match l' with [a;b;c;d] => d | _ => P0 end in
                      p=x \/ p=y \/ p=z \/ p=t.
Proof.
intros p l Hincid; destruct l.
par:simpl; destruct p; try solve [apply (degen_bool _ Hincid) | tauto].
Qed.

Definition point1 (l:Line) := match all_points_of_line l with [a;b;c;d] => a | _ => P0 end.
Definition point2 (l:Line) := match all_points_of_line l with [a;b;c;d] => b | _ => P0 end.
Definition point3 (l:Line) := match all_points_of_line l with [a;b;c;d] => c | _ => P0 end.
Definition point4 (l:Line) := match all_points_of_line l with [a;b;c;d] => d | _ => P0 end.

Lemma plane_line :
  forall s:list Point,
    In s list_planes ->
    forall l:Line, (exists p:Point, incid_lp p l && negb (belongs p s)) -> exists q:Point, incid_lp q l && (belongs q s).
Proof.
unfold list_planes; intros.
repeat (match goal with H: In s (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x s xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption];
  rewrite <- H in H0; rewrite <- H; clear H; destruct H0 as [r Hr].
par:revert Hr; rewrite and_bool; intros Hr; destruct Hr as [Hir Hbr];
generalize (incid_lp_points r l Hir); intros Hi; destruct l;simpl in Hi; intuition; subst;
  try solve [  apply (degen_bool _ Hbr) | apply (degen_bool _ Hir) | match goal with |- exists q : Point, is_true ((incid_lp q ?l) && _) =>
solve [exists (point1 l); reflexivity | exists (point2 l); reflexivity | exists (point3 l); reflexivity | exists (point4 l); reflexivity] end].
Qed.

Lemma plane_line_unique :
    forall s:list Point,
    In s list_planes ->
    forall l:Line,  (exists p:Point, incid_lp p l && negb (belongs p s)) ->
                    forall (p q:Point), incid_lp p l && (belongs p s) -> incid_lp q l && (belongs q s) -> p=q.
Proof.
unfold list_planes; intros.
repeat (match goal with H: In s (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x s xs H); clear H; intros H'; destruct H' end); try solve [apply False_ind; eapply in_nil; eassumption]; rewrite <-H in H0,H1,H2; clear H.
par: time (destruct H0 as [r Hr];
revert H1 H2 Hr; do 3 rewrite and_bool; intros [H1 H1'] [H2 H2'] [Hr Hr'];generalize (incid_lp_points p l H1);
generalize (incid_lp_points q l H2);
generalize (incid_lp_points r l Hr); intros Hip Hiq Hir; destruct l; simpl in Hip, Hiq, Hir; intuition; subst; 
  solve [reflexivity | apply (degen_bool _ H1) | apply (degen_bool _ H1') 
             | apply (degen_bool _ H2) |  apply (degen_bool _ H2')
             | apply (degen_bool _ Hr) |  apply (degen_bool _ Hr')]).
Qed.

(*
Each point is contained in 13 lines and 13 planes 
Each line is contained in 4 planes and contains 4 points
Each plane contains 13 points and 13 lines
*)
