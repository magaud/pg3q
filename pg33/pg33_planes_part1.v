Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG33.pg33_inductive PG33.pg33_planes. (* PG32.pg32_proofs.*)
(*Require Import PG33.pg32_spreads.*)

Require Import List.
Import ListNotations.
Ltac witnesses l m := let Hdiff := fresh  in assert (Hdiff:l<>m) by discriminate;
                      let Hinter := fresh in assert(Hinter:intersect l m) by reflexivity;
                             exists l; exists m; exists Hdiff; exists Hinter; unfold plane; simpl; reflexivity.

Lemma plane_equiv_2 : forall l:list Point,
    In l list_planes ->
    exists l1, exists l2, exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  unfold list_planes; intros.
  repeat (match goal with H: In l (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x l xs H); clear H; intros H'; destruct H' end).
1-40:match goal with H: [?x0; ?x1; ?x2; ?x3; ?x4; ?x5; ?x6; ?x7; ?x8; ?x9; ?x10; ?x11; ?x12] = ?l |- _ => rewrite <- H; witnesses (l_from_points x0 x1) (l_from_points x5 x6) end.
apply False_ind; eapply in_nil; eassumption.
Qed.
  
Lemma plane_equiv : forall l:list Point,
    In l list_planes <-> exists l1, exists l2,
        exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  intros; split.
  apply plane_equiv_2.
  intros [l1 [l2 [Hdiff [Hinter H]]]]; rewrite H.
  apply plane_equiv_1.
Qed.

Definition belongs p l := if (in_dec Point_dec p l) then true else false.

Lemma a_point_belongs_to_13_planes :
  forall p:Point, 
    length (filter (fun (l:list Point) => belongs p l) list_planes) = 13.
Proof.
intros p; destruct p; simpl; reflexivity.
Qed.

Definition lines_of_plane (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2) :=
  filter (fun (l:Line) =>
            let l' := all_points_of_line l in
            let x := match l' with [a;b;c;d] => a | _ => P0 end in
            let y := match l' with [a;b;c;d] => b | _ => P0 end in
            let z := match l' with [a;b;c;d] => c | _ => P0 end in
            let t := match l' with [a;b;c;d] => d | _ => P0 end in
            belongs x (plane l1 l2 Hdiff Hinter) && belongs y (plane l1 l2 Hdiff Hinter) &&
            belongs z (plane l1 l2 Hdiff Hinter) && belongs t (plane l1 l2 Hdiff Hinter)) all_lines.

Lemma a_plane_has_13_lines : forall (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2),
    length (lines_of_plane l1 l2 Hdiff Hinter) = 13.
Proof.
intros l1 l2; destruct l1; destruct l2; intros; try time (solve [(apply False_ind; apply Hdiff; reflexivity) | apply (degen_bool _ Hinter) | unfold lines_of_plane; (*simpl;*) reflexivity]). 
Qed.

Lemma a_line_belongs_to_exactly_4_planes : forall l:Line,
    length (filter (fun (p:list Point) =>
                      let l' := all_points_of_line l in
                      let x := match l' with [a;b;c;d] => a | _ => P0 end in
                      let y := match l' with [a;b;c;d] => b | _ => P0 end in
                      let z := match l' with [a;b;c;d] => c | _ => P0 end in
                      let t := match l' with [a;b;c;d] => d | _ => P0 end in
                      belongs x p && belongs y p && belongs z p && belongs t p) list_planes) = 4.
Proof.
  intros l; destruct l; intros; unfold list_planes; (*simpl;*) reflexivity.
Qed.
