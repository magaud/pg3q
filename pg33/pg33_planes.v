Require Import ssreflect ssrfun ssrbool.
Require Import Generic.lemmas.
Require Import PG33.pg33_inductive. (* PG32.pg32_proofs.*)
(*Require Import PG33.pg32_spreads.*)

Require Import List.
Import ListNotations.

Definition all_points := [
                         P0; P1; P2; P3; P4; P5; P6; P7; P8; P9;
                         P10; P11; P12; P13; P14; P15; P16; P17; P18; P19;
                         P20; P21; P22; P23; P24; P25; P26; P27; P28; P29;
                         P30; P31; P32; P33; P34; P35; P36; P37; P38; P39 ].

Definition all_lines := 
[L0; L1; L2; L3; L4; L5; L6; L7; L8; L9; L10; L11; L12; L13; L14; L15; L16; L17; L18; L19; L20; L21; L22; L23; L24; L25; L26; L27; L28; L29; L30; L31; L32; L33; L34; L35; L36; L37; L38; L39; L40; L41; L42; L43; L44; L45; L46; L47; L48; L49; L50; L51; L52; L53; L54; L55; L56; L57; L58; L59; L60; L61; L62; L63; L64; L65; L66; L67; L68; L69; L70; L71; L72; L73; L74; L75; L76; L77; L78; L79; L80; L81; L82; L83; L84; L85; L86; L87; L88; L89; L90; L91; L92; L93; L94; L95; L96; L97; L98; L99; L100; L101; L102; L103; L104; L105; L106; L107; L108; L109; L110; L111; L112; L113; L114; L115; L116; L117; L118; L119; L120; L121; L122; L123; L124; L125; L126; L127; L128; L129].

Definition lines_through_point (x:Point) := filter (fun (l:Line) => incid_lp x l) all_lines. 

Lemma a_point_is_incident_to_13_lines : forall x, length (lines_through_point x) = 13.
Proof.
destruct x; reflexivity.
Qed.

(* points_of_line *)
Definition all_points_of_line (l:Line) := List.filter (fun x => incid_lp x l) (all_points).

Lemma a_line_contains_4_points : forall l, length (all_points_of_line l) = 4.
Proof.
  destruct l; reflexivity.
Qed.                                     

Definition inter (l1 l2 : Line) : list Point :=
  List.filter (fun n => List.existsb (eqP n) (all_points_of_line l2)) (all_points_of_line l1).
(*
Lemma inter_spec : forall l1 l2, l1<>l2 -> length (inter l1 l2) = 0 \/ length (inter l1 l2) = 1.
Proof.
  intros l1 l2 Hl1l2; destruct l1; destruct l2.
  par:solve [ apply False_ind; apply Hl1l2; reflexivity | right; reflexivity | left; reflexivity ].
Qed.
*)  
Definition intersect (l1:Line) (l2:Line) := match (inter l1 l2) with [] => false | _ => true end.

Lemma Point_dec : forall p q : Point, {p=q}+{p<>q}.
Proof.
intros p q; case p; case q; solve [left; reflexivity | right; discriminate].
Defined.

Definition Point_eqb (P Q:Point) :=
  match P with
  | P0 => match Q with P0 => true | _ => false end
  | P1 => match Q with P1 => true | _ => false end
  | P2 => match Q with P2 => true | _ => false end
  | P3 => match Q with P3 => true | _ => false end
  | P4 => match Q with P4 => true | _ => false end
  | P5 => match Q with P5 => true | _ => false end
  | P6 => match Q with P6 => true | _ => false end
  | P7 => match Q with P7 => true | _ => false end
  | P8 => match Q with P8 => true | _ => false end
  | P9 => match Q with P9 => true | _ => false end
  | P10 => match Q with P10 => true | _ => false end
  | P11 => match Q with P11 => true | _ => false end
  | P12 => match Q with P12 => true | _ => false end
  | P13 => match Q with P13 => true | _ => false end
  | P14 => match Q with P14 => true | _ => false end
  | P15 => match Q with P15 => true | _ => false end
  | P16 => match Q with P16 => true | _ => false end
  | P17 => match Q with P17 => true | _ => false end
  | P18 => match Q with P18 => true | _ => false end
  | P19 => match Q with P19 => true | _ => false end
  | P20 => match Q with P20 => true | _ => false end
  | P21 => match Q with P21 => true | _ => false end
  | P22 => match Q with P22 => true | _ => false end
  | P23 => match Q with P23 => true | _ => false end
  | P24 => match Q with P24 => true | _ => false end
  | P25 => match Q with P25 => true | _ => false end
  | P26 => match Q with P26 => true | _ => false end
  | P27 => match Q with P27 => true | _ => false end
  | P28 => match Q with P28 => true | _ => false end
  | P29 => match Q with P29 => true | _ => false end
  | P30 => match Q with P30 => true | _ => false end
  | P31 => match Q with P31 => true | _ => false end
  | P32 => match Q with P32 => true | _ => false end
  | P33 => match Q with P33 => true | _ => false end
  | P34 => match Q with P34 => true | _ => false end
  | P35 => match Q with P35 => true | _ => false end
  | P36 => match Q with P36 => true | _ => false end
  | P37 => match Q with P37 => true | _ => false end
  | P38 => match Q with P38 => true | _ => false end
  | P39 => match Q with P39 => true | _ => false end
  end.

Lemma Point_eqb_true : forall P Q, Point_eqb P Q = true <-> P=Q.
Proof.
  intros P Q; split.
  intros H; destruct P; destruct Q; try solve [reflexivity | apply (degen_bool _ H)].
  intros H; destruct P; destruct Q; solve [reflexivity | discriminate].
Qed.

Lemma Point_eqb_false : forall P Q, Point_eqb P Q = false <-> P<>Q.
Proof.
 intros P Q; split.
  intros H; destruct P; destruct Q; solve [ reflexivity | discriminate].
  intros H; destruct P; destruct Q; try solve [reflexivity | discriminate | apply False_ind; apply H; reflexivity].
Qed.

Definition incid_6_points (a b c d e f:Point) :=
  fun (y:Point) =>
    incid_lp y (l_from_points a d) || incid_lp y (l_from_points a e) || incid_lp y (l_from_points a f) ||
             incid_lp y (l_from_points b d) || incid_lp y (l_from_points b e) || incid_lp y (l_from_points b f) ||
             incid_lp y (l_from_points c d) || incid_lp y (l_from_points c e) || incid_lp y (l_from_points c f).
  
Definition plane (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2) :=
  let l1' := all_points_of_line l1 in
  let x1 := match l1' with [a;b;c;d] => a | _ => P0 end in
  let y1 := match l1' with [a;b;c;d] => b | _ => P0 end in
  let z1 := match l1' with [a;b;c;d] => c | _ => P0 end in
  let t1 := match l1' with [a;b;c;d] => d | _ => P0 end in
  let l2' := all_points_of_line l2 in
  let x2 := match l2' with [a;b;c;d] => a | _ => P0 end in
  let y2 := match l2' with [a;b;c;d] => b | _ => P0 end in
  let z2 := match l2' with [a;b;c;d] => c | _ => P0 end in
  let t2 := match l2' with [a;b;c;d] => d | _ => P0 end in
  
  let i := match (inter l1 l2) with [x] => x | _ => P0 end in
  
  filter (fun (y:Point) =>
            incid_lp y l1 || incid_lp y l2 ||
            if (Point_eqb i x1)
            then
              if (Point_eqb i x2)
              then incid_6_points y1 z1 t1 y2 z2 t2 y
              else
                if (Point_eqb i y2)
                then incid_6_points y1 z1 t1 x2 z2 t2 y
                else
                  if (Point_eqb i z2)
                  then incid_6_points y1 z1 t1 x2 y2 t2 y
                  else
                    if (Point_eqb i t2)
                    then incid_6_points y1 z1 t1 x2 y2 z2 y
                    else false
            else  if (Point_eqb i y1) then
                   if (Point_eqb i x2)
                    then incid_6_points x1 z1 t1 y2 z2 t2 y
                    else
                      if (Point_eqb i y2)
                      then incid_6_points x1 z1 t1 x2 z2 t2 y
                      else
                        if (Point_eqb i z2)
                        then incid_6_points x1 z1 t1 x2 y2 t2 y
                        else
                          if (Point_eqb i t2)
                          then incid_6_points x1 z1 t1 x2 y2 z2 y
                          else false
                  else if (Point_eqb i z1) then
                          if (Point_eqb i x2)
                          then incid_6_points x1 y1 t1 y2 z2 t2 y
                          else
                            if (Point_eqb i y2)
                            then incid_6_points x1 y1 t1 x2 z2 t2 y
                            else
                              if (Point_eqb i z2)
                              then incid_6_points x1 y1 t1 x2 y2 t2 y
                              else
                                if (Point_eqb i t2)
                                then incid_6_points x1 y1 t1 x2 y2 z2 y
                                else false
                       else if (Point_eqb i t1) then
                              if (Point_eqb i x2)
                              then incid_6_points x1 y1 z1 y2 z2 t2 y
                              else
                                if (Point_eqb i y2)
                                then incid_6_points x1 y1 z1 x2 z2 t2 y
                                else
                                  if (Point_eqb i z2)
                                  then incid_6_points x1 y1 z1 x2 y2 t2 y
                                  else
                                    if (Point_eqb i t2)
                                    then incid_6_points x1 y1 z1 x2 y2 z2 y
                                    else false
                            else false)
         all_points.
(*
Lemma a_plane_has_13_points : forall  (l1 : Line) (l2 : Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2), length (plane l1 l2 Hdiff Hinter) = 13.
Proof.
  intros l1 l2; destruct l1; destruct l2; intros Hdiff Hinter.    
  par:time (solve [(apply False_ind; apply Hdiff; reflexivity) | apply (degen_bool _ Hinter) | reflexivity]).
Qed.
*)
(*
Ltac solve_In := solve [apply in_eq | apply in_cons; solve_In].
Ltac solve_In' := repeat (match goal with H: In ?p ?q |- _ => inversion H; clear H; [intuition | idtac] end).

Lemma decomp_list : forall x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20 x21 x22 x23 x24 x25 x26 x27 x28 x29 x30 x31 x32 x33 x34, forall x:(list Point),
    In x [x0; x1; x2; x3; x4; x5; x6; x7; x8; x9; x10; x11; x12; x13; x14; x15; x16; x17; x18; x19; x20; x21; x22; x23; x24; x25; x26; x27; x28; x29; x30; x31; x32; x33; x34] <-> x=x0 \/ x=x1 \/ x=x2 \/ x=x3 \/ x=x4 \/ x=x5 \/ x=x6 \/ x=x7\/ x=x8\/ x=x9\/ x=x10\/ x=x11\/ x=x12\/ x=x13\/ x=x14\/ x=x15\/ x=x16\/ x=x17\/ x=x18\/ x=x19\/ x=x20\/ x=x21\/ x=x22\/ x=x23\/ x=x24\/ x=x25\/ x=x26\/ x=x27\/ x=x28\/ x=x29\/ x=x30\/ x=x31\/ x=x32\/ x=x33\/ x=x34.
Proof.
  intros; split.
  intros Hin; solve_In'.
  inversion H. 
  intros H; decompose [or] H; subst; solve_In.
Qed.
*)
Ltac large_or := solve [left; reflexivity | right; large_or].

Definition list_planes := [
                         [P0; P1; P2; P4; P5; P8; P13; P14; P17; P19; P24; P26; P34];
                         [P0; P1; P3; P4; P7; P12; P13; P16; P18; P23; P25; P33; P39];
                         [P0; P1; P4; P6; P11; P13; P21; P27; P28; P29; P31; P32; P35];
                         [P0; P1; P4; P9; P10; P13; P15; P20; P22; P30; P36; P37; P38];
                         [P0; P2; P3; P6; P11; P12; P15; P17; P22; P24; P32; P38; P39];
                         [P0; P2; P7; P9; P17; P23; P24; P25; P27; P28; P31; P36; P37];
                         [P0; P2; P10; P16; P17; P18; P20; P21; P24; P29; P30; P33; P35];
                         [P0; P3; P5; P10; P12; P20; P26; P27; P28; P30; P31; P34; P39];
                         [P0; P3; P8; P9; P12; P14; P19; P21; P29; P35; P36; P37; P39];
                         [P0; P5; P6; P9; P11; P16; P18; P26; P32; P33; P34; P36; P37];
                         [P0; P5; P7; P15; P21; P22; P23; P25; P26; P29; P34; P35; P38];
                         [P0; P6; P7; P8; P10; P11; P14; P19; P20; P23; P25; P30; P32];
                         [P0; P8; P14; P15; P16; P18; P19; P22; P27; P28; P31; P33; P38];
                         [P1; P2; P3; P5; P6; P9; P14; P15; P18; P20; P25; P27; P35];
                         [P1; P2; P5; P7; P12; P14; P22; P28; P29; P30; P32; P33; P36];
                         [P1; P2; P5; P10; P11; P14; P16; P21; P23; P31; P37; P38; P39];
                         [P1; P3; P8; P10; P18; P24; P25; P26; P28; P29; P32; P37; P38];
                         [P1; P3; P11; P17; P18; P19; P21; P22; P25; P30; P31; P34; P36];
                         [P1; P6; P7; P10; P12; P17; P19; P27; P33; P34; P35; P37; P38];
                         [P1; P6; P8; P16; P22; P23; P24; P26; P27; P30; P35; P36; P39];
                         [P1; P7; P8; P9; P11; P12; P15; P20; P21; P24; P26; P31; P33];
                         [P1; P9; P15; P16; P17; P19; P20; P23; P28; P29; P32; P34; P39];
                         [P2; P3; P4; P6; P7; P10; P15; P16; P19; P21; P26; P28; P36];
                         [P2; P3; P6; P8; P13; P15; P23; P29; P30; P31; P33; P34; P37];
                         [P2; P4; P9; P11; P19; P25; P26; P27; P29; P30; P33; P38; P39];
                         [P2; P4; P12; P18; P19; P20; P22; P23; P26; P31; P32; P35; P37];
                         [P2; P7; P8; P11; P13; P18; P20; P28; P34; P35; P36; P38; P39];
                         [P2; P8; P9; P10; P12; P13; P16; P21; P22; P25; P27; P32; P34]; 
                         [P3; P4; P5; P7; P8; P11; P16; P17; P20; P22; P27; P29; P37];
                         [P3; P4; P7; P9; P14; P16; P24; P30; P31; P32; P34; P35; P38];
                         [P3; P5; P13; P19; P20; P21; P23; P24; P27; P32; P33; P36; P38];
                         [P3; P9; P10; P11; P13; P14; P17; P22; P23; P26; P28; P33; P35];
                         [P4; P5; P6; P8; P9; P12; P17; P18; P21; P23; P28; P30; P38];
                         [P4; P5; P8; P10; P15; P17; P25; P31; P32; P33; P35; P36; P39];
                         [P4; P6; P14; P20; P21; P22; P24; P25; P28; P33; P34; P37; P39];
                         [P4; P10; P11; P12; P14; P15; P18; P23; P24; P27; P29; P34; P36];
                         [P5; P6; P7; P9; P10; P13; P18; P19; P22; P24; P29; P31; P39];
                         [P5; P11; P12; P13; P15; P16; P19; P24; P25; P28; P30; P35; P37];
                         [P6; P12; P13; P14; P16; P17; P20; P25; P26; P29; P31; P36; P38];
                         [P7; P13; P14; P15; P17; P18; P21; P26; P27; P30; P32; P37; P39]
                         ].

Lemma plane_equiv_1 : forall (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2),
    In (plane l1 l2 Hdiff Hinter) list_planes.
Proof.
  intros l1 l2 Hdiff Hinter; destruct l1; destruct l2; intros.
                                                                                                 (* 5505:{unfold plane; simpl. *)
                                                                                               par:time solve[apply False_ind; apply Hdiff; reflexivity  | apply (degen_bool _ Hinter) |unfold plane; (*simpl;*) large_or].
                                                                                        
Qed.

Ltac witnesses l m := let Hdiff := fresh  in assert (Hdiff:l<>m) by discriminate;
                      let Hinter := fresh in assert(Hinter:intersect l m) by reflexivity;
                             exists l; exists m; exists Hdiff; exists Hinter; unfold plane; simpl; reflexivity.

Lemma plane_equiv_2 : forall l:list Point,
    In l list_planes ->
    exists l1, exists l2, exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  unfold list_planes; intros.
  repeat (match goal with H: In l (?x::?xs) |- _ =>  let H':= fresh in generalize (@in_inv (list Point) x l xs H); clear H; intros H'; destruct H' end).
1-40:match goal with H: [?x0; ?x1; ?x2; ?x3; ?x4; ?x5; ?x6; ?x7; ?x8; ?x9; ?x10; ?x11; ?x12] = ?l |- _ => rewrite <- H; witnesses (l_from_points x0 x1) (l_from_points x5 x6) end.
apply False_ind; eapply in_nil; eassumption.
Qed.
  
Lemma plane_equiv : forall l:list Point,
    In l list_planes <-> exists l1, exists l2,
        exists Hdiff:l1<>l2, exists Hinter:intersect l1 l2, l = plane l1 l2 Hdiff Hinter.
Proof.
  intros; split.
  apply plane_equiv_2.
  intros [l1 [l2 [Hdiff [Hinter H]]]]; rewrite H.
  apply plane_equiv_1.
Qed.

Definition belongs p l := if (in_dec Point_dec p l) then true else false.

Lemma a_point_belongs_to_13_planes :
  forall p:Point, 
    length (filter (fun (l:list Point) => belongs p l) list_planes) = 13.
Proof.
intros p; destruct p; simpl; reflexivity.
Qed.

Definition lines_of_plane (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2) :=
  filter (fun (l:Line) =>
            let l' := all_points_of_line l in
            let x := match l' with [a;b;c;d] => a | _ => P0 end in
            let y := match l' with [a;b;c;d] => b | _ => P0 end in
            let z := match l' with [a;b;c;d] => c | _ => P0 end in
            let t := match l' with [a;b;c;d] => d | _ => P0 end in
            belongs x (plane l1 l2 Hdiff Hinter) && belongs y (plane l1 l2 Hdiff Hinter) &&
            belongs z (plane l1 l2 Hdiff Hinter) && belongs t (plane l1 l2 Hdiff Hinter)) all_lines.

Lemma a_plane_has_13_lines : forall (l1:Line) (l2:Line) (Hdiff:l1<>l2) (Hinter: intersect l1 l2),
    length (lines_of_plane l1 l2 Hdiff Hinter) = 13.
Proof.
intros l1 l2; destruct l1; destruct l2; intros; try time (solve [(apply False_ind; apply Hdiff; reflexivity) | apply (degen_bool _ Hinter) | unfold lines_of_plane; (*simpl;*) reflexivity]). 
Qed.

Lemma a_line_belongs_to_exactly_4_planes : forall l:Line,
    length (filter (fun (p:list Point) =>
                      let l' := all_points_of_line l in
                      let x := match l' with [a;b;c;d] => a | _ => P0 end in
                      let y := match l' with [a;b;c;d] => b | _ => P0 end in
                      let z := match l' with [a;b;c;d] => c | _ => P0 end in
                      let t := match l' with [a;b;c;d] => d | _ => P0 end in
                      belongs x p && belongs y p && belongs z p && belongs t p) list_planes) = 4.
Proof.
  intros l; destruct l; intros; unfold list_planes; (*simpl;*) reflexivity.
Qed.
